The interface is divided into several panels:
 _______________________________
|                     | |       |
|                     |F|   B   |
|                     | |-------|
|                     '-|       |
|                       |   C   |
|           A           |       |
|                       |-------|
|                       |   D   |
|                       |-------|
|                       |   E   |
|_______________________|_______|

A ... Map panel
B ... Message panel
C ... Inventory/selection panel
D ... Character stats panel
E ... Minimap panel
F ... Equipment panel


<b>Map panel</b>

This panel shows the main map with your character in the centre.

Use PageUp and PageDown to move the camera between elevation levels.


<b>Message panel</b>

This panel shows instructions and things happening in your surrounding. Most messages are accompanied by sounds.


<b>Inventory panel</b>

This panel shows your inventory, and any game menus where you can select between different options. Options are preceded by a letter that is used to select it. This is the place where most user interaction appears.


<b>Character stats panel</b>

This panel shows the stats of your character. For detailed stats, press [^c].

Stats in the first row are: health points (HP), armor (IBP: Impact, Blade, Pierce), elevation level (L), game turn (T).

Stats in the second row are: speed (Sp), strength (St), constitution (Co), dexterity (Dx), attack (At), defense, (De), mass of inventory (M).


<b>Minimap</b>

This panel shows an overview of the world around your character. 

Zoom the MiniMap with [z] and [Z], pan with Ctrl + NumPad arrows, switch levels with PageUp and PageDown.


<b>Equipment</b>

The equipment display shows icons of all items that you have equipped, quivered or otherwise in use.