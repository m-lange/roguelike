To move your character around, activate the NumPad and use the arrow keys there.

To use stairs, use [+] and [-] on the NumPad.

Jump with [j].

To view higher or lower levels of the map, use [PageUp] and [PageDown].

To zoom the MiniMap, use [z] and [Z]. To pan the MiniMap, use Ctrl + NumPad arrows.

To list items or creatures at your characters position, press [,]. To list items and creatures around you, left-click or press [;] and navigate the cursor using the NumPad arrows.

To inspect items and creatures around you, right-click or press [^I].

To view a list of all available commands, use [?]. You can also press [?] during any multi-stage action to get help. Press [^h] to view the game manual.