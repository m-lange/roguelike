﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Map;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using Map.LayeredGrid.Algorithm.Impl;
using Map.LayeredGrid.Impl;
using Map.LayeredGrid.IO;
using Map.MiniMap;
using Map.TileMap;
using ComponentSystem;
using EventSystem;
using TurnSystem;
using Game;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Interface;
using Game.AI;
using Game.EventListeners;
using Game.Events;
using Game.Data;
using Game.Parts;
using Game.Serialization;
using Game.Util;
using Game.Visual;
using UI;

public class GameApp : MonoBehaviour, GameEventListener<QuitGameEvent>, MapGen.MapProgressListener {

	static GameApp instance;
	public static GameApp Instance { get { return instance; } }

	public int uiWidth = 250;


	public CameraVisual cam;
	public RawImage mapImage;
	public RectTransform playerMarker;
	public RectTransform miniMapPanel;
	public RectTransform miniMapPanelInner;
	public RectTransform mapFrame;

	public Camera mainCamera;
	public Camera uiCamera;


	public RectTransform uiPanel;
	public MessageWindow messageWindow;
	public InventoryWindow inventoryWindow;
	public InfoWindow statsWindow;
	public EquipmentWindow equipmentWindow;

	//ActorPart playerActor;
	GridLocation position = new GridLocation(0, 10, 10);

	Action pendingAction = null;
	Queue<EventData> pendingInput = new Queue<EventData>(); 

	//bool isPlayerTurn;
	/// <summary>
	/// True on first update when player is on turn
	/// </summary>
	//bool firstPlayerUpdate = true;


	//bool load = true;

	// Use this for initialization
	void Awake () {
		instance = this;
		string savegame = null;

		bool playerExists = System.IO.File.Exists (
			                    GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName + GlobalParameters.Instance.SaveExtension);
		bool mapExists = System.IO.File.Exists (
			                 GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName + GlobalParameters.Instance.MapExtension);
		bool isSavePoint = false;
		if (playerExists && mapExists) {
			savegame = GlobalParameters.Instance.PlayerName;
		} else {
			playerExists = System.IO.File.Exists (
			                    GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName +"_savepoint" + GlobalParameters.Instance.SaveExtension);
			mapExists = System.IO.File.Exists (
				GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName +"_savepoint" + GlobalParameters.Instance.MapExtension);
			if (playerExists && mapExists) {
				savegame = GlobalParameters.Instance.PlayerName;
				isSavePoint = true;
			}
		}

		int mainCamWidth = Screen.width - uiWidth;
		float relWidth = mainCamWidth / (float)Screen.width;

		mainCamera.rect = new Rect ( 0, 0, relWidth, 1 );
		uiCamera.rect = new Rect ( relWidth, 0, 1-relWidth, 1 );

		float aspect = mainCamWidth / (float) Screen.height;

		GameData.Initialize (GlobalParameters.Instance.mapFile, position, mapImage, playerMarker, mapFrame, uiPanel, miniMapPanel, miniMapPanelInner, messageWindow, inventoryWindow, statsWindow, aspect, savegame, isSavePoint, this);
		GameData.EventManager.Listen (this);

		//playerActor = GameData.Player.GetPart<ActorPart> ();

	}



	void Start () {
		
	}


	public void Notify (QuitGameEvent evt) {
		if (evt.deleteSaveGame) {
			GameData.SaveOnQuit = false;
			string save = GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName + GlobalParameters.Instance.SaveExtension;
			string map = GlobalParameters.Instance.SavePath + "/" + GlobalParameters.Instance.PlayerName + GlobalParameters.Instance.MapExtension;
			if (System.IO.File.Exists (save)) {
				System.IO.File.Delete(save);
			}
			if (System.IO.File.Exists (map)) {
				System.IO.File.Delete(map);
			}
		}
		Application.Quit ();
	}


	bool isStarted = true;
	string progressInfo = "";
	public void NotifyProgress(int progress, string message) {
		progressInfo = message+" "+progress+"%";
	}
	public void NotifyCompleted(string message) { 

	}


	System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
	bool waitForInput = false;
	int waitCounter = 0;

	void Update () {
		if (!isStarted) {
			return;
		}

		CheckGameOver ();
		LayeredGrid<TileInfo> grid = GameData.Map;

		//bool forceUpdate = false;
		if (pendingAction != null) {
			if (pendingAction.HandleUserInput ()) {
				pendingAction = null;
				pendingInput.Clear ();
			}
		} else if (waitForInput) {
			bool inp = HandleUserInput ();
			if (inp) {
				waitCounter = 0;
			}
		}

		int cnt = 0;
		if (pendingAction == null) {
			float maxTime = 1000 * 0.4f / Application.targetFrameRate;
			watch.Reset ();
			watch.Start ();
			while ( true ) {
				TickResult result = GameData.TurnManager.Proceed (GameData.PlayerBounds);
				bool isPlayer = GameData.IsPlayer (GameData.TurnManager.Entities [GameData.TurnManager.CurrentEntity]); 
				if (result == TickResult.WaitForAction) {
					waitForInput = true;
					waitCounter++;
					break;
				} else {
					waitForInput = false;
					if (result == TickResult.Nothing) {
					} else if (result.Location != null) {
					} else {
						pendingAction = result.PendingAction;
						break;
					}
					if (isPlayer) {
						break;
					}
				}
				if (watch.ElapsedMilliseconds > maxTime) {
					break;
				}
				cnt ++;
			}
		}
		if (waitForInput && waitCounter == 1) {
			StartPlayerTurn ();
			//System.GC.Collect();
		}
	}

	void StartPlayerTurn() {
		//Debug.Log("Update stats "+GameData.TurnManager.TurnCounter+" "+GameData.TurnManager.CurrentEntity);
		statsWindow.UpdateInfo();
		equipmentWindow.UpdateInfo();
	}

	void CheckGameOver () {
		if ((! GameData.PlayerHp.Immortal) && GameData.PlayerHp.Hp <= 0) {
			StartPlayerTurn ();
			GameData.PlayerActor.SetNextAction (new GameOverAction (GameData.PlayerActor, "Game over!"));
		}
	}

	bool HandleUserInput () {

		Direction direction = InputUtil.HandleDirectionInputPressed();

		ActorPart playerActor = GameData.PlayerActor;
		string character = InputUtil.GetInputCharacter ();
		bool isShift = (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift));
		bool isControl = (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl));
		if (direction != null) {
			if (isControl) {
				//playerActor.SetNextAction (new ScrollMiniMapAction (playerActor, direction));
				ScrollMiniMap(direction);
				return false;
			} else {
				playerActor.SetNextAction (new WalkAction (playerActor, direction, true, true, true));
			}
		} else if (character == ".") {
			playerActor.SetNextAction (WaitAction.Instance);
		} else if (Input.GetKeyDown (KeyCode.PageUp)) {
			playerActor.SetNextAction (new CameraLayerAction (GameData.Camera, Direction.UP));
		} else if (Input.GetKeyDown (KeyCode.PageDown)) {
			playerActor.SetNextAction (new CameraLayerAction (GameData.Camera, Direction.DOWN));
		} else if (character == ",") {
			playerActor.SetNextAction (new LookHereAction (playerActor));
		} else if (character == ";") {
			playerActor.SetNextAction (new LookThereAction (playerActor, null));
		} else if (character == "?") {
			playerActor.SetNextAction (new ShowHelpAction (playerActor.Entity));
		} else if (character == "#") {
			playerActor.SetNextAction (new SpecialCommandAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.C)) {
			if (isControl) {
				playerActor.SetNextAction (new ShowActorStatsAction (playerActor));
			} else {
				if (isShift) {
					playerActor.SetNextAction (new ConsumeAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new CloseAction (playerActor, null));
				}
			}
		} else if (Input.GetKeyDown (KeyCode.O) && !isShift) {
			playerActor.SetNextAction (new OpenAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.P)) {
			if (isShift) {
				playerActor.SetNextAction (new PickAction (playerActor, null, false));
			} else {
				playerActor.SetNextAction (new PickAction (playerActor, null, true));
			}
		} else if (Input.GetKeyDown (KeyCode.D) && !isShift) {
			playerActor.SetNextAction (new DropAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.H) && !isShift && !isControl) {
			playerActor.SetNextAction (new HarvestAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.H) && isControl) {
			playerActor.SetNextAction (new ShowHelpTextAction (playerActor));
		} else if (Input.GetKeyDown (KeyCode.L) && !isShift) {
			playerActor.SetNextAction (new LootAction (playerActor));
		} else if (Input.GetKeyDown (KeyCode.W) && !isShift) {
			playerActor.SetNextAction (new FollowPathAction (playerActor, null, Terra.ACCESS_OPEN, true, true, true, 3 * GameData.VisionRangeX));
		} else if (Input.GetKeyDown (KeyCode.I)) {
			if (isControl) {
				if (isShift) {
					playerActor.SetNextAction (new InspectNearbyAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new InspectItemAction (playerActor, null));
				}
			} else {
				if (isShift) {
					playerActor.SetNextAction (new RearrangeInventoryAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new ShowInventoryAction (playerActor.Entity));
				}
			}
		} else if (Input.GetKeyDown (KeyCode.J) && !isShift) {
			playerActor.SetNextAction (new JumpAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.A) && !isShift) {
			playerActor.SetNextAction (new UseAction (playerActor, null));
		} else if (Input.GetKeyDown (KeyCode.E)) {
			if (isShift) {
				playerActor.SetNextAction (new UnequipAction (playerActor, null));
			} else {
				playerActor.SetNextAction (new EquipAction (playerActor, null));
			}
		} else if (Input.GetKeyDown (KeyCode.F)) {
			if( isControl ) {
				playerActor.SetNextAction (new FontSizeAction(isShift));
			} else {
				if( !isShift ) playerActor.SetNextAction (new FireAction (playerActor, null, null));
			}
		} else if (Input.GetKeyDown (KeyCode.Q) && isControl) {
			playerActor.SetNextAction (new QuitAction (playerActor));
		} else if (Input.GetKeyDown (KeyCode.Q)) {
			if (isShift) {
				playerActor.SetNextAction (new UnquiverAction (playerActor));
			} else {
				playerActor.SetNextAction (new QuiverAction (playerActor, null));
			}
		} else if (Input.GetKeyDown (KeyCode.R) && !isShift) {
			playerActor.SetNextAction (new ReadBookAction (playerActor));
		} else if (Input.GetKeyDown (KeyCode.S)) {
			if (isShift) {
				playerActor.SetNextAction (new SaveAction (false));
			} else {
				playerActor.SetNextAction (new SearchAction (playerActor));
			}
		} else if (Input.GetKeyDown (KeyCode.T)) {
			if (isShift) {
				playerActor.SetNextAction (new TradeAction (playerActor, null));
			} else {
				playerActor.SetNextAction (new ThrowAction (playerActor, null, null));
			}
		} else if (Input.GetKeyDown (KeyCode.M)) {
			if (isShift) {
				playerActor.SetNextAction (new RemoveMapMarkerAction (playerActor));
			} else {
				if(isControl) {
					playerActor.SetNextAction (new InspectMapMarkersAction (playerActor));
				} else {
					playerActor.SetNextAction (new AddMapMarkerAction (playerActor));
				}
			}
		} else if (Input.GetKeyDown (KeyCode.Z)) {
			if (isShift) {
				ZoomMiniMap(false);
				return false;
			} else {
				ZoomMiniMap(true);
				return false;
			}
		} else if (Input.GetKeyDown (KeyCode.Escape)) {
			//playerActor.CancelAction();
			playerActor.SetNextAction (new ShowInventoryAction (playerActor.Entity));
		} else if (Input.GetMouseButtonDown (0) && GameData.Cam.pixelRect.Contains(Input.mousePosition)) {
			GridLocation location = InputUtil.GetMousePosition ( GameData.CameraLocation.Location, Input.mousePosition );
			playerActor.SetNextAction (new LookThereAction(playerActor, location) );
		} else if (Input.GetMouseButtonDown (1) && GameData.Cam.pixelRect.Contains(Input.mousePosition)) {
			GridLocation location = InputUtil.GetMousePosition ( GameData.CameraLocation.Location, Input.mousePosition );
			playerActor.SetNextAction (new InspectNearbyAction(playerActor, location) );
		} else {
			return false;
		}
		return true;
	}

	public void ScrollMiniMap (Direction direction) {
		if( direction == Direction.UP || direction == Direction.DOWN ) {
			ChangeCameraLayer(direction);
		} else {
			GridLocation pos = GameData.MiniMap.CurrentCenter;
			int zoom = GameData.MiniMap.Zoom;
			int delta = 20 / zoom;

			GridLocation newPos = new GridLocation(pos.Layer, pos.X+direction.dx*delta, pos.Y+direction.dy*delta);
			GameData.MiniMap.UpdateLocation( newPos );
		}
	}
	void ChangeCameraLayer (Direction direction) {
		if (direction == Direction.UP) {
			LocationPart locPart = GameData.Camera.GetPart<LocationPart> (LocationPart.partId);
			if (locPart.Location.Layer < GameData.Map.TopLayer) {
				locPart.Location = new ImmutableGridLocation(locPart.Location.Layer + 1, locPart.Location.X, locPart.Location.Y);
			}
		} else if (direction == Direction.DOWN) {
			LocationPart locPart = GameData.Camera.GetPart<LocationPart> (LocationPart.partId);
			if (locPart.Location.Layer > GameData.Map.BottomLayer) {
				locPart.Location = new ImmutableGridLocation(locPart.Location.Layer - 1, locPart.Location.X, locPart.Location.Y);
			}
		}
	}
	public void ZoomMiniMap (bool zoomIn) {
		int zoom = GameData.MiniMap.Zoom;
		if (zoomIn && zoom < 6) {
			GameData.MiniMap.SetZoom (zoom + 1);
		} else if ((!zoomIn) && zoom > 1) {
			GameData.MiniMap.SetZoom (zoom - 1);
		}
	}
/*
	bool HandleInputQueue ()
	{	
		if (pendingInput.Count == 0) {
			return true;
		}
		EventData data = pendingInput.Dequeue ();

		if (data.keyCode == KeyCode.None) {
			return false;
		} else {
			KeyCode key = data.keyCode;

			Direction direction = null;
			if (key == KeyCode.Keypad1) {
				direction = Direction.SW;
			} else if (key == KeyCode.Keypad2) {
				direction = Direction.S;
			} else if (key == KeyCode.Keypad3) {
				direction = Direction.SE;
			} else if (key == KeyCode.Keypad4) {
				direction = Direction.W;
			} else if (key == KeyCode.Keypad6) {
				direction = Direction.E;
			} else if (key == KeyCode.Keypad7) {
				direction = Direction.NW;
			} else if (key == KeyCode.Keypad8) {
				direction = Direction.N;
			} else if (key == KeyCode.Keypad9) {
				direction = Direction.NE;
			} else if (key == KeyCode.KeypadMinus) {
				direction = Direction.DOWN;
			} else if (key == KeyCode.KeypadPlus) {
				direction = Direction.UP;
			}

			string character = InputUtil.GetInputCharacter ();
			ActorPart playerActor = GameData.PlayerActor;
			if (direction != null) {
				playerActor.SetNextAction (new WalkAction (playerActor, direction, true, true, true));
			} else if (key == KeyCode.PageUp) {
				playerActor.SetNextAction (new CameraLayerAction (GameData.Camera, Direction.UP));
			} else if (key == KeyCode.PageDown) {
				playerActor.SetNextAction (new CameraLayerAction (GameData.Camera, Direction.DOWN));
			} else if (character == ".") {
				playerActor.SetNextAction ( WaitAction.Instance );
			} else if (character == ",") {
				playerActor.SetNextAction (new LookHereAction (playerActor));
			} else if (character == ";") {
				playerActor.SetNextAction (new LookThereAction (playerActor, null));
			} else if (character == "?") {
				playerActor.SetNextAction (new ShowHelpAction (playerActor.Entity));
			} else if (key == KeyCode.H && data.control && !data.shift) {
				playerActor.SetNextAction ( new ShowHelpTextAction(playerActor) );
			} else if (key == KeyCode.O && !data.shift) {
				playerActor.SetNextAction (new OpenAction (playerActor, null));
			} else if (key == KeyCode.C) {
				if (data.shift) {
					playerActor.SetNextAction (new ConsumeAction(playerActor, null) );
				} else {
					playerActor.SetNextAction (new CloseAction (playerActor, null));
				}
			} else if (key == KeyCode.P) {
				if (data.shift) {
					playerActor.SetNextAction (new PickAction (playerActor, null, false));
				} else {
					playerActor.SetNextAction (new PickAction (playerActor, null, true));
				}
			} else if (key == KeyCode.D && !data.shift) {
				playerActor.SetNextAction (new DropAction (playerActor, null));
			} else if (key == KeyCode.I) {
				if (data.shift) {
					playerActor.SetNextAction (new RearrangeInventoryAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new ShowInventoryAction (playerActor.Entity));
				}
			} else if (key == KeyCode.A && !data.shift) {
				playerActor.SetNextAction (new UseAction (playerActor, null));
			} else if (key == KeyCode.E) {
				if (data.shift) {
					playerActor.SetNextAction (new UnequipAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new EquipAction (playerActor, null));
				}
			} else if (key == KeyCode.F) {
				if( data.control ) {
					playerActor.SetNextAction (new FontSizeAction(data.shift));
				} else {
					if( !data.shift ) playerActor.SetNextAction (new FireAction (playerActor, null, null));
				}
				playerActor.SetNextAction (new FireAction (playerActor, null, null));
			} else if (key == KeyCode.Q && data.control) {
				playerActor.SetNextAction (new QuitAction (playerActor));
			} else if (key == KeyCode.Q) {
				if (data.shift) {
					playerActor.SetNextAction (new UnquiverAction (playerActor));
				} else {
					playerActor.SetNextAction (new QuiverAction (playerActor, null));
				}
			} else if (key == KeyCode.S) {
				if (data.shift) {
					playerActor.SetNextAction (new SaveAction (false));
				} else {
					playerActor.SetNextAction (new SearchAction (playerActor));
				}
			} else if (key == KeyCode.T) {
				if (data.shift) {
					playerActor.SetNextAction (new TradeAction (playerActor, null));
				} else {
					playerActor.SetNextAction (new ThrowAction (playerActor, null, null));
				}
			} else if (key == KeyCode.Z) {
				if (data.shift) {
					playerActor.SetNextAction (new ZoomMinimapAction(playerActor.Entity, false));
				} else {
					playerActor.SetNextAction (new ZoomMinimapAction(playerActor.Entity, true));
				}
			} else {
				return false;
			}
		}
		return true;
	}
*/

	void OnApplicationQuit () {
		GameData.SaveOptions();
		if (GameData.SaveOnQuit) {
			GameData.SaveGame (false);
		} else {
			GameData.SaveDeath();
		}
	}

	void OnGUI () {
		/*
		if (Event.current.isKey && Event.current.type == EventType.KeyDown && Event.current.keyCode != KeyCode.None) {
			pendingInput.Enqueue ( new EventData(Event.current) );
		}
		if (Event.current.isMouse && Event.current.type == EventType.MouseDown) {
			pendingInput.Enqueue ( new EventData(Event.current) );
		}
		*/

		if (!isStarted) {
			GUI.Box(new Rect(0,0,Screen.width, Screen.height), progressInfo);
		}
	}

	struct EventData {
		public readonly KeyCode keyCode;
		public readonly bool shift;
		public readonly bool control;

		public readonly Vector2 mousePosition;
		public readonly int mouseButton;

		public EventData(Event evt) {
			this.keyCode = evt.keyCode;
			this.shift = evt.shift;
			this.control = evt.control;

			this.mousePosition = new Vector2(evt.mousePosition.x, Screen.height - evt.mousePosition.y);
			this.mouseButton = evt.button;

		}
	}

}
