﻿using System;
using System.Collections;
using System.Collections.Generic;

public class TileInfo {

	public byte Type;
	public bool Seen;

	//public bool IsWall { get { return isWall; } }
	//public bool IsViewBlocking { get { return isViewBlocking; } }


	public TileInfo(byte type) {
		this.Type = type;
		this.Seen = false;
	}
	public TileInfo(byte type, bool seen) {
		this.Type = type;
		this.Seen = seen;
	}

	public override bool Equals(Object obj) {

		/*
		if (obj is TileInfo) {
			TileInfo other = (TileInfo) obj;
			return other.Type == Type;
		} else {
			return false;
		}
		*/

		TileInfo other = obj as TileInfo;
		if (other == null) {
			return false;
		}
		return other.Type == Type;
	}

	public override int GetHashCode() {
		return Type;
	}
}
