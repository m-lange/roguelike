﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;

public class TileTypeInfo {



	public readonly string Id;
	public readonly string Name;
	public readonly string GroundName;
	public readonly char DefaultMapping;
	public readonly bool IsWalkable;
	public readonly bool IsWall;
	public readonly bool IsViewBlocking;
	public readonly bool IsDoorOpen;
	public readonly bool IsDoorClosed;
	public readonly bool IsDoorLocked;
	public readonly bool IsStairsUp;
	public readonly bool IsStairsDown;


	//public bool IsWall { get { return isWall; } }
	//public bool IsViewBlocking { get { return isViewBlocking; } }

	public TileTypeInfo(string id, string name, char defaultMapping, bool isWalkable, bool isWall, bool isViewBlocking,
					bool isDoorOpen = false, bool isDoorClosed = false, bool isDoorLocked = false,
					bool isStairsUp = false , bool isStairsDown = false) 
		: this (id, name, null, defaultMapping, isWalkable, isWall, isViewBlocking,
					isDoorOpen,  isDoorClosed, isDoorLocked,
					isStairsUp, isStairsDown) {

	}

	public TileTypeInfo(string id, string name, string groundName, char defaultMapping, bool isWalkable, bool isWall, bool isViewBlocking,
					bool isDoorOpen = false, bool isDoorClosed = false, bool isDoorLocked = false,
					bool isStairsUp = false , bool isStairsDown = false) {
		this.Id = id;
		this.Name = name;
		this.GroundName = groundName == null ? name : groundName;
		this.DefaultMapping = defaultMapping;
		this.IsWalkable = isWalkable;
		this.IsWall = isWall;
		this.IsViewBlocking = isViewBlocking;
		this.IsDoorOpen = isDoorOpen;
		this.IsDoorClosed = isDoorClosed;
		this.IsDoorLocked = isDoorLocked;
		this.IsStairsUp = isStairsUp;
		this.IsStairsDown = isStairsDown;
	}

	public bool IsWallLevel (int level) {
		switch (level) {
		case Terra.ACCESS_OPEN: return (! IsWalkable);
		case Terra.ACCESS_CLOSED: return (! IsWalkable) && (! IsDoorClosed);
		case Terra.ACCESS_LOCKED: return (! IsWalkable) && (! IsDoorClosed) && (! IsDoorLocked);
		default: return false;
		}
	}

}
