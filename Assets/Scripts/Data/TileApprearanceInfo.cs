﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileApprearanceInfo {

	public string TextureName;
	public Object Prefab;
	public Object Texture;
	public Color32 MapColor;

	public TileApprearanceInfo(string prefab, string texture, Color32 mapColor) {
		this.Prefab = Resources.Load (prefab);
		this.Texture = Resources.Load (texture);
		this.TextureName = texture;
		this.MapColor = mapColor;
	}
}
