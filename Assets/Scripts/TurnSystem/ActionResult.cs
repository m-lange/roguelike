﻿
namespace TurnSystem {
	public class ActionResult {

		public enum ResultType {
			Success, Failure, Alternative, Pending
		}

		public static readonly ActionResult SUCCESS = new ActionResult (true);
		public static readonly ActionResult FAILURE = new ActionResult (false);
		public static readonly ActionResult PENDING = new ActionResult ();

		public readonly ResultType resultType;
		public readonly Action alternative;

		ActionResult() {
			resultType = ResultType.Pending;
			this.alternative = null;
		}

		ActionResult(Action alternative) {
			resultType = ResultType.Alternative;
			this.alternative = alternative;
		}
		ActionResult(bool success) {
			if (success) {
				resultType = ResultType.Success;
			} else {
				resultType = ResultType.Failure;
			}
			this.alternative = null;
		}


		public static ActionResult Alternative(Action alternative) {
			return new ActionResult (alternative);
		}
	}
}
