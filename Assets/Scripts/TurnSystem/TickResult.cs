﻿
using Map.LayeredGrid;

namespace TurnSystem {
	public class TickResult {

		public static readonly TickResult Nothing = new TickResult();
		public static readonly TickResult WaitForAction = new TickResult();

		public static TickResult ActionPerformed(GridLocation location) {
			return new TickResult (location);
		}

		public static TickResult ActionPending(Action pendingAction) {
			return new TickResult (pendingAction);
		}

		GridLocation location;
		Action pendingAction;

		public GridLocation Location {
			get { return location; }
		}
		public Action PendingAction {
			get { return pendingAction; }
		}

		TickResult() {
			location = null;
			pendingAction = null;
		}
		TickResult(GridLocation location) {
			this.location = location;
		}
		TickResult(Action pendingAction) {
			this.pendingAction = pendingAction;
		}
	}
}
