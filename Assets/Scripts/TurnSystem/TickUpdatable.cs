﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TurnSystem {
	public interface TickUpdatable {
		void UpdateTick ();
	}
}
