﻿using System;
using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using EventSystem;
using Game.Parts;
using Game.Events;

namespace TurnSystem {
	public class SpatialEntityManager : GameEventListener<EntityMovedEvent> {

		LayeredGrid<TileInfo> grid;
		int chunkSize;
		int visionRangeX;
		int visionRangeY;
		//Dictionary<GridLocation, HashSet<LocationPart>> chunks;
		//Dictionary<GridLocation, List<LocationPart>> chunks;
		List<LocationPart>[,,] chunks;

		public SpatialEntityManager ( LayeredGrid<TileInfo> grid, int chunkSize, int visionRangeX, int visionRangeY ) {
			//chunks = new Dictionary<GridLocation, HashSet<LocationPart>> ();
			//chunks = new Dictionary<GridLocation, List<LocationPart>> ();
			this.grid = grid;
			this.chunkSize = chunkSize;
			this.visionRangeX = visionRangeX;
			this.visionRangeY = visionRangeY;
			Bounds bounds = grid.Bounds;
			if (bounds.xmin != 0 || bounds.ymin != 0) {
				throw new ArgumentException (GetType ().Name + " is only usable with maps starting at (0, 0).");
			}
			int d = bounds.GetDepth ();
			int w = UnityEngine.Mathf.CeilToInt(bounds.GetWidth () / (float) chunkSize);
			int h = UnityEngine.Mathf.CeilToInt(bounds.GetHeight () / (float) chunkSize);
			chunks = new List<LocationPart>[d, w, h];
			for (int l = 0; l < d; l++) {
				for (int x = 0; x < w; x++) {
					for (int y = 0; y < h; y++) {
						chunks[l,x,y] = new List<LocationPart>();
					}
				}
			}
			//UnityEngine.Debug.Log( d+" "+w+" "+h );
		}


		public void AddEntity(LocationPart location) {
			GetChunk(location.Location).Add(location);
		}

		public void RemoveEntity(LocationPart location) {
			GetChunk(location.Location).Remove(location);
		}

		public void Notify(EntityMovedEvent evt) {
			List<LocationPart> oldChunk = GetChunk (evt.oldLocation);
			List<LocationPart> newChunk = GetChunk (evt.location.Location);
			if (! ReferenceEquals( oldChunk, newChunk) ) {
				bool found = oldChunk.Remove (evt.location);
				if (found) {
					newChunk.Add (evt.location);
				}
			}
		}


		public void UpdateSeen () {
			VisualPart vis;
			List<LocationPart> chunk;
			LocationPart loc;
			int len;

			int d = chunks.GetLength (0);
			int w = chunks.GetLength (1);
			int h = chunks.GetLength (2);
			for (int l = 0; l < d; l++) {
				for (int x = 0; x < w; x++) {
					for (int y = 0; y < h; y++) {
						chunk = chunks [l, x, y];
						len = chunk.Count;
						for (int i = 0; i < len; i++) {
							loc = chunk[i];
							vis = loc.Entity.GetPart<VisualPart> ( VisualPart.partId );
							if (vis != null) {
								if (vis.AlwaysVisible && grid [loc.Location].Seen) {
									vis.SetVisible (true);
								}
							}
						}
					}
				}
			}
		}


		public void UpdateVisibility(VisibilityGrid visible, GridLocation cameraPosition) {
			Bounds newBounds = GetCameraBounds (cameraPosition);
			VisualPart vis;
			foreach (LocationPart loc in GetInChunkBounds(newBounds)) {
				vis = loc.Entity.GetPart<VisualPart> (VisualPart.partId);
				if (vis != null) {
					vis.SetVisible ( visible.Contains(loc.Location) && visible.IsCenterVisible (loc.Location) );
				}
			}
		}

		public IEnumerable<LocationPart> GetAt (GridLocation loc) {
			tempChunk = GetChunk (loc);
			LocationPart part;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					yield return part;
				}
			}
		}
		public IEnumerable<P> GetAt<P>( GridLocation loc ) where P : Part<P> {
			tempChunk = GetChunk (loc);
			LocationPart part;
			P p;
			int id = PartType.GetFor<P>().id;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					p = part.Entity.GetPart<P>(id);
					if (p != null) {
						yield return p;
					}
				}
			}
		}
		public IEnumerable<P> GetAt<P>( int partId, GridLocation loc ) where P : Part<P> {
			tempChunk = GetChunk (loc);
			LocationPart part;
			P p;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					p = part.Entity.GetPart<P>(partId);
					if (p != null) {
						yield return p;
					}
				}
			}
		}
		public IEnumerable<Entity> GetAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			tempChunk = GetChunk (loc);
			LocationPart part;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					if (condition == null || condition(part.Entity)) {
						yield return part.Entity;
					}
				}
			}
		}


		public bool IsAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			tempChunk = GetChunk (loc);
			LocationPart part;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					if (condition == null || condition(part.Entity)) {
						return true;
					}
				}
			}
			return false;
		}

		public bool IsAt<P>( GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			return IsAt<P>(id, loc, condition);
		}

		//GridLocation tempLocation = new GridLocation(0,0,0);
		List<LocationPart> tempChunk;

		public bool IsAt<P>( int partId, GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			tempChunk = GetChunk (loc);
			LocationPart part;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					if (part.Entity.HasPart(partId)
						&& (condition == null || condition(part.Entity.GetPart<P>(partId))) ) {
						return true;
					}
				}
			}
			return false;
		}


		public Entity OneAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			tempChunk = GetChunk (loc);
			LocationPart part;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					if ( condition == null || condition(part.Entity) ) {
						return part.Entity;
					}
				}
			}
			return null;
		}

		public P OneAt<P>( GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			return OneAt<P>(id, loc, condition);
		}

		public P OneAt<P>( int partId, GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			tempChunk = GetChunk (loc);
			LocationPart part;
			P p;
			int len = tempChunk.Count;
			for (int i = 0; i < len; i++) {
				part = tempChunk [i];
				if (part.Location.Equals (loc)) {
					p = part.Entity.GetPart<P>(partId);
					if (p != null && (condition == null || condition(p))) {
						return p;
					}
				}
			}
			return null;
		}

		public IEnumerable<LocationPart> GetInBounds (Bounds bounds) {
			Bounds cBounds = ToChunkBounds (bounds);
			//GridLocation idx = new GridLocation (0,0,0);
			List<LocationPart> lst;
			LocationPart part;
			int len;
			int lBase = grid.BottomLayer;
			for (int l = cBounds.lmin; l <= cBounds.lmax; l++) {
				for (int x = cBounds.xmin; x <= cBounds.xmax; x++) {
					for (int y = cBounds.ymin; y <= cBounds.ymax; y++) {
						lst = chunks [l-lBase, x, y];
						len = lst.Count;
						for (int i = 0; i < len; i++) {
							part = lst[i];
							if ( bounds.Contains(part.Location) ) {
								yield return part;
							}
						}
					}
				}
			}
		}

		public IEnumerable<P> GetInBounds<P> (int partId, Bounds bounds, Func<Entity, bool> filter) where P : Part<P> {
			Bounds cBounds = ToChunkBounds (bounds);
			//GridLocation idx = new GridLocation (0,0,0);
			List<LocationPart> lst;
			LocationPart part;
			P p;
			int len;
			int lBase = grid.BottomLayer;
			int w = chunks.GetLength (1);
			int h = chunks.GetLength (2);
			for (int l = cBounds.lmin; l <= cBounds.lmax; l++) {
				for (int x = cBounds.xmin; x <= cBounds.xmax; x++) {
					if (x >= 0 && x < w) {
						for (int y = cBounds.ymin; y <= cBounds.ymax; y++) {
							if (y >= 0 && y < h) {
								//UnityEngine.Debug.Log(l-lBase+" "+x+" "+y);
								lst = chunks [l - lBase, x, y];
								len = lst.Count;
								for (int i = 0; i < len; i++) {
									part = lst [i];
									if (bounds.Contains (part.Location)) {
										p = part.Entity.GetPart<P> (partId);
										if (p != null && (filter == null || filter (part.Entity))) {
											yield return p;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		public IEnumerable GetInChunkBounds (Bounds bounds) {
			List<LocationPart> lst;
			//LocationPart part;
			int len;
			int lBase = grid.BottomLayer;
			int w = chunks.GetLength (1);
			int h = chunks.GetLength (2);
			for (int l = bounds.lmin; l <= bounds.lmax; l++) {
				for (int x = bounds.xmin; x <= bounds.xmax; x++) {
					if (x >= 0 && x < w) {
						for (int y = bounds.ymin; y <= bounds.ymax; y++) {
							if (y >= 0 && y < h) {
								lst = chunks [l - lBase, x, y];
								len = lst.Count;
								for (int i = 0; i < len; i++) {
									yield return lst [i];
								}
							}
						}
					}
				}
			}
		}

		public List<LocationPart> GetChunk (GridLocation loc) {
			return chunks[ loc.Layer - grid.BottomLayer, loc.X / chunkSize, loc.Y / chunkSize ];
		}

		public Bounds ToChunkBounds(Bounds bounds) {
			Bounds res = new Bounds ( bounds.lmin, bounds.lmax, 0, 0, 0, 0 );
			res.xmin = ToChunk (bounds.xmin);
			res.ymin = ToChunk (bounds.ymin);
			res.xmax = ToChunk (bounds.xmax);
			res.ymax = ToChunk (bounds.ymax);
			return res;
		}

		bool IntersectsChunk(GridLocation chunkLoc, Bounds bounds) {
			if( bounds.lmax < chunkLoc.Layer || bounds.lmin > chunkLoc.Layer ) {
				return false;
			}
			if( bounds.xmax < chunkLoc.X || bounds.xmin > chunkLoc.X ) {
				return false;
			}
			if( bounds.ymax < chunkLoc.Y || bounds.ymin > chunkLoc.Y ) {
				return false;
			}
			return true;
		}


		int ToChunk(int x) {
			return x / chunkSize;
		}
		GridLocation ToChunk(GridLocation loc) {
			int l = loc.Layer;
			int x = ToChunk(loc.X);
			int y = ToChunk(loc.Y);

			return new ImmutableGridLocation (l, x, y);
		}
		void ToChunk(GridLocation loc, ref GridLocation result) {
			result.Layer = loc.Layer;
			result.X = ToChunk(loc.X);
			result.Y = ToChunk(loc.Y);
		}


		Bounds GetCameraBounds(GridLocation position) {
			Bounds b = new Bounds ();
			b.xmin = ToChunk(position.X - visionRangeX);
			b.xmax = ToChunk(position.X + visionRangeX);
			b.ymin = ToChunk(position.Y - visionRangeY);
			b.ymax = ToChunk(position.Y + visionRangeY);
			if (position.Layer >= 0) {
				b.lmin = 0;
				b.lmax = grid.TopLayer;
			} else {
				b.lmin = position.Layer;
				b.lmax = position.Layer;
			}
			return b;
		}
		
	}
}
