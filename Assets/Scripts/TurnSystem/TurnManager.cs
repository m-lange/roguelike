﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Game.Events;
using Game.Util;

namespace TurnSystem {
	public class TurnManager : GameEventListener<RemoveEntityEvent> {

		SpatialEntityManager spatialManager;
		int turnCounter;

		int currentEntity;

		Dictionary<long, Entity> entityIds;
		List<Entity> entities;
		List<Entity> toAdd = new List<Entity> ();
		List<Entity> toRemove = new List<Entity> ();

		List<TickUpdatable> tickUpdatables = new List<TickUpdatable> ();

		bool endOfTurn;

		public TurnManager(SpatialEntityManager spatialManager) {
			this.spatialManager = spatialManager;
			entities = new List<Entity> ();
			entityIds = new Dictionary<long, Entity> ();
			currentEntity = 0;
		}

		public int TurnCounter {
			get { return turnCounter; }
			set { turnCounter = value; }
		}

		public bool IsEndOfTurn {
			get { return endOfTurn; }
		}


		public int EntityCount {
			get{ return entities.Count; }
		}
		public List<Entity> Entities {
			get { return entities; }
		}
		public int CurrentEntity {
			get { return currentEntity; }
			set { currentEntity = value; }
		}

		internal void AddEntity(Entity e) {
			//if (entityIds.ContainsKey (e.Id)) {
			//	throw new ArgumentException ("Entity "+e.Id+" ("+TextUtil.GetName(e)+") is already in TurnManager");
			//}
			toAdd.Add (e);
		}
		void RemoveEntity (Entity e, bool permanent) {

			PickablePart pick = e.GetPart<PickablePart> (PickablePart.partId);
			if (pick != null) {
				if (pick.CarrierEntity >= 0) {
					Entity carrier = entityIds [pick.CarrierEntity];
					InventoryPart inv = carrier.GetPart<InventoryPart> (InventoryPart.partId);
					if (inv != null) {
						inv.RemoveItem (pick, -1);
					}
				}
			}
			
			toRemove.Add (e);
			if (permanent) {
				entityIds.Remove (e.Id);
			}
		}

		public void AddUpdatable(TickUpdatable updatable) {
			tickUpdatables.Add (updatable);
		}


		public void Notify (RemoveEntityEvent evt) {
			RemoveEntity ( evt.entity, evt.permanent );
		}

		public Entity GetEntity(long id) {
			if (entityIds.ContainsKey (id)) {
				return entityIds [id];
			}
			return null;
		}
		
		public IEnumerable<Entity> GetAll( int partId ) {
			foreach(Entity e in entities) {
				if( e.HasPart(partId) ) {
					yield return e;
				}
			}
		}

		public Dictionary<long, Entity> EntityIds {
			get { return entityIds; }
		}

		public TickResult Proceed (Map.LayeredGrid.Bounds bounds) {
			endOfTurn = false;
			TickResult tickResult = TickResult.Nothing;

			Entity entity = entities [currentEntity];
			ActorPart actor = entity.GetPart<ActorPart> (ActorPart.partId);
			bool isActor = actor != null;
			bool endOfEntityTurn = true;
			if (isActor) {
				if (bounds.Contains (entity.GetPart<LocationPart> (LocationPart.partId).Location)) {
					if (actor.CanAct ()) {
						Action action = actor.GetNextAction ();
						if (action == null) {
							return TickResult.WaitForAction;
						}
						while ( true ) {
							ActionResult result = action.Execute ();
							if (result.resultType == ActionResult.ResultType.Failure) {
								return TickResult.WaitForAction;
							} else if (result.resultType == ActionResult.ResultType.Success) {
								break;
							} else if (result.resultType == ActionResult.ResultType.Alternative) {
								action = result.alternative;
							} else if (result.resultType == ActionResult.ResultType.Pending) {
								return TickResult.ActionPending (action);
							}
						}
						actor.UseActionPoints (action);
						if (action.RequiresUpdate () != null) {
							tickResult = TickResult.ActionPerformed (action.RequiresUpdate ());
						}
					}
					endOfEntityTurn = !actor.CanAct ();
					if(endOfEntityTurn) {
						actor.UpdateActionPoints();
					}
				}
			}

			if (endOfEntityTurn) {
				entity.Update ();
				if (!entity.IsActive) {
					RemoveEntity(entity, true);
					//toRemove.Add (entity);
				}
			} 
			AddAndRemove ();

			foreach (TickUpdatable u in tickUpdatables) {
				u.UpdateTick ();
			}

			if (endOfEntityTurn) {
				currentEntity = (currentEntity + 1);
				if (currentEntity >= entities.Count) {
					currentEntity %= entities.Count;
					//entityManager.Update ();
					turnCounter += 1;
					endOfTurn = true;
				}
			} 
			return tickResult;
		}

		public void Initialize() {
			Entity e;
			int len = toAdd.Count;
			for(int i=0; i<len; i++) {
				e = toAdd[i];
				entities.Add (e);
				entityIds [e.Id] = e;
				e.Initialize ();
				if (e.HasPart<LocationPart> ()) {
					spatialManager.AddEntity (e.GetPart<LocationPart> ());
				}
			}
			toAdd.Clear ();
		}

		void AddAndRemove () {
			Entity e;
			LocationPart loc;

			int len = toAdd.Count;
			if (len > 0) {
				for (int i = 0; i < len; i++) {
					e = toAdd [i];
					entities.Add (e);
					entityIds [e.Id] = e;
					e.Initialize ();
					loc = e.GetPart<LocationPart> (LocationPart.partId);
					if (loc != null) {
						spatialManager.AddEntity (loc);
					}
				}
				//toAdd = new List<Entity>();
				toAdd.Clear ();
			}

			len = toRemove.Count;
			if (len > 0) {
				for (int i = 0; i < len; i++) {
					e = toRemove [i];
					e.SetActive(false);
					//UnityEngine.Debug.Log ("Removing: "+e.Id);
					int idx = entities.IndexOf (e);
					e.Cleanup ();
					if (idx >= 0) {
						//UnityEngine.Debug.Log ("Found: " + e.Id);
						if (idx <= currentEntity) {
							currentEntity--;
						}
						entities.RemoveAt (idx);
						//entityIds.Remove (e.Id);
						loc = e.GetPart<LocationPart> (LocationPart.partId);
						if (loc != null) {
							spatialManager.RemoveEntity (loc);
						}
					} else {
						//UnityEngine.Debug.Log ("Not found: " + e.Id);
					}
				}
				//toRemove = new List<Entity>();
				toRemove.Clear ();
			}
		}
		
	}
}
