﻿using System;
using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using EventSystem;
using Game.Parts;
using Game.Events;

namespace TurnSystem {
	public class DictSpatialEntityManager : GameEventListener<EntityMovedEvent> {

		LayeredGrid<TileInfo> grid;
		int chunkSize;
		int visionRangeX;
		int visionRangeY;
		//Dictionary<GridLocation, HashSet<LocationPart>> chunks;
		Dictionary<GridLocation, List<LocationPart>> chunks;

		public DictSpatialEntityManager(LayeredGrid<TileInfo> grid, int chunkSize, int visionRangeX, int visionRangeY) {
			//chunks = new Dictionary<GridLocation, HashSet<LocationPart>> ();
			chunks = new Dictionary<GridLocation, List<LocationPart>> ();
			this.grid = grid;
			this.chunkSize = chunkSize;
			this.visionRangeX = visionRangeX;
			this.visionRangeY = visionRangeY;
		}

		public void AddEntity(LocationPart location) {
			GridLocation chunkLoc = ToChunk (location.Location);
			if (chunks.ContainsKey (chunkLoc)) {
				chunks [chunkLoc].Add (location);
			} else {
				//HashSet<LocationPart> lst = new HashSet<LocationPart> ();
				List<LocationPart> lst = new List<LocationPart> ();
				lst.Add (location);
				chunks [chunkLoc] = lst;
			}
		}

		public void RemoveEntity(LocationPart location) {
			GridLocation chunkLoc = ToChunk (location.Location);
			if (chunks.ContainsKey (chunkLoc)) {
				chunks [chunkLoc].Remove (location);
				if (chunks [chunkLoc].Count == 0) {
					chunks.Remove (chunkLoc);
				}
			}
		}

		public void Notify(EntityMovedEvent evt) {
			GridLocation oldChunk = ToChunk (evt.oldLocation);
			GridLocation newChunk = ToChunk (evt.location.Location);
			if (!oldChunk.Equals (newChunk)) {
				bool found = false;
				if (chunks.ContainsKey (oldChunk)) {
					found = chunks [oldChunk].Remove (evt.location);
					if (chunks [oldChunk].Count == 0) {
						chunks.Remove (oldChunk);
					}
				}
				if (found) {
					if (chunks.ContainsKey (newChunk)) {
						chunks [newChunk].Add (evt.location);
					} else {
						//HashSet<LocationPart> lst = new HashSet<LocationPart> ();
						List<LocationPart> lst = new List<LocationPart> ();
						lst.Add (evt.location);
						chunks [newChunk] = lst;
					}
				}
				/*
				if (evt.location.Entity.HasPart<NamePart> ()) {
					UnityEngine.Debug.Log (evt.location.Entity.GetPart<NamePart> ().Name);
				} else {
					UnityEngine.Debug.Log ("Unknown");
				}
				UnityEngine.Debug.Log (oldChunk+" -> "+newChunk+" "+chunks.Count);
				*/
			}
		}


		public void UpdateSeen() {
			VisualPart vis;
			foreach (List<LocationPart> chunk in chunks.Values) {
				foreach (LocationPart loc in chunk) {
					vis = loc.Entity.GetPart<VisualPart> ( VisualPart.partId );
					if (vis != null) {
						if (vis.AlwaysVisible && grid [loc.Location].Seen) {
							vis.SetVisible (true);
						}
					}
				}
			}
		}


		public void UpdateVisibility(VisibilityGrid visible, GridLocation cameraPosition) {
			Bounds newBounds = GetCameraBounds (cameraPosition);
			VisualPart vis;
			foreach (LocationPart loc in GetInChunkBounds(newBounds)) {
				vis = loc.Entity.GetPart<VisualPart> (VisualPart.partId);
				if (vis != null) {
					vis.SetVisible ( visible.Contains(loc.Location) && visible.IsCenterVisible (loc.Location) );
				}
			}
		}

		public IEnumerable<LocationPart> GetAt( GridLocation loc ) {
			GridLocation idx = ToChunk (loc);
			if (chunks.ContainsKey (idx)) {
				List<LocationPart> lst = chunks [idx];
				foreach (LocationPart part in lst) {
					if (part.Location.Equals (loc)) {
						yield return part;
					}
				}
			}
		}
		public IEnumerable<P> GetAt<P>( GridLocation loc ) where P : Part<P> {
			GridLocation idx = ToChunk (loc);
			if (chunks.ContainsKey (idx)) {
				int id = PartType.GetFor<P>().id;
				P p;
				List<LocationPart> lst = chunks [idx];
				foreach (LocationPart part in lst) {
					p = part.Entity.GetPart<P>(id);
					if (p != null && part.Location.Equals (loc)) {
						yield return p;
					}
				}
			}
		}
		public IEnumerable<P> GetAt<P>( int partId, GridLocation loc ) where P : Part<P> {
			GridLocation idx = ToChunk (loc);
			if (chunks.ContainsKey (idx)) {
				P p;
				List<LocationPart> lst = chunks [idx];
				foreach (LocationPart part in lst) {
					p = part.Entity.GetPart<P>(partId);
					if (p != null && part.Location.Equals (loc)) {
						yield return p;
					}
				}
			}
		}
		public IEnumerable<Entity> GetAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			GridLocation idx = ToChunk (loc);
			if (chunks.ContainsKey (idx)) {
				List<LocationPart> lst = chunks [idx];
				foreach (LocationPart part in lst) {
					if (part.Location.Equals (loc) && (condition == null || condition(part.Entity))) {
						yield return part.Entity;
					}
				}
			}
		}


		public bool IsAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			GridLocation idx = ToChunk (loc);
			if (chunks.ContainsKey (idx)) {
				List<LocationPart> lst = chunks [idx];
				foreach (LocationPart part in lst) {
					if (part.Location.Equals (loc)
						&& (condition == null || condition(part.Entity))) {
						return true;
					}
				}
			}
			return false;
		}

		public bool IsAt<P>( GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			return IsAt<P>(id, loc, condition);
		}

		GridLocation tempLocation = new GridLocation(0,0,0);
		List<LocationPart> tempChunk;

		public bool IsAt<P>( int partId, GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			ToChunk (loc, ref tempLocation);
			if (chunks.ContainsKey (tempLocation)) {
				tempChunk = chunks [tempLocation];
				//P pt;
				LocationPart part;
				int cnt = tempChunk.Count;
				for(int i=0; i<cnt; i++) {
					part = tempChunk[i];
				//foreach (LocationPart part in tempChunk) {
					//pt = part.Entity.GetPart<P>(partId);
					if (part.Entity.HasPart(partId) && part.Location.Equals (loc)
						&& (condition == null || condition(part.Entity.GetPart<P>(partId)))) {
						return true;
					}
				}
			}
			return false;
		}


		public Entity OneAt( GridLocation loc, Func<Entity, bool> condition = null ) {
			//GridLocation idx = ToChunk (loc);
			ToChunk (loc, ref tempLocation);
			if (chunks.ContainsKey (tempLocation)) {
				tempChunk = chunks [tempLocation];
				foreach (LocationPart part in tempChunk) {
					if (part.Location.Equals (loc) 
						&& (condition == null || condition(part.Entity))) {
						return part.Entity;
					}
				}
			}
			return null;
		}

		public P OneAt<P>( GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			ToChunk (loc, ref tempLocation);
			if (chunks.ContainsKey (tempLocation)) {
				int id = PartType.GetFor<P>().id;
				P pt;
				tempChunk = chunks [tempLocation];
				foreach (LocationPart part in tempChunk) {
					pt = part.Entity.GetPart<P>(id);
					if (part.Location.Equals (loc) && pt != null 
						&& (condition == null || condition(pt))) {
						return pt;
					}
				}
			}
			return null;
		}

		public P OneAt<P>( int partId, GridLocation loc, Func<P, bool> condition = null ) where P : Part<P> {
			ToChunk (loc, ref tempLocation);
			if (chunks.ContainsKey (tempLocation)) {
				P pt;
				tempChunk = chunks [tempLocation];
				foreach (LocationPart part in tempChunk) {
					pt = part.Entity.GetPart<P>(partId);
					if (part.Location.Equals (loc) && pt != null 
						&& (condition == null || condition(pt))) {
						return pt;
					}
				}
			}
			return null;
		}

		public IEnumerable<LocationPart> GetInBounds( Bounds bounds ) {
			Bounds cBounds = ToChunkBounds (bounds);
			GridLocation idx = new GridLocation (0,0,0);
			List<LocationPart> lst;
			for (int l = cBounds.lmin; l <= cBounds.lmax; l++) {
				for (int x = cBounds.xmin; x <= cBounds.xmax; x++) {
					for (int y = cBounds.ymin; y <= cBounds.ymax; y++) {
						idx.Set (l, x, y);
						if (chunks.ContainsKey (idx)) {
							lst = chunks [idx];
							foreach (LocationPart part in lst) {
								if ( bounds.Contains(part.Location) ) {
									yield return part;
								}
							}
						}
					}
				}
			}
		}

		public IEnumerable<P> GetInBounds<P>( Bounds bounds, Func<Entity, bool> filter ) where P : Part<P> {
			Bounds cBounds = ToChunkBounds (bounds);
			GridLocation idx = new GridLocation (0,0,0);
			List<LocationPart> lst;
			int id = PartType.GetFor<P>().id;
			for (int l = cBounds.lmin; l <= cBounds.lmax; l++) {
				for (int x = cBounds.xmin; x <= cBounds.xmax; x++) {
					for (int y = cBounds.ymin; y <= cBounds.ymax; y++) {
						idx.Set (l, x, y);
						if (chunks.ContainsKey (idx)) {
							lst = chunks [idx];
							foreach (LocationPart part in lst) {
								if ( part.Entity.HasPart(id) 
									&& bounds.Contains(part.Location)
									&& (filter == null || filter(part.Entity)) ) {
									yield return part.Entity.GetPart<P>(id);
								}
							}
						}
					}
				}
			}
		}

		public IEnumerable<P> GetInBounds<P>( int partId, Bounds bounds, Func<Entity, bool> filter ) where P : Part<P> {
			Bounds cBounds = ToChunkBounds (bounds);
			GridLocation idx = new GridLocation (0,0,0);
			List<LocationPart> lst;
			for (int l = cBounds.lmin; l <= cBounds.lmax; l++) {
				for (int x = cBounds.xmin; x <= cBounds.xmax; x++) {
					for (int y = cBounds.ymin; y <= cBounds.ymax; y++) {
						idx.Set (l, x, y);
						if (chunks.ContainsKey (idx)) {
							lst = chunks [idx];
							foreach (LocationPart part in lst) {
								if ( part.Entity.HasPart(partId) 
									&& bounds.Contains(part.Location)
									&& (filter == null || filter(part.Entity)) ) {
									yield return part.Entity.GetPart<P>(partId);
								}
							}
						}
					}
				}
			}
		}

		public IEnumerable GetInChunkBounds( Bounds bounds ) {
			foreach( KeyValuePair<GridLocation, List<LocationPart>> kv in chunks ) {
				if (IntersectsChunk (kv.Key, bounds)) {
					foreach (LocationPart part in kv.Value) {
						yield return part;
					}
				}
			}
		}

		public Bounds ToChunkBounds(Bounds bounds) {
			Bounds res = new Bounds ( bounds.lmin, bounds.lmax, 0, 0, 0, 0 );
			res.xmin = ToChunk (bounds.xmin);
			res.ymin = ToChunk (bounds.ymin);
			res.xmax = ToChunk (bounds.xmax);
			res.ymax = ToChunk (bounds.ymax);
			return res;
		}

		bool IntersectsChunk(GridLocation chunkLoc, Bounds bounds) {
			if( bounds.lmax < chunkLoc.Layer || bounds.lmin > chunkLoc.Layer ) {
				return false;
			}
			if( bounds.xmax < chunkLoc.X || bounds.xmin > chunkLoc.X ) {
				return false;
			}
			if( bounds.ymax < chunkLoc.Y || bounds.ymin > chunkLoc.Y ) {
				return false;
			}
			return true;
		}


		int ToChunk(int x) {
			int i = x / chunkSize;
			if (x < 0) {
				i -= 1;
			}
			return i;
		}
		GridLocation ToChunk(GridLocation loc) {
			int l = loc.Layer;
			int x = ToChunk(loc.X);
			int y = ToChunk(loc.Y);

			return new ImmutableGridLocation (l, x, y);
		}
		void ToChunk(GridLocation loc, ref GridLocation result) {
			result.Layer = loc.Layer;
			result.X = ToChunk(loc.X);
			result.Y = ToChunk(loc.Y);
		}


		Bounds GetCameraBounds(GridLocation position) {
			Bounds b = new Bounds ();
			b.xmin = ToChunk(position.X - visionRangeX);
			b.xmax = ToChunk(position.X + visionRangeX);
			b.ymin = ToChunk(position.Y - visionRangeY);
			b.ymax = ToChunk(position.Y + visionRangeY);
			if (position.Layer >= 0) {
				b.lmin = 0;
				b.lmax = grid.TopLayer;
			} else {
				b.lmin = position.Layer;
				b.lmax = position.Layer;
			}
			return b;
		}
		
	}
}
