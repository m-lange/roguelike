﻿
using Map.LayeredGrid;

namespace TurnSystem {

	public interface Action {

		ActionResult Execute();
		int GetCost ();
		GridLocation RequiresUpdate ();
		bool HandleUserInput();
	}
}
