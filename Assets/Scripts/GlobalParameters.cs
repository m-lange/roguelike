﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

using Map.LayeredGrid;
using Map.LayeredGrid.Impl;
using Map.LayeredGrid.IO;
using Game.Data;
using EventSystem;


public class GlobalParameters : MonoBehaviour {

	public static readonly char[] letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
	public static readonly HashSet<char> letterSet = new HashSet<char>(letters);
	public static readonly Dictionary<char, int> letterIndices = new Dictionary<char, int>();


	static readonly char[] invalidCharacters = Path.GetInvalidFileNameChars();

	static GlobalParameters () {
		for (int i = 0; i < letters.Length; i++) {
			letterIndices[letters[i]] = i;
		}
	}


	public static readonly HashSet<char> numbers = new HashSet<char>("0123456789".ToCharArray());


	static GlobalParameters instance;


	public int mapChunkSize = 8;
	public int entityChunkSize = 8;
	public int visionRange = 20;
	public int updateRadius = 40;
	public int targetFps = 60;
	public int miniMapZoom = 2;
	public string mapFile;

	public GameObject[] toEnable;

	string mapPath = "maps";
	string savePath = "save";
	string mapExtension = ".ascii";
	string saveExtension = ".xml";

	string templatePath = "data/templates";
	string texturePath = "data/textures";
	string deathsPath = "record/deaths.xml";
	string optionsPath = "data/options.xml";

	float layerHeight = 5f;

	string playerName = "";
	int selectedMap = 0;
	string[] mapFiles;

	bool nameSelected = false;
	bool started = false;
	bool startComplete = false;

	int randomSeed = RandUtils.NextInt(int.MaxValue);



	/*
	TileTypeInfo[] tileInfo;
	TileApprearanceInfo[] tileAppearance;

	Dictionary<string, byte> tileInfoIndexMap;
	Dictionary<string, TileTypeInfo> tileInfoMap;
	Dictionary<string, TileApprearanceInfo> tileAppearanceMap;
	*/

	public static GlobalParameters Instance {
		get{ return instance; }
	}

	public string PlayerName {
		get { return playerName; }
	}

	public string MapPath {
		get{ return mapPath; }
	}
	public string SavePath {
		get{ return savePath; }
	}

	public string TemplatePath {
		get { return templatePath; }
	}

	public string TexturePath {
		get { return texturePath; }
	}

	public string DeathsPath {
		get { return deathsPath; }
	}

	public string OptionsPath {
		get { return optionsPath; }
	}

	public string MapExtension {
		get { return mapExtension; }
	}

	public string SaveExtension {
		get { return saveExtension; }
	}

	public float LayerHeight {
		get { return layerHeight; }
	}

	public int MiniMapZoom {
		get { return miniMapZoom; }
	}

	public int RandomSeed {
		get { return randomSeed; }
	}

	// Use this for initialization
	void Awake () {
		instance = this;



		//Debug.Log (Map.LayeredGrid.Algorithm.Impl.RecursiveShadowCastingVisibility<TileInfo>.GetOctant (2, -5));
		string[] args = System.Environment.GetCommandLineArgs ();
		if (args.Length > 1) {
			mapFile = args [1];
		}

		//QualitySettings.vSyncCount = 1;
		QualitySettings.vSyncCount = 1;
		Application.targetFrameRate = targetFps;

		DirectoryInfo appDir = Directory.GetParent (GetAppDir ());

		if (!mapPath.Contains (":")) {
			mapPath = appDir.ToString () + "/" + mapPath;
		}
		if (!savePath.Contains (":")) {
			savePath = appDir.ToString () + "/" + savePath;
		}
		if (!templatePath.Contains (":")) {
			templatePath = appDir.ToString () + "/" + templatePath;
		}
		if (!texturePath.Contains (":")) {
			texturePath = appDir.ToString () + "/" + texturePath;
		}
		if (!deathsPath.Contains (":")) {
			deathsPath = appDir.ToString () + "/" + deathsPath;
		}
		if (!optionsPath.Contains (":")) {
			optionsPath = appDir.ToString () + "/" + optionsPath;
		}
		foreach (GameObject go in toEnable) {
			go.SetActive (false);
		}

		//Sprites.GetMesh("Items/Item", "Items/AxeTex");
		//SimpleLayeredGrid<TileInfo> map = new MapGen.MapGenerator().Create();
		//SimpleLayeredGridReader<TileInfo>.Write( map, MapPath, "town.ascii", Terra.TileInfo, Terra.DefaultMapping );

		//Debug.Log( Game.Parts.ActorPart.PartType.id );
		//Debug.Log( Game.Parts.CameraPart.PartType.id );

		//ComponentSystem.Entity e = Game.Factories.WeaponFactory.CreateAxe(new GridLocation(0,0,0), false);
		//e.HasPart<Game.Parts.NamePart>();

		/*
		foreach (KeyValuePair<string, string> kv in Game.Factories.HerbFactory.Generators) {
			Debug.Log(kv.Key+" "+kv.Value);
		}
		*/
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI ()
	{
		if (!nameSelected) {
			if (Event.current.isKey
			    && Event.current.type == EventType.KeyDown
			    && Event.current.keyCode == KeyCode.Return
			    && playerName.Length > 0) {
				if (!FillMapSelection ()) {
					StartCoroutine (StartDelayed ());
				}
				nameSelected = true;
			} else {
				ShowLoginForm ();
			}
		} else {
			if (!started) {
				if (Event.current.isKey
				    && Event.current.type == EventType.KeyDown
					&& Event.current.keyCode == KeyCode.Return) {
					StartCoroutine (StartDelayed ());
					started = true;
				} else {
					ShowMapForm ();
				}
			} else {
				if (!startComplete) {
					ShowLoadScreen ();
				}
			}
		}
	}

	bool FillMapSelection () {
		bool playerExists = System.IO.File.Exists (
			                 GlobalParameters.Instance.SavePath + "/" + playerName + SaveExtension);
		bool mapExists = System.IO.File.Exists (
			                 GlobalParameters.Instance.SavePath + "/" + playerName + MapExtension);

		if (playerExists && mapExists) {
			started = true;
			return false;
		} else {
			playerExists = System.IO.File.Exists (
			                 GlobalParameters.Instance.SavePath + "/" + playerName+"_savepoint" + SaveExtension);
			mapExists = System.IO.File.Exists (
							GlobalParameters.Instance.SavePath + "/" + playerName+"_savepoint" + MapExtension);
			if (playerExists && mapExists) {
				started = true;
				return false;
			}
			DirectoryInfo dir = new DirectoryInfo (GlobalParameters.Instance.MapPath);
			FileInfo[] files = dir.GetFiles ();
			files = Array.FindAll (files, (fi) => fi.Name.EndsWith (mapExtension));
			mapFiles = new string[files.Length+1];
			mapFiles[0] = "PROCEDURAL";
			for (int i = 0; i < files.Length; i++) {
				mapFiles[i+1] = files[i].Name;
			}
			return true;
		}
	}

	IEnumerator StartDelayed () {
		if (mapFiles != null) {
			mapFile = mapFiles[selectedMap];
		}
		//Debug.Log(mapFile);
		yield return new WaitForSeconds (0.1f);
		foreach (GameObject go in toEnable) {
			go.SetActive (true);
		}
		startComplete = true;
	}

	void ShowLoginForm () {
		int centreX = Screen.width / 2;
		int centreY = Screen.height / 2;

		GUI.Label (new Rect (centreX - 100, (centreY - 90), 200, 55), "Give your character a name or enter the name of a saved character to resume a game.");
		GUI.SetNextControlName ("PlayerName");
		string tempName = GUI.TextField (new Rect (centreX - 100, (centreY - 30), 200, 30), playerName);
		if (isValidName (tempName)) {
			playerName = tempName;
		}
		GUI.FocusControl("PlayerName");
		if (GUI.Button (new Rect (centreX - 100, (centreY + 5), 200, 30), "Start / Load")) {
			if (playerName.Length > 0) {
				if (!FillMapSelection ()) {
					StartCoroutine (StartDelayed ());
				}
				nameSelected = true;
			}
		}
	}

	bool isValidName(string name) {
		return name.IndexOfAny(invalidCharacters) < 0;
	}

	bool isFirstMapFrame = true;
	void ShowMapForm () {
		int centreX = Screen.width / 2;
		int centreY = Screen.height / 2;

		GUI.Label (new Rect (centreX - 130, (centreY - 90), 260, 55), "Select a map.");

		GUI.SetNextControlName ("MapFile");
		selectedMap = GUI.SelectionGrid (new Rect (centreX - 130, (centreY - 30), 260, 130), selectedMap, mapFiles, 1);

		GUI.Label (new Rect (centreX - 130, (centreY + 130), 260, 20), "Random seed");
		GUI.SetNextControlName ("RandSeed");
		string randSeed = GUI.TextField (new Rect (centreX - 130, (centreY + 150), 260, 30), randomSeed.ToString (), 10);
		randSeed = Regex.Replace (randSeed, "[^0-9]", "");
		int newRandSeed;
		if(int.TryParse (randSeed, out newRandSeed)) {
			randomSeed = newRandSeed;
		}

		if (isFirstMapFrame) {
			GUI.FocusControl ("MapFile");
			isFirstMapFrame = false;
		}

		if (GUI.Button (new Rect (centreX - 130, (centreY + 185), 260, 30), "Select")) {
			StartCoroutine (StartDelayed ());
			started = true;
		}
		if (Event.current.isKey && Event.current.type == EventType.KeyDown) {
			if (Event.current.keyCode == KeyCode.UpArrow && selectedMap > 0) {
				selectedMap--;
			} else if (Event.current.keyCode == KeyCode.DownArrow && selectedMap < mapFiles.Length-1) {
				selectedMap++;
			}
		}
	}
	void ShowLoadScreen() {
		GUIStyle style = new GUIStyle( );
		style.alignment = TextAnchor.MiddleCenter;
		style.normal.textColor = Color.white;

		int centreX = Screen.width/2;
		int centreY = Screen.height/2;
		string text = (mapFiles == null) ? "Loading game..." : (mapFile == "PROCEDURAL") ? "Generating world..." : "Loading map...";
		GUI.Label (new Rect (centreX - 75, (centreY - 20), 150, 30), text, style);
	}

	private static string GetAppDir() {
		string path = Application.dataPath;
		if (Application.platform == RuntimePlatform.OSXPlayer) {
			path += "/../";
		} else if (Application.platform == RuntimePlatform.WindowsPlayer) {
			//path += "/../";
		}
		return path;
	}
}
