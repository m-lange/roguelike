﻿using System;
using System.Collections;
using System.Collections.Specialized;



namespace Collections {
	public class ValueSortedDict<K, V> {

		OrderedDictionary dict = new OrderedDictionary();
		Comparison<V> comparer;


		public ValueSortedDict(Comparison<V> comparer) {
			this.comparer = comparer;
		}


		public V this[K key] {
			get { return (V) dict[key]; }
			set { Update (key, value); }
		}
		public V this[int idx] {
			get { return (V) dict[idx]; }
		}

		public ICollection Keys {
			get { return dict.Keys; }
		}
		public int Count {
			get { return dict.Count; }
		}

		public bool Contains(K key) {
			return dict.Contains (key);
		}
		public void Remove(K key) {
			dict.Remove (key);
		}
		public void RemoveAt(int index) {
			dict.RemoveAt (index);
		}
		public void Clear () {
			dict.Clear();
		}

		void Update(K key, V value) {
			if (dict.Contains (key)) {
				if (this [key].Equals(value)) {
					dict.Remove (key);
					Insert (key, value);
				}
			} else {
				Insert (key, value);
			}
		}
		void Insert(K key, V value) {
			if (dict.Count == 0) {
				dict [key] = value;
			} else {
				int idx = BinarySearch (value, 0, dict.Count-1);
				dict.Insert (idx, key, value);
			}
		}

		int BinarySearch(V value, int low, int high) {
			if (high <= low) return ( comparer(value, this[low]) > 0 ) ?  (low + 1): low;

			int mid = (low + high)/2;

			if(comparer(value, this[mid]) == 0) return mid+1;

			if(comparer(value, this[mid]) > 0) return BinarySearch(value, mid+1, high);
			return BinarySearch(value, low, mid-1);
		}

	}
}
