﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Collections {
	public interface Pool<T> {


		T Pop ();
		void Push (T obj);

	}
}
