﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Collections {
	public class GeneratorPool<T, A> : Pool<T> {

		Queue<T> queue;
		Func<A, T> generator;
		Action<T> releaser;
		A argument;

		public GeneratorPool( 
			Func<A, T> generator,
			Action<T> releaser,
			A argument) {
			this.generator = generator;
			this.releaser = releaser;
			this.argument = argument;

			queue = new Queue<T> ();
		}

		public T Pop() {
			if (queue.Count == 0) {
				return generator (argument);
			}
			return queue.Dequeue ();
		}

		public void Push(T obj) {
			releaser (obj);
			queue.Enqueue (obj);
		}
	}
}