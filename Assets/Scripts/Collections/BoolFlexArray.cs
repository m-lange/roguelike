﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collections {
	public class BoolFlexArray {
		
		bool[] values;

		public BoolFlexArray () {
			values = new bool[0];
		}

		public bool Get (int idx) {
			if (idx >= values.Length) {
				return false;
			}
			return values [idx];
		}

		public void Set (int idx, bool value) {
			if (! value) {
				RemoveAt(idx);
			}
			Extend(idx);
			values[idx] = value;
		}

		public bool[] Values {
			get { return values; }
		}

		public bool Contains (int idx) {
			if (idx >= values.Length) {
				return false;
			}
			return values [idx];
		}

		void Extend (int maxIdx) {
			if (maxIdx >= values.Length) {
				bool[] newData = new bool[maxIdx+1];
				values.CopyTo( newData, 0 );
				values = newData;
			}			
		}
		void Shrink (int maxIdx) {
			if (maxIdx < values.Length - 1) {
				bool[] newData = new bool[maxIdx + 1];
				for (int i = 0; i <= maxIdx; i++) {
					newData[i] = values[i];
				}
				values = newData;
			}			
		}
		void RemoveAt (int idx) {
			if (idx <= values.Length) {
				values [idx] = false;
			}
			int newMax = values.Length - 1;
			while ( ! values [newMax] ) {
				newMax--;
			}
			if (newMax != values.Length - 1) {
				Shrink(newMax);
			}
		}
		
	}
}
