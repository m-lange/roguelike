﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;



namespace Collections {

	public class OrderedDict<K, V> : IEnumerable {

		OrderedDictionary dict = new OrderedDictionary();


		public OrderedDict() {
		}


		public V this[K key] {
			get { return (V) dict[key]; }
			set { dict [key] = value; }
		}
		public V this[int idx] {
			get { return (V) dict[idx]; }
		}

		public ICollection Keys {
			get { return dict.Keys; }
		}
		public ICollection Values {
			get { return dict.Values; }
		}
		public int Count {
			get { return dict.Count; }
		}

		public bool Contains(K key) {
			return dict.Contains (key);
		}
		public bool ContainsValue(V value) {
			foreach(V v in dict.Values) {
				if (v.Equals (value)) {
					return true;
				}
			}
			return false;
		}
		public void Remove(K key) {
			dict.Remove (key);
		}
		public void RemoveAt(int index) {
			dict.RemoveAt (index);
		}
		public void Insert(int index, K key, V obj) {
			dict.Insert (index, key, obj);
		}
		public IEnumerator GetEnumerator() {
			return dict.GetEnumerator ();
		}
		public OrderedDict<K, V> Filter(Func<V, bool> filter) {
			OrderedDict<K, V> result = new OrderedDict<K, V> ();
			IEnumerator en = dict.GetEnumerator ();
			while (en.MoveNext()) {
				DictionaryEntry kv = (DictionaryEntry) en.Current;
				V value = (V) kv.Value;
				if (filter == null || filter(value)) {
					result [(K) kv.Key] = value;
				}
			}
			return result;
		}

	}
}
