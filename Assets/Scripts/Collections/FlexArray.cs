﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collections {
	public class FlexArray<T> where T : class {
		
		T[] values;

		public FlexArray () {
			values = new T[0];
		}

		public T Get (int idx) {
			if (idx >= values.Length) {
				return null;
			}
			return values [idx];
		}

		public void Set (int idx, T value) {
			if (value == null) {
				RemoveAt(idx);
			}
			Extend(idx);
			values[idx] = value;
		}

		public T[] Values {
			get { return values; }
		}

		public bool Contains (int idx) {
			if (idx >= values.Length) {
				return false;
			}
			return values [idx] != null;
		}

		void Extend (int maxIdx) {
			if (maxIdx >= values.Length) {
				T[] newData = new T[maxIdx+1];
				values.CopyTo( newData, 0 );
				values = newData;
			}			
		}
		void Shrink (int maxIdx) {
			if (maxIdx < values.Length - 1) {
				T[] newData = new T[maxIdx + 1];
				for (int i = 0; i <= maxIdx; i++) {
					newData[i] = values[i];
				}
				values = newData;
			}			
		}
		void RemoveAt (int idx) {
			if (idx <= values.Length) {
				values [idx] = null;
			}
			int newMax = values.Length - 1;
			while ( values [newMax] == null ) {
				newMax--;
			}
			if (newMax != values.Length - 1) {
				Shrink(newMax);
			}
		}
		
	}
}
