﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Data;

namespace MapGen {


	/// <summary>
	/// Dungeon generator.
	/// http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
	/// </summary>
	public class DungeonGenerator {

		static readonly Pos[] Neighbors = new Pos[]{
			new Pos(-1,  0),
			new Pos( 1,  0),
			new Pos( 0, -1),
			new Pos( 0,  1)
		};
		static readonly Pos[] Neighbors8 = new Pos[]{
			new Pos(-1,  -1),
			new Pos(-1,   0),
			new Pos(-1,   1),
			new Pos( 0,  -1),
			new Pos( 0,   1),
			new Pos( 1,  -1),
			new Pos( 1,   0),
			new Pos( 1,   1),
		};

		/*
		public static readonly int WALL = 0;
		public static readonly int OPEN = 1;
		public static readonly int DOOR_OPEN = 2;
		public static readonly int DOOR_CLOSED = 3;
		public static readonly int DOOR_LOCKED = 4;
		public static readonly int DOOR_HIDDEN = 5;
		*/
		public static readonly int VOID = Terra.TileInfoId( Terra.Void );
		public static readonly int WALL = Terra.TileInfoId( Terra.Wall );
		public static readonly int OPEN = Terra.TileInfoId( Terra.FloorStone );
		public static readonly int DOOR_OPEN = Terra.TileInfoId( Terra.DoorOpen );
		public static readonly int DOOR_CLOSED = Terra.TileInfoId( Terra.DoorClosed );
		public static readonly int DOOR_LOCKED = Terra.TileInfoId( Terra.DoorLocked );
		public static readonly int DOOR_HIDDEN = Terra.TileInfoId( Terra.DoorHidden );

		MazeGenerator mazeGenerator;

		DungeonGeneratorSettings prefs;
		MapProgressListener progressListener;

		/// <summary>
		/// Dungeon generator.
		/// http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
		/// </summary>
		public DungeonGenerator (DungeonGeneratorSettings settings,
				MazeGenerator mazeGenerator,
				MapProgressListener progressListener) {
			this.prefs = settings;
			this.mazeGenerator = mazeGenerator;
			this.progressListener = progressListener;
		}

		void FireProgress (int progress, string message) {
			if (progressListener != null) {
				progressListener.NotifyProgress(progress, message);
			}
		}

		public int[,] Create (int[,] map, List<Pos> stairsList, out List<Rect> rooms) {
			if (map == null) {
				map = new int[prefs.width, prefs.height];
				GridUtil.FillMap (map, WALL);
			} else {
				if (map.GetLength (0) != prefs.width || map.GetLength (1) != prefs.height) {
					throw new ArgumentException ("Map dimensions don't fit.");
				}
			}

			HashSet<Pos> stairs = (stairsList == null) ? new HashSet<Pos> () : new HashSet<Pos> (stairsList);
			rooms = new List<Rect>();

			PlaceRooms (map, stairs, ref rooms);
			FillMaze (map);

			List<Pos> doors = Connect (map);

			ReduceDeadEnds (map, stairs);
			ReplaceSolid (map, VOID);
			PlaceDoors (map, doors);


			return map;
		}

		void PlaceDoors (int[,] map, List<Pos> doors) {
			float[] cumProb = new float[prefs.doorProbabilities.Length];
			cumProb [0] = prefs.doorProbabilities [0];
			for (int i = 1; i < cumProb.Length; i++) {
				cumProb [i] = cumProb [i - 1] + prefs.doorProbabilities [i];
			}
			float sumProb = cumProb [cumProb.Length - 1];

			foreach (Pos door in doors) {
				if (map [door.x, door.y] == OPEN) {
					float rand = RandUtils.NextFloat (sumProb);
					//if (rand < sumProb) {
						if (rand < cumProb [0]) {
							map [door.x, door.y] = OPEN;
						} else if (rand < cumProb [1]) {
							map [door.x, door.y] = DOOR_OPEN;
						} else if (rand < cumProb [2]) {
							map [door.x, door.y] = DOOR_CLOSED;
						} else if (rand < cumProb [3]) {
							map [door.x, door.y] = DOOR_LOCKED;
						} else if (rand < cumProb [4]) {
							map [door.x, door.y] = DOOR_HIDDEN;
						} 
					//}
				}
			}
		}

		void ReplaceSolid (int[,] map, int value) {
			bool[,] temp = new bool[prefs.width, prefs.height];

			for (int x = 0; x < prefs.width; x++) {
				for (int y = 0; y < prefs.height; y++) {
					if (map [x, y] == WALL) {
						bool solid = true;
						foreach (Pos n in Neighbors8) {
							int xx = x + n.x;
							int yy = y + n.y;
							if (xx >= 0 && yy >= 0 && xx < prefs.width && yy < prefs.height) {
								if (map [xx, yy] != WALL) {
									solid = false;
									break;
								}
							}
						}
						if (solid) {
							temp [x, y] = true;
						}
					}
				}
			}
			for (int x = 0; x < prefs.width; x++) {
				for (int y = 0; y < prefs.height; y++) {
					if (temp [x, y]) {
						map[x,y] = value;
					}
				}
			}
		}

		void ReduceDeadEnds (int[,] map, HashSet<Pos> stairs) {
			bool found = true;
			while ( found ) {
				found = false;
				for (int x = 1; x < prefs.width - 1; x++) {
					for (int y = 1; y < prefs.height - 1; y++) {
						if ( map [x, y] != WALL ) {
							Pos open = null;
							Pos current = new Pos (x, y);
							while ( true ) {
								int cntWall = 0;
								foreach (Pos n in Neighbors) {
									if (map [current.x + n.x, current.y + n.y] == WALL) {
										cntWall++;
									} else {
										open = n;
									}
								}
								if (cntWall == 3 && (stairs == null || ! stairs.Contains(new Pos(current.x, current.y)) )) {
									found = true;
									map [current.x, current.y] = WALL;
									current.x += open.x;
									current.y += open.y;
								} else {
									break;
								}
							}
						}
					}
				}
			}
		}

		List<Pos> Connect (int[,] map) {
			List<Pos> doors = new List<Pos> ();

			int maxId = 0;
			int[,] components = FillComponents (map, out maxId);

			HashSet<int> connected = new HashSet<int> ();
			HashSet<int> unconnected = new HashSet<int> ();
			for (int i = 0; i <= maxId; i++) {
				unconnected.Add (i);
			}
			Dictionary<Pos, Pos> connectors = GetConnectors (components);
			Dictionary<Pos, Pos> connectorsPreserved = new Dictionary<Pos, Pos> (connectors);

			int startId = RandUtils.NextInt (maxId + 1);
			connected.Add (startId);
			unconnected.Remove (startId);

			Dictionary<Pos, Pos> conn = new Dictionary<Pos, Pos> ();
			while ( connectors.Count > 0 ) {
				conn.Clear ();
				foreach (KeyValuePair<Pos, Pos> kv in connectors) {
					if (connected.Contains (kv.Value.x) || connected.Contains (kv.Value.y)) {
						conn [kv.Key] = kv.Value;
					}
				}
				if (conn.Count == 0) {
					break;
				}
				KeyValuePair<Pos, Pos> sel = RandUtils.Select (conn);
				map [sel.Key.x, sel.Key.y] = OPEN;
				components [sel.Key.x, sel.Key.y] = sel.Value.x;
				doors.Add (sel.Key);

				if (unconnected.Contains (sel.Value.x)) {
					unconnected.Remove (sel.Value.x);
					connected.Add (sel.Value.x);
				}
				if (unconnected.Contains (sel.Value.y)) {
					unconnected.Remove (sel.Value.y);
					connected.Add (sel.Value.y);
				}
				List<Pos> remove = new List<Pos> ();
				foreach (KeyValuePair<Pos, Pos> kv in connectors) {
					if (connected.Contains (kv.Value.x) && connected.Contains (kv.Value.y)) {
						remove.Add (kv.Key);
					}
				}
				foreach (Pos key in remove) {
					connectors.Remove (key);
				}
			}

			int trials = 0;
			while ( unconnected.Count > 0 && trials < 20 ) {
				//UnityEngine.Debug.Log ("Unconnected: " + unconnected.Count);
				List<int> tempUnconn = new List<int> (unconnected);
				foreach (int id in tempUnconn) {
					ConnectRemaining (map, components, id, trials > 10, ref connected, ref unconnected, ref doors);
				}
				trials++;
			}
			if (unconnected.Count > 0) {
				UnityEngine.Debug.Log ("Finally unconnected: " + unconnected.Count);
			}

			/*
			for (int x = 1; x < width-1; x++) {
				for (int y = 1; y < height-1; y++) {
					if (map [x, y] == WALL) {
						Pos n1 = null;
						Pos n2 = null;
						foreach (Pos n in Neighbors) {
							if (map [x+n.x, y+n.y] != WALL) {
								if (n1 == null) {
									n1 = n;
								} else if (n2 == null) {
									n2 = n;
								} else {
									n1 = null;
									n2 = null;
									break;
								}
							}
						}
						if (n1 != null && n2 != null && n1.x == -n2.x && n1.y == -n2.y) {
							if (RandUtils.NextBool (additionalDoorProb)) {
								map [x, y] = OPEN;
								doors.Add ( new Pos(x, y) );
							}
						}
					}
				}
			}
			*/
			foreach (Pos key in connectorsPreserved.Keys) {
				if (RandUtils.NextBool (prefs.additionalDoorProb)) {
					int cnt = 0;
					foreach (Pos n in Neighbors) {
						if (map [key.x + n.x, key.y + n.y] != WALL) {
							cnt++;
						}
					}
					if (cnt < 3) {
						map [key.x, key.y] = OPEN;
						doors.Add (key);
					}
				}
			}

			return doors;
		}

		void ConnectRemaining (int[,] map, int[,] comp, int id, bool random, ref HashSet<int> connected, ref HashSet<int> unconnected, ref List<Pos> doors) {
			List<Pos[]> exits = new List<Pos[]> ();

			for (int x = 1; x < prefs.width - 1; x++) {
				for (int y = 1; y < prefs.height - 1; y++) {
					if (comp [x, y] == id) {
						foreach (Pos n in Neighbors) {
							if (map [x + n.x, y + n.y] == WALL) {
								exits.Add (new Pos[] { 
									new Pos (x + n.x, y + n.y),
									n
								});
							}
						}
					}
				}
			}
			RandUtils.Shuffle (exits);
			//UnityEngine.Debug.Log(id+" "+exits.Count);

			List<Pos> path = new List<Pos> ();
			foreach (Pos[] ex in exits) {
				path.Clear ();
				Pos pos = ex [0];
				Pos dir = ex [1];
				bool success = false;
				while ( true ) {
					int c = comp [pos.x, pos.y];
					if (c < 0) {
						path.Add (pos);
						if (random) {
							Pos oldDir = dir;
							do {
								dir = RandUtils.Select (Neighbors);
							} while((oldDir.x == -dir.x && oldDir.y == -dir.y));
							//UnityEngine.Debug.Log(">> "+dir+" "+oldDir);
						}
						pos = new Pos (pos.x + dir.x, pos.y + dir.y);
						if (pos.x <= 0 || pos.y <= 0 || pos.x >= prefs.width - 1 || pos.y >= prefs.height - 1) {
							success = false;
							break;
						}
						if (map [pos.x, pos.y] == WALL) {
							bool illeg = false;
							foreach (Pos n in Neighbors) {
								if (!n.Equals (dir)) {
									if (map [pos.x + n.x, pos.y + n.y] != WALL) {
										//UnityEngine.Debug.Log (ex [0] + " " + pos);
										illeg = true;
										break;
									}
								}
							}
							if (illeg) {
								success = false;
								break;
							}
						}
					} else {
						if (connected.Contains (c)) {
							success = true;
							break;
						} else {
							success = false;
							break;
						}
					}
				}
				if (success) {
					//UnityEngine.Debug.Log(path.Count);
					//UnityEngine.Debug.Log(path [0]);

					doors.Add (path [0]);
					foreach (Pos p in path) {
						map[p.x, p.y] = OPEN;
						comp[p.x, p.y] = id;
						//doors.Add (p);
					}
					connected.Add(id);
					unconnected.Remove(id);
					break;
				}
			}

		}

		Dictionary<Pos, Pos> GetConnectors (int[,] comp) {
			Dictionary<Pos, Pos> connectors = new Dictionary<Pos, Pos>();
			int v1;
			int v2;
			for (int x = 1; x < prefs.width-1; x++) {
				for (int y = 1; y < prefs.height-1; y++) {
					if (comp [x, y] < 0) {
						v1 = -1;
						v2 = -1;
						foreach (Pos n in Neighbors) {
							int v = comp [n.x + x, n.y + y];
							if (v >= 0) {
								if (v1 < 0) {
									v1 = v;
								} else if (v2 < 0 && v != v1) {
									v2 = v;
								}
							}
						}
						if (v1 >= 0 && v2 >= 0) {
							connectors[new Pos(x,y)] = new Pos(v1, v2);
						}
					}
				}
			}
			return connectors;
		}

		int[,] FillComponents (int[,] map, out int maxId) {
			int[,] comp = new int[prefs.width, prefs.height];
			for (int x = 0; x < prefs.width; x++) {
				for (int y = 0; y < prefs.height; y++) {
					comp[x,y] = -1;
				}
			}

			maxId = 0;
			for (int x = 0; x < prefs.width; x++) {
				for (int y = 0; y < prefs.height; y++) {
					if (map [x, y] == OPEN && comp[x, y] < 0) {
						FloodFill(map, comp, x, y, maxId);
						maxId++;
					}
				}
			}
			maxId--;
			return comp;
		}

		void FloodFill (int[,] map, int[,] comp, int x, int y, int compId) {

			Queue<Pos> open = new Queue<Pos> ();
			open.Enqueue (new Pos (x, y));

			while ( open.Count > 0 ) {
				Pos curr = open.Dequeue();

				if (curr.x < 0 || curr.y < 0 || curr.x >= prefs.width || curr.y >= prefs.height) {
					continue;
				}
				int c = comp [curr.x, curr.y];
				if (c >= 0 || map [curr.x, curr.y] != OPEN) {
					continue;
				}
				comp [curr.x, curr.y] = compId;
				foreach (Pos n in Neighbors) {
					open.Enqueue(new Pos(curr.x+n.x, curr.y+n.y));
					//FloodFill(map, comp, x+n.x, y+n.y, compId);
				}
			}

		}
		/*
		void FloodFill (int[,] map, int[,] comp, int x, int y, int compId) {
			if (x < 0 || y < 0 || x >= width || y >= height) {
				return;
			}
			int c = comp [x, y];
			if (c >= 0 || map[x,y] != OPEN) {
				return;
			}
			comp[x,y] = compId;
			foreach (Pos n in Neighbors) {
				FloodFill(map, comp, x+n.x, y+n.y, compId);
			}
		}
		*/

		void FillMaze (int[,] map) {
			for (int x = 1; x < prefs.width - 1; x++) {
				for (int y = 1; y < prefs.height - 1; y++) {
					if (CanStartMaze (map, x, y)) {
						mazeGenerator.FillMaze (map, x, y, prefs.corridorStraightness);
					}
				}
			}
		}

		bool CanStartMaze (int[,] map, int x, int y) {
			if (x <= 0 || y <= 0 || x >= prefs.width - 1 || y >= prefs.height - 1) {
				return false;
			}
			for (int xx = x - 1; xx <= x + 1; xx++) {
				for (int yy = y - 1; yy <= y + 1; yy++) {
					if(map[xx,yy] != WALL) {
						return false;
					}
				}
			}
			return true;
		}

		void PlaceRooms (int[,] map, HashSet<Pos> stairs, ref List<Rect> rooms) {
			if (stairs != null) {
				foreach (Pos stair in stairs) {
					if (map [stair.x, stair.y] == WALL) {
						int cnt = 0;
						int minW = prefs.minRoomWidth;
						int maxW = prefs.maxRoomWidth;
						while ( cnt < 300 ) {
							if (cnt == 100) {
								minW = 1;
							}
							int w = RandUtils.NextInt (minW, maxW + 1) + 2;
							int h = RandUtils.NextInt (minW, maxW + 1) + 2;
							int xmin = stair.x - (w - 2);
							int xmax = stair.x - 1;
							int ymin = stair.y - (h - 2);
							int ymax = stair.y - 1;
							if (xmin < 0) {
								xmin = 0;
							}
							if (xmax > prefs.width - w) {
								xmax = prefs.width - w;
							}
							if (ymin < 0) {
								ymin = 0;
							}
							if (ymax > prefs.height - h) {
								ymax = prefs.height - h;
							}
							int x = RandUtils.NextInt (xmin, xmax + 1);
							int y = RandUtils.NextInt (ymin, ymax + 1);
							if (PlaceRoom (map, stairs, x, y, w, h)) {
								rooms.Add (new Rect (x, y, x + w, y + w));
								break;
							}
							cnt++;
						}
						if (cnt == 300) {
							UnityEngine.Debug.Log ("Skip " + stair);
						}
					}
				}
			}
			for (int i = 0; i < prefs.roomAttempts; i++) {
				int w = RandUtils.NextInt (prefs.minRoomWidth, prefs.maxRoomWidth + 1) + 2;
				int h = RandUtils.NextInt (prefs.minRoomWidth, prefs.maxRoomWidth + 1) + 2;
				int x = RandUtils.NextInt (prefs.width - w + 1);
				int y = RandUtils.NextInt (prefs.height - h + 1);
				if (PlaceRoom (map, null, x, y, w, h)) {
					rooms.Add (new Rect (x, y, x + w, y + w));
				}
			}
		}

		bool PlaceRoom (int[,] map, HashSet<Pos> stairs, int x, int y, int w, int h) {
			bool blocked = false;
			for (int xx = x; xx < x + w; xx++) {
				for (int yy = y; yy < y + h; yy++) {
					if( xx >= map.GetLength(0) || yy >= map.GetLength(1) ) {
						UnityEngine.Debug.Log( xx+" "+yy+" "+map.GetLength(0)+" "+map.GetLength(1) );
					}
					if (map [xx, yy] != WALL) {
						blocked = true;
						break;
					}
					if (stairs != null) {
						if (xx == x || yy == y || xx == x + w - 1 || yy == y + h - 1) {
							if (stairs.Contains (new Pos (xx, yy))) {
								blocked = true;
								break;
							}
						}
					}
				}
				if (blocked) {
					break;
				}
			}
			if (!blocked) {
				for (int xx = x + 1; xx < x + w - 1; xx++) {
					for (int yy = y + 1; yy < y + h - 1; yy++) {
						map [xx, yy] = OPEN;
					}
				}
			}
			return ! blocked;
		}
	}



	public class Pos {
		public int x;
		public int y;
		public Pos(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public float Distance (Pos other) {
			return Distance(other.x, other.y);
		}		
		public float Distance (int x, int y) {
			return (float) Math.Sqrt( DistanceSq(x, y) );
		}		
		public float Distance (float x, float y) {
			return (float) Math.Sqrt( DistanceSq(x, y) );
		}		
		public float DistanceSq (Pos other) {
			return DistanceSq(other.x, other.y);
		}
		public float DistanceSq (int x, int y) {
			int dx = this.x - x;
			int dy = this.y - y;
			return dx*dx + dy*dy;
		}
		public float DistanceSq (float x, float y) {
			float dx = this.x - x;
			float dy = this.y - y;
			return dx*dx + dy*dy;
		}

		public override string ToString () {
			return string.Format ("[{0} {1}]", x, y);
		}

		public override bool Equals(Object obj) {
			if ( typeof(Pos).IsAssignableFrom( obj.GetType() ) ) {
				Pos other = (Pos) obj;
				return other.x == x && other.y == y;
			} else {
				return false;
			}
		}

		public override int GetHashCode() {
			int hash=13;
			hash = hash * 17 + x;
			hash = hash * 17 + y;
			return hash;
		}
	}




		public class DungeonGeneratorSettings {
			public readonly int width;
			public readonly int height;
			public readonly int roomAttempts;
			public readonly int minRoomWidth;
			public readonly int maxRoomWidth;
			public readonly float additionalDoorProb;
			public readonly float[] doorProbabilities;
			public readonly float corridorStraightness;

			public DungeonGeneratorSettings (int width, int height, int roomAttempts, int minRoomWidth, int maxRoomWidth,
					float additionalDoorProb,
					float[] doorProbabilities,
					float corridorStraightness) {
				this.width = width;
				this.height = height;
				this.roomAttempts = roomAttempts;
				this.minRoomWidth = minRoomWidth;
				this.maxRoomWidth = maxRoomWidth;
				this.additionalDoorProb = additionalDoorProb;
				this.doorProbabilities = doorProbabilities;
				this.corridorStraightness = corridorStraightness;
			}
		}
}
