﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;

namespace MapGen {
	public class Templates {

		static string NO_DATA = "NODATA";
		static string ALIAS = "ALIAS";
		static string TEMPLATE = "TEMPLATE";
		static string LAYER = "LAYER";
		static string END = "END";

		OrderedDict<string, Template> all;
		
		public Templates ( Dictionary<string, byte> tileInfo, params string[] files ) {
			all = new OrderedDict<string, Template> ();

			foreach (string file in files) {
				OrderedDict<string, Dictionary<int, int[,]>> temp = ReadTemplateFile (GlobalParameters.Instance.TemplatePath + "/" + file, tileInfo);
				foreach (DictionaryEntry e in temp) {
					if (all.Contains (e.Key.ToString ())) {
						throw new ArgumentException("Template with identifier "+e.Key.ToString ()+" already exists.");
					}
					all[ e.Key.ToString () ] = new Template( e.Key.ToString (), (Dictionary<int, int[,]>) e.Value) ;
				}
			}
			//string path = GlobalParameters.Instance.TemplatePath;
			//string[] files = Directory.GetFiles(path);
		}

		public OrderedDict<string, Template> All {
			get { return all; }
		}

		OrderedDict<string, Dictionary<int, int[,]>> ReadTemplateFile (string path, Dictionary<string, byte> tileInfo) {

			string text = System.IO.File.ReadAllText (path);

			string[] lines = text.Split ( new string[]{Environment.NewLine}, StringSplitOptions.None );

			int noData = -1;

			Dictionary<char, int> alias = new Dictionary<char, int> ();

			OrderedDict<string, Dictionary<int, int[,]>> templates = new OrderedDict<string, Dictionary<int, int[,]>> ();

			string currentTemplate = null;

			char[] space = new char[]{ ' ' };

			for (int i = 0; i < lines.Length; i++) {
				string line = lines [i];
				if (line.StartsWith (NO_DATA)) {
					noData = int.Parse (line.Split (space, System.StringSplitOptions.RemoveEmptyEntries) [1].Trim ());
				} else if (line.StartsWith (ALIAS)) {
					List<string> aliases = NonKeywordLines (lines, i + 1);
					foreach (string l in aliases) {
						char c = l [0];
						string al = l.Substring (1).Trim ();
						//UnityEngine.Debug.Log (c+" "+al);
						if (alias.ContainsKey (c)) {
							throw new FormatException ("Duplicate character " + c + " in alias list (" + alias [c] + ", " + al + ").");
						}
						alias [c] = tileInfo [al];
					}
				} else if (line.StartsWith (TEMPLATE)) {
					string[] templateTokens = line.Split (space, System.StringSplitOptions.RemoveEmptyEntries);
					currentTemplate = templateTokens [1];
					if (templates.Contains (currentTemplate)) {
						throw new ArgumentException ("Duplicate template " + currentTemplate + " in " + path);
					}
					templates [currentTemplate] = new Dictionary<int, int[,]> ();
				} else if (line.StartsWith (LAYER)) {
					string[] levelTokens = line.Split (space, System.StringSplitOptions.RemoveEmptyEntries);
					int level = int.Parse (levelTokens [1].Trim ());
					List<string> layerString = NonKeywordLines (lines, i + 1);
					layerString.Reverse ();
					int height = layerString.Count;
					int width = 0;
					foreach (string l in layerString) {
						if (l.Length > width) {
							width = l.Length;
						}
					}
					int[,] arr = new int[width, height];

					for (int x = 0; x < width; x++) {
						for (int y = 0; y < height; y++) {
							arr [x, y] = noData;
							string ln = layerString [y];
							if (ln.Length > x) {
								char c = ln [x];
								if (alias.ContainsKey (c)) {
									arr [x, y] = alias [c];
								}
							}
						}
					}
					templates[currentTemplate][level] = arr;
				} 
			}

			return templates;
		}


		static List<string> NonKeywordLines(string[] lines, int startIndex) {
			List<string> result = new List<string> ();
			for (int i = startIndex; i < lines.Length; i++) {
				string line = lines [i];
				if (line.StartsWith (NO_DATA) 
				|| line.StartsWith (TEMPLATE) 
				|| line.StartsWith (LAYER) 
				|| line.StartsWith (ALIAS) 
				|| line.StartsWith (END)) {
					break;
				} else {
					result.Add (line);
				}
			}
			return result;
		}

	}

	public class Template {
		string name;
		Dictionary<int, int[,]> layers;

		public Template(string name, Dictionary<int, int[,]> layers) {
			this.name = name;
			this.layers = layers;
		}

		public int Width(int layer) {
			return layers[layer].GetLength(0);
		}

		public int Height(int layer) {
			return layers[layer].GetLength(1);
		}
		public string Name {
			get { return name; }
		}

		public Dictionary<int, int[,]> Layers {
			get { return layers; }
		}
	}

}
