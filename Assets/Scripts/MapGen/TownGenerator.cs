﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm.Impl;
using Collections;
using Game.Data;

namespace MapGen {
	public class TownGenerator {

		const int LEFT = 0;
		const int RIGHT = 1;
		const int TOP = 2;
		const int BOTTOM = 3;

		static readonly int[] Opposite = new int[]{ RIGHT, LEFT, BOTTOM, TOP };

		static readonly Pos[] Neighbors = new Pos[]{
			new Pos(-1,  0),
			new Pos( 1,  0),
			new Pos( 0,  1),
			new Pos( 0, -1)
		};

		/*
		static readonly Pos[] Neighbors8 = new Pos[]{
			new Pos(-1, -1),
			new Pos(-1,  0),
			new Pos(-1,  1),
			new Pos( 0, -1),
			new Pos( 0,  1),
			new Pos( 1, -1),
			new Pos( 1,  0),
			new Pos( 1,  1),
		};
		*/

		public static readonly int VOID = Terra.TileInfoId( Terra.Void );
		public static readonly int WALL = Terra.TileInfoId( Terra.Wall );
		public static readonly int FLOOR_MEADOW = Terra.TileInfoId( Terra.FloorMeadow );
		public static readonly int FLOOR_PATH = Terra.TileInfoId( Terra.FloorPath );
		public static readonly int FLOOR_STONE = Terra.TileInfoId( Terra.FloorStone );
		public static readonly int FLOOR_WOOD = Terra.TileInfoId( Terra.FloorWood );
		public static readonly int FLOOR_SHOP = Terra.TileInfoId( Terra.FloorShop );
		public static readonly int DOOR_OPEN = Terra.TileInfoId( Terra.DoorOpen );
		public static readonly int DOOR_CLOSED = Terra.TileInfoId( Terra.DoorClosed );
		public static readonly int DOOR_LOCKED = Terra.TileInfoId( Terra.DoorLocked );
		public static readonly int DOOR_HIDDEN = Terra.TileInfoId( Terra.DoorHidden );
		public static readonly int WINDOW_CLOSED = Terra.TileInfoId( Terra.WindowClosed );
		public static readonly int STAIRS_UP = Terra.TileInfoId( Terra.StairsUp );
		public static readonly int STAIRS_DOWN = Terra.TileInfoId( Terra.StairsDown );
		public static readonly int ROOF = Terra.TileInfoId( Terra.Roof );
		public static readonly int WATER_DEEP = Terra.TileInfoId( Terra.WaterDeep );

		

		TownGeneratorSettings prefs;
		MapProgressListener progressListener;

		public TownGenerator (TownGeneratorSettings settings, MapProgressListener progressListener) {
			this.prefs = settings;
			this.progressListener = progressListener;
		}

		void FireProgress (int progress, string message) {
			if (progressListener != null) {
				progressListener.NotifyProgress(progress, message);
			}
		}

		public Dictionary<int, int[,]> Create (out List<Building> buildings) {
			Stopwatch watch = new Stopwatch();
			watch.Start();
			
			Dictionary<int, int[,]> layers = new Dictionary<int, int[,]> ();
			layers [0] = new int[prefs.width, prefs.height];
			layers [-1] = new int[prefs.width, prefs.height];

			GridUtil.FillMap (layers [-1], WALL);
			GridUtil.FillMap (layers [0], FLOOR_MEADOW);
			//CreateTownWall( map );

			RectZone rootZone = new RectZone (new Rect (0, 0, 0, 0), new Borders (0, 0, 0, 0), null, -1);
			List<RectZone> zones = CreateMainRoads (layers [0], rootZone);

			List<RectZone> removed = new List<RectZone> ();
			zones = CreateRoadsSubdivide (layers [0], zones, 2, prefs.minLargeBlockWidth, prefs.minLargeBlockHeight);


			RectZone cloister = SelectCloister (rootZone);
			zones.Remove (cloister);

			List<RectZone> cloisterRemoved = new List<RectZone> ();
			List<RectZone> cloisterBlocks = CreateRoadsSubdivide (layers [0], new List<RectZone>{ cloister }, 9, prefs.minNormalBlockSize, prefs.minNormalBlockSize, 0);
			cloisterBlocks = FilterZonesRadius (cloisterBlocks, 8, ref cloisterRemoved);

			List<RectZone> smallCloisterBlocks = CreateRoadsSubdivide (layers [0], cloisterRemoved, 9, (int)(prefs.minNormalBlockSize * 0.666f), (int)(prefs.minNormalBlockSize * 0.666f), 0);
			cloisterRemoved.Clear ();
			smallCloisterBlocks = FilterZonesRadius (smallCloisterBlocks, 6, ref cloisterRemoved);

			List<RectZone> tinyCloisterBlocks = CreateRoadsSubdivide (layers [0], cloisterRemoved, 11, prefs.minNormalBlockSize / 3, prefs.minNormalBlockSize / 3, 0);
			cloisterRemoved.Clear ();
			tinyCloisterBlocks = FilterZonesRadius (tinyCloisterBlocks, 4, ref cloisterRemoved);

			cloisterBlocks.AddRange(smallCloisterBlocks);
			cloisterBlocks.AddRange(tinyCloisterBlocks);



			zones = CreateRoadsSubdivide (layers [0], zones, 9, prefs.minLargeBlockWidth, prefs.minLargeBlockHeight);

			bool top = RandUtils.NextBool (0.5f);
			RectZone mainChurch = SelectMainSquare (rootZone, false, top);
			zones.Remove (mainChurch);

			zones = CreateRoadsSubdivide (layers [0], zones, 9, prefs.minNormalBlockSize, prefs.minNormalBlockSize);
			zones = FilterZonesRadius (zones, 8, ref removed);

			RectZone mainSquare = SelectMainSquare (rootZone, true, top);
			zones.Remove (mainSquare);

			// subdivide zones overlapping city walls
			List<RectZone> additionalSmall = RandUtils.Draw (zones, (int)(zones.Count * 0.333f));
			removed.AddRange (additionalSmall);

			List<RectZone> smallBlocks = CreateRoadsSubdivide (layers [0], removed, 8, (int)(prefs.minNormalBlockSize * 0.666f), (int)(prefs.minNormalBlockSize * 0.666f));
			removed.Clear ();
			smallBlocks = FilterZonesRadius (smallBlocks, 6, ref removed);

			List<RectZone> tinyBlocks = CreateRoadsSubdivide (layers [0], removed, 10, prefs.minNormalBlockSize / 3, prefs.minNormalBlockSize / 3);
			removed.Clear ();
			tinyBlocks = FilterZonesRadius (tinyBlocks, 4, ref removed);

			foreach (RectZone rem in removed) {
				rem.DeepRemoveFromParent ();
			}

			RectZone secondarySquare = SelectSecondarySquare (zones, prefs.townRadius / 2);
			RectZone secondaryChurch = secondarySquare.parent.right;
			zones.Remove (secondarySquare);
			zones.Remove (secondaryChurch);
			if (zones.Contains (secondarySquare)) {
				UnityEngine.Debug.Log("ERROR !!!");
			}

			zones.AddRange (smallBlocks);
			zones.AddRange (tinyBlocks);

			RectZone park1 = SelectPark (zones, prefs.townRadius * 0.6f);
			RectZone park2 = SelectPark (zones, prefs.townRadius * 0.6f);
			zones.Remove (park1);
			zones.Remove (park2);

			List<RectZone> allZones = new List<RectZone> ();
			allZones.Add (mainSquare);
			allZones.Add (mainChurch);
			allZones.Add (secondarySquare);
			allZones.Add (secondaryChurch);
			allZones.Add (park1);
			allZones.Add (park2);
			allZones.AddRange (cloisterBlocks);


			watch.Stop();
			UnityEngine.Debug.Log("Subdividing: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			CreateRiver(layers[0], allZones, WATER_DEEP, 5);

			watch.Stop();
			UnityEngine.Debug.Log("Creating river: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			List<RectZone> riverRemoved = new List<RectZone>();
			zones = FilterZonesBlocked (layers[0], zones, WATER_DEEP, ref riverRemoved);

			List<RectZone> smallRiverBlocks = CreateRoadsSubdivide (layers [0], riverRemoved, 9, (int)(prefs.minNormalBlockSize * 0.666f), (int)(prefs.minNormalBlockSize * 0.666f), -1);
			riverRemoved.Clear ();
			smallRiverBlocks = FilterZonesBlocked (layers[0], smallRiverBlocks, WATER_DEEP, ref riverRemoved);

			List<RectZone> tinyRiverBlocks = CreateRoadsSubdivide (layers [0], riverRemoved, 11, prefs.minNormalBlockSize / 3, prefs.minNormalBlockSize / 3, -1);
			riverRemoved.Clear ();
			tinyRiverBlocks = FilterZonesBlocked (layers[0], tinyRiverBlocks, WATER_DEEP, ref riverRemoved);

			zones.AddRange(smallRiverBlocks);
			zones.AddRange(tinyRiverBlocks);

			allZones.AddRange (zones);
			allZones.AddRange (riverRemoved);

			watch.Stop();
			UnityEngine.Debug.Log("Filtering: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			GridUtil.Erode4(layers[0], WATER_DEEP, FLOOR_MEADOW);
			GridUtil.Erode4(layers[0], WATER_DEEP, FLOOR_MEADOW);
			GridUtil.Erode4(layers[0], WATER_DEEP, FLOOR_MEADOW);
			GridUtil.Dilate4(layers[0], WATER_DEEP, WATER_DEEP);
			GridUtil.Erode4(layers[0], WATER_DEEP, FLOOR_MEADOW);
			List<Pos> riverCells = GridUtil.Filter( layers[0], (v) => v == WATER_DEEP );


			watch.Stop();
			UnityEngine.Debug.Log("Dilate/erode river: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			foreach (RectZone r in allZones) {
				GridUtil.FillRect (layers[0], r.rect, 111);
			}

			int marg = 8;
			GridUtil.Buffer (layers[0], marg + prefs.wallWidth, 111, 999, true, FLOOR_MEADOW, GridUtil.ChebyshevDistance);
			GridUtil.Buffer (layers[0], marg, 111, FLOOR_MEADOW, true, 999, GridUtil.ChebyshevDistance);
			GridUtil.Replace (layers[0], 999, WALL);
			//GridUtil.ReplaceSolid (layers[0], WALL, FLOOR_STONE, Neighbors8);

			foreach (RectZone r in allZones) {
				GridUtil.FillRect (layers[0], r.rect, WALL);
			}
			foreach (RectZone r in riverRemoved) {
				GridUtil.FillRect (layers[0], r.rect, FLOOR_MEADOW);
				allZones.Remove(r);
			}
			GridUtil.Fill(layers[0], riverCells, WATER_DEEP);

			FillRoads (layers[0], allZones);


			watch.Stop();
			UnityEngine.Debug.Log("Blocks: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			CreateCloister(layers, cloisterBlocks, prefs.townRadius);

			watch.Stop();
			UnityEngine.Debug.Log("Cloister: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			DrawMainRoads (layers[0]);

			GridUtil.FillRect (layers[0], mainSquare.rect, FLOOR_PATH);
			GridUtil.FillRect (layers[0], mainChurch.rect, FLOOR_MEADOW);
			GridUtil.FillRect (layers[0], secondarySquare.rect, FLOOR_PATH);
			GridUtil.FillRect (layers[0], secondaryChurch.rect, FLOOR_MEADOW);
			GridUtil.FillRect (layers[0], park1.rect, FLOOR_MEADOW);
			GridUtil.FillRect (layers[0], park2.rect, FLOOR_MEADOW);

			foreach (RectZone r in zones) {
				GridUtil.FillRect (layers[0], r.rect, FLOOR_MEADOW);
			}

			watch.Stop();
			UnityEngine.Debug.Log("Fill: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			int minSize1 = 8;
			List<RectZone> smallSolitaryBuildings = zones.FindAll ((z) => z.rect.width < minSize1 || z.rect.height < minSize1);
			foreach (RectZone r in smallSolitaryBuildings) {
				zones.Remove (r);
			}

			int minSize2 = 20;
			List<RectZone> subsizeBlocks = zones.FindAll ((z) => z.rect.width < minSize2 || z.rect.height < minSize2);
			foreach (RectZone r in subsizeBlocks) {
				zones.Remove (r);
			}

			buildings = new List<Building> ();
			foreach (RectZone r in smallSolitaryBuildings) {
				PlaceBuilding (layers[0], r.rect, FLOOR_WOOD, WALL);
				int door = RandUtils.Select (Opposite);
				Building b = new Building (r.rect, door, RandUtils.NextBool (prefs.backDoorProb) ? Opposite [door] : -1);
				buildings.Add (b);
			}
			foreach (RectZone r in subsizeBlocks) {
				buildings.AddRange (PlaceBuildingsBlock (layers[0], r, 4, 7, 6, FLOOR_WOOD, WALL));
			}
			foreach (RectZone r in zones) {
				buildings.AddRange (PlaceBuildingsBlock (layers[0], r, 4, 7, 8, FLOOR_WOOD, WALL));
			}

			watch.Stop();
			UnityEngine.Debug.Log("Buildings: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			/*List<Pos> doors =*/ PlaceDoors (layers[0], buildings);

			watch.Stop();
			UnityEngine.Debug.Log("Doors: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			CreateChurches(layers, new RectZone[]{mainChurch, secondaryChurch});

			watch.Stop();
			UnityEngine.Debug.Log("Churches: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			CreateBasements(layers[0], layers[-1], buildings);

			watch.Stop();
			UnityEngine.Debug.Log("Basement: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();


			CreateUpperStoreys(layers, buildings, prefs.townRadius);

			watch.Stop();
			UnityEngine.Debug.Log("Upper: "+watch.ElapsedMilliseconds+"ms");
			watch.Reset(); watch.Start();

			CreateWindows(layers, buildings);
			watch.Stop();
			UnityEngine.Debug.Log("Windows: "+watch.ElapsedMilliseconds+"ms");
			//watch.Reset(); watch.Start();


			return layers;
		}
		void CreateRiver (int[,] map, List<RectZone> blockedZones, int value, int radius) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			int w2 = Mathf.CeilToInt (w / 2f);
			int h2 = Mathf.CeilToInt (h / 2f);
			int[,] cost = new int[w2, h2];
			foreach (RectZone z in blockedZones) {
				GridUtil.FillRectHalfSize (cost, z.rect, -1, radius);
			}
			for (int x = 0; x < w2; x++) {
				for (int y = 0; y < h2; y++) {
					if (cost [x, y] >= 0) {
						float p1 = Util.Perlin.Noise (12f * x / (float)w2, 12f * y / (float)h2);
						cost [x, y] = (int)(1 + 50 * (1f + p1));
					}
				}
			}
			//IO.FileIO.WriteMatrix( GlobalParameters.Instance.SavePath+"/perlin.csv", cost, ";" );

			int start = RandUtils.NextInt (h2 / 3, 2 * h2 / 3 + 1);
			int end = RandUtils.NextInt (h2 / 3, 2 * h2 / 3 + 1);

			float sqrt2 = Mathf.Sqrt (2f);
			AStar2DPathFinding<int> pf = new AStar2DPathFinding<int> (cost, 
				  (source, target, v) => {
				  	if( v <= 0 ) return -1;
					float factor = 1f;
					if (source.X != target.X && source.Y != target.Y) {
						factor = sqrt2;
					}
					return Mathf.RoundToInt (v * factor);
				}, 1);
			List<GridLocation> path = pf.FindPath (new GridLocation (0, 0, start), new GridLocation (0, w2 - 1, end));

			for (int i = 0; i < path.Count; i++) {
				GridLocation loc = path [i];
				map [loc.X * 2, loc.Y * 2] = value;
				if (i < path.Count - 1) {
					GridLocation loc2 = path [i + 1];
					map[ loc.X+loc2.X, loc.Y+loc2.Y ] = value;
				}
			}

			GridUtil.Buffer(map, radius, WATER_DEEP, WATER_DEEP, false, 0, GridUtil.EuclideanDistance);
		}

		void CreateCloister (Dictionary<int, int[,]> layers, List<RectZone> cloisterBlocks, int townRadius) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy); 

			int[,] layer = layers [0];
			if (!layers.ContainsKey (1)) {
				layers[1] = new int[prefs.width, prefs.height];
			}
			int[,] layersRoof = layers [1];

			foreach (RectZone r in cloisterBlocks) {
				GridUtil.FillRect (layer, r.rect, 999);
			}
			GridUtil.Erode (layer, 999, FLOOR_MEADOW);
			GridUtil.Erode (layer, 999, FLOOR_MEADOW);
			GridUtil.Erode (layer, 999, WALL);
			GridUtil.Replace (layer, 999, FLOOR_MEADOW);

			Rect bounds = null;
			List<Pos> walls = new List<Pos> ();
			foreach (RectZone z in cloisterBlocks) {
				for (int x = z.rect.xmin; x <= z.rect.xmax; x++) {
					for (int y = z.rect.ymin; y <= z.rect.ymax; y++) {
						if (layer [x, y] == WALL) {
							layersRoof [x, y] = ROOF;
							if ((layer [x, y + 1] == FLOOR_MEADOW && layer [x, y - 1] == FLOOR_MEADOW)
							    || (layer [x + 1, y] == FLOOR_MEADOW && layer [x - 1, y] == FLOOR_MEADOW))
								walls.Add (new Pos (x, y));
						}
					}
				}
				if (bounds == null) {
					bounds = new Rect (z.rect);
				} else {
					bounds.Add (z.rect);
				}
			}
			bounds.Shrink (3);
			ShrinkCloisterBounds(bounds, layer, center, townRadius);

			List<Pos> doors = RandUtils.Draw (walls, 2);
			foreach (Pos p in doors) {
				layer[p.x, p.y] = DOOR_LOCKED;
			}

			UnityEngine.Debug.Log( bounds.width+" x "+bounds.height );

			Templates cloisters = new Templates (Terra.TileInfoIndexMap, "Cloisters.ascii");
			OrderedDict<string, Template> candidates = FilterTemplates (cloisters, bounds);
			if (candidates.Count > 0) {
				Template[] cands = new Template[candidates.Count];
				candidates.Values.CopyTo (cands, 0);
				Template template = RandUtils.Select (
								cands.OrderByDescending ((t) => t.Width (0) * t.Height (0))
									 .Take (cands.Length / 4 + 1).ToList ());
				InsertTemplate (layers, template, bounds, 0.5f, 0.5f);
			}
		}

		void ShrinkCloisterBounds(Rect bounds, int[,] layer, Pos center, int townRadius) {
			bool horizontal = Mathf.Abs (bounds.yCenter - center.y) < townRadius / 2;
			bool left = horizontal ? bounds.xCenter < center.x : bounds.yCenter < center.y;
			for (int x = bounds.xmin; x <= bounds.xmax; x++) {
				for (int y = bounds.ymin; y <= bounds.ymax; y++) {
					if (layer [x, y] == WALL) {
						if (horizontal) {
							if (left) {
								if (x >= bounds.xmin)
									bounds.xmin = x + 1;
							} else {
								if (x <= bounds.xmax)
									bounds.xmax = x - 1;
							}
						} else {
							if (left) {
								if (y >= bounds.ymin)
									bounds.ymin = y + 1;
							} else {
								if (y <= bounds.ymax)
									bounds.ymax = y - 1;
							}
						}
					}
				}
			}
		}

		void CreateWindows (Dictionary<int, int[,]> layers, List<Building> buildings) {
			foreach (Building b in buildings) {
				if (b.numStoreys < 2) {
					continue;
				}
				for (int s = 1; s < b.numStoreys; s++) {
					if (b.north == null || b.north.numStoreys <= s) {
						if (RandUtils.NextBool (prefs.windowProb)) {
							PlaceWindow(layers[s], b, TOP);
						}
					}
					if (b.south == null || b.south.numStoreys <= s) {
						if (RandUtils.NextBool (prefs.windowProb)) {
							PlaceWindow(layers[s], b, BOTTOM);
						}
					}
					if (b.east == null || b.east.numStoreys <= s) {
						if (RandUtils.NextBool (prefs.windowProb)) {
							PlaceWindow(layers[s], b, RIGHT);
						}
					}
					if (b.west == null || b.west.numStoreys <= s) {
						if (RandUtils.NextBool (prefs.windowProb)) {
							PlaceWindow(layers[s], b, LEFT);
						}
					}
				}
			}
		}

		Pos PlaceWindow (int[,] map, Building building, int side) {
			int length = (side < 2) ? building.rect.height : building.rect.width;
			int cnt = 0;
			while ( cnt < 100 ) {
				int v = RandUtils.NextInt (1, length);
				int x = (side == 0) ? building.rect.xmin : (side == 1) ? building.rect.xmax : building.rect.xmin + v;
				int y = (side == 3) ? building.rect.ymin : (side == 2) ? building.rect.ymax : building.rect.ymin + v;
				//Debug.Log(x+" "+y);
				if (map [x, y] == WALL) {
					map[x,y] = WINDOW_CLOSED;
					return new Pos(x,y);
				}
				cnt++;
			}
			return null;
		}

		void CreateUpperStoreys (Dictionary<int, int[,]> layers, List<Building> buildings, int townRadius) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy); 
			foreach (Building b in buildings) {
				int numStoreys = 1;
				float relCenterDist = center.Distance (b.rect.xCenter, b.rect.yCenter) / (float)townRadius;
				float oneUpper = 1 - relCenterDist;
				if (RandUtils.NextBool (oneUpper)) {
					float twoUpper = 1 - (relCenterDist * 1.5f);
					numStoreys = RandUtils.NextBool (twoUpper) ? 3 : 2;
				}
				b.numStoreys = numStoreys;
				for (int i = 1; i <= numStoreys; i++) {
					if (!layers.ContainsKey (i)) {
						layers[i] = new int[prefs.width, prefs.height];
					}
					if(i < numStoreys) {
						PlaceBuilding(layers[i], b.rect, FLOOR_WOOD, WALL);
					} else {
						GridUtil.FillRectWhere(layers[i], b.rect, ROOF, VOID);
					}
				}
			}

			foreach (Building b in buildings) {
				for (int i = 0; i < b.numStoreys-1; i++) {
					int[,] bottom = layers[i];
					int[,] top = layers[i+1];
					Pos stairs = new Pos(0,0);
					int cnt = 0;
					while(cnt < 250) {
						stairs.x = RandUtils.NextInt(b.rect.xmin+1, b.rect.xmax);
						stairs.y = RandUtils.NextInt(b.rect.ymin+1, b.rect.ymax);
						int vb = bottom[stairs.x, stairs.y];
						int vt = top[stairs.x, stairs.y];
						if ( vb != WALL && vt != WALL && vb != STAIRS_UP && vt != STAIRS_UP && vb != STAIRS_DOWN && vt != STAIRS_DOWN ) {
							bottom[stairs.x, stairs.y] = STAIRS_UP;
							top[stairs.x, stairs.y] = STAIRS_DOWN;
							break;
						}
						cnt++;
					}
				}
			}
		}

		void CreateChurches (Dictionary<int, int[,]> layers, RectZone[] churchZones) {
			Templates churches = new Templates (Terra.TileInfoIndexMap, "Churches.ascii");
			foreach (RectZone church in churchZones) {
				OrderedDict<string, Template> mainCurchCand = FilterTemplates (churches, church.rect);
				if (mainCurchCand.Count > 0) {
					Template[] cands = new Template[mainCurchCand.Count];
					mainCurchCand.Values.CopyTo (cands, 0);
					Template template = RandUtils.Select (
									cands.OrderByDescending ((t) => t.Width (0) * t.Height (0))
										 .Take (cands.Length / 4 + 1).ToList ());
					InsertTemplate (layers, template, church.rect, 0, 0.5f);
				}
			}
		}

		void CreateBasements (int[,] map, int[,] basementMap, List<Building> buildings) {
			foreach (Building b in buildings) {
				if (RandUtils.NextBool (prefs.basementProb)) {
					b.hasBasement = true;
					GridUtil.FillRect( basementMap, b.rect, FLOOR_STONE, -1 );
					Pos stairs = new Pos(0,0);
					int cnt = 0;
					while(cnt < 250) {
						stairs.x = RandUtils.NextInt(b.rect.xmin+1, b.rect.xmax);
						stairs.y = RandUtils.NextInt(b.rect.ymin+1, b.rect.ymax);
						if ( map[stairs.x, stairs.y] != WALL && basementMap[stairs.x, stairs.y] != WALL ) {
							map[stairs.x, stairs.y] = STAIRS_DOWN;
							basementMap[stairs.x, stairs.y] = STAIRS_UP;
							break;
						}
						cnt++;
					}
				}
			}
		}


		OrderedDict<string, Template> FilterTemplates (Templates templates, Rect size) {
			OrderedDict<string, Template> candidates = templates.All.Filter( (template) => {
				return template.Layers.ContainsKey( 0 ) 
				&& template.Width(0) <= size.width 
				&& template.Height(0) <= size.height;
			 } );
			return candidates;
		}

		RectZone SelectCloister (RectZone root) {
			bool left = RandUtils.NextBool (0.5f);
			bool top = RandUtils.NextBool (0.5f);

			RectZone quadrant = (left ? root.left : root.right);
			quadrant = (top ? quadrant.left : quadrant.right);
			RectZone current = quadrant;

			bool hori = RandUtils.NextBool (0.5f);
			if (hori) {
				top = !top;
			} else {
				left = !left;
			}
			while ( true ) {
				RectZone child = current.horizontal 
					? (left ? current.left : current.right) 
					: (top ? current.left : current.right);
				if (child == null) {
					return current;
				}
				current = child;
 			}
		}

		RectZone SelectMainSquare (RectZone root, bool left, bool top) {
			RectZone quadrant = (left ? root.left : root.right);
			quadrant = (top ? quadrant.left : quadrant.right);
			RectZone current = quadrant;
			while ( true ) {
				RectZone child = current.horizontal 
					? (left ? current.right : current.left) 
					: (top ? current.right : current.left);
				if (child == null) {
					return current;
				}
				current = child;
 			}
		}

		RectZone SelectSecondarySquare (List<RectZone> zones, float minCenterDist) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy); 

			RectZone square = null;
			while ( true ) {
				square = RandUtils.Select (zones);
				if (center.Distance (square.rect.xCenter, square.rect.yCenter) >= minCenterDist) {
					if(square.parent != null 
							&& square.parent.horizontal 
							&& square.parent.left == square 
							&& square.parent.right != null 
							&& square.parent.right.left == null 
							&& square.parent.right.right == null) {
						return square;
					}
				}
			}
		}
		RectZone SelectPark (List<RectZone> zones, float minCenterDist) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy); 

			RectZone square = null;
			while ( true ) {
				square = RandUtils.Select (zones);
				if (center.Distance (square.rect.xCenter, square.rect.yCenter) >= minCenterDist) {
					return square;
				}
			}
		}

		List<Pos> PlaceDoors (int[,] map, List<Building> buildings) {
			List<Pos> doors = new List<Pos> ();
			foreach (Building b in buildings) {
				if (b.primaryEntrance < 0) {
					b.primaryEntrance = RandUtils.NextInt (4);
				}
				int s = b.primaryEntrance;
				//int length = (s < 2) ? b.rect.height : b.rect.width;

				if (s < 2) {
					int x = (s == 0) ? b.rect.xmin : b.rect.xmax;
					Pos pos = new Pos (x, RandUtils.RoundRandom (0.5f * (b.rect.ymin + b.rect.ymax)));
					b.doors.Add(pos);
					doors.Add (pos);
				} else {
					int y = (s == 2) ? b.rect.ymax : b.rect.ymin;
					Pos pos = new Pos (RandUtils.RoundRandom (0.5f * (b.rect.xmin + b.rect.xmax)), y);
					b.doors.Add(pos);
					doors.Add (pos);
				}
				if (b.secondaryEntrance >= 0) {
					s = b.secondaryEntrance;
					//length = (s < 2) ? b.rect.height : b.rect.width;
					if (s < 2) {
						int x = (s == 0) ? b.rect.xmin : b.rect.xmax;
						int y = RandUtils.RoundRandom (0.5f * (b.rect.ymin + b.rect.ymax));
						if (map [x, y] == WALL) {
							Pos pos = new Pos (x, y);
							b.doors.Add(pos);
							doors.Add (pos);
						}
					} else {
						int y = (s == 2) ? b.rect.ymax : b.rect.ymin;
						int x = RandUtils.RoundRandom (0.5f * (b.rect.xmin + b.rect.xmax));
						if (map [x, y] == WALL) {
							Pos pos = new Pos (x, y);
							b.doors.Add(pos);
							doors.Add (pos);
						}
					}
				}
			}
			//foreach(Pos door in doors) {
			//	map[door.x, door.y] = DOOR_LOCKED;
			//}
			PlaceDoors(map, doors);
			return doors;
		}

		void PlaceDoors (int[,] map, List<Pos> doors) {
			float[] cumProb = new float[prefs.doorProbabilities.Length];
			cumProb [0] = prefs.doorProbabilities [0];
			for (int i = 1; i < cumProb.Length; i++) {
				cumProb [i] = cumProb [i - 1] + prefs.doorProbabilities [i];
			}
			float sumProb = cumProb [cumProb.Length - 1];

			foreach (Pos door in doors) {
				float rand = RandUtils.NextFloat ();
				if (rand < sumProb) {
					if (rand < cumProb [0]) {
						map [door.x, door.y] = DOOR_OPEN;
					} else if (rand < cumProb [1]) {
						map [door.x, door.y] = DOOR_CLOSED;
					} else if (rand < cumProb [2]) {
						map [door.x, door.y] = DOOR_LOCKED;
					} else if (rand < cumProb [3]) {
						map [door.x, door.y] = DOOR_HIDDEN;
					} 
				}
			}
		}


		List<Building> PlaceBuildingsBlock (int[,] map, RectZone zone, int minWidth, int maxWidth, int maxDepth, int floor, int wall) {
			List<int>[] points = new List<int>[4];
			Rect rect = zone.rect;
			//Debug.Log( rect.width+" "+rect.height );
			for (int s = 0; s < points.Length; s++) {
				points [s] = new List<int> ();
				int length = (s < 2) ? zone.rect.height : zone.rect.width;
				int sum = 0;
				points [s].Add (0);

				bool success = false;
				int cnt = 0;
				while ( cnt < 250 ) {
					if (sum == length) {
						success = true;
						break;
					} else if (sum + maxWidth - 1 < length) {
						int w = RandUtils.NextInt (minWidth, maxWidth + 1);
						sum += w - 1;
						points [s].Add (sum);
					} else if (sum + minWidth - 1 <= length) {
						int w = length - sum;
						sum += w;
						points [s].Add (sum);
					} else {
						sum = 0;
						points [s].Clear ();
						points [s].Add (0);
					}
					cnt++;
				}
				if (success) {
					/*
					System.Text.StringBuilder b = new System.Text.StringBuilder ();
					foreach (int v in points[s]) {
						b.Append (v.ToString ());
						b.Append (" ");
					}
					*/
					//Debug.Log (length);
					//Debug.Log (b);
				} else {
					UnityEngine.Debug.Log ("Failed " + length);
				}
			}

			List<Building> buildings = new List<Building> ();
			List<Building>[] buildingsSides = new List<Building> [points.Length];
			for (int s = 0; s < points.Length; s++) {
				buildingsSides [s] = new List<Building> ();

				int maxDepthLocal = (int)((s < 2) ? rect.width / 2 - 1 : rect.height / 2 - 1);
				if (maxDepth < maxDepthLocal) {
					maxDepthLocal = maxDepth;
				}
				if (maxDepthLocal < minWidth) {
					maxDepthLocal = minWidth;
				}
				List<int> pts = points [s];
				for (int i = 0; i < pts.Count - 1; i++) {
					//if (RandUtils.NextBool (prefs.blockGapProb)) {
					//	continue;
					//}
					int dim2 = 0;
					if ((s % 2 == 0 && i == pts.Count - 2) || (s % 2 != 0 && i == 0)) {
						continue;
					}
					if (i == 0) {
						switch (s) {
						case LEFT:
							dim2 = points [3] [1];
							break;
						case RIGHT:
							dim2 = rect.width - points [3] [points [3].Count - 2];
							break;
						case TOP:
							dim2 = rect.height - points [0] [points [0].Count - 2];
							break;
						case BOTTOM:
							dim2 = points [0] [1];
							break;
						default: 
							break;
						}
						//Debug.Log("start "+dim2);
					} else if (i == pts.Count - 2) {
						switch (s) {
						case LEFT:
							dim2 = points [2] [1];
							break;
						case RIGHT:
							dim2 = rect.width - points [2] [points [2].Count - 2];
							break;
						case TOP:
							dim2 = rect.height - points [1] [points [1].Count - 2];
							break;
						case BOTTOM:
							dim2 = points [1] [1];
							break;
						default: 
							break;
						}
						//Debug.Log("end "+dim2);
					} else {
						dim2 = RandUtils.NextInt (minWidth, maxDepthLocal + 1) - 1;
					}
					Rect building = null;
					if (s < 2) {
						int x = (s == 0) ? rect.xmin : rect.xmax - dim2;
						building = new Rect (x, rect.ymin + pts [i], x + dim2, rect.ymin + pts [i + 1]);
					} else {
						int y = (s == 2) ? rect.ymax - dim2 : rect.ymin;
						building = new Rect (rect.xmin + pts [i], y, rect.xmin + pts [i + 1], y + dim2);
					}

					int backDoor = (i == 0 || i == pts.Count - 2 || !RandUtils.NextBool (prefs.backDoorProb)) ? -1 : Opposite [s];
					Building b = new Building (building, s, backDoor);
					buildingsSides [s].Add (b);
				}
				buildings.AddRange (buildingsSides [s]);
			}
			for (int s = 0; s < points.Length; s++) {
				List<Building> side = buildingsSides [s];
				for (int i = 0; i < side.Count - 1; i++) {
					if (s < 2) {
						side [i].north = side [i + 1];
						side [i + 1].south = side [i];
					} else {
						side [i].east = side [i + 1];
						side [i + 1].west = side [i];
					}
				}
			}
			// SE
			buildingsSides [LEFT] [0].east = buildingsSides [BOTTOM] [0];
			buildingsSides [BOTTOM] [0].west = buildingsSides [LEFT] [0];
			// NE
			buildingsSides [LEFT].Last ().north = buildingsSides [TOP] [0];
			buildingsSides [TOP] [0].south = buildingsSides [LEFT].Last ();
			// SW
			buildingsSides [BOTTOM].Last ().north = buildingsSides [RIGHT] [0];
			buildingsSides [RIGHT] [0].south = buildingsSides [BOTTOM].Last ();
			// NW
			buildingsSides [TOP].Last ().east = buildingsSides [RIGHT].Last ();
			buildingsSides [RIGHT].Last ().west = buildingsSides [TOP].Last ();

			List<Building> final = new List<Building> ();
			foreach (Building b in buildings) {
				if (RandUtils.NextBool (prefs.blockGapProb)) {
					if(b.east != null) b.east.west = null;
					if(b.west != null) b.west.east = null;
					if(b.north != null) b.north.south = null;
					if(b.south != null) b.south.north = null;
				} else {
					PlaceBuilding (map, b.rect, floor, wall);
					final.Add(b);
				}
			}
			return final;
		}

		void PlaceBuilding (int[,] map, Rect rect, int floor, int wall) {

			for (int x = rect.xmin; x <= rect.xmax; x++) {
				for (int y = rect.ymin; y <= rect.ymax; y++) {
					map[x,y] = wall;
				}
			}
			for (int x = rect.xmin+1; x < rect.xmax; x++) {
				for (int y = rect.ymin+1; y < rect.ymax; y++) {
					map[x,y] = floor;
				}
			}
		}

		void FillRoads (int[,] map, List<RectZone> zones) {
			foreach (RectZone zone in zones) {
				Rect r = zone.rect;
				Borders b = zone.borders;
				for (int x = r.xmin - b.left; x <= r.xmax + b.right; x++) {
					for (int y = r.ymin - b.bottom; y <= r.ymax + b.top; y++) {
						if (! r.Contains (x, y)) {
							map[x,y] = FLOOR_PATH;
						}
					}
				}
			}
		}

		List<RectZone> FilterZonesRadius (List<RectZone> zones, int walldistance, ref List<RectZone> removed) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy); 
			int radSq = (prefs.townRadius - walldistance) * (prefs.townRadius - walldistance);

			List<RectZone> newZones = new List<RectZone> ();
			foreach (RectZone zone in zones) {
				if (center.DistanceSq (zone.rect.xmin, zone.rect.ymin) > radSq
				    || center.DistanceSq (zone.rect.xmin, zone.rect.ymax) > radSq
				    || center.DistanceSq (zone.rect.xmax, zone.rect.ymin) > radSq
				    || center.DistanceSq (zone.rect.xmax, zone.rect.ymax) > radSq) {
					removed.Add(zone);
				} else {
					newZones.Add (zone);
				}
			}
			return newZones;
		}

		List<RectZone> FilterZonesBlocked (int[,] map, List<RectZone> zones, int blocking, ref List<RectZone> removed) {

			List<RectZone> newZones = new List<RectZone> ();
			foreach (RectZone zone in zones) {
				bool blocked  = false;
				for (int x = zone.rect.xmin; x <= zone.rect.xmax; x++) {
					for (int y = zone.rect.ymin; y <= zone.rect.ymax; y++) {
						if (map [x, y] == blocking) {
							blocked = true;
							break;
						}
					}
					if(blocked) break;
				}
				if (blocked) {
					removed.Add(zone);
				} else {
					newZones.Add (zone);
				}
			}
			return newZones;
		}

		Pos NearestRoad (int[,] map, Pos start, int baseId, int roadId, out int distance) {
			int minDist = int.MaxValue;
			Pos result = null;
			foreach (Pos n in Neighbors) {
				Pos pos = new Pos (start.x + n.x, start.y + n.y);
				int dist = 1;
				while ( true ) {
					int v = map [pos.x, pos.y];
					if (v == roadId) {
						if (dist == 1) {
							dist = -1;
						}
						break;
					} else if (v != baseId) {
						dist = -1;
						break;
					}
					pos.x += n.x;
					pos.y += n.y;
					dist++;
				}
				if (dist > 0 && dist < minDist) {
					minDist = dist;
					result = pos;
				}
			}
			distance = (result == null || minDist == int.MaxValue) ? -1 : minDist;
			return result;
		}

		List<RectZone> CreateRoadsSubdivide (int[,] map, List<RectZone> zones, int maxDepth, int minBlockWidth, int minBlockHeight, int overrideRoadWidth = -1, bool horizontal = true) {
			List<RectZone> newZones = new List<RectZone> ();

			float fac = 1.3f;
			foreach (RectZone zone in zones) {
				bool wOk = zone.rect.width >= 2 * minBlockWidth;
				bool hOk = zone.rect.height >= 2 * minBlockHeight;
				if (wOk && hOk) {
					horizontal = (zone.rect.width > fac * zone.rect.height) ? true : (zone.rect.height > fac * zone.rect.width) ? false : RandUtils.NextBool (0.5f);
				} else {
					if (wOk) {
						horizontal = true;
					} else if (hOk) {
						horizontal = false;
					} else {
						newZones.Add(zone);
						continue;
					}
				}
				/*
				if (depth == 1) {
					horizontal = RandUtils.NextBool(0.5f);
				}
				*/
				int w = overrideRoadWidth >= 0 ? overrideRoadWidth : prefs.depthRoadWidth[ zone.depth ];
				int rad = w / 2;
				RectZone left = null;
				RectZone right = null;
				if (horizontal) {
					int xmin = Mathf.FloorToInt (zone.rect.xmin + zone.rect.width * 0.4f);
					int xmax = Mathf.FloorToInt (zone.rect.xmin + zone.rect.width * 0.6f);
					int x = (xmin == xmax) ? xmin : RandUtils.NextInt (xmin, xmax + 1);
					left = new RectZone(
						new Rect (zone.rect.xmin, zone.rect.ymin, x - rad - 1, zone.rect.ymax),
						new Borders(zone.borders.left, w, zone.borders.top, zone.borders.bottom),
						zone, zone.depth+1 );
					right = new RectZone(
						new Rect (x - rad + w, zone.rect.ymin, zone.rect.xmax, zone.rect.ymax),
						new Borders(w, zone.borders.right, zone.borders.top, zone.borders.bottom),
						zone, zone.depth+1 );
					zone.horizontal = true;
				} else {
					int ymin = Mathf.CeilToInt (zone.rect.ymin + zone.rect.height * 0.4f);
					int ymax = Mathf.FloorToInt (zone.rect.ymin + zone.rect.height * 0.6f);
					int y = RandUtils.NextInt (ymin, ymax + 1);
					right = new RectZone(
						new Rect (zone.rect.xmin, zone.rect.ymin, zone.rect.xmax, y - rad - 1),
						new Borders(zone.borders.left, zone.borders.right, w, zone.borders.bottom),
						zone, zone.depth+1 );
					left = new RectZone(
						new Rect (zone.rect.xmin, y - rad + w, zone.rect.xmax, zone.rect.ymax),
						new Borders(zone.borders.left, zone.borders.right, zone.borders.top, w),
						zone, zone.depth+1 );
					zone.horizontal = false;
				}
				zone.left = left;
				zone.right = right;
				//FillRect(map, road, FLOOR_PATH);
				if (zone.depth < maxDepth) {
					List<RectZone> temp = new List<RectZone> ();
					temp.Add (left);
					temp.Add (right);
					//Debug.Log("DEPTH: "+zone.depth);
					newZones.AddRange ( CreateRoadsSubdivide (map, temp, maxDepth, minBlockWidth, minBlockHeight, overrideRoadWidth, !horizontal) );
				} else {
					newZones.Add (left);
					newZones.Add (right);
				}
			}
			return newZones;
		}

		List<RectZone> CreateMainRoads(int[,] map, RectZone root) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			int w = prefs.RoadWidth(0);
			int rad = w / 2;

			int xmin = cx - (prefs.townRadius - 1 - 5);
			int xmax = cx + (prefs.townRadius - 1 - 5);
			int ymin = cy - (prefs.townRadius - 1 - 5);
			int ymax = cy + (prefs.townRadius - 1 - 5);

			RectZone left = new RectZone(new Rect(xmin, ymin, cy-rad-1, ymax), new Borders(0,0,0,0), root, 0);
			RectZone right = new RectZone(new Rect(xmin, ymin, cy-rad-1, ymax), new Borders(0,0,0,0), root, 0);
			root.left = left;
			root.right = right;
			root.horizontal = true;
			left.horizontal = false;
			right.horizontal = false;

			int w0 = prefs.RoadWidth(0);
			int w1 = prefs.RoadWidth(1);
			List<RectZone> zones = new List<RectZone>();
			RectZone bottomLeft = new RectZone(
				new Rect(xmin, ymin, cy-rad-1, cx-rad-1),
				new Borders(w1, w0, w0, w1),
				left, 1 );
			RectZone topLeft = new RectZone(
				new Rect(xmin, cy-rad+w, cx-rad-1, ymax),
				new Borders(w1, w0, w1, w0),
				left, 1 );
			RectZone bottomRight = new RectZone(
				new Rect(cx-rad+w, ymin, xmax, cy-rad-1),
				new Borders(w0, w1, w0, w1),
				right, 1 );
			RectZone topRight = new RectZone(
				new Rect(cx-rad+w, cy-rad+w, xmax, ymax),
				new Borders(w0, w1, w1, w0),
				right, 1 );

			left.left = topLeft;
			left.right = bottomLeft;
			right.left = topRight;
			right.right = bottomRight;

			zones.Add( bottomLeft );
			zones.Add( topLeft );
			zones.Add( bottomRight );
			zones.Add( topRight );



			return zones;
		}

		void DrawMainRoads(int[,] map) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			int w = prefs.RoadWidth(0);
			int rad = w / 2;

			for (int x = 0; x < prefs.width; x++) {
				for (int y = cy-rad; y < cy-rad+w; y++) {
					map[x,y] = FLOOR_PATH;
				}
			}
			for (int x = cx-rad; x < cx-rad+w; x++) {
				for (int y = 0; y < prefs.height; y++) {
					map[x,y] = FLOOR_PATH;
				}
			}
		}
		/*
		void CreateTownWall (int[,] map) {
			int cx = prefs.width / 2;
			int cy = prefs.height / 2;
			Pos center = new Pos (cx, cy);

			float dMinSq = prefs.townRadius * prefs.townRadius;
			float dMaxSq = (prefs.townRadius + prefs.wallWidth) * (prefs.townRadius + prefs.wallWidth);
			for (int x = 0; x < prefs.width; x++) {
				for (int y = 0; y < prefs.height; y++) {
					float distSq = center.DistanceSq (x, y);
					if (distSq >= dMinSq && distSq <= dMaxSq) {
						map[x,y] = WALL;
					}
				}
			}
		}
		*/

		void InsertTemplate (Dictionary<int, int[,]> layers, Template template, Rect bounds, 
		                     float horizontalAlignment, float verticalAlignment) {
			int xmin = bounds.xmin;
			int xmax = bounds.xmax - template.Width (0);
			int ymin = bounds.ymin;
			int ymax = bounds.ymax - template.Height (0);

			int xll = xmin + Mathf.RoundToInt ((xmax - xmin) * horizontalAlignment);
			int yll = ymin + Mathf.RoundToInt ((ymax - ymin) * verticalAlignment);
			foreach (KeyValuePair<int, int[,]> kv in template.Layers) {
				if (!layers.ContainsKey (kv.Key)) {
					layers[kv.Key] = new int[prefs.width, prefs.height];
				}
				GridUtil.InsertTemplate (layers[kv.Key], template.Layers [kv.Key], xll, yll, Terra.TileInfoId (Terra.Void));
			}
		}

	}

	public class Rect {
		public int xmin;
		public int xmax;
		public int ymin;
		public int ymax;

		public Rect ( int xmin, int ymin, int xmax, int ymax ) {
			this.xmin = xmin;
			this.ymin = ymin;
			this.xmax = xmax;
			this.ymax = ymax;
		}
		public Rect ( Rect other ) {
			this.xmin = other.xmin;
			this.ymin = other.ymin;
			this.xmax = other.xmax;
			this.ymax = other.ymax;
		}
		public int width {
			get { return xmax - xmin; }
		}
		public int height {
			get { return ymax - ymin; }
		}
		public float xCenter {
			get { return (xmax + xmin) / 2f; }
		}
		public float yCenter {
			get { return (ymax + ymin) / 2f; }
		}
		public bool Contains (int x, int y) {
			return x >= xmin && x <= xmax && y >= ymin && y <= ymax;
		}
		public void Add (Rect other) {
			if(other.xmin < xmin) xmin = other.xmin;
			if(other.xmax > xmax) xmax = other.xmax;
			if(other.ymin < ymin) ymin = other.ymin;
			if(other.ymax > ymax) ymax = other.ymax;
		}
		public void Shrink (int value) {
			xmin += value;
			xmax -= value;
			ymin += value;
			ymax -= value;
		}
		public override string ToString () {
			return string.Format ("[Rect: xmin={0}, ymin={1}, xmax={2}, ymax={3}]", xmin, ymin, xmax, ymax);
		}
	}

	public class Borders {
		public int left;
		public int right;
		public int top;
		public int bottom;

		public Borders ( int left, int right, int top, int bottom ) {
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
		}
	}

	public class RectZone {
		public Rect rect;
		public Borders borders;
		public RectZone parent;

		public bool horizontal;
		public RectZone left;
		public RectZone right;

		public int depth;


		public RectZone ( Rect rect, Borders borders, RectZone parent, int depth ) {
			this.rect = rect;
			this.borders = borders;
			this.parent = parent;
			this.depth = depth;
		}
		public void DeepRemoveFromParent () {
			bool rem = false;
			if (parent.left == this) {
				parent.left = null;
				rem = true;
			} else if (parent.right == this) {
				parent.right = null;
				rem = true;
			}
			if (rem) {
				if (parent.left == null && parent.right == null) {
					parent.DeepRemoveFromParent();
				}
				parent = null;
			}
		}


	}
	public class Building {
		public Rect rect;
		public int primaryEntrance;
		public int secondaryEntrance;
		public List<Pos> doors = new List<Pos>();
		public bool isShop;
		public bool hasBasement;
		public int numStoreys = 1;

		public Building north;
		public Building south;
		public Building east;
		public Building west;


		public Building ( Rect rect, int primaryEntrance, int secondaryEntrance ) {
			this.rect = rect;
			this.primaryEntrance = primaryEntrance;
			this.secondaryEntrance = secondaryEntrance;
			this.isShop = false;
		}
	}

	public class TownGeneratorSettings {
		public readonly int width;
		public readonly int height;
		public readonly int townRadius;
		public readonly int wallWidth;
		public readonly int minLargeBlockWidth;
		public readonly int minLargeBlockHeight;
		public readonly int minNormalBlockSize;
		public readonly int[] depthRoadWidth;
		public readonly float blockGapProb;
		public readonly float backDoorProb;
		public readonly float windowProb;
		public readonly float basementProb;
		public readonly float[] doorProbabilities;

		public TownGeneratorSettings (
				int width, int height,
				int townRadius, int wallWidth,
				int minLargeBlockWidth,
				int minLargeBlockHeight,
				int minNormalBlockSize,
				int[] depthRoadWidth,
				float blockGapProb,
				float backDoorProb,
				float windowProb,
				float basementProb,
				float[] doorProbabilities) {
			this.width = width;
			this.height = height;
			this.townRadius = townRadius;
			this.wallWidth = wallWidth;
			this.minLargeBlockWidth = minLargeBlockWidth;
			this.minLargeBlockHeight = minLargeBlockHeight;
			this.minNormalBlockSize = minNormalBlockSize;
			this.depthRoadWidth = depthRoadWidth;
			this.blockGapProb = blockGapProb;
			this.backDoorProb = backDoorProb;
			this.windowProb = windowProb;
			this.basementProb = basementProb;
			this.doorProbabilities = doorProbabilities;
		}

		public int RoadWidth (int depth) {
			if (depth >= depthRoadWidth.Length) {
				return depthRoadWidth[depthRoadWidth.Length-1];
			}
			return depthRoadWidth[depth];
		}
	}

}
