﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen {
	public interface MazeGenerator {

		void FillMaze(int[,] template, int x, int y, float straightProb);
		
	}
}
