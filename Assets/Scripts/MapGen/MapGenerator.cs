﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using ComponentSystem;
using Map.LayeredGrid;
using Map.LayeredGrid.Impl;
using Game.Data;
using Game.Factories;

using UnityEngine;

namespace MapGen {
	public class MapGenerator : MapProgressListener {

		Func<GridLocation, bool, bool, Entity>[] traderGenerators = new Func<GridLocation, bool, bool, Entity>[] {
			TraderFactory.CreateGeneralTrader,
			TraderFactory.CreateGeneralTrader,
			TraderFactory.CreateGeneralTrader,
			TraderFactory.CreateGeneralTrader,

			TraderFactory.CreateProvisionTrader,
			TraderFactory.CreateProvisionTrader,
			TraderFactory.CreateProvisionTrader,
			TraderFactory.CreateProvisionTrader,
			TraderFactory.CreateProvisionTrader,

			TraderFactory.CreateToolTrader,
			TraderFactory.CreateToolTrader,
			TraderFactory.CreateToolTrader,

			TraderFactory.CreateWeaponTrader,
			TraderFactory.CreateWeaponTrader,
			TraderFactory.CreateWeaponTrader,

			TraderFactory.CreateArmorTrader,
			TraderFactory.CreateArmorTrader,
			TraderFactory.CreateArmorTrader,

			TraderFactory.CreateHerbTrader,

			TraderFactory.CreateBookTrader,

			TraderFactory.CreateAnatomist,
		};

		int mapSize = 400;
		int townRadius = 130;
		int maxShopCenterDistance = 70;

		MapProgressListener progressListener;
		
		public MapGenerator (MapProgressListener progressListener) {
			this.progressListener = progressListener;
		}

		void FireProgress (int progress, string message) {
			if (progressListener != null) {
				progressListener.NotifyProgress(progress, message);
			}
		}
		public void NotifyProgress(int progress, string message) {
			FireProgress(progress, message);
		}
		public void NotifyCompleted(string message) { }

		public LayeredGrid<TileInfo> Create (out List<Entity> entities) {
			int width = mapSize;
			int height = mapSize;


			//int empty = Terra.TileInfoId (Terra.Void);
			//int wall = Terra.TileInfoId (Terra.Wall);
			//int floor = Terra.TileInfoId (Terra.FloorStone);

			entities = new List<Entity> ();

			Debug.Log ("Generating town");
			TileInfo noData = new TileInfo (Terra.TileInfoId (Terra.Void));
			List<Building> buildings;
			Dictionary<int, int[,]> town = new MapGen.TownGenerator (
				                               new MapGen.TownGeneratorSettings (
					                               width, height, 
					                               townRadius, 5, 32, 20, 16,
					                               new int[]{ 5, 3, 3, 2, 2, 2, 2, 1, 1, 1, 1, -1 },
					                               0.05f, 0.4f, 1.0f, 
					                               0.3f,
					                               new float[]{ 0.0f, 0.05f, 0.95f, 0.0f }),
				                               this)
				.Create (out buildings);



			entities.AddRange (CreateShops (town [0], buildings));

			BasicLayeredGrid<TileInfo> map = new BasicLayeredGrid<TileInfo> (noData);
			foreach (KeyValuePair<int, int[,]> kv in town) {
				if (kv.Key >= 0) {
					map.AddLayer (new SimpleGrid<TileInfo> (0, 0, MapGen.GridUtil.ToTileMap (kv.Value), noData, true), kv.Key);
				}
			}

			Debug.Log ("Generating dungeon");
			int baseXll = width / 2 - townRadius;
			int baseYll = height / 2 - townRadius;
			int baseWidth = 2 * townRadius;

			List<Rect> rooms1;
			int[,] dungeon1 = GenerateDungeon (
				                  GridUtil.Crop (town [-1], baseXll, baseYll, baseWidth, baseWidth),
				                  null,
				                  baseXll, baseYll, baseWidth, baseWidth,
				                  10000, 3, 11,
				                  0.1f,
				                  out rooms1);
			List<Pos> stairs = GenerateStairs (dungeon1, rooms1, 25);
			map.AddLayer (new SimpleGrid<TileInfo> (baseXll, baseYll, MapGen.GridUtil.ToTileMap (dungeon1), noData, true), -1);

			List<Rect> rooms2;
			int[,] dungeon2 = GenerateDungeon (
				                  null,
				                  stairs,
				                  baseXll, baseYll, baseWidth, baseWidth,
				                  10000, 3, 17,
				                  0.06f,
				                  out rooms2);

			int base2Width = (int)(townRadius * 0.8f);
			int base2Xll = RandUtils.NextInt (baseXll, baseXll + baseWidth - base2Width);
			int base2Yll = RandUtils.NextInt (baseYll, baseYll + baseWidth - base2Width);

			rooms2 = rooms2.Where ((r) => r.xmin > base2Xll-baseXll && r.ymin > base2Yll-baseYll && r.xmax < base2Xll-baseXll + base2Width && r.ymax < base2Yll-baseYll + base2Width).ToList ();
			List<Pos> stairs2 = GenerateStairs (dungeon2, rooms2, 5);
			map.AddLayer (new SimpleGrid<TileInfo> (baseXll, baseYll, MapGen.GridUtil.ToTileMap (dungeon2), noData, true), -2);

			foreach (Pos s in stairs2) {
				s.x -= base2Xll - baseXll;
				s.y -= base2Yll - baseYll;
				Debug.Log(s.x+" "+s.y);
			}
			Debug.Log(rooms2.Count+" "+stairs2.Count);

			List<Rect> rooms3;
			int[,] dungeon3 = GenerateDungeon (
				                  null,
				                  stairs2,
				                  base2Xll, base2Yll, base2Width, base2Width,
				                  10000, 3, 11,
				                  0.06f,
				                  out rooms3);
			List<Pos> stairs3 = GenerateStairs (dungeon3, rooms3, 1);
			map.AddLayer (new SimpleGrid<TileInfo> (base2Xll, base2Yll, MapGen.GridUtil.ToTileMap (dungeon3), noData, true), -3);

			List<Rect> rooms4;
			int[,] dungeon4 = GenerateDungeon (
				                  null,
				                  stairs3,
				                  base2Xll, base2Yll, base2Width, base2Width,
				                  10000, 3, 9,
				                  0.06f,
				                  out rooms4);
			List<Pos> stairs4 = GenerateStairs (dungeon4, rooms4, 1);
			map.AddLayer (new SimpleGrid<TileInfo> (base2Xll, base2Yll, MapGen.GridUtil.ToTileMap (dungeon4), noData, true), -4);


			List<Rect> rooms5;
			int[,] dungeon5 = GenerateDungeon (
				                  null,
				                  stairs4,
				                  base2Xll, base2Yll, base2Width, base2Width,
				                  10000, 3, 9,
				                  0.06f,
				                  out rooms5);
			//List<Pos> stairs5 = GenerateStairs (dungeon5, rooms5, 1);
			map.AddLayer (new SimpleGrid<TileInfo> (base2Xll, base2Yll, MapGen.GridUtil.ToTileMap (dungeon5), noData, true), -5);


			CreateLinks(map);

			LocationCollections locs = FilterLocations(map, townRadius);

			entities.Add( CreatePlayer (width, height, townRadius) );
			entities.AddRange(CreateHumans(locs, map, buildings));

			entities.AddRange(CreateAnimals(locs));
			entities.AddRange(CreateMonsters(locs));
			entities.AddRange(CreateItems(locs));
			entities.AddRange(CreatePlants(locs));
			entities.AddRange(CreateBooks(locs));

			return map;

		}
		LocationCollections FilterLocations (LayeredGrid<TileInfo> map, int townRadius) {
			LocationCollections locs = new LocationCollections();
			Pos center = new Pos (map.Bounds.GetWidth () / 2, map.Bounds.GetHeight () / 2);

			locs.meadow = map.GetAll (0, (info) => info.Type == TownGenerator.FLOOR_MEADOW);
			locs.meadowTown = map.GetAll (0, (info, x, y) => info.Type == TownGenerator.FLOOR_MEADOW && center.Distance (x, y) < townRadius);
			locs.path = map.GetAll (0, (info) => info.Type == TownGenerator.FLOOR_PATH);
			locs.stone = map.GetAll (0, (info) => info.Type == TownGenerator.FLOOR_STONE);
			locs.wood = map.GetAll (0, (info) => info.Type == TownGenerator.FLOOR_WOOD);
			locs.woodUpper1 = map.GetAll (1, (info) => info.Type == TownGenerator.FLOOR_WOOD);
			locs.woodUpper2 = map.GetAll (2, (info) => info.Type == TownGenerator.FLOOR_WOOD);

			locs.stoneBasement1 = map.GetAll (-1, (info) => info.Type == TownGenerator.FLOOR_STONE);
			locs.stoneBasement1Peri = map.GetAll (-1, (info, x, y) => info.Type == TownGenerator.FLOOR_STONE && center.Distance (x, y) > townRadius * 0.333f);

			locs.stoneBasement2 = map.GetAll (-2, (info) => info.Type == TownGenerator.FLOOR_STONE);
			locs.stoneBasement3 = map.GetAll (-3, (info) => info.Type == TownGenerator.FLOOR_STONE);
			locs.stoneBasement4 = map.GetAll (-4, (info) => info.Type == TownGenerator.FLOOR_STONE);
			locs.stoneBasement5 = map.GetAll (-5, (info) => info.Type == TownGenerator.FLOOR_STONE);

			locs.roof1 = map.GetAll (1, (info) => info.Type == TownGenerator.ROOF);
			locs.roof2 = map.GetAll (2, (info) => info.Type == TownGenerator.ROOF);
			locs.roof3 = map.GetAll (3, (info) => info.Type == TownGenerator.ROOF);

			return locs;
		}

		static List<Pos> GenerateStairs (int[,] map, List<Rect> rooms, int count) {
			List<Rect> rooms2 = RandUtils.Draw (new List<Rect> (rooms), count);
			List<Pos> stairs = new List<Pos> ();
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			foreach (Rect r in rooms2) {
				Pos stair = new Pos (0, 0);
				int cnt = 0;
				while ( cnt < 250 ) {
					stair.x = RandUtils.NextInt (r.xmin, r.xmax + 1);
					stair.y = RandUtils.NextInt (r.ymin, r.ymax + 1);
					int x = stair.x;
					int y = stair.y;
					if (x <= 0 || y <= 0 || x >= w-1 || y >= h-1) {
						Debug.Log("Stairs out of bounds: "+x+" "+y);
					} else {
						if ( map[x, y] != TownGenerator.WALL && map[x, y] != TownGenerator.STAIRS_UP ) {
							map[x, y] = TownGenerator.STAIRS_DOWN;
							stairs.Add(stair);
							break;
						}
					}
					cnt++;
				}
			}
			return stairs;
		}

		static int[,] GenerateDungeon (int[,] template, List<Pos> stairs, 
		                               int xll, int yll, int width, int height, 
		                               int roomAttempts, int minRoomSize, int maxRoomsize, 
									   float additionalDoorProb,
		                               out List<Rect> rooms) {
			int[,] dungeon = new MapGen.DungeonGenerator (
				                 new MapGen.DungeonGeneratorSettings (
					                 width, height, 
					                 roomAttempts, minRoomSize, maxRoomsize, 
					                 additionalDoorProb, // additional doors 
					                 new float[]{ 0.15f, 0.05f, 0.4f, 0.3f, 0.1f },
					                 0.6f),
				                 new MapGen.DepthfirstMazeGenerator (TownGenerator.VOID, TownGenerator.WALL, TownGenerator.FLOOR_STONE),
				                 null)
				.Create (template, stairs, out rooms);
			if (stairs != null) {
				foreach (Pos stair in stairs) {
					dungeon [stair.x, stair.y] = TownGenerator.STAIRS_UP;
				}
			}
			return dungeon;
		}

		void AddEntities (Func<GridLocation, bool, Entity> generator, List<GridLocation> locations, int count, ref List<Entity> addTo) {
			for (int i = 0; i < count; i++) {
				GridLocation loc = RandUtils.Select (locations);
				addTo.Add (generator (loc, false));
			}
		}
		void AddEntities (Func<GridLocation, bool, Entity> generator, List<GridLocation> locations, int count, int countPerLocation, ref List<Entity> addTo) {
			for (int i = 0; i < count; i++) {
				GridLocation loc = RandUtils.Select (locations);
				for (int j = 0; j < count; j++) {
					addTo.Add (generator (loc, false));
				}
			}
		}

		List<Entity> CreateHumans (LocationCollections locs, LayeredGrid<TileInfo> map, List<Building> buildings) {
			List<Entity> entities = new List<Entity> ();

			AddEntities( CreatureFactory.CreateCiticen, locs.path, 80, ref entities );

			AddEntities( CreatureFactory.CreateSoldier, locs.path, 40, ref entities );

			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement1Peri, 10, ref entities );
			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement1, 10, ref entities );
			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement2, 30, ref entities );
			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement3,  5, ref entities );
			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement4,  8, ref entities );
			AddEntities( CreatureFactory.CreateBrigand, locs.stoneBasement5, 10, ref entities );


			List<Building> notShop = buildings.Where ((b) => !b.isShop).ToList ();
			notShop = RandUtils.Draw (notShop, notShop.Count / 4);
			foreach (Building b in notShop) {
				GridLocation loc = null;
				while ( loc == null || map.Get(loc).Type == TownGenerator.WALL ) {
					loc = new GridLocation(RandUtils.NextInt(b.numStoreys), 
							RandUtils.NextInt(b.rect.xmin+1, b.rect.xmax),
							RandUtils.NextInt(b.rect.ymin+1, b.rect.ymax) );
				}
				//UnityEngine.Debug.Log( loc );
				entities.Add( CreatureFactory.CreateAngryCiticen(loc, false) );
			}

			return entities;
		}
		List<Entity> CreateAnimals (LocationCollections locs) {
			List<Entity> entities = new List<Entity> ();

			AddEntities( CreatureFactory.CreateRat, 		locs.path, 				 80, ref entities );
			AddEntities( CreatureFactory.CreateRat, 		locs.stoneBasement1, 	150, ref entities );
			AddEntities( CreatureFactory.CreateRat, 		locs.stoneBasement2, 	 80, ref entities );
			AddEntities( CreatureFactory.CreateRat, 		locs.stoneBasement3, 	 20, ref entities );
			AddEntities( CreatureFactory.CreateRat, 		locs.stoneBasement4, 	 10, ref entities );
			AddEntities( CreatureFactory.CreateRat, 		locs.stoneBasement5, 	 10, ref entities );

			AddEntities( CreatureFactory.CreateGiantSpider, locs.stoneBasement1Peri, 40, ref entities );
			AddEntities( CreatureFactory.CreateGiantSpider, locs.stoneBasement2,     60, ref entities );
			AddEntities( CreatureFactory.CreateGiantSpider, locs.stoneBasement3,     10, ref entities );
			AddEntities( CreatureFactory.CreateGiantSpider, locs.stoneBasement4,     10, ref entities );
			AddEntities( CreatureFactory.CreateGiantSpider, locs.stoneBasement5,     10, ref entities );

			AddEntities( CreatureFactory.CreateSheep, 		locs.meadow, 			20, ref entities );
			AddEntities( CreatureFactory.CreateSheep, 		locs.meadowTown, 		30, ref entities );
			AddEntities( CreatureFactory.CreateSheep, 		locs.path, 				20, ref entities );

			AddEntities( CreatureFactory.CreateDog, 		locs.path, 				10, ref entities );

			AddEntities( CreatureFactory.CreateCat, 		locs.roof1, 			20, ref entities );
			AddEntities( CreatureFactory.CreateCat, 		locs.roof2, 			20, ref entities );
			//AddEntities( CreatureFactory.CreateCat, 		locs.roof3, 			25, ref entities );

			//AddEntities( CreatureFactory.CreateSnake, 		locs.meadowTown,		10, ref entities );
			AddEntities( CreatureFactory.CreateSnake, 		locs.stoneBasement1,	20, ref entities );
			AddEntities( CreatureFactory.CreateSnake, 		locs.stoneBasement2,	50, ref entities );
			AddEntities( CreatureFactory.CreateSnake, 		locs.stoneBasement3,	10, ref entities );
			AddEntities( CreatureFactory.CreateSnake, 		locs.stoneBasement4,	10, ref entities );
			AddEntities( CreatureFactory.CreateSnake, 		locs.stoneBasement5,	10, ref entities );


			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.meadowTown,	8, ref entities );
			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.stoneBasement1,	15, ref entities );
			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.stoneBasement2,	20, ref entities );
			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.stoneBasement3,	 5, ref entities );
			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.stoneBasement4,	 5, ref entities );
			AddEntities( CreatureFactory.CreateSnappingTurtle, 		locs.stoneBasement5,	 5, ref entities );

			AddEntities( CreatureFactory.CreateToadQueen, 		locs.meadowTown,	     4, ref entities );
			AddEntities( CreatureFactory.CreateToadQueen, 		locs.stoneBasement1,	20, ref entities );
			AddEntities( CreatureFactory.CreateToadQueen, 		locs.stoneBasement2,	40, ref entities );
			AddEntities( CreatureFactory.CreateToadQueen, 		locs.stoneBasement3,	10, ref entities );
			AddEntities( CreatureFactory.CreateToadQueen, 		locs.stoneBasement4,	10, ref entities );
			AddEntities( CreatureFactory.CreateToadQueen, 		locs.stoneBasement5,	10, ref entities );

			AddEntities( CreatureFactory.CreateToad, 		locs.meadowTown,	     8, ref entities );
			AddEntities( CreatureFactory.CreateToad, 		locs.stoneBasement1,	20, ref entities );
			AddEntities( CreatureFactory.CreateToad, 		locs.stoneBasement2,	25, ref entities );
			AddEntities( CreatureFactory.CreateToad, 		locs.stoneBasement3,	 5, ref entities );
			AddEntities( CreatureFactory.CreateToad, 		locs.stoneBasement4,	 5, ref entities );
			AddEntities( CreatureFactory.CreateToad, 		locs.stoneBasement5,	 5, ref entities );

			return entities;
		}

		List<Entity> CreateMonsters (LocationCollections locs) {
			List<Entity> entities = new List<Entity> ();
			AddEntities( CreatureFactory.CreateGolem, 		locs.path, 				8, ref entities );
			AddEntities( CreatureFactory.CreateGolem, 		locs.stoneBasement1, 	20, ref entities );
			AddEntities( CreatureFactory.CreateGolem, 		locs.stoneBasement2, 	30, ref entities );


			AddEntities( CreatureFactory.CreateSundew, 		locs.meadowTown, 		15, ref entities );
			AddEntities( CreatureFactory.CreateSundew, 		locs.stoneBasement1, 	20, ref entities );
			AddEntities( CreatureFactory.CreateSundew, 		locs.stoneBasement2, 	20, ref entities );

			AddEntities( CreatureFactory.CreateVenusFlytrap, 		locs.meadowTown, 		10, ref entities );
			AddEntities( CreatureFactory.CreateVenusFlytrap, 		locs.stoneBasement1, 	20, ref entities );
			AddEntities( CreatureFactory.CreateVenusFlytrap, 		locs.stoneBasement2, 	20, ref entities );

			return entities;
		}

		List<Entity> CreateItems (LocationCollections locs) {
			List<Entity> entities = new List<Entity> ();

			AddEntities( ItemFactory.CreateStone, 		locs.path, 				50, ref entities );
			AddEntities( ItemFactory.CreateStone, 		locs.stoneBasement1, 	50, ref entities );
			AddEntities( ItemFactory.CreateStone, 		locs.stoneBasement2, 	50, ref entities );

			//AddEntities( ItemFactory.CreateOilLatern, 		locs.path, 				50, ref entities );
			//AddEntities( ItemFactory.CreateOilCan, 		locs.path, 				50, ref entities );

			for (int i = 0; i < 20; i++) {
				GridLocation loc = RandUtils.Select( locs.wood );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}
			for (int i = 0; i < 20; i++) {
				GridLocation loc = RandUtils.Select( locs.woodUpper1 );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}
			for (int i = 0; i < 10; i++) {
				GridLocation loc = RandUtils.Select( locs.woodUpper2 );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}
			for (int i = 0; i < 30; i++) {
				GridLocation loc = RandUtils.Select( locs.stoneBasement1 );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}
			for (int i = 0; i < 50; i++) {
				GridLocation loc = RandUtils.Select( locs.stoneBasement2 );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}

			for (int i = 0; i < 8; i++) {
				GridLocation loc = RandUtils.Select( locs.stone );
				string creator = RandUtils.Select( EntityFactory.TraderItems );
				entities.Add( EntityFactory.GetCreator( creator )(loc, false) );
			}

			for (int i = 0; i < 10; i++) {
				GridLocation loc = RandUtils.Select( locs.wood );
				entities.Add( ContainerFactory.CreateChest(loc, false, (RandUtils.NextInt(5) + 1) * 100 ) );
			}
			for (int i = 0; i < 10; i++) {
				GridLocation loc = RandUtils.Select( locs.stoneBasement1 );
				entities.Add( ContainerFactory.CreateChest(loc, false, (RandUtils.NextInt(5) + 1) * 100 ) );
			}
			for (int i = 0; i < 20; i++) {
				GridLocation loc = RandUtils.Select( locs.stoneBasement2 );
				entities.Add( ContainerFactory.CreateChest(loc, false, (RandUtils.NextInt(10) + 1) * 100 ) );
			}

			return entities;
		}

		List<Entity> CreatePlants (LocationCollections locs) {
			List<Entity> entities = new List<Entity> ();

			AddEntities( CreatureFactory.CreateTree, 		locs.meadow, 				100, ref entities );
			AddEntities( CreatureFactory.CreateTree, 		locs.meadowTown, 			 80, ref entities );

			AddEntities( HerbFactory.CreateNutritionHerb, 		locs.meadowTown, 10, ref entities );

			AddEntities( HerbFactory.CreatePoisonHerb, 			locs.meadowTown, 10, ref entities );
			AddEntities( HerbFactory.CreateSlowPoisonHerb, 		locs.meadowTown, 10, ref entities );

			AddEntities( HerbFactory.CreateAntiPoisonHerb, 		locs.meadowTown,  5, ref entities );
			AddEntities( HerbFactory.CreatePoisonResistanceHerb,locs.meadowTown,  4, ref entities );

			AddEntities( HerbFactory.CreateHealingHerb, 		locs.meadowTown,  6, ref entities );
			AddEntities( HerbFactory.CreateExtraHealingHerb, 	locs.meadowTown,  3, ref entities );
			AddEntities( HerbFactory.CreateFullHealingHerb, 	locs.meadowTown,  4, ref entities );

			AddEntities( HerbFactory.CreateGainStrengthHerb, 	locs.meadowTown,  2, ref entities );
			AddEntities( HerbFactory.CreateGainConstitutionHerb,locs.meadowTown,  2, ref entities );

			AddEntities( HerbFactory.CreateConfusionHerb, 		locs.meadowTown,  6, ref entities );
			AddEntities( HerbFactory.CreateBerserkHerb, 		locs.meadowTown,  4, ref entities );
			AddEntities( HerbFactory.CreateStunningHerb, 		locs.meadowTown,  6, ref entities );
			AddEntities( HerbFactory.CreateSpeedHerb, 			locs.meadowTown,  3, ref entities );

			AddEntities( HerbFactory.CreateSavePointHerb, 		locs.meadowTown,  2, ref entities );

			return entities;
		}

		List<Entity> CreateBooks (LocationCollections locs) {
			List<Entity> entities = new List<Entity> ();


			AddEntities( BookFactory.CreateBookOfHerbs, 			locs.woodUpper1, 2, ref entities );
			AddEntities( BookFactory.CreateBookOfMushrooms, 		locs.woodUpper1, 2, ref entities );
			AddEntities( BookFactory.CreateBookOfShrubs, 			locs.woodUpper1, 2, ref entities );
			AddEntities( BookFactory.CreateBookOfGrasses,	 		locs.woodUpper1, 2, ref entities );
			AddEntities( BookFactory.CreateBookOfAnimals,	 		locs.woodUpper1, 2, ref entities );
			AddEntities( BookFactory.CreateBookOfCarnivorousPlants,	locs.woodUpper1, 2, ref entities );
			return entities;
		}


		Entity CreatePlayer (int width, int height, int townRadius) {
			int x = 0;
			int y = 0;
			if (RandUtils.NextBool (0.5f)) {
				x = width / 2 - townRadius - 5;
				y = height / 2;
				if (RandUtils.NextBool (0.5f)) {
					x = width / 2 + townRadius + 5;
				}
			} else {
				x = width / 2;
				y = height / 2 - townRadius - 5;
				if (RandUtils.NextBool (0.5f)) {
					y = height / 2 + townRadius + 5;
				}
			}
			GridLocation loc = new GridLocation(0, x, y);
			return CreatureFactory.CreatePlayer( loc, false );
		}

		List<Entity> CreateShops (int[,] layer, List<Building> buildings) {
			Pos center = new Pos (layer.GetLength(0) / 2, layer.GetLength(1) / 2);
			IEnumerable<Building> candidates = buildings.Where ((b) => center.Distance (b.rect.xCenter, b.rect.yCenter) <= maxShopCenterDistance);

			List<Building> filtered = candidates.OrderByDescending ((b) => b.rect.width * b.rect.height).Take (traderGenerators.Length).ToList();
			filtered = RandUtils.Shuffle(filtered);

			List<Entity> traders = new List<Entity> ();
			int cnt = 0;
			foreach (Building b in filtered) {
				if (b.doors.Count > 0) {
					Pos door = b.doors [0];
					layer [door.x, door.y] = TownGenerator.DOOR_OPEN;
				}
				GridLocation loc = null;
				while ( loc == null || layer [loc.X, loc.Y] == TownGenerator.WALL ) {
					loc = new GridLocation (0, 
						RandUtils.NextInt (b.rect.xmin + 1, b.rect.xmax),
						RandUtils.NextInt (b.rect.ymin + 1, b.rect.ymax));
				}
				Entity trader = traderGenerators[cnt](loc, false, true);
				if (trader != null) {
					traders.Add (trader);
					b.isShop = true;
					GridUtil.FillRectWhere( layer, b.rect, TownGenerator.FLOOR_SHOP, TownGenerator.FLOOR_WOOD );
				}
				cnt++;
			}
			return traders;
		}


		void CreateLinks (LayeredGrid<TileInfo> map) {

			int up = Terra.TileInfoId (Terra.StairsUp);
			int down = Terra.TileInfoId (Terra.StairsDown);
			for (int l = map.BottomLayer; l <= map.TopLayer; l++) {
				Grid<TileInfo>[] layers = map.GetLayer (l);
				foreach (Grid<TileInfo> grid in layers) {
					for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
						for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
							int v = grid.Get(x, y).Type;
							if (v == up) {
								GridLocation src = new GridLocation (l, x, y);
								GridLocation trg = new GridLocation (l+1, x, y);
								map.AddDirectedLink(src, trg);
							} else if (v == down) {
								GridLocation src = new GridLocation (l, x, y);
								GridLocation trg = new GridLocation (l-1, x, y);
								map.AddDirectedLink(src, trg);
							}
						}
					}
				}
			}
		}

		class LocationCollections {
			public List<GridLocation> meadow;
			public List<GridLocation> meadowTown;
			public List<GridLocation> path;
			public List<GridLocation> stone;
			public List<GridLocation> wood;
			public List<GridLocation> woodUpper1;
			public List<GridLocation> woodUpper2;
			public List<GridLocation> stoneBasement1;
			public List<GridLocation> stoneBasement1Peri;
			public List<GridLocation> stoneBasement2;
			public List<GridLocation> stoneBasement3;
			public List<GridLocation> stoneBasement4;
			public List<GridLocation> stoneBasement5;

			public List<GridLocation> roof1;
			public List<GridLocation> roof2;
			public List<GridLocation> roof3;
		}
		
		
	}
}
