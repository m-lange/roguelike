﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen {
	public interface MapProgressListener {

		void NotifyProgress(int progress, string message);
		void NotifyCompleted(string message);
		
	}
}
