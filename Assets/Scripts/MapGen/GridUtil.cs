﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MapGen {
	public class GridUtil {

		public delegate float DistanceMetric(int x1, int y1, int x2, int y2);


		public static void Buffer (int[,] map, float radius, int source, int value, bool restricted, int target, DistanceMetric metric) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			int intRad = (int)Math.Floor (radius);
			bool[,] buff = new bool[w, h];
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == source) {
						for (int xx = x - intRad; xx <= x + intRad; xx++) {
							if (xx >= 0 && xx < w) {
								for (int yy = y - intRad; yy <= y + intRad; yy++) {
									if (yy >= 0 && yy < h) {
										if (((!restricted) || map [xx, yy] == target) && metric (x, y, xx, yy) <= radius) {
											//map[xx,yy] = value;
											buff [xx, yy] = true;
										}
									}
								}
							}
						}
					}
				}
			}
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (buff [x, y]) {
						map[x,y] = value;
					}
				}
			}
		}

		public static void Erode (int[,] map, int source, int value) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			bool[,] buff = new bool[w, h];
			bool erode;
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == source) {
						erode = false;
						for (int xx = x - 1; xx <= x + 1; xx++) {
							if (xx >= 0 && xx < w) {
								for (int yy = y - 1; yy <= y + 1; yy++) {
									if (yy >= 0 && yy < h) {
										if (map [xx, yy] != source) {
											erode = true;
											break;
										}
									}
								}
							}
							if(erode) break;
						}
						if (erode) {
							buff [x, y] = true;
						}
					}
				}
			}
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (buff [x, y]) {
						map[x,y] = value;
					}
				}
			}
		}
		public static void Erode4 (int[,] map, int source, int value) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			bool[,] buff = new bool[w, h];
			bool erode;
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == source) {
						erode = false;
						for (int xx = x - 1; xx <= x + 1; xx+=2) {
							if (xx >= 0 && xx < w) {
								if (map [xx, y] != source) {
									erode = true;
									break;
								}
							}
						}
						for (int yy = y - 1; yy <= y + 1; yy+=2) {
							if (yy >= 0 && yy < h) {
								if (map [x, yy] != source) {
									erode = true;
									break;
								}
							}
						}
						if (erode) {
							buff [x, y] = true;
						}
					}
				}
			}
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (buff [x, y]) {
						map[x,y] = value;
					}
				}
			}
		}

		public static void Dilate4 (int[,] map, int source, int value) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			bool[,] buff = new bool[w, h];
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == source) {
						for (int xx = x - 1; xx <= x + 1; xx += 2) {
							if (xx >= 0 && xx < w) {
								if (map [xx, y] != source) {
									buff [xx, y] = true;
								}
							}
						}
						for (int yy = y - 1; yy <= y + 1; yy += 2) {
							if (yy >= 0 && yy < h) {
								if (map [x, yy] != source) {
									buff [x, yy] = true;
								}
							}
						}
					}
				}
			}
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (buff [x, y]) {
						map[x,y] = value;
					}
				}
			}
		}
		public static void Replace (int[,] map, int replace, int with) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == replace) {
						map [x, y] = with;
					}
				}
			}
		}

		public static void FillMap (int[,] map, int value) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					map[x,y] = value;
				}
			}
		}
		public static void Fill (int[,] map, List<Pos> positions, int value) {
			int len = positions.Count;
			Pos pos;
			for(int i=0; i<len;i++) {
				pos = positions[i];
				map[pos.x, pos.y] = value;
			}
		}
		public static void FillRect (int[,] map, Rect rect, int value) {
			for (int x = rect.xmin; x <= rect.xmax; x++) {
				for (int y = rect.ymin; y <= rect.ymax; y++) {
					map[x,y] = value;
				}
			}
		}
		public static void FillRect (bool[,] map, Rect rect, bool value) {
			for (int x = rect.xmin; x <= rect.xmax; x++) {
				for (int y = rect.ymin; y <= rect.ymax; y++) {
					map[x,y] = value;
				}
			}
		}
		public static void FillRectWhere (int[,] map, Rect rect, int value, int target) {
			for (int x = rect.xmin; x <= rect.xmax; x++) {
				for (int y = rect.ymin; y <= rect.ymax; y++) {
					if (map [x, y] == target) {
						map [x, y] = value;
					}
				}
			}
		}
		public static void FillRect (int[,] map, Rect rect, int value, int buffer) {
			for (int x = rect.xmin-buffer; x <= rect.xmax+buffer; x++) {
				for (int y = rect.ymin-buffer; y <= rect.ymax+buffer; y++) {
					map[x,y] = value;
				}
			}
		}
		public static void FillRectHalfSize (int[,] map, Rect rect, int value, int buffer) {
			for (int x = rect.xmin-buffer; x <= rect.xmax+buffer; x++) {
				for (int y = rect.ymin-buffer; y <= rect.ymax+buffer; y++) {
					map[x/2,y/2] = value;
				}
			}
		}

		public static int[,] Crop (int[,] map, int xll, int yll, int width, int height) {
			int[,] result = new int[width, height];
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < width; y++) {
					result[x,y] = map[x+xll, y+yll];
				}
			}
			return result;
		}

		public static List<Pos> Filter<V> (V[,] map, Func<V, bool> filter) {
			List<Pos> result = new List<Pos> ();
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (filter (map [x, y])) {
						result.Add(new Pos(x,y));
					}
				}
			}
			return result;
		}

		public static void ReplaceSolid (int[,] map, int replace, int with, Pos[] neighbors) {
			int w = map.GetLength (0);
			int h = map.GetLength (1);
			bool[,] temp = new bool[w, h];

			Pos pos;
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (map [x, y] == replace) {
						bool solid = true;
						int len = neighbors.Length;
						for(int i=0; i<len;i++) {
							pos = neighbors[i];
							int xx = x + pos.x;
							int yy = y + pos.y;
							if (xx >= 0 && yy >= 0 && xx < w && yy < h) {
								if (map [xx, yy] != replace) {
									solid = false;
									break;
								}
							}
						}
						if (solid) {
							temp [x, y] = true;
						}
					}
				}
			}
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					if (temp [x, y]) {
						map[x,y] = with;
					}
				}
			}
		}


		public static float EuclideanDistanceSq (int x1, int y1, int x2, int y2) {
			int dx = x1 - x2;
			int dy = y1 - y2;
			return dx*dx + dy*dy;
		}
		public static float EuclideanDistance (int x1, int y1, int x2, int y2) {
			int dx = x1 - x2;
			int dy = y1 - y2;
			return (float) Math.Sqrt( dx*dx + dy*dy );
		}
		public static float ManhattanDistance (int x1, int y1, int x2, int y2) {
			int dx = Math.Abs(x1 - x2);
			int dy = Math.Abs(y1 - y2);
			return dx + dy;
		}
		public static float ChebyshevDistance (int x1, int y1, int x2, int y2) {
			int dx = Math.Abs(x1 - x2);
			int dy = Math.Abs(y1 - y2);
			return Math.Max( dx, dy );
		}

		public static TileInfo[,] ToTileMap (int[,] data) {
			int w = data.GetLength (0);
			int h = data.GetLength (1);

			TileInfo[,] result = new TileInfo[w, h];
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					result[x,y] = new TileInfo((byte) data[x,y]);
				}
			}
			return result;
		}

		public static void InsertTemplate (int[,] map, int[,] template, int xll, int yll, int noData) {
			int w = template.GetLength (0);
			int h = template.GetLength (1);
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					int v = template [x, y];
					if (v != noData) {
						map[x+xll, y+yll] = v;
					}
				}
			}
		}
	}
}
