﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen {
	public class DepthfirstMazeGenerator : MazeGenerator {

		static readonly Pos[] Neighbors2 = new Pos[] {
			new Pos(-2,  0),
			new Pos( 2,  0),
			new Pos( 0, -2),
			new Pos( 0,  2)
		};
		//readonly int empty;
		readonly int wall;
		readonly int floor;
		public DepthfirstMazeGenerator ( int empty, int wall, int floor ) {
			//this.empty = empty;
			this.wall = wall;
			this.floor = floor;
		}

		public void FillMaze (int[,] template, int x, int y, float straightProb) {
			Stack<Pos> stack = new Stack<Pos> ();

			List<Pos> candidates;
			Pos next;
			Pos current = new Pos (x, y);
			template [current.x, current.y] = floor;

			Pos prevDir = new Pos (0, 0);
			while ( true ) {
				candidates = GetCarvableNeighbors (template, current);
				if (candidates.Count > 0) {
					stack.Push (current);
					Pos dir = null;
					if (straightProb >= 0 && candidates.Contains (prevDir)) {
						if (RandUtils.NextBool (straightProb)) {
							dir = prevDir;
						} else {
							if (candidates.Count > 1) {
								candidates.Remove (prevDir);
							}
							dir = RandUtils.Select (candidates);
						}
					} else {
						dir = RandUtils.Select (candidates);
					}
					prevDir = dir;
					next = new Pos( current.x + dir.x, current.y + dir.y );
					template[ next.x, next.y ] = floor;
					template[ current.x + dir.x/2, current.y + dir.y/2 ] = floor;
					current = next;
				} else {
					if (stack.Count == 0) {
						break;
					}
					current = stack.Pop();
				}
			}
		}


		bool CanCarve (int[,] map, int x, int y) {
			if (x <= 0 || y <= 0 || x >= map.GetLength (0) - 1 || y >= map.GetLength (1) - 1) {
				return false;
			}
			for (int xx = -1; xx <= 1; xx++) {
				for (int yy = -1; yy <= 1; yy++) {
					if ((xx != 0 || yy != 0) && map [x + xx, y + yy] != wall) {
						return false;
					}
				}
			}
			return true;
		}

		List<Pos> GetCarvableNeighbors (int[,] map, Pos pos) {
			List<Pos> cells = new List<Pos> ();
			Pos n;
			for(int i=0; i<Neighbors2.Length; i++) {
				n = Neighbors2[i];
				if (CanCarve (map, pos.x+n.x, pos.y+n.y)) {
					//cells.Add ( new Pos(pos.x+n.x, pos.y+n.y) );
					cells.Add ( new Pos(n.x, n.y) );
				}
			}
			return cells;
		}

		
	}
}
