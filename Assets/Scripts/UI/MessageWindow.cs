﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Game.Events;
using EventSystem;

namespace UI {
	public class MessageWindow : MonoBehaviour, GameEventListener<MessageEvent> {

		public int numLines = 10;
		public Scrollbar scrollBar;
		public ScrollRect scrollRect;

		Text textField;
		List<string> text;

		// Use this for initialization
		void Start () {

			text = new List<string> ();
			textField = GetComponent<Text> ();

			Notify(new MessageEvent(null, string.Format("Version {0}", Application.version) ));
			Notify(new MessageEvent(null, string.Format("Welcome {0}!", GlobalParameters.Instance.PlayerName) ));
			Notify(new MessageEvent(null, "Press ? or Ctrl+h for help." ));

			GameData.EventManager.Listen (this);
		}
		
		// Update is called once per frame
		void Update () {
			
		}


		public void Notify(MessageEvent evt) {
			string[] lines = evt.message.Split ('\n');
			text.AddRange (lines);
			while (text.Count > numLines) {
				text.RemoveAt (0);
			}
			textField.text = string.Join("\n", text.ToArray());
			StartCoroutine (ScrollDown());
		}

		IEnumerator ScrollDown() {
			yield return null;
			yield return null;
			//scrollBar.value = 0;
			scrollRect.verticalNormalizedPosition = 0;
		}
	}
}
