﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ComponentSystem;
using Game.Events;
using Game.Parts;
using EventSystem;

namespace UI {
	public class InfoWindow : MonoBehaviour {
		
		public static readonly string WarningStart = "<color=red>";
		public static readonly string WarningEnd = "</color>";
		
		//string text = 
		//	"HP{0}/{1} L{2} T{3} [{4},{5}] {6} {7}\n"+
		//	"Sp{8} St{9} Co{10} Dx{11} At{12} De{13} AC{14} M{15}";
		string text = 
			"HP{0}/{1} IBP {2}/{3}/{4} L{5} T{6} "+WarningStart+"{7} {8}"+WarningEnd+"\n"+
			"Sp{9} St{10} Co{11} Dx{12} At{13} De{14} M{15}";
		Text textField;

		Entity player; 
		ActorPart actor;
		HealthPointsPart hp;
		InventoryPart inventory;
		//LocationPart location;

		Entity cameraEmpty; 
		LocationPart camLocation;

		// Use this for initialization
		void Start () {
			textField = GetComponent<Text> ();

			player = GameData.Player;
			actor = player.GetPart<ActorPart> (ActorPart.partId);
			hp = player.GetPart<HealthPointsPart> (HealthPointsPart.partId);
			inventory = player.GetPart<InventoryPart> (InventoryPart.partId);
			//location = player.GetPart<LocationPart> (LocationPart.partId);

			cameraEmpty = GameData.Camera;
			camLocation = cameraEmpty.GetPart<LocationPart> (LocationPart.partId);
		}

		// Update is called once per frame
		void Update () {
			
		}
		public void UpdateInfo() {
			//Debug.Log("UPDATE "+Time.frameCount);
			textField.text = string.Format ( text, 
			    hp.Hp, hp.MaxHp, actor.Armor.ByType[0], actor.Armor.ByType[1], actor.Armor.ByType[2],
				camLocation.Location.Layer, 
				GameData.TurnManager.TurnCounter,
				//location.Location.X, location.Location.Y,
				actor.Stats.SatiationStatus == Game.Rules.GameRules.SatiationStatus.Normal ? "" : actor.Stats.SatiationStatus.ToString(),
				actor.Stats.BurdeningStatus == Game.Rules.GameRules.BurdeningStatus.Unburdened ? "" : actor.Stats.BurdeningStatus.ToString(),

				actor.Speed, actor.Strength, actor.Constitution,
				actor.Dexterity, actor.Attack, actor.Defense, /*(string.Join("/", actor.Armor.ByType)),*/
				inventory.GetMass() / 1000  );
		}
	}
}
