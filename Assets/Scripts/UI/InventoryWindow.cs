﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Collections;
using Game;
using Game.ActorEffects.Continuous;
using Game.Events;
using Game.Parts;
using Game.Util;
using Game.Data;
using Game.Rules;
using EventSystem;
using ComponentSystem;

namespace UI {
	public class InventoryWindow : MonoBehaviour, 
				GameEventListener<ShowInventoryEvent>, 
				GameEventListener<ShowEntitySelectionEvent>,
				GameEventListener<ShowTradeSelectionEvent>,
				GameEventListener<ShowEntityListEvent>,
				GameEventListener<ShowTextSelectionEvent>,
				GameEventListener<ShowHelpEvent>,
				GameEventListener<ShowActorStatsEvent> {
		
		Text textField;
		public Scrollbar scrollBar;
		public ScrollRect scrollRect;

		public RawImage icon;
		public GameObject iconPanel;

		// Use this for initialization
		void Start () {

			textField = GetComponent<Text> ();

			GameData.EventManager.Listen<ShowInventoryEvent> (this);
			GameData.EventManager.Listen<ShowEntitySelectionEvent> (this);
			GameData.EventManager.Listen<ShowTradeSelectionEvent> (this);
			GameData.EventManager.Listen<ShowEntityListEvent> (this);
			GameData.EventManager.Listen<ShowTextSelectionEvent> (this);
			GameData.EventManager.Listen<ShowHelpEvent> (this);
			GameData.EventManager.Listen<ShowActorStatsEvent> (this);

			//iconGO = icon.gameObject;
			iconPanel.SetActive(false);
		}
		
		// Update is called once per frame
		void Update () {
			if( Input.GetKeyDown(KeyCode.UpArrow) ) {
				scrollBar.value += scrollBar.size * 0.5f;
			} else if( Input.GetKeyDown(KeyCode.DownArrow) ) {
				scrollBar.value -= scrollBar.size * 0.5f;
			}
		}

		public void Notify(ShowEntitySelectionEvent evt) {
			iconPanel.SetActive(false);
			if (evt.inventory == null) {
				textField.text = "";
			} else {
			/*
				textField.text = "";
				List<string> lines = new List<string> ();
				lines.Add (evt.title);
				foreach (string key in evt.inventory.Keys) {
					Entity entity = evt.inventory [key];
					if (evt.filter == null || evt.filter (entity)) {
						bool equiped = entity.HasPart<EquipmentPart> () && entity.GetPart<EquipmentPart> ().IsEquiped;
						string eq = 
							entity.HasPart<PickablePart> () && entity.GetPart<PickablePart> ().IsAtReady 
								? (equiped ? TextUtil.EquipedAndAtReady : TextUtil.AtTheReady)
								: (equiped ? TextUtil.Equiped : TextUtil.UnEquiped);
						string line = key + eq + TextUtil.GetNameIndef(entity.GetPart<NamePart> ());
						string ident = GameData.GetIdentifiedName (entity);
						if (ident != null) {
							line += TextUtil.NewLine+TextUtil.Indentation+ident;
						}
						lines.Add ( line );
					}
				}
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
				*/

				List<string> lines = InventoryTextSorted(evt.inventory, GetEntityLine);
				lines.Insert (0, evt.title);
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
			}
			scrollRect.verticalNormalizedPosition = 1;
		}

		public void Notify(ShowTextSelectionEvent evt) {
			iconPanel.SetActive(false);
			if (evt.entries == null) {
				textField.text = evt.title;
			} else {
				textField.text = "";
				List<string> lines = new List<string> ();
				lines.Add (evt.title);
				string name;
				for(int i=0; i<evt.entries.Length; i++) {
					name = evt.entries[i];
					lines.Add (GlobalParameters.letters[i] + TextUtil.UnEquiped + name );
				}
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
			}

			if (evt.selected != null) {
				iconPanel.SetActive (true);
				if (evt.selected.HasPart<VisualPart> ()) {
					VisualPart vis = evt.selected.GetPart<VisualPart> (VisualPart.partId);
					icon.texture = (Texture)Resources.Load (vis.Texture);
				}
			} else {
				iconPanel.SetActive (false);
			}
			scrollRect.verticalNormalizedPosition = 1;
		}

		public void Notify(ShowTradeSelectionEvent evt) {
			iconPanel.SetActive(false);
			if (evt.inventory == null) {
				textField.text = "";
			} else {
			/*
				textField.text = "";
				List<string> lines = new List<string> ();
				lines.Add (evt.title);
				foreach (string key in evt.inventory.Keys) {
					PickablePart pick = evt.inventory [key];
					bool equiped = pick.Entity.HasPart<EquipmentPart> () && pick.Entity.GetPart<EquipmentPart> ().IsEquiped;
					string eq = 
						pick.IsAtReady 
								? (equiped ? TextUtil.EquipedAndAtReady : TextUtil.AtTheReady)
								: (equiped ? TextUtil.Equiped : TextUtil.UnEquiped);
					string line = (key + eq + TextUtil.GetNameIndef(pick)).PadRight(28)+"$"+pick.GetPrice(evt.sell)+"/pc";
					lines.Add ( line );
				}
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
				*/

				List<string> lines = InventoryTextSorted(evt.inventory, null, evt.sell, evt.specialized, null, GetTradeLine);
				lines.Insert (0, evt.title);
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
			}
			scrollRect.verticalNormalizedPosition = 1;
		}

		public void Notify (ShowEntityListEvent evt) {
			iconPanel.SetActive (false);
			if (evt.entities == null) {
				textField.text = "";
			} else {
				textField.text = "";
				List<string> lines = new List<string> ();
				lines.Add (evt.title);
				foreach (Entity entity in evt.entities) {
					if (evt.filter == null || evt.filter (entity)) {
						if (entity.HasPart<ActorPart> () && entity.HasPart<InventoryPart> ()) {
							InventoryPart inv = entity.GetPart<InventoryPart> ();
							EquipmentPart weapon = inv.GetEquipped (Equip.HandPrim);
							if (weapon != null) {
								lines.Add ( TextUtil.GetNameIndef(entity)+", wielding "+TextUtil.GetNameIndef(weapon) );
								continue;
							}
						}
						string line = TextUtil.GetNameIndef(entity);
						string ident = GameData.GetIdentifiedName (entity);
						if (ident != null) {
							line += TextUtil.NewLine + TextUtil.Indentation + TextUtil.IdentifiedStart + ident + TextUtil.IdentifiedEnd;
						}
						lines.Add ( line );
					}
				}
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
			}
			scrollRect.verticalNormalizedPosition = 1;
		}
		public void Notify (ShowInventoryEvent evt) {
			if (evt.inventory == null) {
				textField.text = "";
				scrollRect.verticalNormalizedPosition = 1;
			} else {
				/*
				textField.text = "";
				List<string> lines = new List<string> ();
				lines.Add (evt.title);
				foreach (string key in evt.inventory.Keys) {
					PickablePart pickable = evt.inventory [key];
					if (evt.filter == null || evt.filter (pickable)) {
						lines.Add (GetInventoryLine(key, pickable, evt.selected));
					}
				}
				*/
				List<string> lines = InventoryTextSorted(evt.inventory, evt.selected, false, false, evt.filter, GetInventoryLine);
				lines.Insert (0, evt.title);
				textField.text = string.Join (TextUtil.NewLine, lines.ToArray ());
			}
			if (evt.selected != null) {
				iconPanel.SetActive (true);
				if (evt.selected.Entity.HasPart<VisualPart> ()) {
					VisualPart vis = evt.selected.Entity.GetPart<VisualPart> ();
					icon.texture = (Texture)Resources.Load (vis.Texture);
				}
			} else {
				iconPanel.SetActive (false);
			}
		}

		List<string> InventoryTextSorted (
			OrderedDict<string, PickablePart> inventory, 
			PickablePart selected, 
			bool sell, bool specialized,
			Func<PickablePart, bool> filter, 
			Func<string, PickablePart, PickablePart, bool, bool, string> lineGen) {

			List<string> money = new List<string> ();
			List<string> equipment = new List<string> ();
			List<string> projectiles = new List<string> ();
			List<string> tools = new List<string> ();
			List<string> containers = new List<string> ();
			List<string> consumables = new List<string> ();
			List<string> herbs = new List<string> ();
			List<string> plants = new List<string> ();
			List<string> books = new List<string> ();

			List<string> misc = new List<string> ();

			foreach (string key in inventory.Keys) {
				PickablePart pickable = inventory [key];
				Entity e = pickable.Entity;
				if (filter == null || filter (pickable)) {
					if (e.HasPart (MoneyPart.partId)) {
						money.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (BookPart.partId)) {
						books.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (EquipmentPart.partId)) {
						equipment.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (ConsumablePart.partId)) {
						ConsumablePart cons = e.GetPart<ConsumablePart> (ConsumablePart.partId);
						if (cons.IsHerb) {
							herbs.Add (lineGen (key, pickable, selected, sell, specialized));
						} else {
							consumables.Add (lineGen (key, pickable, selected, sell, specialized));
						}
					} else if (e.HasPart (UsablePart.partId)) {
						tools.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (ContainerPart.partId)) {
						containers.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (PlantPart.partId)) {
						plants.Add (lineGen (key, pickable, selected, sell, specialized));
					} else if (e.HasPart (ProjectilePart.partId)) {
						projectiles.Add (lineGen (key, pickable, selected, sell, specialized));
					} else {
						misc.Add (lineGen (key, pickable, selected, sell, specialized));
					}	
				}
			}
			List<string> result = new List<string> ();
			if (money.Count > 0) {
				result.Add("-- Gold ---------------------------");
				result.AddRange(money);
			}
			if (equipment.Count > 0) {
				result.Add("-- Equipment ----------------------");
				result.AddRange(equipment);
			}
			if (projectiles.Count > 0) {
				result.Add("-- Ammunition ---------------------");
				result.AddRange(projectiles);
			}
			if (tools.Count > 0) {
				result.Add("-- Tools --------------------------");
				result.AddRange(tools);
			}
			if (containers.Count > 0) {
				result.Add("-- Containers ---------------------");
				result.AddRange(containers);
			}
			if (consumables.Count > 0) {
				result.Add("-- Consumables --------------------");
				result.AddRange(consumables);
			}
			if (herbs.Count > 0) {
				result.Add("-- Herbs --------------------------");
				result.AddRange(herbs);
			}
			if (books.Count > 0) {
				result.Add("-- Books --------------------------");
				result.AddRange(books);
			}
			if (plants.Count > 0) {
				result.Add("-- Plants -------------------------");
				result.AddRange(plants);
			}
			if (misc.Count > 0) {
				result.Add("-- Misc ---------------------------");
				result.AddRange(misc);
			}
			return result;
		}

		List<string> InventoryTextSorted (
			OrderedDict<string, Entity> inventory, 
			Func<string, Entity, string> lineGen) {

			List<string> money = new List<string> ();
			List<string> equipment = new List<string> ();
			List<string> projectiles = new List<string> ();
			List<string> tools = new List<string> ();
			List<string> containers = new List<string> ();
			List<string> consumables = new List<string> ();
			List<string> herbs = new List<string> ();
			List<string> plants = new List<string> ();
			List<string> books = new List<string> ();

			List<string> misc = new List<string> ();

			foreach (string key in inventory.Keys) {
				//PickablePart pickable = inventory [key];
				Entity e = inventory [key];
				if (e.HasPart (MoneyPart.partId)) {
					money.Add (lineGen (key, e));
				} else if (e.HasPart (BookPart.partId)) {
					books.Add (lineGen (key, e));
				} else if (e.HasPart (EquipmentPart.partId)) {
					equipment.Add (lineGen (key, e));
				} else if (e.HasPart (ConsumablePart.partId)) {
					ConsumablePart cons = e.GetPart<ConsumablePart> (ConsumablePart.partId);
					if (cons.IsHerb) {
						herbs.Add (lineGen (key, e));
					} else {
						consumables.Add (lineGen (key, e));
					}
				} else if (e.HasPart (UsablePart.partId)) {
					tools.Add (lineGen (key, e));
				} else if (e.HasPart (ContainerPart.partId)) {
					containers.Add (lineGen (key, e));
				} else if (e.HasPart (PlantPart.partId)) {
					plants.Add (lineGen (key, e));
				} else if (e.HasPart (ProjectilePart.partId)) {
					projectiles.Add (lineGen (key, e));
				} else {
					misc.Add (lineGen (key, e));
				}
			}
			List<string> result = new List<string> ();
			if (money.Count > 0) {
				result.Add("-- Gold ---------------------------");
				result.AddRange(money);
			}
			if (equipment.Count > 0) {
				result.Add("-- Equipment ----------------------");
				result.AddRange(equipment);
			}
			if (projectiles.Count > 0) {
				result.Add("-- Ammunition ---------------------");
				result.AddRange(projectiles);
			}
			if (tools.Count > 0) {
				result.Add("-- Tools --------------------------");
				result.AddRange(tools);
			}
			if (containers.Count > 0) {
				result.Add("-- Containers ---------------------");
				result.AddRange(containers);
			}
			if (consumables.Count > 0) {
				result.Add("-- Consumables --------------------");
				result.AddRange(consumables);
			}
			if (herbs.Count > 0) {
				result.Add("-- Herbs --------------------------");
				result.AddRange(herbs);
			}
			if (books.Count > 0) {
				result.Add("-- Books --------------------------");
				result.AddRange(books);
			}
			if (plants.Count > 0) {
				result.Add("-- Plants -------------------------");
				result.AddRange(plants);
			}
			if (misc.Count > 0) {
				result.Add("-- Misc ---------------------------");
				result.AddRange(misc);
			}
			return result;
		}


		string GetInventoryLine (string key, PickablePart pickable, PickablePart selected, bool dummy, bool dummy2) {
			Entity e = pickable.Entity;
			EquipmentPart equipment = e.GetPart<EquipmentPart> (EquipmentPart.partId);
			OnOffPart light = e.GetPart<OnOffPart> (OnOffPart.partId);
			bool equiped = equipment != null && equipment.IsEquiped;
			bool lightOn = light != null && light.IsOn;
			string eq = 
				pickable.IsAtReady 
					? (equiped ? TextUtil.EquipedAndAtReady : TextUtil.AtTheReady)
					: (equiped ? TextUtil.Equiped : TextUtil.UnEquiped);

			string ident = GameData.GetIdentifiedName (pickable.Entity);

			string start = (pickable == selected) ? TextUtil.SelectionStart : lightOn ? TextUtil.LightOnStart : "";
			string end = (pickable == selected) ? TextUtil.SelectionEnd : lightOn ? TextUtil.LightOnEnd : "";
			string line = start + key + eq + TextUtil.GetNameIndef (e) + end;

			if (ident != null) {
				line += TextUtil.NewLine + TextUtil.Indentation + TextUtil.IdentifiedStart + ident + TextUtil.IdentifiedEnd;
			}
			return line;
		}

		string GetEntityLine (string key, Entity entity) {
			PickablePart pick = entity.GetPart<PickablePart>(PickablePart.partId);
			bool equiped = entity.HasPart (EquipmentPart.partId) && entity.GetPart<EquipmentPart> (EquipmentPart.partId).IsEquiped;
			string eq = 
				( pick != null && pick.IsAtReady )
					? (equiped ? TextUtil.EquipedAndAtReady : TextUtil.AtTheReady)
					: (equiped ? TextUtil.Equiped : TextUtil.UnEquiped);
			string line = key + eq + TextUtil.GetNameIndef (entity);
			string ident = GameData.GetIdentifiedName (entity);
			if (ident != null) {
				line += TextUtil.NewLine + TextUtil.Indentation + TextUtil.IdentifiedStart + ident + TextUtil.IdentifiedEnd;
			}
			return line;
		}

		string GetTradeLine (string key, PickablePart pickable, PickablePart selected, bool sell, bool specialized) {
			bool equiped = pickable.Entity.HasPart<EquipmentPart> () && pickable.Entity.GetPart<EquipmentPart> ().IsEquiped;
			string eq = 
				pickable.IsAtReady 
						? (equiped ? TextUtil.EquipedAndAtReady : TextUtil.AtTheReady)
						: (equiped ? TextUtil.Equiped : TextUtil.UnEquiped);
			string line = (key + eq + TextUtil.GetNameIndef(pickable)).PadRight(31)+"$"+GameRules.CalcPrice( pickable, sell, specialized )+"/pc";
			string ident = GameData.GetIdentifiedName (pickable.Entity);
			if (ident != null) {
				line += TextUtil.NewLine + TextUtil.Indentation + TextUtil.IdentifiedStart + ident + TextUtil.IdentifiedEnd;
			}
			return line;
		}

		public void Notify(ShowHelpEvent evt) {
			iconPanel.SetActive(false);
			if (evt.text == null) {
				textField.text = string.Join (TextUtil.NewLine, HelpText.CommandHelp);
			} else {
				textField.text = evt.text;
			}
			scrollRect.verticalNormalizedPosition = 1;
		}


		public void Notify (ShowActorStatsEvent evt) {
			iconPanel.SetActive (false);
			if (evt.actor == null) {
				textField.text = "";
				return;
			} 
			/*
			int padding = 25;
			Entity entity = evt.actor.Entity;
			ActorPart actor = evt.actor;
			string s = "<size=14>You</size>" + TextUtil.NewLine;

			s += TextUtil.NewLine;
			s += "-- Stats ------------------------------" + TextUtil.NewLine;
			if (entity.HasPart<HealthPointsPart> ()) {
				HealthPointsPart hp = entity.GetPart<HealthPointsPart> ();
				s += "Health: ".PadRight (padding) + hp.Hp + "/" + hp.MaxHp + TextUtil.NewLine;
			}

			s += "Speed: ".PadRight (padding) + actor.Speed + " (" + actor.Stats.BaseSpeed + ")" + TextUtil.NewLine;
			s += "Strength: ".PadRight (padding) + actor.Strength + " (" + actor.Stats.BaseStrength + "+" + actor.Stats.StrengthPoints + ")" + TextUtil.NewLine;
			s += "Constitution: ".PadRight (padding) + actor.Constitution + " (" + actor.Stats.BaseConstitution + "+" + actor.Stats.ConstitutionPoints + ")" + TextUtil.NewLine;
			s += "Dexterity: ".PadRight (padding) + actor.Dexterity + " (" + actor.Stats.BaseDexterity + "+" + actor.Stats.DexterityPoints + ")" + TextUtil.NewLine;
			s += "Attack: ".PadRight (padding) + actor.Attack + " (" + actor.Stats.BaseAttack + "+" + actor.Stats.AttackPoints + ")" + TextUtil.NewLine;
			s += "Defense: ".PadRight (padding) + actor.Defense + " (" + actor.Stats.BaseDefense + "+" + actor.Stats.DefensePoints + ")" + TextUtil.NewLine;
			s += "Armor: ".PadRight (padding) + actor.Armor + " (" + actor.Stats.BaseArmor + ")" + TextUtil.NewLine;

			s += TextUtil.NewLine;
			s += "-- Skills -----------------------------" + TextUtil.NewLine;
			foreach (Skills skill in Enum.GetValues(typeof(Skills)).Cast<Skills>()) {
				if (skill == Skills.None) {
					continue;
				}
				int idx = (int)skill;
				s += (skill.ToString () + ": ").PadRight (padding) + actor.Stats.Skill [idx] + " (" + actor.Stats.SkillPoints [idx] + ")" + TextUtil.NewLine;
			}

			s += TextUtil.NewLine;
			s += "-- Effects ----------------------------" + TextUtil.NewLine;
			if (actor.Effects.Count > 0) {
				foreach (ContinuousEffect eff in actor.Effects) {
					if (eff.Name == null) {
						continue;
					}
					s += eff.Name + TextUtil.NewLine;
				}
			} else {
				s += "none" + TextUtil.NewLine;
			}

			s += TextUtil.NewLine;
			s += "-- Kills ------------------------------"+ TextUtil.NewLine;
			if (GameData.KillRecord.Kills.Count > 0) {
				foreach (KeyValuePair<string, int> kv in GameData.KillRecord.Kills) {
					s += kv.Key.PadRight (padding)+kv.Value+TextUtil.NewLine;
				}
			} else {
				s += "none" + TextUtil.NewLine;
			}
			*/
			textField.text = TextUtil.CharacterInfo(evt.actor);
			scrollRect.verticalNormalizedPosition = 1;
		}
	}
}
