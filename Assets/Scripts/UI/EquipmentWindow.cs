﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ComponentSystem;
using Game.Events;
using Game.Parts;
using Game.Data;
using EventSystem;

namespace UI {
	public class EquipmentWindow : MonoBehaviour {
		
		ActorPart actor;

		List<RawImage> icons;

		// Use this for initialization
		void Start () {
			Entity player = GameData.Player;
			actor = player.GetPart<ActorPart> (ActorPart.partId);
			icons = new List<RawImage>();
		}

		public void UpdateInfo () {
			InventoryPart inv = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				List<PickablePart> equipment = new List<PickablePart> ();
				List<PickablePart> other = new List<PickablePart> ();

				foreach (PickablePart item in inv.Items) {
					//DictionaryEntry kv = (DictionaryEntry)entry;
					//PickablePart item = (PickablePart)kv.Value;
					if (item.IsAtReady) {
						other.Add (item);
					} else {
						EquipmentPart eq = item.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
						if (eq != null && eq.IsEquiped) {
							equipment.Add (item);
						} else {
							LightSourcePart light = item.Entity.GetPart<LightSourcePart> (LightSourcePart.partId);
							if (light != null && light.IsOn) {
								other.Add (item);
							}
						}
					}
				}

				equipment.AddRange (other);
				int cnt = 0;
				foreach (PickablePart item in equipment) {
					VisualPart vis = item.Entity.GetPart<VisualPart> (VisualPart.partId);
					if (vis != null) {
						if (icons.Count <= cnt) {
							GameObject go = GameObject.Instantiate (Resources.Load<GameObject> (Prefabs.Icon32px), gameObject.transform);
							RawImage img = go.GetComponent<RawImage> ();
							icons.Add (img);
						}
						RawImage icon = icons [cnt];
						icon.texture = (Texture)Resources.Load (vis.Texture);
						icon.gameObject.SetActive(true);
						cnt++;
					}
				}
				for (int i = cnt; i < icons.Count; i++) {
					icons[i].gameObject.SetActive(false);
				}
			}
		}
	}
}
