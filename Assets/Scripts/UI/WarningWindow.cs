﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ComponentSystem;
using Game.Events;
using Game.Parts;
using EventSystem;

namespace UI {
	public class WarningWindow : MonoBehaviour, 
				GameEventListener<ShowWarningEvent> {
		
		public Text textField;

		GameObject go;

		// Use this for initialization
		void Start () {
			go = gameObject;
			GameData.EventManager.Listen<ShowWarningEvent> (this);
			go.SetActive(false);
		}


		public void Notify (ShowWarningEvent evt) {
			if (evt.text == null) {
				go.SetActive (false);
			} else {
				textField.text = evt.text;
				go.SetActive (true);
				StartCoroutine(CloseAfter(10f));
			}
		}


		System.Collections.IEnumerator CloseAfter (float time) {
			yield return new WaitForSeconds(time);
			go.SetActive (false);
		}


		// Update is called once per frame
		void Update () {
			
		}
	}
}
