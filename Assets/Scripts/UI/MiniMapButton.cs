﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Map;

namespace UI {
	public class MiniMapButton : MonoBehaviour {

		public string direction;
		public bool zoomIn;
		public bool zoomOut;
		Direction dir;

		Button mButton;

		// Use this for initialization
		void Start () {
			if (!string.IsNullOrEmpty (direction)) {
				dir = Direction.BY_NAME [direction];
			}
			mButton = GetComponent<Button>();
			mButton.onClick.AddListener( ButtonClicked );
		}

		void ButtonClicked () {
			if (zoomIn) {
				GameApp.Instance.ZoomMiniMap(true);
			} else if (zoomOut) {
				GameApp.Instance.ZoomMiniMap(false);
			} else {
				GameApp.Instance.ScrollMiniMap (dir);
			}
		}
		
		// Update is called once per frame
		void Update () {

		}
		
	}
}
