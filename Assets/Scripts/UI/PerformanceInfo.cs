﻿using UnityEngine;
using System.Collections;

public class PerformanceInfo : MonoBehaviour
{

	float deltaTime = 0.0f;
	GUIStyle style = new GUIStyle();

	void Awake () {

		int w = Screen.width, h = Screen.height;
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = 12;
		style.font = (Font) Resources.Load("consolab");
		style.normal.textColor = new Color (0.7f, 0.7f, 0.7f, 1.0f);


		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0, 0, Color.black);
		texture.Apply();

		style.normal.background = texture;


	}
	
	void Update()
	{

		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;


	}
	
	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

		Vector2 size = style.CalcSize(new GUIContent(text));
		Rect rect = new Rect(0, 0, size.x + 6, size.y + 6);

		GUI.Box(rect, text, style);
		//GUI.Label(rect, text, style);

	}
}