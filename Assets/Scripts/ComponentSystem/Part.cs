﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComponentSystem {

	//[DataContract(Namespace="ComponentSystem")]
	[DataContract]
	public abstract class Part {

		Entity entity;

		//bool isActive = true;
		
		[IgnoreDataMember]
		public Entity Entity {
			get { return entity; }
			set { entity = value; }
		}

		public abstract void Initialize();
		public abstract void Update();
		public abstract void Cleanup ();
	}
	//[DataContract(Namespace="ComponentSystem")]
	[DataContract]
	public abstract class Part<P> : Part where P : Part {

		public static readonly PartType partType = PartType.GetFor<P>(); 
		public static readonly int partId = partType.id; 

	}
}
