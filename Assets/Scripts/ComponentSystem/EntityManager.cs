﻿using System.Collections;
using System.Collections.Generic;

namespace ComponentSystem {
	public class EntityManager {

		List<Entity> entities;
		List<Entity> toAdd = new List<Entity> ();

		public EntityManager() {
			entities = new List<Entity> ();
		}


		public int EntityCount {
			get{ return entities.Count; }
		}

		public void AddEntity(Entity e) {
			toAdd.Add (e);
		}

		public void Update() {
			int len = toAdd.Count;
			Entity e;
			for(int i=0; i<len;i++) {
				e = toAdd[i];
				entities.Add (e);
				e.Initialize ();
			}
			toAdd.Clear ();

			List<Entity> toRemove = new List<Entity> ();
			len = entities.Count;
			for(int i=0; i<len;i++) {
				e = entities[i];
				e.Update ();
				if (!e.IsActive) {
					toRemove.Add (e);
				}
			}

			len = toRemove.Count;
			for(int i=0; i<len;i++) {
				entities.Remove (toRemove[i]);
			}
		}

		public void Initialize() {
			int len = toAdd.Count;
			Entity e;
			for(int i=0; i<len;i++) {
				e = toAdd[i];
				entities.Add (e);
				e.Initialize ();
			}
			toAdd.Clear ();
		}


	}
}
