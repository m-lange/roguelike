﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;

namespace ComponentSystem {
	
	public class Entity {

		static long idCounter = 0;

		readonly long id;
		bool isActive = false;
		//bool isInitialized = false;
		bool updatable;
		//Dictionary<Type, Part> parts = new Dictionary<Type, Part>();

		FlexArray<Part> parts = new FlexArray<Part>();
		BoolFlexArray hasPart = new BoolFlexArray();
		
		/*public Entity() {
			id = idCounter;
			idCounter++;
			this.updatable = true;
		}*/
		public Entity(bool updatable) {
			id = idCounter;
			idCounter++;
			this.updatable = updatable;
		}
		public Entity(long id, bool updatable) {
			this.id = id;
			if (id >= idCounter) {
				idCounter = id + 1;
			}
			this.updatable = updatable;
		}

		public long Id {
			get { return id; }
		}
		public bool IsActive {
			get { return isActive; }
		}
		public void SetActive(bool active) {
			this.isActive = active;
		}

		public bool Updatable {
			get { return updatable; }
			set { updatable = value; }
		}

		public FlexArray<Part> Parts {
			get { return parts; }
		}

		public void Initialize () {
			foreach (Part p in parts.Values) {
				if (p != null) {
					p.Initialize ();
				}
			}
			//isInitialized = true;
			isActive = true;
		}
		public void Update () {
			if (updatable) {
				foreach (Part p in parts.Values) {
					if (p != null) {
						p.Update ();
					}
				}
			}
		}
		public void Cleanup () {
			foreach (Part p in parts.Values) {
				if (p != null) {
					p.Cleanup ();
				}
			}
			isActive = false;
		}

		//[System.Obsolete]
		public bool HasPart<T>() where T : Part<T> {
			return hasPart.Get(PartType.GetFor<T>().id);
		}

		//[System.Obsolete]
		public bool HasPart(Type partType) {
			return hasPart.Get(PartType.GetFor(partType).id);
		}

		public bool HasPart(int id) {
			return hasPart.Get(id);
		}

		//[System.Obsolete]
		public T GetPart<T>() where T : Part<T> {
			return (T) parts.Get(PartType.GetFor<T>().id);
		}
		//[System.Obsolete]
		public Part GetPart(Type partType) {
			return parts.Get(PartType.GetFor(partType).id);
		}

		public T GetPart<T>(int id) where T : Part<T> {
			return (T) parts.Get(id);
		}
		public void Attach(Part part) {
			Type baseClass = part.GetType ();
			int id = PartType.GetFor(baseClass).id;
			if (HasPart (id)) {
				throw new ArgumentException ("Part of type "+baseClass.Name+" already exists.");
			}
			parts.Set(id, part);
			hasPart.Set(id, true);
			part.Entity = this;
		}
		public void Detach (Part part) {
			Type baseClass = part.GetType ();
			int id = PartType.GetFor (baseClass).id;
			if (!HasPart (id)) {
				throw new ArgumentException ("Part of type " + baseClass.Name + " not in entity.");
			}
			part.Cleanup ();
			part.Entity = null;
			parts.Set (id, null);
			hasPart.Set (id, false);
		}

		public void Attach<P>(Part<P> part) where P : Part {
			Type baseClass = part.GetType ();
			int id = PartType.GetFor(baseClass).id;
			if (HasPart (id)) {
				throw new ArgumentException ("Part of type "+baseClass.Name+" already exists.");
			}
			parts.Set(id, part);
			hasPart.Set(id, true);
			part.Entity = this;
		}
		public void Detach<P>(Part<P> part) where P : Part {
			Type baseClass = part.GetType ();
			int id = PartType.GetFor (baseClass).id;
			if (!HasPart (id)) {
				throw new ArgumentException ("Part of type " + baseClass.Name + " not in entity.");
			}
			part.Cleanup ();
			part.Entity = null;
			hasPart.Set(id, false);
			parts.Set (id, null);
		}

		public override bool Equals(Object obj) {
			if ( typeof(Entity).IsAssignableFrom( obj.GetType() ) ) {
				Entity other = (Entity) obj;
				return other.id == id;
			} else {
				return false;
			}
		}

		public override int GetHashCode() {
			return (int) id;
		}

	}
}
