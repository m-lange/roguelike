﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComponentSystem {

	//[DataContract(Namespace="ComponentSystem")]
	public class PartType {

		private static int idCounter = 0;
		static Dictionary<Type, PartType> assignedTypes = new Dictionary<Type, PartType>();

		public readonly Type type;
		public readonly int id;

		private PartType ( Type type ) {
			this.type = type;
			this.id = idCounter;
			idCounter++;
		}

		public static PartType GetFor<P> () where P : Part {
			return GetFor( typeof(P) );
		}
		public static PartType GetFor (Type type) {
			if (assignedTypes.ContainsKey (type)) {
				return assignedTypes [type];
			} else {
				PartType pt = new PartType(type);
				assignedTypes[type] = pt;
				return pt;
			}
		}
	}
}
