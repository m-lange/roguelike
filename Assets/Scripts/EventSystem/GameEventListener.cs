﻿

namespace EventSystem {
	public interface GameEventListener {

	}
	public interface GameEventListener<E> : GameEventListener where E : GameEvent {
		void Notify(E evt);
	}
}