﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EventSystem {
	public class EventManager {

		Dictionary<Type, List<GameEventListener>> listeners;

		public EventManager() {
			listeners = new Dictionary<Type, List<GameEventListener>> ();
		}

		public void Notify<E>( E evt ) where E : GameEvent {
			Type evtType = typeof(E);
			if (listeners.ContainsKey (evtType)) {
				List<GameEventListener> lst = listeners [evtType];
				int len = lst.Count;
				for(int i=0; i<len;i++) {
					GameEventListener<E> l = (GameEventListener<E>)lst[i];
					l.Notify (evt);
				}
			}
		}

		public void Listen<E>( GameEventListener<E> listener ) where E : GameEvent {
			Type eventType = typeof(E);
			if (listeners.ContainsKey (eventType)) {
				listeners [eventType].Add (listener);
			} else {
				List<GameEventListener> lst = new List<GameEventListener> ();
				lst.Add (listener);
				listeners [eventType] = lst;
			}
		}

		public void Mute<E>( GameEventListener<E> listener ) where E : GameEvent {
			Type eventType = typeof(E);
			if (listeners.ContainsKey (eventType)) {
				listeners [eventType].Remove (listener);
			}
		}
	}
}
