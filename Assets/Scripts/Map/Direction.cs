﻿using System.Collections.Generic;
using Map.LayeredGrid;
using UnityEngine;

namespace Map {
	public class Direction {


		public static readonly Direction UP = new Direction(  1,  0,  0, "Up");
		public static readonly Direction DOWN = new Direction( -1,  0,  0, "Down");

		public static readonly Direction N  = new Direction(  0,  0,  1,  "N");
		public static readonly Direction NE = new Direction(  0,  1,  1,  "NE");
		public static readonly Direction E  = new Direction(  0,  1,  0,  "E");
		public static readonly Direction SE = new Direction(  0,  1, -1,  "SE");
		public static readonly Direction S  = new Direction(  0,  0, -1,  "S");
		public static readonly Direction SW = new Direction(  0, -1, -1,  "SW");
		public static readonly Direction W  = new Direction(  0, -1,  0,  "W");
		public static readonly Direction NW = new Direction(  0, -1,  1,  "NW");

		public static readonly Direction[] ALL = new Direction[]{
			N, NE, E, SE, S, SW, W, NW, UP, DOWN
		};
		public static readonly Direction[] HORIZONTAL = new Direction[]{
			N, NE, E, SE, S, SW, W, NW
		};
		public static readonly Dictionary<string, Direction> BY_NAME = new Dictionary<string, Direction>{
			{"N", N}, 
			{"NE", NE}, 
			{"E", E}, 
			{"SE", SE}, 
			{"S", S}, 
			{"SW", SW}, 
			{"W", W}, 
			{"NW", NW},
			{"UP", UP},
			{"DOWN", DOWN}
		};

		public readonly string name;
		public readonly int dl;
		public readonly int dx;
		public readonly int dy;

		Direction(int dl, int dx, int dy, string name) {
			this.name = name;
			this.dl = dl;
			this.dx = dx;
			this.dy = dy;
		}

		public GridLocation Transform(GridLocation source) {
			return new GridLocation ( source.Layer + dl, source.X + dx, source.Y + dy );
		}
		public void Transform(GridLocation source, GridLocation result) {
			result.Set(source.Layer + dl, source.X + dx, source.Y + dy);
		}
		public void Move(GridLocation source) {
			source.Layer += dl;
			source.X += dx;
			source.Y += dy;
		}
		public override string ToString () {
			return name;
		}

		public static Direction GetFromTo (GridLocation source, GridLocation target) {
			if (source.Layer < target.Layer) {
				return Direction.UP;
			} else if (source.Layer > target.Layer) {
				return Direction.DOWN;
			} else {
				return GetFromToXY(source, target);
			}
		}
		public static Direction GetFromToXY(GridLocation source, GridLocation target) {
			int dx = target.X - source.X;
			int dy = target.Y - source.Y;
			if (dx < 0) {
				if (dy < 0) {
					return SW;
				} else if (dy > 0) {
					return NW;
				} else {
					return W;
				}
			} else if (dx > 0) {
				if (dy < 0) {
					return SE;
				} else if (dy > 0) {
					return NE;
				} else {
					return E;
				}
			} else {
				if (dy < 0) {
					return S;
				} else if (dy > 0) {
					return N;
				} else {
					return null;
				}
			}
		}
		
		public static Direction GetFromToXYFar(GridLocation source, GridLocation target) {
			int dx = target.X - source.X;
			int dy = target.Y - source.Y;
			float angle = - Vector2.SignedAngle( new Vector2(0,1), new Vector2(dx, dy) );
			if( angle < 0 ) {
				angle = 360f + angle;
			}
			angle += 22.5f;
			int sector = (int) (angle / 45);
			return HORIZONTAL[sector % HORIZONTAL.Length];
		}

	}
}
