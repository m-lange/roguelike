﻿using System;
using System.Runtime.Serialization;


namespace Map.LayeredGrid {

	[DataContract]
	public class ImmutableGridLocation : GridLocation {
		

		public ImmutableGridLocation(GridLocation other) : base(other) {
		}
		public ImmutableGridLocation(int layer, int x, int y) : base(layer, x, y) {
		}

		[DataMember]
		public override int Layer { 
			get { return layer; } 
			set { throw new NotSupportedException("Setting layer of an ImmutableGridLocation is not supported;"); } 
		}
		[DataMember]
		public override int X { 
			get { return x; } 
			set { throw new NotSupportedException("Setting x coordinate of an ImmutableGridLocation is not supported;"); } 
		}
		[DataMember]
		public override int Y {
			get { return y; } 
			set { throw new NotSupportedException("Setting y coordinate of an ImmutableGridLocation is not supported;"); } 
		}

		public override void Set( GridLocation other ) {
			throw new NotSupportedException("Setting values of an ImmutableGridLocation is not supported;");
		}


		public override bool Equals(Object obj) {
			GridLocation loc = obj as GridLocation;
			if(loc == null) return false;
			if(ReferenceEquals(this, loc)) return true;
			return loc.Layer == layer && loc.X == x && loc.Y == y;
			/*
			if (obj == null) {
				return false;
			}
			if ( ReferenceEquals ( this, obj ) ) {
        		return true;
    		}
			if ( typeof(GridLocation).IsAssignableFrom( obj.GetType() ) ) {
				GridLocation other = (GridLocation) obj;
				return other.Layer == layer && other.X == x && other.Y == y;
			} else {
				return false;
			}
			*/
		}

		public override int GetHashCode() {
			int hash=13;
			hash = hash * 17 + layer;
			hash = hash * 17 + x;
			hash = hash * 17 + y;
			return hash;
		}

		public override string ToString (){
			return string.Format ("[Loc {0}, {1}, {2}]", layer, x, y);
		}
	}
}

