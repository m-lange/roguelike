﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map.LayeredGrid {

	public class Bounds {
		public int xmin;
		public int xmax;
		public int ymin;
		public int ymax;
		public int lmin;
		public int lmax;

		public Bounds() : this(0, 0, 0, 0, 0, 0) {

		}

		public Bounds(int xmin, int ymin, int xmax, int ymax) : this (0, 0, xmin, ymin, xmax, ymax) {
			
		}
		public Bounds(int lmin, int lmax, int xmin, int ymin, int xmax, int ymax) {
			this.lmin = lmin;
			this.lmax = lmax;
			this.xmin = xmin;
			this.ymin = ymin;
			this.xmax = xmax;
			this.ymax = ymax;
			//Debug.Log(xmin+" "+xmax+" "+GetWidth());
		}


		public int GetWidth() {
			return 1 + xmax - xmin;
		}
		public int GetHeight() {
			return 1 + ymax - ymin;
		}
		public int GetDepth() {
			return 1 + lmax - lmin;
		}

		public void Add(int xmin, int ymin, int xmax, int ymax) {
			if (xmin < this.xmin)
				this.xmin = xmin;
			if (ymin < this.ymin)
				this.ymin = ymin;
			if (xmax > this.xmax)
				this.xmax = xmax;
			if (ymax > this.ymax)
				this.ymax = ymax;
		}
		public void Add(int lmin, int lmax, int xmin, int ymin, int xmax, int ymax) {
			if (lmin < this.lmin)
				this.lmin = lmin;
			if (xmin < this.xmin)
				this.xmin = xmin;
			if (ymin < this.ymin)
				this.ymin = ymin;
			if (lmax > this.lmax)
				this.lmax = lmax;
			if (xmax > this.xmax)
				this.xmax = xmax;
			if (ymax > this.ymax)
				this.ymax = ymax;
		}

		public void Set(int xmin, int ymin, int xmax, int ymax) {
			Set (0, 0, xmin, ymin, xmax, ymax);
		}

		public void Set(int lmin, int lmax, int xmin, int ymin, int xmax, int ymax) {
			this.lmin = lmin;
			this.lmax = lmax;
			this.xmin = xmin;
			this.ymin = ymin;
			this.xmax = xmax;
			this.ymax = ymax;
		}

		public bool Contains(GridLocation pos) {
			return Contains(pos.Layer, pos.X, pos.Y);
		}
		public bool Contains(int l, int x, int y) {
			return l >= lmin && l <= lmax && x >= xmin && x <= xmax && y >= ymin && y <= ymax;
		}
		public bool Contains(int x, int y) {
			return x >= xmin && x <= xmax && y >= ymin && y <= ymax;
		}


		public bool Intersects(int xmin, int ymin, int xmax, int ymax) {
			if( xmax < this.xmin || xmin > this.xmax ) {
				return false;
			}
			if( ymax < this.ymin || ymin > this.ymax ) {
				return false;
			}
			return true;
		}

		public bool Equals(Bounds other) {
			return lmin == other.lmin && lmax == other.lmax
				&& xmin == other.xmin && xmax == other.xmax
				&& ymin == other.ymin && ymax == other.ymax;
		}

		public override string ToString (){
			return string.Format ("[Bounds {0} - {1}, {2} - {3}, {4} - {5}]", lmin, lmax, xmin, xmax, ymin, ymax);
		}
	}

}
