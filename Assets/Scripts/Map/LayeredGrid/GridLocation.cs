﻿using System;
using System.Runtime.Serialization;


namespace Map.LayeredGrid {

	[DataContract]
	public class GridLocation {

		protected int layer;
		protected int x;
		protected int y;

		public GridLocation(GridLocation other) {
			this.layer = other.Layer;
			this.x = other.X;
			this.y = other.Y;
		}
		public GridLocation(int layer, int x, int y) {
			this.layer = layer;
			this.x = x;
			this.y = y;
		}

		[DataMember]
		public virtual int Layer{ 
			get { return layer; } 
			set { layer = value; } 
		}
		[DataMember]
		public virtual int X { 
			get { return x; } 
			set { x = value; } 
		}
		[DataMember]
		public virtual int Y {
			get { return y; } 
			set { y = value; } 
		}

		public virtual void Set( GridLocation other ) {
			this.layer = other.layer;
			this.x = other.x;
			this.y = other.y;
		}
		public virtual void Set( int layer, int x, int y ) {
			this.layer = layer;
			this.x = x;
			this.y = y;
		}
		public int RectDistance(GridLocation other) {
			int dx = Math.Abs (x - other.x);
			int dy = Math.Abs (y - other.y);
			return (dx > dy) ? dx : dy;
		}
		public float Distance(GridLocation other) {
			int dx = x - other.x;
			int dy = y - other.y;
			return (float) Math.Sqrt( dx*dx + dy*dy );
		}
		public int DistanceSq(GridLocation other) {
			int dx = x - other.x;
			int dy = y - other.y;
			return dx*dx + dy*dy;
		}
		public float Distance3d(GridLocation other) {
			int dx = x - other.x;
			int dy = y - other.y;
			int dz = layer - other.layer;
			return (float) Math.Sqrt( dx*dx + dy*dy + dz*dz );
		}
		public int DistanceSq3d(GridLocation other) {
			int dx = x - other.x;
			int dy = y - other.y;
			int dz = layer - other.layer;
			return dx*dx + dy*dy + dz*dz;
		}
		
		public override bool Equals (object obj) {
			GridLocation loc = obj as GridLocation;
			if(loc == null) return false;
			if(ReferenceEquals(this, loc)) return true;
			return loc.Layer == layer && loc.X == x && loc.Y == y;
			/*
			if (obj == null) {
				return false;
			}
			if ( ReferenceEquals ( this, obj ) ) {
        		return true;
    		}
			if ( typeof(GridLocation).IsAssignableFrom( obj.GetType() ) ) {
				GridLocation other = (GridLocation) obj;
				return other.layer == layer && other.x == x && other.y == y;
			} else {
				return false;
			}
			*/
		}

		public override int GetHashCode() {
			int hash=13;
			hash = hash * 17 + layer;
			hash = hash * 17 + x;
			hash = hash * 17 + y;
			return hash;
		}

		public override string ToString (){
			return string.Format ("[Loc {0}, {1}, {2}]", layer, x, y);
		}
	}
}

