﻿using Map.LayeredGrid;

namespace Map.LayeredGrid.IO {

	public class MapEntity {

		public readonly string entityType;
		public readonly int entityCount;
		public readonly int layer = 0;
		public readonly GridLocation location = null;
		public readonly string[] types;

		public MapEntity(string entityType, int entityCount, int layer, string[] types) {
			this.entityType = entityType;
			this.entityCount = entityCount;
			this.layer = layer;
			this.types = types;
		}

		public MapEntity(string entityType, int entityCount, GridLocation location) {
			this.entityType = entityType;
			this.entityCount = entityCount;
			this.location = location;
		}
	}
}
