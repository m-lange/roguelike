﻿using System;
using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Impl;

namespace Map.LayeredGrid.IO {
	public class SimpleLayeredGridReader<T> {

		static string NO_DATA = "NODATA";
		static string LAYER = "LAYER";
		static string VISIBILITY = "VISIBILITY";
		static string ALIAS = "ALIAS";
		static string ENTITIES = "ENTITIES";
		static string LINK = "LINK";
		static string END = "END";



		public static BasicLayeredGrid<T> Read (string path, string file, Func<int, bool, T> tileGenerator, Func<T, int> isLink, Dictionary<string, byte> tileInfo, out List<MapEntity> entities, out CharacterMapping mapping)
		{
			string text = System.IO.File.ReadAllText (path + "/" + file);

			string[] lines = text.Split ( new string[]{Environment.NewLine}, StringSplitOptions.None );

			int noData = -1;
			mapping = null;

			Dictionary<char, int> alias = new Dictionary<char, int> ();
			entities = new List<MapEntity> ();
			List<GridLink> links = new List<GridLink> ();
			List<List<string>> layers = new List<List<string>> ();
			List<List<string>> visibility = new List<List<string>> ();
			List<int> layerLevels = new List<int> ();
			List<int> layerXOffset = new List<int> ();
			List<int> layerYOffset = new List<int> ();

			char[] space = new char[]{ ' ' };

			for (int i = 0; i < lines.Length; i++) {
				string line = lines [i];
				if (line.StartsWith (NO_DATA)) {
					noData = int.Parse (line.Split (space, System.StringSplitOptions.RemoveEmptyEntries) [1].Trim ());
				} else if (line.StartsWith (ALIAS)) {
					List<string> aliases = NonKeywordLines (lines, i + 1);
					foreach (string l in aliases) {
						char c = l [0];
						string al = l.Substring (1).Trim ();
						//UnityEngine.Debug.Log (c+" "+al);
						if (alias.ContainsKey (c)) {
							throw new FormatException ("Duplicate character " + c + " in alias list (" + alias [c] + ", " + al + ").");
						}
						alias [c] = tileInfo [al];

					}
					mapping = new CharacterMapping (alias);
				} else if (line.StartsWith (ENTITIES)) {
					List<string> aliases = NonKeywordLines (lines, i + 1);
					foreach (string l in aliases) {
						if (l.Trim ().Length > 0) {
							string[] parts = l.Trim ().Split (new char[]{ '|' }, StringSplitOptions.RemoveEmptyEntries);
							string[] ent = parts [0].Trim ().Split (space, StringSplitOptions.RemoveEmptyEntries);
							string[] condition = null;
							if (parts.Length > 1) {
								condition = parts [1].Trim ().Split (space, StringSplitOptions.RemoveEmptyEntries);
								for (int c = 0; c < condition.Length; c++) {
									condition [c] = condition [c].Trim ();
								}
							}
							if (ent.Length == 3) {
								entities.Add (new MapEntity (
									ent [0].Trim (), 
									int.Parse (ent [1].Trim ()), 
									int.Parse (ent [2].Trim ()),
									condition));
							} else if (ent.Length == 5) {
								entities.Add (new MapEntity (
									ent [0].Trim (), 
									int.Parse (ent [1].Trim ()), 
									new GridLocation (int.Parse (ent [2].Trim ()), 
										int.Parse (ent [3].Trim ()), 
										int.Parse (ent [4].Trim ()))));
							} else {
								throw new FormatException ("Entity entry (" + l + ") must have 2 or 4 numbers");
							}
						}
					}
				} else if (isLink == null && line.StartsWith (LINK)) {
					List<string> linkLines = NonKeywordLines (lines, i + 1);
					foreach (string l in linkLines) {
						if (l.Trim ().Length > 0) {
							string[] split = l.Split (space, System.StringSplitOptions.RemoveEmptyEntries);
							GridLocation src = new GridLocation (int.Parse (split [0].Trim ()), int.Parse (split [1].Trim ()), int.Parse (split [2].Trim ()));
							GridLocation trg = new GridLocation (int.Parse (split [3].Trim ()), int.Parse (split [4].Trim ()), int.Parse (split [5].Trim ()));
							links.Add (new GridLink (src, trg));
							links.Add (new GridLink (trg, src));
						}
					}
				} else if (line.StartsWith (LAYER)) {
					string[] levelTokens = line.Split (space, System.StringSplitOptions.RemoveEmptyEntries);
					int level = int.Parse (levelTokens [1].Trim ());
					List<string> levelLines = NonKeywordLines (lines, i + 1);
					layers.Add (levelLines);
					layerLevels.Add (level);
					if (levelTokens.Length == 4) {
						layerXOffset.Add (int.Parse (levelTokens [2].Trim ()));
						layerYOffset.Add (int.Parse (levelTokens [3].Trim ()));
					} else {
						layerXOffset.Add (0);
						layerYOffset.Add (0);
					}
				} else if (line.StartsWith (VISIBILITY)) {
					List<string> levelLines = NonKeywordLines (lines, i + 1);
					visibility.Add (levelLines);
				}
			}
			BasicLayeredGrid<T> grid = new BasicLayeredGrid<T> (tileGenerator (noData, false));

			for (int i = 0; i < layers.Count; i++) {
				int layer = layerLevels[i];
				List<string> layerString = layers [i];
				layerString.Reverse ();
				List<string> visibilityString = null;
				if (visibility.Count > i) {
					visibilityString = visibility [i];
					visibilityString.Reverse ();
				}

				int height = layerString.Count;
				int width = 0;
				foreach (string l in layerString) {
					if (l.Length > width)
						width = l.Length;
				}
				int[,] arr = new int[width, height];
				bool[,] vis = new bool[width, height];

				int xmin = width;
				int xmax = 0;
				int ymin = height;
				int ymax = 0;
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						arr [x, y] = noData;
						vis [x, y] = false;
						string line = layerString [y];
						if (line.Length > x) {
							char c = line [x];
							if (alias.ContainsKey (c)) {
								arr [x, y] = alias [c];
							}
						}

						if (visibilityString != null && visibilityString.Count > y) {
							string visLine = visibilityString [y];
							if (visLine.Length > x) {
								char c = visLine [x];
								vis [x, y] = (c == '+');
							}
						}

						if (!arr [x, y].Equals (noData)) {
							if (x < xmin)
								xmin = x;
							if (x > xmax)
								xmax = x;
							if (y < ymin)
								ymin = y;
							if (y > ymax)
								ymax = y;
						}
					}
				}
				//Debug.Log (xmin + "-" + xmax + ", " + ymin + "-" + ymax);
				int xOffs = layerXOffset[i];
				int yOffs = layerYOffset[i];
				T[,] data = new T[xmax - xmin + 1, ymax - ymin + 1];
				for (int x = xmin; x <= xmax; x++) {
					for (int y = ymin; y <= ymax; y++) {
						T t = tileGenerator(arr[x,y], vis[x,y]);
						data[x-xmin, y-ymin] = t;
						if (isLink != null) {
							int lnk = isLink(t);
							if(lnk > 0) {
								GridLocation src = new GridLocation (layer, x+xOffs, y+yOffs);
								GridLocation trg = new GridLocation (layer+1, x+xOffs, y+yOffs);
								links.Add (new GridLink (src, trg));
							} else if(lnk < 0) {
								GridLocation src = new GridLocation (layer, x+xOffs, y+yOffs);
								GridLocation trg = new GridLocation (layer-1, x+xOffs, y+yOffs);
								links.Add (new GridLink (src, trg));
							}
						}
					}
				}
				SimpleGrid<T> sGrid = new SimpleGrid<T> (xOffs+xmin, yOffs+ymin, data, tileGenerator( noData, false ), false);

				grid.AddLayer (sGrid, layerLevels [i]);
			}
			foreach (GridLink link in links) {
				grid.AddDirectedLink ( link.Source, link.Target );
			}
			//UnityEngine.Debug.Log (grid.BottomLayer);
			return grid;
		}

		static List<string> NonKeywordLines(string[] lines, int startIndex) {
			List<string> result = new List<string> ();
			for (int i = startIndex; i < lines.Length; i++) {
				string line = lines [i];
				if (line.StartsWith (NO_DATA) 
				|| line.StartsWith (LAYER) 
				|| line.StartsWith (VISIBILITY) 
				|| line.StartsWith (ALIAS) 
				|| line.StartsWith (LINK) 
				|| line.StartsWith (ENTITIES) 
				|| line.StartsWith (END)) {
					break;
				} else {
					result.Add (line);
				}
			}
			return result;
		}

		public static void Write (BasicLayeredGrid<TileInfo> map, string path, string file, TileTypeInfo[] tileInfo, CharacterMapping mapping) {
			//string text = "NODATA "+map.NoData.Type+"\n";

			string nl = Environment.NewLine;
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			sb.Append ("NODATA " + map.NoData.Type + nl);
			sb.Append ("ALIAS");
			sb.Append (nl);
			foreach (KeyValuePair<char, int> kv in mapping.CharToInt) {
				sb.Append (kv.Key + " " + tileInfo [kv.Value].Id + nl);
			}

			int lmin = map.BottomLayer;
			int lmax = map.TopLayer;
			for (int l = lmin; l <= lmax; l++) {
				Grid<TileInfo>[] layer = map.GetLayer (l);
				foreach (Grid<TileInfo> grid in layer) {
					sb.Append (string.Format ("LAYER {0} {1} {2}", l, grid.Xll, grid.Yll));
					sb.Append (nl);
					for (int y = grid.Yll + grid.Height - 1; y >= grid.Yll; y--) {
						for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
							TileInfo info = grid [x, y];
							//if (!mapping.IntToChar.ContainsKey (info.Type)) {
							//	UnityEngine.Debug.Log(info.Type);
							//}
							sb.Append( mapping.IntToChar[ info.Type ] );
							//text += mapping.IntToChar[ info.Type ];
						}
						sb.Append( nl );
						//text += "\n";
					}
					sb.Append("VISIBILITY");
					sb.Append( nl );
					for (int y = grid.Yll + grid.Height - 1; y >= grid.Yll; y--) {
						for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
							TileInfo info = grid [x, y];
							//text += info.Seen ? "+" : "-";
							sb.Append(info.Seen ? "+" : "-");
						}
						sb.Append( nl );
						//text += "\n";
					}
				}
			}

			HashSet<GridLink> links = new HashSet<GridLink> ();
			if (map.Links.Count > 0) {
				sb.Append("LINK");
				sb.Append( nl );
				foreach (GridLink link in map.Links.Values) {
					GridLink other = new GridLink (link.Target, link.Source);
					if (!links.Contains (other)) {
						links.Add (link);
						sb.Append(string.Format("{0} {1} {2}  {3} {4} {5}", 
							link.Source.Layer, link.Source.X, link.Source.Y, 
							link.Target.Layer, link.Target.X, link.Target.Y));
						sb.Append( nl );
					}
				}
			}
			sb.Append("END");
			sb.Append( nl );
			System.IO.File.WriteAllText(path+"/"+file, sb.ToString());

		}

		
	}
}
