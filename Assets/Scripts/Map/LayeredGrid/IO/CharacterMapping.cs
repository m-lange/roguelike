﻿using System.Collections;
using System.Collections.Generic;

namespace Map.LayeredGrid.IO {
	public class CharacterMapping {

		public readonly Dictionary<char, int> CharToInt;
		public readonly Dictionary<int, char> IntToChar;

		public CharacterMapping(Dictionary<char, int> alias) {
			CharToInt = alias;
			IntToChar = new Dictionary<int, char> ();
			foreach (KeyValuePair<char, int> kv in alias) {
				IntToChar [kv.Value] = kv.Key;
			}
		}

	}
}
