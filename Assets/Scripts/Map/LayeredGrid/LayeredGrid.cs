﻿using System;
using System.Collections.Generic;

namespace Map.LayeredGrid {
	public abstract class LayeredGrid<T> {

		public T this[int layer, int x, int y] {
			get { return Get(layer, x,y); }
			set { Set(layer,x,y,value); }
		}
		public T this[GridLocation loc] {
			get { return Get(loc); }
			set { Set(loc,value); }
		}

		public abstract T Get (int layer, int x, int y);
		public abstract T Get (GridLocation loc);
		public abstract Grid<T>[] GetLayer (int layer);

		public abstract void Set(int layer, int x, int y, T value);
		public abstract void Set(GridLocation loc, T value);

		public abstract bool Contains(int layer, int x, int y);
		public abstract bool Contains(GridLocation loc);

		public abstract bool ContainsExact(int layer, int x, int y);
		public abstract bool ContainsExact(GridLocation loc);

		public abstract bool Intersects (int layer, int xmin, int ymin, int xmax, int ymax);

		public abstract GridLink GetLink ( GridLocation source );
		public abstract GridLink GetLink ( int layer, int x, int y );

		public abstract void AddLink ( GridLocation source, GridLocation target );
		public abstract void AddDirectedLink ( GridLocation source, GridLocation target );


		public abstract Dictionary<GridLocation, GridLink> Links { get; }

		//public abstract int Width { get; }
		//public abstract int Height { get; }

		public abstract LayeredGrid.Bounds Bounds { get; }
		public abstract int TopLayer { get; }
		public abstract int BottomLayer { get; }
		public abstract int LayerCount { get; }
		public abstract T NoData { get; }
		public abstract bool IsNoData (T value);

		public abstract List<GridLocation> GetAll (int layer, Func<T, bool> filter);
		public abstract List<GridLocation> GetAll (int layer, Func<T, int, int, bool> filter);

		public abstract LayeredGrid<T> Clone ();

	}
}
