﻿using System;

namespace Map.LayeredGrid {

	public class GridLink {

		GridLocation source;
		GridLocation target;


		public GridLocation Source { get { return source; } }
		public GridLocation Target { get { return target; } }

		public GridLink(GridLocation source, GridLocation target) {
			this.source = source;
			this.target = target;
		}

		public override bool Equals(Object obj) {
			if ( typeof(GridLink).IsAssignableFrom( obj.GetType() ) ) {
				GridLink other = (GridLink) obj;
				return source.Equals(other.source) && target.Equals(other.target);
			} else {
				return false;
			}
		}

		public override int GetHashCode() {
			int hash=13;
			hash = hash * 17 + source.GetHashCode();
			hash = hash * 17 + target.GetHashCode();
			return hash;
		}

	}
}
