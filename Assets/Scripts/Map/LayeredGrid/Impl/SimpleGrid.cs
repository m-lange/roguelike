﻿using System;


namespace Map.LayeredGrid.Impl {
	public class SimpleGrid<T> : Grid<T> {

		private int width;
		private int height;
		private int xll;
		private int yll;
		private T[,] data;
		private T noData;
		private Bounds bounds;

		public SimpleGrid(int xll, int yll, int width, int height, Func<int, int, T> generator, T noData) {
			this.xll = xll;
			this.yll = yll;
			this.width = width;
			this.height = height;
			this.noData = noData;
			data = new T[width, height];
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					data [x,y] = generator(x+xll,y+yll);
				}
			}
			bounds = new Bounds (xll, yll, xll+width-1, yll+height-1);
		}

		public SimpleGrid (int xll, int yll, T[,] data, T noData, bool autoCrop)
		{
			this.noData = noData;
			int xOffset = 0;
			int yOffset = 0;
			int width = data.GetLength (0);
			int height = data.GetLength (1);
			if (autoCrop) {
				int xmin = data.GetLength (0) - 1;
				int ymin = data.GetLength (1) - 1;
				int xmax = 0;
				int ymax = 0;

				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						if (!data [x, y].Equals (noData)) {
							if (x < xmin)
								xmin = x;
							if (x > xmax)
								xmax = x;
							if (y < ymin)
								ymin = y;
							if (y > ymax)
								ymax = y;
						}
					}
				}
				//UnityEngine.Debug.Log("Bounds: "+xmin+" "+ymin+" "+ymin+" "+ymax);
				if (xmax >= xmin && ymax >= ymin) {
					xOffset = xmin;
					yOffset = ymin;
					width  = (xmax - xmin) + 1;
					height = (ymax - ymin) + 1;
				}
				//UnityEngine.Debug.Log("Cropping: "+xOffset+" "+yOffset);
			}

			this.xll = xll + xOffset;
			this.yll = yll + yOffset;
			this.width = width;
			this.height = height;
			this.data = new T[width, height];
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					this.data [x,y] = data[x+xOffset,y+yOffset];
				}
			}
			bounds = new Bounds (this.xll, this.yll, this.xll+width-1, this.yll+height-1);
		}


		public override int Width { get{ return width; } }
		public override int Height { get{ return height; } }
		public override int Xll { get{ return xll; } }
		public override int Yll { get{ return yll; } }

		public override Bounds Bounds { get{ return bounds; } }

		public override T Get (int x, int y) { return data [x-xll, y-yll]; }
		public override void Set(int x, int y, T value) { data [x-xll, y-yll] = value; }
		public override bool Contains(int x, int y) {
			return x >= xll && y >= yll && x < xll+width && y < yll+height;
		}

		public override bool Intersects(int xmin, int ymin, int xmax, int ymax) {
			if( xmax < this.xll || xmin > this.xll+width ) {
				return false;
			}
			if( ymax < this.yll || ymin > this.yll+height ) {
				return false;
			}
			return true;
		}

		public override T NoData { get{ return noData; } }

		public override bool IsNoData (T value) {
			return value.Equals (noData);
		}

		public override Grid<T> Clone () {
			return new SimpleGrid<T> (xll, yll, data, noData, false);
		}

	}
}
