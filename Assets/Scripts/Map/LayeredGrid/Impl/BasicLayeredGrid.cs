﻿using System;
using System.Collections.Generic;

namespace Map.LayeredGrid.Impl {
	public class BasicLayeredGrid<T> : LayeredGrid<T> {

		int bottomLayer;
		SimpleGrid<T>[] layers;
		LayeredGrid.Bounds[] layerBounds;

		T noData;
		private Bounds bounds;

		Dictionary<GridLocation,GridLink> links = new Dictionary<GridLocation,GridLink> ();


		public BasicLayeredGrid(T noData) {
			this.noData = noData;
			bottomLayer = 0;
			layers = new SimpleGrid<T>[0];
			layerBounds = new LayeredGrid.Bounds[0];

			//bounds = new Bounds ();
		}

		public BasicLayeredGrid(T noData, SimpleGrid<T>[] layers, int bottomLayer) {
			this.noData = noData;
			this.bottomLayer = bottomLayer;
			this.layers = new SimpleGrid<T>[layers.Length];
			this.layerBounds = new LayeredGrid.Bounds[layers.Length];

			for (int l = 0; l < layers.Length; l++) {
				LayeredGrid.Bounds bounds = null;
				SimpleGrid<T> layer = (SimpleGrid<T>) layers [l].Clone ();
				this.layers[l] = layer;
				bounds = new LayeredGrid.Bounds (layer.Xll, layer.Yll, layer.Xll+layer.Width, layer.Yll+layer.Height);

				this.layerBounds [l] = new LayeredGrid.Bounds (layer.Xll, layer.Yll, layer.Xll+layer.Width, layer.Yll+layer.Height);

				if (this.bounds == null) {
					this.bounds = new Bounds (l-bottomLayer, l-bottomLayer, bounds.xmin, bounds.ymin, bounds.xmax, bounds.ymax);
				} else {
					this.bounds.Add (l-bottomLayer, l-bottomLayer, bounds.xmin, bounds.ymin, bounds.xmax, bounds.ymax);
				}
			}
		}

		public void AddLayer (SimpleGrid<T> grid, int layer) {
			int l = layer - bottomLayer;
			if (l > 0 && l < layers.Length - 1 && layers [l] != null) {
				throw new ArgumentException("The map already has layer "+layer);
			}
			if (l < 0) {
				SimpleGrid<T>[] newLayers = new SimpleGrid<T>[layers.Length - l];
				LayeredGrid.Bounds[] newBounds = new LayeredGrid.Bounds[layers.Length - l];
				for (int i = 0; i < newLayers.Length; i++) {
					if (i < -l) {
						newLayers [i] = null;
						newBounds [i] = null;
					} else {
						newLayers [i] = layers [i+l];
						newBounds [i] = layerBounds [i+l];
					}
				}
				layers = newLayers;
				layerBounds = newBounds;
				bottomLayer += l;
				l = layer - bottomLayer;
			} else if (l >= layers.Length) {
				SimpleGrid<T>[] newLayers = new SimpleGrid<T>[l+1];
				LayeredGrid.Bounds[] newBounds = new LayeredGrid.Bounds[l+1];
				for (int i = 0; i < newLayers.Length; i++) {
					if (i < layers.Length) {
						newLayers [i] = layers [i];
						newBounds [i] = layerBounds [i];
					} else {
						newLayers [i] = null;
						newBounds [i] = null;
					}
				}
				layers = newLayers;
				layerBounds = newBounds;
			}
			layers[l] = grid;
			LayeredGrid.Bounds lBounds = new LayeredGrid.Bounds (grid.Xll, grid.Yll, grid.Xll + grid.Width, grid.Yll + grid.Height);
			layerBounds [l] = lBounds;

			if (this.bounds == null) {
				this.bounds = new Bounds (layer, layer, lBounds.xmin, lBounds.ymin, lBounds.xmax, lBounds.ymax);
			} else {
				this.bounds.Add (layer, layer, lBounds.xmin, lBounds.ymin, lBounds.xmax, lBounds.ymax);
			}
		}

		public override T Get (GridLocation loc) {
			int l = loc.Layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return noData;
			}
			SimpleGrid<T> lay = layers[l];
			if (lay.Contains (loc.X, loc.Y)) {
				return lay.Get( loc.X, loc.Y );
			}
			return noData;
		}

		public override T Get (int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return noData;
			}
			SimpleGrid<T> lay = layers[l];
			if (lay.Contains (x, y)) {
				return lay.Get( x, y );
			}
			return noData;
		}

		public override Grid<T>[] GetLayer (int layer) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return new Grid<T>[0];
			}
			return new Grid<T>[] { layers [l] };
		}


		public override void Set(GridLocation loc, T value) {
			Set (loc.Layer, loc.X, loc.Y, value);
		}
		public override void Set(int layer, int x, int y, T value) {
			int l = layer - bottomLayer;
			//bool found = false;
			SimpleGrid<T> lay = layers[l];
			if (lay.Contains (x, y)) {
				lay.Set(x, y, value);
			} else {
				throw new ArgumentException ("Grid location ("+layer+", "+x+", "+y+") out of coverage.");
			}
		}


		public override bool ContainsExact(GridLocation loc) {
			return ContainsExact (loc.Layer, loc.X, loc.Y);
		}

		public override bool ContainsExact(int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return false;
			}
			if (layers[l].Contains (x, y)) {
				return true;
			}
			return false;
		}

		public override bool Contains(GridLocation loc) {
			return Contains (loc.Layer, loc.X, loc.Y);
		}

		public override bool Contains(int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return false;
			}
			return layerBounds[l].Contains(x,y);
		}


		public override bool Intersects(int layer, int xmin, int ymin, int xmax, int ymax) {
			int l = layer - bottomLayer;
			return layerBounds[l].Intersects(xmin, ymin, xmax, ymax);
		}


		public override GridLink GetLink ( GridLocation source ) {
			if ( ! links.ContainsKey (source) ) {
				return null;
			} else {
				return links[source];
			}
		}
		public override GridLink GetLink ( int layer, int x, int y ) {
			return GetLink (new GridLocation (layer, x, y));
		}

		public override void AddLink ( GridLocation source, GridLocation target ) {
			ImmutableGridLocation src = new ImmutableGridLocation (source);
			ImmutableGridLocation trg = new ImmutableGridLocation (target);

			links [source] = new GridLink (src, trg);
			links [target] = new GridLink (trg, src);
		}
		public override void AddDirectedLink ( GridLocation source, GridLocation target ) {
			ImmutableGridLocation src = new ImmutableGridLocation (source);
			ImmutableGridLocation trg = new ImmutableGridLocation (target);
			links [source] = new GridLink (src, trg);
		}


		public override Dictionary<GridLocation, GridLink> Links { get{ return links; } }


		public override int TopLayer { get { return bottomLayer + layers.Length - 1; } }
		public override int BottomLayer { get { return bottomLayer; } }
		public override int LayerCount { get{ return layers.Length; } }

		public override T NoData { get{ return noData; } }
		public override bool IsNoData (T value) {
			return value.Equals (noData);
		}

		public override Bounds Bounds { get{ return bounds; } }



		public override LayeredGrid<T> Clone () {
			return new BasicLayeredGrid<T> (noData, layers, bottomLayer);
		}


		public override List<GridLocation> GetAll( int layer, Func<T, bool> filter ) {
			//int l = layer - bottomLayer;
			List<GridLocation> result = new List<GridLocation> ();
			Grid<T>[] layers = GetLayer (layer);
			Grid<T> grid;
			int len = layers.Length;
			for(int i=0; i<len; i++) {
				grid = layers[i];
				for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
					for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
						T info = grid.Get( x, y );
						if (filter (info)) {
							result.Add (new GridLocation(layer, x, y));
						}
					}
				}
			}
			return result;
		}

		public override List<GridLocation> GetAll( int layer, Func<T, int, int, bool> filter ) {
			//int l = layer - bottomLayer;
			List<GridLocation> result = new List<GridLocation> ();
			Grid<T>[] layers = GetLayer (layer);
			Grid<T> grid;
			int len = layers.Length;
			for(int i=0; i<len; i++) {
				grid = layers[i];
				for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
					for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
						T info = grid.Get( x, y );
						if (filter (info, x, y)) {
							result.Add (new GridLocation(layer, x, y));
						}
					}
				}
			}
			return result;
		}
	}
}
