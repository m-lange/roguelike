﻿using System;
using System.Collections.Generic;

namespace Map.LayeredGrid.Impl {
	public class SimpleLayeredGrid<T> : LayeredGrid<T> {

		int bottomLayer;
		SimpleGrid<T>[][] layers;
		LayeredGrid.Bounds[] layerBounds;

		T noData;
		private Bounds bounds;

		Dictionary<GridLocation,GridLink> links = new Dictionary<GridLocation,GridLink> ();


		public SimpleLayeredGrid(T noData) {
			this.noData = noData;
			bottomLayer = 0;
			layers = new SimpleGrid<T>[0][];
			layerBounds = new LayeredGrid.Bounds[0];

			//bounds = new Bounds ();
		}

		public SimpleLayeredGrid(T noData, SimpleGrid<T>[][] layers, int bottomLayer) {
			this.noData = noData;
			this.bottomLayer = bottomLayer;
			this.layers = new SimpleGrid<T>[layers.Length][];
			this.layerBounds = new LayeredGrid.Bounds[layers.Length];

			for (int l = 0; l < layers.Length; l++) {
				SimpleGrid<T>[] lay = new SimpleGrid<T>[layers[l].Length];
				LayeredGrid.Bounds bounds = null;

				for (int i = 0; i < lay.Length; i++) {
					SimpleGrid<T> layer = (SimpleGrid<T>) layers [l] [i].Clone ();
					lay [i] = layer;
					if (bounds == null) {
						bounds = new LayeredGrid.Bounds (layer.Xll, layer.Yll, layer.Xll+layer.Width, layer.Yll+layer.Height);
					} else {
						bounds.Add (layer.Xll, layer.Yll, layer.Xll+layer.Width, layer.Yll+layer.Height);
					}
				}
				this.layers [l] = lay;
				this.layerBounds [l] = bounds;

				if (this.bounds == null) {
					this.bounds = new Bounds (l-bottomLayer, l-bottomLayer, bounds.xmin, bounds.ymin, bounds.xmax, bounds.ymax);
				} else {
					this.bounds.Add (l-bottomLayer, l-bottomLayer, bounds.xmin, bounds.ymin, bounds.xmax, bounds.ymax);
				}
			}
		}

		public void AddLayer(SimpleGrid<T> grid, int layer) {
			int l = layer - bottomLayer;
			if (l < 0) {
				SimpleGrid<T>[][] newLayers = new SimpleGrid<T>[layers.Length - l][];
				LayeredGrid.Bounds[] newBounds = new LayeredGrid.Bounds[layers.Length - l];
				for (int i = 0; i < newLayers.Length; i++) {
					if (i < -l) {
						newLayers [i] = new SimpleGrid<T>[0];
						newBounds [i] = null;
					} else {
						newLayers [i] = layers [i+l];
						newBounds [i] = layerBounds [i+l];
					}
				}
				layers = newLayers;
				layerBounds = newBounds;
				bottomLayer += l;
				l = layer - bottomLayer;
			} else if (l >= layers.Length) {
				SimpleGrid<T>[][] newLayers = new SimpleGrid<T>[l+1][];
				LayeredGrid.Bounds[] newBounds = new LayeredGrid.Bounds[l+1];
				for (int i = 0; i < newLayers.Length; i++) {
					if (i < layers.Length) {
						newLayers [i] = layers [i];
						newBounds [i] = layerBounds [i];
					} else {
						newLayers [i] = new SimpleGrid<T>[0];
						newBounds [i] = null;
					}
				}
				layers = newLayers;
				layerBounds = newBounds;
			}
			SimpleGrid<T>[] oldLayer = layers [l];
			SimpleGrid<T>[] newLayer = new SimpleGrid<T>[oldLayer.Length + 1];
			for (int i = 0; i < oldLayer.Length; i++) {
				newLayer [i] = oldLayer [i];
			}
			newLayer [oldLayer.Length] = grid;
			LayeredGrid.Bounds lBounds = layerBounds [l];
			if (lBounds == null) {
				lBounds = new LayeredGrid.Bounds (grid.Xll, grid.Yll, grid.Xll + grid.Width, grid.Yll + grid.Height);
			} else {
				lBounds.Add (grid.Xll, grid.Yll, grid.Xll + grid.Width, grid.Yll + grid.Height);
			}
			layerBounds [l] = lBounds;
			layers [l] = newLayer;
			if (this.bounds == null) {
				this.bounds = new Bounds (layer, layer, lBounds.xmin, lBounds.ymin, lBounds.xmax, lBounds.ymax);
			} else {
				this.bounds.Add (layer, layer, lBounds.xmin, lBounds.ymin, lBounds.xmax, lBounds.ymax);
			}
		}

		public override T Get (GridLocation loc) {
			return Get (loc.Layer, loc.X, loc.Y);
		}
		public override T Get (int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return noData;
			}
			SimpleGrid<T>[] allLayers = layers [l];
			if (allLayers == null || allLayers.Length == 0) {
				return noData;
			}
			int len = allLayers.Length;
			SimpleGrid<T> lay;
			for(int i=0; i<len;i++) {
				lay = allLayers[i];
				if (lay.Contains (x, y)) {
					T v = lay [x, y];
					if (! v.Equals(noData)) {
						return v;
					}
				}
			}
			return noData;
		}

		public override Grid<T>[] GetLayer (int layer) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return new Grid<T>[0];
			}
			return layers [l];
		}


		public override void Set(GridLocation loc, T value) {
			Set (loc.Layer, loc.X, loc.Y, value);
		}
		public override void Set(int layer, int x, int y, T value) {
			int l = layer - bottomLayer;
			bool found = false;

			SimpleGrid<T>[] allLayers = layers[l];
			int len = allLayers.Length;
			SimpleGrid<T> lay;
			for(int i=0; i<len;i++) {
				lay = allLayers[i];
				if (lay.Contains (x, y)) {
					lay [x, y] = value;
					found = true;
					break;
				}
			}
			if (!found) {
				throw new ArgumentException ("Grid location ("+layer+", "+x+", "+y+") out of coverage.");
			}
		}


		public override bool ContainsExact(GridLocation loc) {
			return ContainsExact (loc.Layer, loc.X, loc.Y);
		}

		public override bool ContainsExact(int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return false;
			}

			SimpleGrid<T>[] allLayers = layers[l];
			int len = allLayers.Length;
			for(int i=0; i<len;i++) {
				if (allLayers[i].Contains (x, y)) {
					return true;
				}
			}
			return false;
		}

		public override bool Contains(GridLocation loc) {
			return Contains (loc.Layer, loc.X, loc.Y);
		}

		public override bool Contains(int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return false;
			}
			return layerBounds[l].Contains(x,y);
		}


		public override bool Intersects(int layer, int xmin, int ymin, int xmax, int ymax) {
			int l = layer - bottomLayer;
			return layerBounds[l].Intersects(xmin, ymin, xmax, ymax);
		}


		public override GridLink GetLink ( GridLocation source ) {
			if ( ! links.ContainsKey (source) ) {
				return null;
			} else {
				return links[source];
			}
		}
		public override GridLink GetLink ( int layer, int x, int y ) {
			return GetLink (new GridLocation (layer, x, y));
		}

		public override void AddLink ( GridLocation source, GridLocation target ) {
			ImmutableGridLocation src = new ImmutableGridLocation (source);
			ImmutableGridLocation trg = new ImmutableGridLocation (target);

			links [source] = new GridLink (src, trg);
			links [target] = new GridLink (trg, src);
		}
		public override void AddDirectedLink ( GridLocation source, GridLocation target ) {
			ImmutableGridLocation src = new ImmutableGridLocation (source);
			ImmutableGridLocation trg = new ImmutableGridLocation (target);
			links [source] = new GridLink (src, trg);
		}


		public override Dictionary<GridLocation, GridLink> Links { get{ return links; } }


		public override int TopLayer { get { return bottomLayer + layers.Length - 1; } }
		public override int BottomLayer { get { return bottomLayer; } }
		public override int LayerCount { get{ return layers.Length; } }

		public override T NoData { get{ return noData; } }
		public override bool IsNoData (T value) {
			return value.Equals (noData);
		}

		public override Bounds Bounds { get{ return bounds; } }



		public override LayeredGrid<T> Clone () {
			return new SimpleLayeredGrid<T> (noData, layers, bottomLayer);
		}


		public override List<GridLocation> GetAll( int layer, Func<T, bool> filter ) {
			//int l = layer - bottomLayer;
			List<GridLocation> result = new List<GridLocation> ();
			Grid<T>[] layers = GetLayer (layer);
			Grid<T> grid;
			int len = layers.Length;
			for(int i=0; i<len; i++) {
				grid = layers[i];
				for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
					for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
						T info = grid [x, y];
						if (filter (info)) {
							result.Add (new GridLocation(layer, x, y));
						}
					}
				}
			}
			return result;
		}


		public override List<GridLocation> GetAll( int layer, Func<T, int, int, bool> filter ) {
			//int l = layer - bottomLayer;
			List<GridLocation> result = new List<GridLocation> ();
			Grid<T>[] layers = GetLayer (layer);
			Grid<T> grid;
			int len = layers.Length;
			for(int i=0; i<len; i++) {
				grid = layers[i];
				for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
					for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
						T info = grid [x, y];
						if (filter (info, x, y)) {
							result.Add (new GridLocation(layer, x, y));
						}
					}
				}
			}
			return result;
		}
		/*
		class LayerBounds {

			public LayerBounds(int xmin, int ymin, int xmax, int ymax) {
				this.xmin = xmin;
				this.ymin = ymin;
				this.xmax = xmax;
				this.ymax = ymax;
			}

			public int xmin;
			public int ymin;
			public int xmax;
			public int ymax;

			public void Add(SimpleGrid<T> layer) {
				if (layer.Xll < xmin)
					xmin = layer.Xll;
				if (layer.Yll < ymin)
					ymin = layer.Yll;
				if (layer.Xll+layer.Width > xmax)
					xmax = layer.Xll+layer.Width;
				if (layer.Yll+layer.Height > ymax)
					ymax = layer.Yll+layer.Height;
			}

			public bool Contains(int x, int y) {
				return x >= xmin && y >= ymin && x <= xmax && y <= ymax;
			}

			public bool Intersects(int xmin, int ymin, int xmax, int ymax) {
				if( xmax < this.xmin || xmin > this.xmax ) {
					return false;
				}
				if( ymax < this.ymin || ymin > this.ymax ) {
					return false;
				}
				return true;
			}
		}
		*/
	}
}
