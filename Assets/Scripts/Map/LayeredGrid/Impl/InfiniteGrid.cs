﻿using System;


namespace Map.LayeredGrid.Impl {
	public class InfiniteGrid<T> : Grid<T> {

		private int tileSize;
		private int xll;
		private int yll;
		private T noData;
		private Bounds bounds;

		private SimpleGrid<T>[,] tiles;


		public InfiniteGrid(int tileSize, T noData) {
			this.tileSize = tileSize;
			this.noData = noData;
			this.xll = 0;
			this.yll = 0;
			tiles = new SimpleGrid<T>[0, 0];
			bounds = new Bounds ();
		}

		public InfiniteGrid(int xll, int yll, int tileSize, T noData, SimpleGrid<T>[,] tiles) {
			this.tileSize = tileSize;
			this.noData = noData;
			this.xll = xll;
			this.yll = yll;
			this.tiles = new SimpleGrid<T>[tiles.GetLength(0), tiles.GetLength(1)];
			for (int tx = 0; tx < tiles.GetLength (0); tx++) {
				for (int ty = 0; ty < tiles.GetLength (1); ty++) {
					this.tiles [tx, ty] = (SimpleGrid<T>) tiles [tx, ty].Clone ();
				}
			}
			bounds = new Bounds (xll, yll, xll + tiles.GetLength(0) * tileSize, tiles.GetLength(1) * tileSize);
		}


		public override int Width { get{ return tiles.GetLength(0) * tileSize; } }
		public override int Height { get{ return tiles.GetLength(1) * tileSize; } }
		public override int Xll { get{ return xll; } }
		public override int Yll { get{ return yll; } }
		public int TileSize { get{ return tileSize; } }

		public override Bounds Bounds { get { return bounds; } }

		public override T Get (int x, int y) {
			int tx = ToTile(x, xll);
			int ty = ToTile(y, yll);
			int ix = ToIndex(x, xll);
			int iy = ToIndex(y, yll);
			if(ContainsTile(tx,ty)) {
				SimpleGrid<T> tile = tiles[tx, ty];
				if (tile != null) {
					return tile [ix, iy];
				} else {
					return noData;
				}
			} else {
				return noData;
			}
		}
		public override void Set(int x, int y, T value) {
			int tx = ToTile(x, xll);
			int ty = ToTile(y, yll);
			int ix = ToIndex(x, xll);
			int iy = ToIndex(y, yll);
			if(ContainsTile(tx,ty)) {
				SimpleGrid<T> tile = tiles[tx, ty];
				if (tile == null) {
					tile = CreateTile ();
					tiles [tx, ty] = tile;
				} 
				tile [ix, iy] = value;
			} else {
				Extend (x, y);
				Set (x, y, value);
			}
		}



		public override bool Contains(int x, int y) {
			int vx = x - xll;
			int vy = y - yll;
			return vx >= 0 && vy >= 0 && vx < Width && y < Height;
		}


		public override bool Intersects(int xmin, int ymin, int xmax, int ymax) {
			if( xmax < this.xll || xmin > this.xll+Width ) {
				return false;
			}
			if( ymax < this.yll || ymin > this.yll+Height ) {
				return false;
			}
			return true;
		}

		public bool ContainsTile(int tx, int ty) {
			return tx >= 0 && ty >= 0 && tx < tiles.GetLength(0) && ty < tiles.GetLength(1);
		}
		void Extend(int x, int y) {
			int tx = ToTile(x, xll);
			int ty = ToTile(y, yll);
			if (ContainsTile (tx, ty)) {
				if (tiles [tx, ty] == null) {
					tiles [tx, ty] = CreateTile ();
				}
			} else {
				if (tiles.GetLength (0) == 0) {
					SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[1, tiles.GetLength (1)];
					this.tiles = newTiles;
					xll = tx * tileSize;
				} else {
					if (tx >= tiles.GetLength (0)) {
						SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[tx + 1, tiles.GetLength (1)];
						CopyTo (tiles, newTiles);
						this.tiles = newTiles;
					} else if (tx < 0) {
						SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[tiles.GetLength (0) - tx, tiles.GetLength (1)];
						CopyTo (tiles, newTiles, -tx, 0);
						this.tiles = newTiles;
						xll += tx * tileSize;
					}
				}

				if (tiles.GetLength (1) == 0) {
					SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[tiles.GetLength (0), 1];
					this.tiles = newTiles;
					yll = ty * tileSize;
				} else {
					if (ty >= tiles.GetLength (1)) {
						SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[tiles.GetLength (0), ty + 1];
						CopyTo (tiles, newTiles);
						this.tiles = newTiles;
					} else if (ty < 0) {
						SimpleGrid<T>[,] newTiles = new SimpleGrid<T>[tiles.GetLength (0), tiles.GetLength (1) - ty];
						CopyTo (tiles, newTiles, 0, -ty);
						this.tiles = newTiles;
						yll += ty * tileSize;
					}
				}

				tx = ToTile(x, xll);
				ty = ToTile(y, yll);
				//UnityEngine.Debug.Log ("X: "+x + " " + y);
				//UnityEngine.Debug.Log ("Xll: "+xll + " " + yll);
				//UnityEngine.Debug.Log ("XT: "+tx + " " + ty);
				//UnityEngine.Debug.Log ("Tiles: "+tiles.GetLength(0) + " " + tiles.GetLength(1));
				this.tiles [tx, ty] = CreateTile ();
				bounds.Set (xll, y, xll + tiles.GetLength (0) * tileSize, tiles.GetLength (1) * tileSize);
			}
		}

		int Mod(int x, int m) {
			int r = x % m;
			return r < 0 ? r + m : r;
		}
		int ToTile(int x, int xll) {
			return (int) Math.Floor( (x - xll) / (float) tileSize );
		}
		int ToIndex(int x, int xll) {
			return Mod(x - xll, tileSize);
		}

		SimpleGrid<T> CreateTile() {
			return new SimpleGrid<T>(0, 0, tileSize, tileSize, (x,y) => noData , noData);
		}
		void CopyTo(SimpleGrid<T>[,] source, SimpleGrid<T>[,] target, int xOffset=0, int yOffset=0) {
			int w = source.GetLength(0);
			int h = source.GetLength(1);
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					target [x + xOffset, y + yOffset] = source [x, y];
				}
			}
		}


		public override T NoData { get{ return noData; } }

		public override bool IsNoData (T value) {
			return value.Equals (noData);
		}


		public override Grid<T> Clone () {
			return new InfiniteGrid<T> (xll, yll, tileSize, noData, tiles);
		}
		
	}
}