﻿using System;
using System.Collections.Generic;

namespace Map.LayeredGrid.Impl {
	public class InfiniteLayeredGrid<T> : LayeredGrid<T> {

		int bottomLayer;
		InfiniteGrid<T>[] layers;

		int tileSize;
		T noData;
		private Bounds bounds;

		Dictionary<GridLocation,GridLink> links = new Dictionary<GridLocation,GridLink> ();


		public InfiniteLayeredGrid(int tileSize, T noData) {
			layers = new InfiniteGrid<T>[0];
			bottomLayer = 0;

			this.tileSize = tileSize;
			this.noData = noData;
			bounds = new Bounds ();
		}

		public InfiniteLayeredGrid(int tileSize, T noData, InfiniteGrid<T>[] layers, int bottomLayer) {
			this.bottomLayer = bottomLayer;

			this.tileSize = tileSize;
			this.noData = noData;


			bounds = null;
			this.layers = new InfiniteGrid<T>[layers.Length];
			for (int l = 0; l < layers.Length; l++) {
				InfiniteGrid<T> layer = layers [l];
				this.layers [l] = (InfiniteGrid<T>) layer.Clone ();
				Bounds lb = layer.Bounds;
				if (bounds == null) {
					bounds = new Bounds (l, l, lb.xmin, lb.xmax, lb.ymin, lb.ymax);
				} else {
					bounds.Add (l, l, lb.xmin, lb.xmax, lb.ymin, lb.ymax);
				}
			}
		}

		public override T Get (GridLocation loc) {
			return Get (loc.Layer, loc.X, loc.Y);
		}
		public override T Get (int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return noData;
			}
			return layers [l][x, y];
		}

		public override Grid<T>[] GetLayer (int layer) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return new Grid<T>[0];
			}
			return new Grid<T>[]{ layers [l] };
		}



		public override void Set(GridLocation loc, T value) {
			Set (loc.Layer, loc.X, loc.Y, value);
		}
		public override void Set(int layer, int x, int y, T value) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				Extend (layer);
				l = layer - bottomLayer;
			}
			layers [l] [x, y] = value;
		}

		void Extend(int layer) {
			int l = layer - bottomLayer;
			if (layers.Length == 0) {
				InfiniteGrid<T>[] newLayers = new InfiniteGrid<T>[1];
				newLayers[0] = new InfiniteGrid<T> (tileSize, noData);
				layers = newLayers;
				bottomLayer = l;
			} else {
				if (l < 0) {
					InfiniteGrid<T>[] newLayers = new InfiniteGrid<T>[layers.Length - l];
					for (int i = 0; i < newLayers.Length; i++) {
						if (i < -l) {
							newLayers [i] = new InfiniteGrid<T> (tileSize, noData);
						} else {
							newLayers [i] = layers [i + l];
						}
					}
					layers = newLayers;
					bottomLayer += l;
				} else if (l >= layers.Length) {
					InfiniteGrid<T>[] newLayers = new InfiniteGrid<T>[l + 1];
					for (int i = 0; i < newLayers.Length; i++) {
						if (i < layers.Length) {
							newLayers [i] = layers [i];
						} else {
							newLayers [i] = new InfiniteGrid<T> (tileSize, noData);
						}
					}
					layers = newLayers;
				}
			}
			// TODO
			// extend bounds
			/*
			if (bounds == null || ()) {
				bounds = new Bounds (l, l, lb.xmin, lb.xmax, lb.ymin, lb.ymax);
			} else {
				bounds.Add (l, l, lb.xmin, lb.xmax, lb.ymin, lb.ymax);
			}
			*/
		}


		public override bool Contains(GridLocation loc) {
			return ContainsExact (loc.Layer, loc.X, loc.Y);
		}
		public override bool Contains(int layer, int x, int y) {
			return ContainsExact (layer, x, y);
		}
		public override bool ContainsExact(GridLocation loc) {
			return ContainsExact (loc.Layer, loc.X, loc.Y);
		}

		public override bool ContainsExact(int layer, int x, int y) {
			int l = layer - bottomLayer;
			if (l < 0 || l >= layers.Length) {
				return false;
			}
			return layers [l].Contains (x, y);
		}


		public override bool Intersects(int layer, int xmin, int ymin, int xmax, int ymax) {
			int l = layer - bottomLayer;
			return layers[l].Intersects(xmin, ymin, xmax, ymax);
		}


		public override GridLink GetLink ( GridLocation source ) {
			if ( ! links.ContainsKey (source) ) {
				return null;
			} else {
				return links[source];
			}
		}
		public override GridLink GetLink ( int layer, int x, int y ) {
			return GetLink (new GridLocation (layer, x, y));
		}

		public override void AddLink ( GridLocation source, GridLocation target ) {
			links [source] = new GridLink (source, target);
			links [target] = new GridLink (target, source);
		}

		public override void AddDirectedLink ( GridLocation source, GridLocation target ) {
			ImmutableGridLocation src = new ImmutableGridLocation (source);
			ImmutableGridLocation trg = new ImmutableGridLocation (target);
			links [source] = new GridLink (src, trg);
		}

		public override Dictionary<GridLocation, GridLink> Links { get{ return links; } }


		public override int TopLayer { get { return bottomLayer + layers.Length - 1; } }
		public override int BottomLayer { get { return bottomLayer; } }
		public override int LayerCount { get{ return layers.Length; } }
		public override T NoData { get{ return noData; } }

		public override Bounds Bounds { get{ return bounds; } }

		public override bool IsNoData (T value) {
			return value.Equals (noData);
		}

		public override List<GridLocation> GetAll( int layer, Func<T, bool> filter ) {
			return new List<GridLocation> ();
		}

		public override List<GridLocation> GetAll( int layer, Func<T, int, int, bool> filter ) {
			return new List<GridLocation> ();
		}
		public override LayeredGrid<T> Clone () {
			return new InfiniteLayeredGrid<T> (tileSize, noData, layers, bottomLayer);
		}

	}
}
