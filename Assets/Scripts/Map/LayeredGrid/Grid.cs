﻿
namespace Map.LayeredGrid {
	public abstract class Grid<T> {

		public T this[int x, int y] {
			get { return Get(x,y); }
			set { Set(x,y,value); }
		}
	    
		public abstract T Get (int x, int y);
		public abstract void Set(int x, int y, T value);
		public abstract bool Contains(int x, int y);
		public abstract bool Intersects (int xmin, int ymin, int xmax, int ymax);

		public abstract int Xll { get; }
		public abstract int Yll { get; }

		public abstract int Width { get; }
		public abstract int Height { get; }
		public abstract LayeredGrid.Bounds Bounds { get; }
		public abstract T NoData { get; }
		public abstract bool IsNoData (T value);


		public abstract Grid<T> Clone ();

	}
}
