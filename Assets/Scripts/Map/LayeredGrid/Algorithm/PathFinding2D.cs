﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;

namespace Map.LayeredGrid.Algorithm {
	public interface PathFinding2D {

		List<GridLocation> FindPath (GridLocation source, GridLocation target);

	}
}
