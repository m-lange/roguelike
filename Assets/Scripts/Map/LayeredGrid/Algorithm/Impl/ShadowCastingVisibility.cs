﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Map.LayeredGrid.Algorithm.Impl {
	public class ShadowCastingVisibility<T> : Visibility {


		LayeredGrid<T> grid;
		//Func<GridLocation, GridLocation, int, bool> 
		VisibilityCalculator visibilityCalculator;
		BlockingEntitiesCalculator blockingEntitiesCalculator;
		WallCalculator wallCalculator;

		public delegate bool VisibilityCalculator(GridLocation source, GridLocation target, int row, bool entityAtTarget);
		public delegate HashSet<GridLocation> BlockingEntitiesCalculator(GridLocation source, int visionRangeX, int visionRangeY);
		public delegate bool WallCalculator(GridLocation location);

		public ShadowCastingVisibility(LayeredGrid<T> grid, 
						VisibilityCalculator visibilityCalculator,
						BlockingEntitiesCalculator blockingEntitiesCalculator,
						WallCalculator wallCalculator) {

			this.grid = grid;
			this.visibilityCalculator = visibilityCalculator;
			this.blockingEntitiesCalculator = blockingEntitiesCalculator;
			this.wallCalculator = wallCalculator;
		}

		public Dictionary<GridLocation, Visible> FindVisible (GridLocation source, int visionRangeX, int visionRangeY, float radius) {
			Dictionary<GridLocation, Visible> result = new Dictionary<GridLocation, Visible> ();

			HashSet<GridLocation> blocking = blockingEntitiesCalculator(source, visionRangeX, visionRangeY);
			result[source] = Visible.centerVisible;
			for (int layer = source.Layer; layer >= 0 || layer == source.Layer; layer--) {
				//bool found = false;
				for (var octant = 0; octant < 8; octant++) {
					int rowVisionRange = visionRangeX;
					int colVisionRange = visionRangeY;
					if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
						rowVisionRange = visionRangeY;
						colVisionRange = visionRangeX;
					}
					if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, blocking, result)) {
						//found = true;
					}
				}
				//if (layer != source.Layer && ! found) {
				//	break;
				//}
			}
			if (source.Layer >= 0) {
				for (int layer = source.Layer + 1; layer <= grid.TopLayer; layer++) {
					bool found = false;
					for (var octant = 0; octant < 8; octant++) {
						int rowVisionRange = visionRangeX;
						int colVisionRange = visionRangeY;
						if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
							rowVisionRange = visionRangeY;
							colVisionRange = visionRangeX;
						}
						if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, blocking, result)) {
							found = true;
						}
					}
					if (!found) {
						break;
					}
				}
			}

			return result;
		}

		bool FindVisibleOctant(GridLocation source, int octant, int layer, int rowVisionRange, int colVisionRange, HashSet<GridLocation> blockingEntities, Dictionary<GridLocation, Visible> result) {
			ShadowLine line = new ShadowLine();
			bool fullShadow = false;
			bool found = false;

			for (var row = 1; row <= rowVisionRange; row++) {
				// Stop once we go out of bounds.
				Vec2 relPos1 = TransformOctant(row, 0, octant);
				Vec2 pos1 = new Vec2 (source.X + relPos1.x, source.Y + relPos1.y); 
				if (layer <= source.Layer) {
					if (!grid.Contains (layer, pos1.x, pos1.y))
						break;
				} 

				for (int col = 0; col <= row; col++) {
					Vec2 relPos = TransformOctant(row, col, octant);
					Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 

					if (layer <= source.Layer) {
						if (!grid.Contains (layer, pos.x, pos.y))
							break;
					}

					if (fullShadow) {
						break;
					} else {
						Shadow projection = ProjectTile(row, col);
						//Shadow halfProjection = ProjectHalfTile(row, col);

						bool notExcluded = (source.Layer == layer || row > 1);
						bool visible = !line.IsInShadow(projection);
						//bool halfVisible = !line.IsInShadow(halfProjection);
						bool halfVisible = !line.IsInShadow(ProjectTileCenter(row, col));

						GridLocation target = new ImmutableGridLocation (layer, pos.x, pos.y);
						bool isBlocking = col >= colVisionRange || (! visibilityCalculator (source, target, row, blockingEntities.Contains(target)));
						bool showAsWall = wallCalculator (target);

						//if (layer > source.Layer && isWall && visible) {
						//	UnityEngine.Debug.Log (layer+" "+pos.x+" "+pos.y);
						//}
						bool isNoData = grid.Get(target).Equals( grid.NoData );
						if(notExcluded) {
							if (halfVisible) {
								if(! isNoData) {
									result[target] = Visible.centerVisible;
								}
								found = true;
							} else if (visible) {
								if(! isNoData) {
									if (showAsWall) {
										result[target] = Visible.centerVisible;
									} else {
										result [target] = Visible.tileVisible;
									}
								}
								found = true;
							}
						}

						if (visible && isBlocking) {
							line.Add(projection);
							fullShadow = line.IsFullShadow();
							if (fullShadow) {
								break;
							}
						}
					}

				}

				if (fullShadow) {
					break;
				}
			}
			return found;
		}

		public bool CanSee (GridLocation source, GridLocation target, float maxDist, bool ignoreLOS) {
			return false;
		}
		public List<GridLocation> GetLOS (GridLocation source, GridLocation target) {
			return null;
		}
		Shadow ProjectTile(int row, int col) {
			float topLeft = col / (row + 2f);
			float bottomRight = (col + 1f) / (row + 1f);
			return new Shadow(topLeft, bottomRight);
		}
		Shadow ProjectHalfTile(int row, int col) {
			float topLeft = (col + 0.25f) / (row + 1.75f);
			float bottomRight = (col + 0.75f) / (row + 1.25f);
			return new Shadow(topLeft, bottomRight);
		}
		float ProjectTileCenter(int row, int col) {
			float center = (col+0.5f) / (row + 1.5f);
			return center;
		}

		Vec2 TransformOctant(int row, int col, int octant) {
			switch (octant) {
				case 0: return new Vec2( col, -row);
				case 1: return new Vec2( row, -col);
				case 2: return new Vec2( row,  col);
				case 3: return new Vec2( col,  row);
				case 4: return new Vec2(-col,  row);
				case 5: return new Vec2(-row,  col);
				case 6: return new Vec2(-row, -col);
				case 7: return new Vec2(-col, -row);
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		
	}

	struct Vec2 {
		public int x;
		public int y;

		public Vec2(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	class ShadowLine {
		List<Shadow> shadows = new List<Shadow>();
		public bool IsInShadow(Shadow projection) {
			foreach (Shadow shadow in shadows) {
				if (shadow.Contains(projection)) return true;
			}

			return false;
		}
		public bool IsInShadow(float projection) {
			foreach (Shadow shadow in shadows) {
				if (shadow.Contains(projection)) return true;
			}

			return false;
		}
		public bool IsFullShadow() {
			return shadows.Count == 1 &&
				shadows[0].start <= 0 &&
				shadows[0].end >= 1;
		}

		public void Add(Shadow shadow) {
			// Figure out where to slot the new shadow in the list.
			int index = 0;
			for (; index < shadows.Count; index++) {
				// Stop when we hit the insertion point.
				if (shadows[index].start >= shadow.start) break;
			}

			// The new shadow is going here. See if it overlaps the
			// previous or next.
			Shadow overlappingPrevious = null;
			if (index > 0 && shadows[index - 1].end > shadow.start) {
				overlappingPrevious = shadows[index - 1];
			}

			Shadow overlappingNext = null;
			if (index < shadows.Count &&
				shadows[index].start < shadow.end) {
				overlappingNext = shadows[index];
			}

			// Insert and unify with overlapping shadows.
			if (overlappingNext != null) {
				if (overlappingPrevious != null) {
					// Overlaps both, so unify one and delete the other.
					overlappingPrevious.end = overlappingNext.end;
					shadows.RemoveAt(index);
				} else {
					// Overlaps the next one, so unify it with that.
					overlappingNext.start = shadow.start;
				}
			} else {
				if (overlappingPrevious != null) {
					// Overlaps the previous one, so unify it with that.
					overlappingPrevious.end = shadow.end;
				} else {
					// Does not overlap anything, so insert.
					shadows.Insert(index, shadow);
				}
			}
		}
	}

	class Shadow {
		public float start;
		public float end;

		public Shadow(float start, float end) {
			this.start = start;
			this.end = end;
		}

		public bool Contains(Shadow other) {
			return start <= other.start && end >= other.end;
		}
		public bool Contains(float other) {
			return start <= other && end >= other;
		}
	}

}
