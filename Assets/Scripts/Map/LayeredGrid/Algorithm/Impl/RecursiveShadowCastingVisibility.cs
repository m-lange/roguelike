﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Map.LayeredGrid.Algorithm.Impl {
	public class RecursiveShadowCastingVisibility<T> : Visibility {


		LayeredGrid<T> grid;
		//Func<GridLocation, GridLocation, int, bool> 
		VisibilityCalculator visibilityCalculator;
		BlockingEntitiesCalculator blockingEntitiesCalculator;
		WallCalculator wallCalculator;
		IlluminationCalculator illuminationCalculator;

		public delegate bool VisibilityCalculator(GridLocation source, GridLocation target, int row, bool entityAtTarget);
		public delegate HashSet<GridLocation> BlockingEntitiesCalculator(GridLocation source, int visionRangeX, int visionRangeY);
		public delegate bool WallCalculator(GridLocation location);
		public delegate bool IlluminationCalculator(GridLocation location);

		public RecursiveShadowCastingVisibility(LayeredGrid<T> grid, 
						VisibilityCalculator visibilityCalculator,
						BlockingEntitiesCalculator blockingEntitiesCalculator,
						WallCalculator wallCalculator,
						IlluminationCalculator illuminationCalculator) {

			this.grid = grid;
			this.visibilityCalculator = visibilityCalculator;
			this.blockingEntitiesCalculator = blockingEntitiesCalculator;
			this.wallCalculator = wallCalculator;
			this.illuminationCalculator = illuminationCalculator;
		}

		public Dictionary<GridLocation, Visible> FindVisible (GridLocation source, int visionRangeX, int visionRangeY, float radius) {
			Dictionary<GridLocation, Visible> result = new Dictionary<GridLocation, Visible> ();

			HashSet<GridLocation> blocking = blockingEntitiesCalculator(source, visionRangeX, visionRangeY);
			int octants = 8;
			float radSq = radius * radius;
			result[source] = Visible.centerVisible;
			for (int layer = source.Layer; layer >= 0 || layer == source.Layer; layer--) {
				//bool found = false;
				for (var octant = 0; octant < octants; octant++) {
					int rowVisionRange = visionRangeX;
					int colVisionRange = visionRangeY;
					if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
						rowVisionRange = visionRangeY;
						colVisionRange = visionRangeX;
					}
					if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, radSq, 1, 0, 1, blocking, result)) {
						//found = true;
					}
				}
				//if (layer != source.Layer && ! found) {
				//	break;
				//}
			}
			if (source.Layer >= 0) {
				for (int layer = source.Layer + 1; layer <= grid.TopLayer; layer++) {
					bool found = false;
					for (var octant = 0; octant < octants; octant++) {
						int rowVisionRange = visionRangeX;
						int colVisionRange = visionRangeY;
						if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
							rowVisionRange = visionRangeY;
							colVisionRange = visionRangeX;
						}
						if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, radSq, 1, 0, 1, blocking, result)) {
							found = true;
						}
					}
					if (!found) {
						break;
					}
				}
			}

			return result;
		}

		/// <summary>
		/// 
		/// \ 4 | 3 /
		///  \  |  /
		/// 5 \ | / 2
		/// ___\|/___
		///    /|\   
		/// 6 / | \ 1
		///  /  |  \
		/// / 7 | 0 \
		/// 
		/// </summary>
		/// <returns><c>true</c>, if visible octant was found, <c>false</c> otherwise.</returns>
		/// <param name="source">Source.</param>
		/// <param name="octant">Octant.</param>
		/// <param name="layer">Layer.</param>
		/// <param name="rowVisionRange">Row vision range.</param>
		/// <param name="colVisionRange">Col vision range.</param>
		/// <param name="row">Row.</param>
		/// <param name="startSlope">Start slope.</param>
		/// <param name="endSlope">End slope.</param>
		/// <param name="result">Result.</param>
		bool FindVisibleOctant (GridLocation source, int octant, int layer, int rowVisionRange, int colVisionRange, float radiusSq, int row, float startSlope, float endSlope, HashSet<GridLocation> blockingEntities, Dictionary<GridLocation, Visible> result) {
			if (endSlope < startSlope) {
				return false;
			}

			bool found = false;

			// Stop once we go out of bounds.
			Vec2 relPos = TransformOctant (row, 0, octant);
			Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 
			if (layer <= source.Layer) {
				if (!grid.Contains (layer, pos.x, pos.y))
					return false;
			} 
			//UnityEngine.Debug.Log (row);
			bool priorBlocked = false;
			int startCol = (int)((row - 0.5f) * startSlope + 0.5f);
			int endCol = (int)((row + 0.5f) * endSlope + 0.49999f);
			if (endCol > row)
				endCol = row;
			if (endCol > colVisionRange)
				endCol = colVisionRange;
			/*
			if (layer == 0 && octant == 0 && row <= 5) {
				UnityEngine.Debug.Log (row + " " + startSlope + "  " + startCol + "  " + ((row - 0.5f) * startSlope));
			}
			*/
			int row2 = row * row;
			GridLocation target = new GridLocation (0, 0, 0);
			for (int col = startCol; col <= endCol; col++) {
				float centerSlope = col / (float)row;
				float topLeft = (col - 0.5f) / (float)(row + 0.5f);
				float bottomRight = (col + 0.5f) / (float)(row - 0.5f);


				//Vec2 relPos = TransformOctant(row, col, octant);
				//Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 
				TransformOctant (row, col, octant, ref relPos);
				pos.x = source.X + relPos.x;
				pos.y = source.Y + relPos.y;
				/*
				if (layer <= source.Layer) {
					if (!grid.Contains (layer, pos.x, pos.y))
						break;
				}*/


				bool notExcluded = (source.Layer == layer || row > 1);
				bool visible = !(topLeft > endSlope || bottomRight < startSlope);
				bool halfVisible = centerSlope >= startSlope && centerSlope <= endSlope;

				//GridLocation target = new ImmutableGridLocation (layer, pos.x, pos.y);
				target.Set (layer, pos.x, pos.y);
				bool isBlocking = (!visibilityCalculator (source, target, row, blockingEntities.Contains (target)));
				bool showAsWall = wallCalculator (target);

				bool isNoData = grid.Get (target).Equals (grid.NoData);

				if (notExcluded) {
					if (halfVisible) {
						if (!isNoData) {
							if (illuminationCalculator(target) || row2 + col * col <= radiusSq) {
								result [new ImmutableGridLocation (target)] = Visible.centerVisible;
							}
						}
						found = true;
					} else if (visible) {
						if (!isNoData) {
							if (illuminationCalculator(target) || row2 + col * col <= radiusSq) {
								if (showAsWall) {
									result [new ImmutableGridLocation (target)] = Visible.centerVisible;
								} else {
									result [new ImmutableGridLocation (target)] = Visible.tileVisible;
									//result[target] = Visible.centerVisible;
								}
							}
						}
						found = true;
					}
				}
				bool blocked = (visible && isBlocking);
				if (row < rowVisionRange) {
					if (blocked && (!priorBlocked)) {
						if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, radiusSq, row + 1, startSlope, topLeft, blockingEntities, result)) {
							found = true;
						}
					} else if (col == endCol) {
						if (!priorBlocked) {
							if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, radiusSq, row + 1, startSlope, endSlope, blockingEntities, result)) {
								found = true;
							}
						} else if ( (!blocked) && priorBlocked ) { 
							if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, radiusSq, row + 1, endSlope, endSlope, blockingEntities, result)) {
								found = true;
							}
						}
					}
				}
				if ( (!blocked) && priorBlocked) {
					// We are one column too far!
					startSlope = (col - 0.5f) / (float) (row - 0.5f);
				}
				priorBlocked = blocked;
			}

			return found;
		}

		public bool CanSee (GridLocation source, GridLocation target, float maxDist, bool ignoreLOS) {
			int dx = target.X - source.X;
			int dy = target.Y - source.Y;
			if (dx > maxDist || -dx > maxDist || dy > maxDist || -dy > maxDist) {
				return false;
			}
			if (ignoreLOS) {
				return true;
			}
			// TODO
			if (source.Layer != target.Layer) {
				return false;
			}
			//if (source.Layer != target.Layer && (source.Layer < 0 || target.Layer < 0)) {
			//	return false;
			//}

			int octant = GetOctant (dx, dy);
			Vec2 tTrans = TransformOctantInv (dx, dy, octant);
			int tRow = tTrans.x;
			int tCol = tTrans.y;
			float slope = tCol / (float)tRow;

			HashSet<GridLocation> blocking = blockingEntitiesCalculator (
				                                 new GridLocation (target.Layer, (int)(0.5f * (source.X + target.X)), (int)(0.5f * (source.Y + target.Y))), 
				                                 1 + Math.Abs (source.X - target.X) / 2, 1 + Math.Abs (source.Y - target.Y) / 2);

			GridLocation targ = new GridLocation (0, 0, 0);
			/*
			int step = 1;
			if (source.Layer > target.Layer) {
				step = -1;
			}
			*/
			// TODO layers!
			int l = source.Layer;
			//for (int l = source.Layer; l <= target.Layer; l+=step) {
				for (int row = 1; row < tRow; row++) {
					int startCol = (int)((row - 0.5f) * slope + 0.5f);
					int endCol = (int)((row + 0.5f) * slope + 0.49999f);
					if (endCol > row)
						endCol = row;

					for (int col = startCol; col <= endCol; col++) {
						Vec2 relPos = TransformOctant (row, col, octant);
						Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 
						targ.Set (l, pos.x, pos.y);
						bool isBlocking = (!visibilityCalculator (source, targ, row, blocking.Contains (targ)));
						if (isBlocking) {
							return false;
						}
					}
				}
			//}
			return true;
		}

		/// <summary>
		/// TOTO: Layers!
		/// </summary>
		/// <returns>The LO.</returns>
		/// <param name="source">Source.</param>
		/// <param name="target">Target.</param>
		public List<GridLocation> GetLOS (GridLocation source, GridLocation target) {
			int dx = target.X - source.X;
			int dy = target.Y - source.Y;

			int octant = GetOctant (dx, dy);
			Vec2 tTrans = TransformOctantInv (dx, dy, octant);
			int tRow = tTrans.x;
			int tCol = tTrans.y;
			float slope = tCol / (float) tRow;

			List<GridLocation> result = new List<GridLocation>();
			for (int row = 1; row < tRow; row++) {
				int startCol = (int) ((row - 0.5f) * slope + 0.5f);
				int endCol = (int) ((row + 0.5f) * slope + 0.49999f);
				if (endCol > row)
					endCol = row;

				for (int col = startCol; col <= endCol; col++) {
					Vec2 relPos = TransformOctant(row, col, octant);
					Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 
					GridLocation targ = new ImmutableGridLocation (source.Layer, pos.x, pos.y);
					result.Add(targ);
				}
			}
			return result;
		}

		Vec2 TransformOctant(int row, int col, int octant) {
			switch (octant) {
				case 0: return new Vec2( col, -row);
				case 1: return new Vec2( row, -col);
				case 2: return new Vec2( row,  col);
				case 3: return new Vec2( col,  row);
				case 4: return new Vec2(-col,  row);
				case 5: return new Vec2(-row,  col);
				case 6: return new Vec2(-row, -col);
				case 7: return new Vec2(-col, -row);
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		void TransformOctant(int row, int col, int octant, ref Vec2 outVec) {
			switch (octant) {
			case 0: outVec.x =  col; outVec.y = -row; break;
			case 1: outVec.x =  row; outVec.y = -col; break;
			case 2: outVec.x =  row; outVec.y =  col; break;
			case 3: outVec.x =  col; outVec.y =  row; break;
			case 4: outVec.x = -col; outVec.y =  row; break;
			case 5: outVec.x = -row; outVec.y =  col; break;
			case 6: outVec.x = -row; outVec.y = -col; break;
			case 7: outVec.x = -col; outVec.y = -row; break;
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		Vec2 TransformOctantInv(int x, int y, int octant) {
			switch (octant) {
			case 0: return new Vec2( -y,  x);
			case 1: return new Vec2(  x, -y);
			case 2: return new Vec2(  x,  y);
			case 3: return new Vec2(  y,  x);
			case 4: return new Vec2(  y, -x);
			case 5: return new Vec2( -x,  y);
			case 6: return new Vec2( -x, -y);
			case 7: return new Vec2( -y, -x);
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		void TransformOctantInv(int x, int y, int octant, ref Vec2 outVec) {
			switch (octant) {
			case 0: outVec.x = -y; outVec.y =  x; break;
			case 1: outVec.x =  x; outVec.y = -y; break;
			case 2: outVec.x =  x; outVec.y =  y; break;
			case 3: outVec.x =  y; outVec.y =  x; break;
			case 4: outVec.x =  y; outVec.y = -x; break;
			case 5: outVec.x = -x; outVec.y =  y; break;
			case 6: outVec.x = -x; outVec.y = -y; break;
			case 7: outVec.x = -y; outVec.y = -x; break;
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		public static int GetOctant(int dx, int dy) {
			if (dx > 0) { // 0-3
				if (dy < 0) { // 0-1
					if (-dy > dx) {
						return 0;
					} else {
						return 1;
					}
				} else { // 2-3
					if (dy > dx) {
						return 3;
					} else {
						return 2;
					}
				}
			} else { // 4-7
				if (dy < 0) { // 6-7
					if (-dy > -dx) {
						return 7;
					} else {
						return 6;
					}
				} else { // 4-5
					if (dy > -dx) {
						return 4;
					} else {
						return 5;
					}
				}
			}
			//return 0;
		}

	}

}
