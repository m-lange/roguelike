﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using Map.LayeredGrid.Algorithm;

namespace Map.LayeredGrid.Algorithm.Impl {
	public class AStar2DPathFinding<T> : PathFinding2D {

		T[,] grid;
		CostCalculator costCalculator;
		float minCost;


		public delegate float CostCalculator(GridLocation source, GridLocation target, T value);

		public AStar2DPathFinding( T[,] grid, CostCalculator costCalculator, float minCost ) {
			this.grid = grid;
			this.costCalculator = costCalculator;
			this.minCost = minCost;
		}

		public float MinCost {
			get { return minCost; }
		}

		public List<GridLocation> FindPath (GridLocation source, GridLocation target) {
			int w = grid.GetLength (0);
			int h = grid.GetLength (1);
			ValueSortedDict<GridLocation, float> open = new ValueSortedDict<GridLocation, float> ((a, b) => {
				if (a < b)
					return -1;
				if (a > b)
					return 1;
				return 0;
			});

			Dictionary<GridLocation, GridLocation> cameFrom = new Dictionary<GridLocation, GridLocation> ();
			Dictionary<GridLocation, float> costSoFar = new Dictionary<GridLocation, float> ();
			Dictionary<GridLocation, int> numPoints = new Dictionary<GridLocation, int> ();

			source = new ImmutableGridLocation (source);
			open [source] = 0;
			costSoFar [source] = 0;

			while ( open.Count > 0 ) {
				IEnumerator en = open.Keys.GetEnumerator ();
				en.MoveNext ();
				GridLocation current = (GridLocation)en.Current;
				if (current.Equals (target)) {
					return ReconstructPath (current, cameFrom);
				}
				open.Remove (current);


				foreach (GridLocation neighbor in GetNeighbours(current, w, h)) {
					float c = costCalculator (current, neighbor, grid [neighbor.X, neighbor.Y]);
					if (c > 0) {
						float cost = costSoFar [current] + c;
						bool add = false;
						if (!costSoFar.ContainsKey (neighbor)) {
							add = true;
						} else {
							float csf = costSoFar [neighbor];
							if (cost < csf) {
								add = true;
							} else if (cost == csf) {
								int cnt = 0;
								if (numPoints.ContainsKey (neighbor)) {
									numPoints[neighbor] += 1;
									cnt = numPoints[neighbor];
								} else {
									numPoints[neighbor] = 2;
									cnt = 2;
								}
								add = RandUtils.NextBool( 1f / (float) cnt );
								//UnityEngine.Debug.Log( "EQUAL "+cnt+" "+add );
							}
						}

						if (add) {
							costSoFar [neighbor] = cost;
							float heuristic = cost + minCost * Heuristic (neighbor, target);
							open [neighbor] = heuristic;
							cameFrom [neighbor] = current;
						}
					}
				}
			}

			return null;
		}

		IEnumerable GetNeighbours (GridLocation current, int w, int h) {
			for (int x = -1; x <= 1; x++) {
				int xx = x + current.X;
				if (xx >= 0 && xx < w) {
					for (int y = -1; y <= 1; y++) {
						if (x != 0 || y != 0) {
							int yy = y + current.Y;
							if (yy >= 0 && yy < h) {
								GridLocation neigh = new ImmutableGridLocation (current.Layer, xx, yy);
								yield return neigh;
							}
						}
					}
				}
			}
		}

		float Heuristic(GridLocation source, GridLocation target) {
			int dx = Math.Abs(source.X - target.X);
			int dy = Math.Abs(source.Y - target.Y);
			return (dx > dy) ? dx : dy;
		}

		List<GridLocation> ReconstructPath(GridLocation target, Dictionary<GridLocation, GridLocation> cameFrom) {
			List<GridLocation> result = new List<GridLocation> ();
			result.Add (target);

			GridLocation current = target;
			while (cameFrom.ContainsKey (current)) {
				current = cameFrom [current];
				result.Add (current);
			}
			result.Reverse();
			return result;
		}

	}
}
