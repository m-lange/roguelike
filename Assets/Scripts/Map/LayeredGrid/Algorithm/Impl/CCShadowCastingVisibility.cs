﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Map.LayeredGrid.Algorithm.Impl {
	public class CCShadowCastingVisibility<T> : Visibility {


		LayeredGrid<T> grid;
		//Func<GridLocation, GridLocation, int, bool> 
		VisibilityCalculator visibilityCalculator;
		WallCalculator wallCalculator;

		public delegate bool VisibilityCalculator(GridLocation source, GridLocation target, int row);
		public delegate bool WallCalculator(GridLocation location);

		public CCShadowCastingVisibility(LayeredGrid<T> grid, 
						VisibilityCalculator visibilityCalculator,
						WallCalculator wallCalculator) {

			this.grid = grid;
			this.visibilityCalculator = visibilityCalculator;
			this.wallCalculator = wallCalculator;
		}

		public Dictionary<GridLocation, Visible> FindVisible (GridLocation source, int visionRangeX, int visionRangeY, float radius) {
			Dictionary<GridLocation, Visible> result = new Dictionary<GridLocation, Visible> ();

			result[source] = Visible.centerVisible;
			for (int layer = source.Layer; layer >= 0 || layer == source.Layer; layer--) {
				//bool found = false;
				for (var octant = 0; octant < 8; octant++) {
					int rowVisionRange = visionRangeX;
					int colVisionRange = visionRangeY;
					if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
						rowVisionRange = visionRangeY;
						colVisionRange = visionRangeX;
					}
					if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, result)) {
						//found = true;
					}
				}
				//if (layer != source.Layer && ! found) {
				//	break;
				//}
			}
			if (source.Layer >= 0) {
				for (int layer = source.Layer + 1; layer <= grid.TopLayer; layer++) {
					bool found = false;
					for (var octant = 0; octant < 8; octant++) {
						int rowVisionRange = visionRangeX;
						int colVisionRange = visionRangeY;
						if ( visionRangeX != visionRangeY && ((octant + 1) / 2) % 2 == 0 ) {
							rowVisionRange = visionRangeY;
							colVisionRange = visionRangeX;
						}
						if (FindVisibleOctant (source, octant, layer, rowVisionRange, colVisionRange, result)) {
							found = true;
						}
					}
					if (!found) {
						break;
					}
				}
			}

			return result;
		}

		bool FindVisibleOctant(GridLocation source, int octant, int layer, int rowVisionRange, int colVisionRange, Dictionary<GridLocation, Visible> result) {
			ShadowLine line = new ShadowLine();
			bool fullShadow = false;
			bool found = false;

			for (var row = 1; row <= rowVisionRange; row++) {
				// Stop once we go out of bounds.
				Vec2 relPos1 = TransformOctant(row, 0, octant);
				Vec2 pos1 = new Vec2 (source.X + relPos1.x, source.Y + relPos1.y); 
				if (layer <= source.Layer) {
					if (!grid.Contains (layer, pos1.x, pos1.y))
						break;
				} 

				for (int col = 0; col <= row; col++) {
					Vec2 relPos = TransformOctant(row, col, octant);
					Vec2 pos = new Vec2 (source.X + relPos.x, source.Y + relPos.y); 

					if (layer <= source.Layer) {
						if (!grid.Contains (layer, pos.x, pos.y))
							break;
					}

					if (fullShadow) {
						break;
					} else {
						Shadow projection = ProjectTile(row, col);
						Shadow halfProjection = ProjectHalfTile(row, col);

						bool notExcluded = (source.Layer == layer || row > 1);
						bool visible = !line.IsInShadow(projection);
						bool halfVisible = !line.IsInShadow(halfProjection);
						//bool halfVisible = !line.IsInShadow(ProjectTileCenter(row, col));

						GridLocation target = new ImmutableGridLocation (layer, pos.x, pos.y);
						bool isBlocking = col >= colVisionRange || (! visibilityCalculator (source, target, row));
						bool showAsWall = wallCalculator (target);

						//if (layer > source.Layer && isWall && visible) {
						//	UnityEngine.Debug.Log (layer+" "+pos.x+" "+pos.y);
						//}
						bool isNoData = grid[target].Equals( grid.NoData );
						if(notExcluded) {
							if (halfVisible) {
								if(! isNoData) {
									result[target] = Visible.centerVisible;
								}
								found = true;
							} else if (visible) {
								if(! isNoData) {
									if (showAsWall) {
										result[target] = Visible.centerVisible;
									} else {
										result [target] = Visible.tileVisible;
									}
								}
								found = true;
							}
						}

						if (visible && isBlocking) {
							line.Add(projection);
							fullShadow = line.IsFullShadow();
							if (fullShadow) {
								break;
							}
						}
					}

				}

				if (fullShadow) {
					break;
				}
			}
			return found;
		}

		public bool CanSee (GridLocation source, GridLocation target, float maxDist, bool ignoreLOS) {
			return false;
		}
		public List<GridLocation> GetLOS (GridLocation source, GridLocation target) {
			return null;
		}

		Shadow ProjectTile(int row, int col) {
			float topLeft = (col-0.5f) / (float) (row + 0.5f);
			float bottomRight = (col + 0.5f) / (float) (row - 0.5f);
			return new Shadow(topLeft, bottomRight);
		}
		Shadow ProjectHalfTile(int row, int col) {
			float topLeft = (col - 0.25f) / (float) (row + 0.25f);
			float bottomRight = (col + 0.25f) / (float) (row - 0.25f);
			return new Shadow(topLeft, bottomRight);
		}
		float ProjectTileCenter(int row, int col) {
			float center = (col) / (float) (row);
			return center;
		}
		/*
		Shadow ProjectTile(int row, int col) {
			float topLeft = (col) / (float) (row + 1f);
			float bottomRight = (col + 1f) / (float) (row + 0f);
			return new Shadow(topLeft, bottomRight);
		}
		Shadow ProjectHalfTile(int row, int col) {
			float topLeft = (col - 0.25f) / (float) (row + 1.25f);
			float bottomRight = (col + 0.25f) / (float) (row + 0.75f);
			return new Shadow(topLeft, bottomRight);
		}
		float ProjectTileCenter(int row, int col) {
			float center = (col+0.5f) / (float) (row + 0.5f);
			return center;
		}
		*/
		Vec2 TransformOctant(int row, int col, int octant) {
			switch (octant) {
				case 0: return new Vec2( col, -row);
				case 1: return new Vec2( row, -col);
				case 2: return new Vec2( row,  col);
				case 3: return new Vec2( col,  row);
				case 4: return new Vec2(-col,  row);
				case 5: return new Vec2(-row,  col);
				case 6: return new Vec2(-row, -col);
				case 7: return new Vec2(-col, -row);
			default:
				throw new ArgumentException ("Octant in range 0..7 required (value was "+octant+")");
			}
		}

		
	}

}
