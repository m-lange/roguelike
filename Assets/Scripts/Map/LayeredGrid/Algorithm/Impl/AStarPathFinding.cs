﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Collections;
using Map.LayeredGrid.Algorithm;

namespace Map.LayeredGrid.Algorithm.Impl {
	public class AStarPathFinding<T> : PathFinding {

		LayeredGrid<T> grid;
		CostCalculator costCalculator;
		int minCost;
		int diagonalCost;


		public delegate float CostCalculator(GridLocation source, GridLocation target, int accessLevel, bool isTarget, bool ignoreSeen);

		public AStarPathFinding( LayeredGrid<T> grid, CostCalculator costCalculator, int minCost, int diagonalCost ) {
			this.grid = grid;
			this.costCalculator = costCalculator;
			this.minCost = minCost;
			this.diagonalCost = diagonalCost;
		}

		public int MinCost {
			get { return minCost; }
		}


			ValueSortedDict<GridLocation, float> open = new ValueSortedDict<GridLocation, float> ((a, b) => {
				if (a < b)
					return -1;
				if (a > b)
					return 1;
				return 0;
			});

			Dictionary<GridLocation, GridLocation> cameFrom = new Dictionary<GridLocation, GridLocation> ();
			Dictionary<GridLocation, float> costSoFar = new Dictionary<GridLocation, float> ();
			Dictionary<GridLocation, int> numPoints = new Dictionary<GridLocation, int> ();


		public List<GridLocation> FindPath (GridLocation source, GridLocation target, int accessLevel, float costLimit, bool allowStairs, bool ignoreSeen) {

			source = new ImmutableGridLocation (source);
			open [source] = 0;
			costSoFar [source] = 0;

			GridLocation tempNeighbor = new GridLocation (0, 0, 0);
			while ( open.Count > 0 ) {
				IEnumerator en = open.Keys.GetEnumerator ();
				en.MoveNext ();
				GridLocation current = (GridLocation)en.Current;
				if (current.Equals (target)) {
					List<GridLocation> path = ReconstructPath (current, cameFrom);
					//UnityEngine.Debug.Log("Path "+path.Count+" "+costSoFar.Count);
					Clear ();
					return path;
				}
				open.Remove (current);

				GridLocation neighbor;
				//foreach (GridLocation neighbor in GetNeighbours(current, allowStairs)) {

				float currCost = costSoFar [current];
				foreach (object dummy in GetNeighbours(current, allowStairs, tempNeighbor)) {
					neighbor = null;
					//float c = costCalculator (current, neighbor, accessLevel, neighbor.Equals (target), ignoreSeen);
					float c = costCalculator (current, tempNeighbor, accessLevel, tempNeighbor.Equals (target), ignoreSeen);
					if (c > 0) {
						float cost = currCost + c;
						if (costLimit <= 0 || cost <= costLimit) {
							bool add = false;
							if (!costSoFar.ContainsKey (tempNeighbor)) {
								add = true;
							} else {
								float csf = costSoFar [tempNeighbor];
								if (cost < csf) {
									add = true;
								} else if (cost == csf) {
									int cnt = 0;
									neighbor = new ImmutableGridLocation (tempNeighbor);
									if (numPoints.ContainsKey (neighbor)) {
										numPoints [neighbor] += 1;
										cnt = numPoints [neighbor];
									} else {
										numPoints [neighbor] = 2;
										cnt = 2;
									}
									add = RandUtils.NextBool (1f / (float)cnt);
									//UnityEngine.Debug.Log( "EQUAL "+cnt+" "+add );
								}
							}

							if (add) {
								if (neighbor == null) {
									neighbor = new ImmutableGridLocation (tempNeighbor);
								}
								costSoFar [neighbor] = cost;
								float heuristic = cost + minCost * Heuristic (neighbor, target);
								open [neighbor] = heuristic;
								cameFrom [neighbor] = current;
							}
						}
						//UnityEngine.Debug.DrawRay( GameData.GridToWorld(tempNeighbor), UnityEngine.Vector3.up * 0.25f, UnityEngine.Color.green, 10, false );
					} 
					/*else {
						UnityEngine.Debug.DrawRay( GameData.GridToWorld(tempNeighbor), UnityEngine.Vector3.up * 0.25f, UnityEngine.Color.red, 10, false );
					}
					*/
				}
			}
			Clear();
			//UnityEngine.Debug.Log("No path");
			return null;
		}

		void Clear () {
			if (open.Count > 0) {
				open.Clear();
			}
			if (cameFrom.Count > 0) {
				cameFrom.Clear();
			}
			if (costSoFar.Count > 0) {
				costSoFar.Clear();
			}
			if (numPoints.Count > 0) {
				numPoints.Clear();
			}
		}
		/*
		IEnumerable GetNeighbours (GridLocation current, bool allowStairs) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (x != 0 || y != 0) {
						int xx = x + current.X;
						int yy = y + current.Y;
						GridLocation neigh = new ImmutableGridLocation (current.Layer, xx, yy);
						yield return neigh;
					}
				}
			}
			if (allowStairs) {
				GridLink link = grid.GetLink (current);
				if (link != null) {
					yield return link.Target;
				}
			}
		}
		*/
		public IEnumerable GetNeighbours (GridLocation current, bool allowStairs, GridLocation result) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (x != 0 || y != 0) {
						result.Set( current.Layer, x + current.X, y + current.Y );
						yield return null;
					}
				}
			}
			if (allowStairs) {
				GridLink link = grid.GetLink (current);
				if (link != null) {
					result.Set( link.Target );
					yield return null;
				}
			}
		}
		public float GetCost (GridLocation source, GridLocation target, int accessLevel, bool isTarget, bool ignoreSeen) {
			return costCalculator(source, target, accessLevel, isTarget, ignoreSeen);
		}
		float Heuristic (GridLocation source, GridLocation target) {
			int dx = Math.Abs (source.X - target.X);
			int dy = Math.Abs (source.Y - target.Y);
			if (dx < dy) {
				return dx * diagonalCost + (dy - dx) * minCost;
			} else {
				return dy * diagonalCost + (dx - dy) * minCost;
			}
		}

		List<GridLocation> ReconstructPath(GridLocation target, Dictionary<GridLocation, GridLocation> cameFrom) {
			List<GridLocation> result = new List<GridLocation> ();
			result.Add (target);

			GridLocation current = target;
			while (cameFrom.ContainsKey (current)) {
				current = cameFrom [current];
				result.Add (current);
			}
			result.Reverse();
			return result;
		}

	}
}
