﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map.LayeredGrid.Algorithm {
	public class VisibilityGrid {

		public const int INVISIBLE = 0;
		public const int TILE_VISIBLE = 1;
		public const int CENTER_VISIBLE = 2;
		

		public int xll;
		public int yll;
		public int width;
		public int height;
		public int depth;
		public int bottomLayer;

		public int currentValue;

		public int[][,] data;

		public VisibilityGrid ( int xll, int yll, int bottomLayer, int width, int height, int depth ) {
			this.xll = xll;
			this.yll = yll;
			this.bottomLayer = bottomLayer;
			this.width = width;
			this.height = height;
			this.depth = depth;

			data = new int[depth][,];
			for(int i=0; i<depth; i++) {
				data[i] = new int[width,height];
			}
		}

		public bool Contains (int l, int x, int y) {
			return l >= bottomLayer && l < bottomLayer + depth
					&& x >= xll && y >= yll
					&& x < xll+width && y < yll+height;
		}

		public bool Contains (GridLocation loc) {
			int l = loc.Layer;
			int x = loc.X;
			int y = loc.Y;
			return l >= bottomLayer && l < bottomLayer + depth
					&& x >= xll && y >= yll
					&& x < xll+width && y < yll+height;
		}

		public int Get (int l, int x, int y) {
			return data[l-bottomLayer][x-xll, y-yll];
		}
		public int Get (GridLocation loc) {
			return data[loc.Layer-bottomLayer][loc.X-xll, loc.Y-yll];
		}
		public int GetVisibility (int l, int x, int y) {
			//if (x - xll < 0 || y - yll < 0 || x-xll >= width || y-yll >= height) {
			//	Debug.Log((x-xll)+" "+(y-yll));
			//}
			int value = data[l-bottomLayer][x-xll, y-yll];
			if( value == currentValue ) return CENTER_VISIBLE;
			if( value == currentValue + 1 ) return TILE_VISIBLE;
			return INVISIBLE;
		}
		public int GetVisibility (GridLocation loc) {
			int value = data[loc.Layer-bottomLayer][loc.X-xll, loc.Y-yll];
			if( value == currentValue ) return CENTER_VISIBLE;
			if( value == currentValue + 1 ) return TILE_VISIBLE;
			return INVISIBLE;
		}

		public bool IsCenterVisible (GridLocation loc) {
			return data [loc.Layer - bottomLayer][ loc.X - xll, loc.Y - yll] == currentValue;
		}
		public bool IsTileVisible (GridLocation loc) {
			int value = data [loc.Layer - bottomLayer][ loc.X - xll, loc.Y - yll];
			return value == currentValue || value == currentValue + 1;
		}

		public void Set (int l, int x, int y, int value) {
			data[l-bottomLayer][x-xll, y-yll] = value;
		}
		public void Set (GridLocation loc, int value) {
			data[loc.Layer-bottomLayer][loc.X-xll, loc.Y-yll] = value;
		}


	}
}
