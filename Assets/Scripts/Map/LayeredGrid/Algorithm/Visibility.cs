﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;

namespace Map.LayeredGrid.Algorithm {
	public interface Visibility {

		Dictionary<GridLocation, Visible> FindVisible (GridLocation source, int visionRangeX, int visionRangeY, float radius);
		bool CanSee (GridLocation source, GridLocation target, float maxDist, bool ignoreLOS);
		List<GridLocation> GetLOS (GridLocation source, GridLocation target);
	}

	public enum Visible {
		centerVisible, tileVisible
	};
}
