﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;

namespace Map.LayeredGrid.Algorithm {
	public interface PathFinding {

		List<GridLocation> FindPath (GridLocation source, GridLocation target, int accessLevel, float costLimit, bool allowStairs, bool ignoreSeen);
		IEnumerable GetNeighbours (GridLocation current, bool allowStairs, GridLocation result);
		float GetCost (GridLocation source, GridLocation target, int accessLevel, bool isTarget, bool ignoreSeen);
	}
}
