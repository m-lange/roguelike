﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;
using Game.Visual;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;

namespace Map.TileMap {
	public class TileChunk : MonoBehaviour {

		GridLocation position;
		/*
		public int layer;
		public int tx;
		public int ty;
		*/
		int xmin, xmax, ymin, ymax;
		int size;

		Func<GridLocation, Transform, Tile> tileGenerator;
		Action<Tile> tileReleaser;

		GameObject go;
		Transform mTransform;

		Tile[,] tiles;

		public void Initialize(GridLocation position, int size, 
			Func<GridLocation, Transform, Tile> tileGenerator, Action<Tile> tileReleaser) {
			this.position = new ImmutableGridLocation( position );
			/*
			layer = position.Layer;
			tx = position.X;
			ty = position.Y;
			*/
			this.size = size;

			xmin = this.position.X * size;
			ymin = this.position.Y * size;
			xmax = xmin + size;
			ymax = ymin + size;

			this.tileGenerator = tileGenerator;
			this.tileReleaser = tileReleaser;

			mTransform = transform;
			CreateTiles ();

			gameObject.isStatic = true;
		}

		void Awake() {
			go = gameObject;
		}

		public bool IsActive {
			get { return go.activeSelf; }
		}
		public void SetActive(bool active) {
			go.SetActive (active);
		}

		void CreateTiles () {
			tiles = new Tile[size,size];
			//StartCoroutine(CreateTilesAsync());
			GridLocation pos = new GridLocation(0,0,0);
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					pos.Set(position.Layer, xmin + x, ymin + y);
					//GridLocation pos = new GridLocation(position.Layer, xmin + x, ymin + y);
					tiles [x, y] = tileGenerator (pos, mTransform);
				}
			}
		}

		public System.Collections.IEnumerator CreateTilesAsync () {

			int tileCount = size * size;
			int perFrame = Mathf.CeilToInt(tileCount / 30f);
			int cnt = 1;
			GridLocation pos = new GridLocation(0,0,0);
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					pos.Set(position.Layer, xmin + x, ymin + y);
					//GridLocation pos = new GridLocation(position.Layer, xmin + x, ymin + y);
					tiles [x, y] = tileGenerator (pos, mTransform);
					if( cnt % perFrame == 0 ) {
						yield return null;
					}
					cnt++;
				}
			}
		}

		public void FreeTiles () {
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					tileReleaser (tiles [x, y]);
				}
			}
		}

		public void UpdateTile(GridLocation pos) {
			int x = pos.X - xmin;
			int y = pos.Y - ymin;
			tileReleaser (tiles [x, y]);
			tiles [x, y] = tileGenerator (pos, mTransform);
		}

		public bool Intersects(int xmin, int ymin, int xmax, int ymax) {
			if( xmax < this.xmin || xmin > this.xmax ) {
				return false;
			}
			if( ymax < this.ymin || ymin > this.ymax ) {
				return false;
			}
			return true;
		}

		public void UpdateVisibility (LayeredGrid<TileInfo> grid, VisibilityGrid visible) {
			int xll = position.X * size;
			int yll = position.Y * size;
			int layer = position.Layer;
			GridLocation pos = new GridLocation (0, 0, 0);
			Tile tile;
			TileInfo inf;
			for (int x = xll; x < xll + size; x++) {
				for (int y = yll; y < yll + size; y++) {
					pos.Set (layer, x, y);
					int vis = 0;
					if (visible != null && visible.Contains (layer, x, y)) {
						vis = visible.GetVisibility (layer, x, y);
					}
					//if (GameData.PlayerLocation.Location.Equals (pos)) {
					//	Debug.Log(pos+" "+vis);
					//}
					if (vis >= VisibilityGrid.TILE_VISIBLE) {
						tile = tiles [x - xll, y - yll];
						if (tile != null) {
							tile.SetSeen (true);
							tile.SetVisible (vis == VisibilityGrid.CENTER_VISIBLE);
						} 
					} else {
						inf = grid.Get (layer, x, y);
						if (inf != null && inf.Seen) {
							tile = tiles [x - xll, y - yll];
							if (tile != null) {
								tile.SetSeen (true);
								tile.SetVisible (false);
							}
						}
					}
				}
			}
		}

		public IEnumerator UpdateVisibilityAsync (LayeredGrid<TileInfo> grid, Dictionary<GridLocation, Visible> visible) {
			int xll = position.X * size;
			int yll = position.Y * size;
			GridLocation pos = new GridLocation (0, 0, 0);
			int perFrame = Mathf.CeilToInt ((size * size) / 10f);
			int cnt = 1;
			for (int x = xll; x < xll + size; x++) {
				for (int y = yll; y < yll + size; y++) {
					pos.Set (position.Layer, x, y);
					if (visible != null && visible.ContainsKey (pos)) {
						Tile tile = tiles [x - xll, y - yll];
						if (tile != null) {
							tile.SetSeen (true);
							tile.SetVisible (visible [pos] == Visible.centerVisible);
						} 
					} else {
						TileInfo inf = grid [pos];
						if (inf != null && inf.Seen) {
							Tile tile = tiles [x - xll, y - yll];
							if (tile != null) {
								tile.SetSeen (true);
								tile.SetVisible (false);
							}
						}
					}
					if (cnt % perFrame == 0) {
						yield return null;
					}
					cnt++;
				}
			}
		}
		
	}
}