﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Collections;
using Game;
using Game.Data;
using Game.Events;
using Game.Visual;
using EventSystem;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;

namespace Map.TileMap {
	public class SimpleTileManager : TileManager, GameEventListener<MapChangedEvent> {

		LayeredGrid<TileInfo> grid;
		//TileTypeInfo[] tileInfo;
		//TileApprearanceInfo[] tileAppearance;
		int chunkSize;
		int visionRangeX;
		int visionRangeY;
		int buildRangeX;
		int buildRangeY;
		float layerHeight;
		Transform parent;
		GridLocation cameraPosition;

		GeneratorPool<Tile, int>[] tilePools;

		Dictionary<GridLocation, TileChunk> chunks;

		UnityEngine.Object empty;

		public SimpleTileManager( LayeredGrid<TileInfo> grid, 
					TileTypeInfo[] tileInfo, 
					//TileApprearanceInfo[] tileAppearance, 
					int chunkSize, 
					int visionRangeX, int visionRangeY, 
					int buildRangeX, int buildRangeY, 
					float layerHeight,
					bool createAll) {
			this.grid = grid;
			//this.tileInfo = tileInfo;
			//this.tileAppearance = tileAppearance;
			this.chunkSize = chunkSize;
			this.visionRangeX = visionRangeX;
			this.visionRangeY = visionRangeY;
			this.buildRangeX = buildRangeX;
			this.buildRangeY = buildRangeY;
			this.layerHeight = layerHeight;
			//this.parent = parent;

			GameObject parentGo = (GameObject) GameObject.Instantiate( Resources.Load("Empty") );
			parentGo.name = "Map";
			this.parent = parentGo.transform;

			this.cameraPosition = new GridLocation (0, 0, 0);


			tilePools = new GeneratorPool<Tile, int>[tileInfo.Length];
			for (int i = 0; i < tilePools.Length; i++) {
				tilePools [i] = new GeneratorPool<Tile, int> ( (typeId) => { return CreateNewTile(typeId); }, ReleaseTile, i);
			}

			chunks = new Dictionary<GridLocation, TileChunk> ();

			empty = Resources.Load ("Empty");

			if (createAll) {
				CreateAllChunks ();
			}
		}

		void CreateAllChunks() {
			LayeredGrid.Bounds bounds = grid.Bounds;

			int xmin = ToChunk (bounds.xmin);
			int ymin = ToChunk (bounds.ymin);
			int xmax = ToChunk (bounds.xmax);
			int ymax = ToChunk (bounds.ymax);

			GridLocation pos = new GridLocation (0,0,0);
			for (int l = bounds.lmin; l <= bounds.lmax; l++) {
				for (int x = xmin; x <= xmax; x++) {
					for (int y = ymin; y <= ymax; y++) {
						pos.Set(l,x,y);
						//GridLocation pos = new GridLocation (l, x, y);
						if (! chunks.ContainsKey (pos)) {
							TileChunk chunk = CreateChunk (pos);
							chunk.SetActive (false);
						}
					}
				}
			}
		}

		Tile CreateNewTile(int typeId) {

			//Debug.Log(typeId+" "+Terra.MeshVisible(typeId).name+" "+Terra.TileInfo[typeId].Id);
			GameObject go = (GameObject) GameObject.Instantiate ( Resources.Load("Terrain/AtlasTile") );
			Tile tile = go.AddComponent<Tile> ();
			tile.type = typeId;
			//tile.SetSeenMesh();
			tile.SetSeen (false);
			tile.SetVisible (false);
			return tile;
		/*
			GameObject go = (GameObject) GameObject.Instantiate (tileAppearance [typeId].Prefab);
			Tile tile = go.AddComponent<Tile> ();
			tile.SetTexture((Texture) tileAppearance [typeId].Texture); // GameObject.Instantiate (tileAppearance [typeId].Texture);
			tile.SetSeen (false);
			return tile;
			*/
		}
		void ReleaseTile(Tile tile) {
			tile.gameObject.SetActive (false);
		}

		Tile GetTile( GridLocation pos, Transform parent ) {
			TileInfo info = grid [pos];

			if (grid.IsNoData (info)) {
				return null;
			}

			Tile tile = tilePools [info.Type].Pop ();
			tile.SetPosition (pos, layerHeight);

			tile.transform.SetParent ( parent );
			tile.gameObject.isStatic = true;
			return tile;
		}

		int ToChunk(int x) {
			int i = x / chunkSize;
			if (x < 0) {
				i -= 1;
			}
			return i;
		}
		TileChunk GetChunkWith(GridLocation pos) {
			int x = ToChunk (pos.X);
			int y = ToChunk (pos.Y);
			return chunks [ new GridLocation(pos.Layer, x, y) ];
		}

		public void UpdateCamera(GridLocation camPosition, GridLocation playerPosition) {
			//PerformanceTimer.Timer.StartTimer ("FOV");

			LayeredGrid.Bounds oldBounds = GetCameraBounds (cameraPosition, visionRangeX, visionRangeY, true);
			LayeredGrid.Bounds newBounds = GetCameraBounds (camPosition, visionRangeX, visionRangeY, true);
			LayeredGrid.Bounds buildBounds = GetCameraBounds (camPosition, buildRangeX, buildRangeY, true);

			if ( (!newBounds.Equals (oldBounds)) || chunks.Count == 0 ) {

				//List<GridLocation> rem = new List<GridLocation> ();
				foreach (KeyValuePair<GridLocation, TileChunk> kv in chunks) {
					TileChunk chunk = kv.Value;
					if ( chunk != null && (chunk.IsActive) && (!newBounds.Contains (kv.Key))) {
						chunk.SetActive (false);
						if (camPosition.Layer != cameraPosition.Layer && camPosition.Layer == playerPosition.Layer) {
							chunk.UpdateVisibility (grid, null);
						}
					}
				}


				HashSet<GridLocation> toCreate = new HashSet<GridLocation> ();
				GridLocation pos = new GridLocation (0,0,0);
				for (int l = buildBounds.lmin; l <= buildBounds.lmax; l++) {
					for (int x = buildBounds.xmin; x <= buildBounds.xmax; x++) {
						for (int y = buildBounds.ymin; y <= buildBounds.ymax; y++) {
							pos.Set (l, x, y);
							//GridLocation pos = new GridLocation (l, x, y);
							if (chunks.ContainsKey (pos)) {
								TileChunk chunk = chunks [pos];
								if ( chunk != null && !chunk.IsActive) {
									chunk.SetActive (true);
								}
							} else {
								int xx = x * chunkSize;
								int yy = y * chunkSize;
								if (grid.Intersects (l, xx, yy, xx + chunkSize - 1, yy + chunkSize - 1)) {
									if (camPosition.Layer == cameraPosition.Layer) {
										GridLocation copy = new ImmutableGridLocation( pos );
										toCreate.Add ( copy );
										chunks [copy] = null;
									} else {
										CreateChunk (pos);
									}
								}
							}
						}
					}
				}
				GlobalParameters.Instance.StartCoroutine(CreateChunksAsync(toCreate));
			}

			this.cameraPosition.Set(camPosition);
			//PerformanceTimer.Timer.StopTimer ("FOV");
			//UnityEngine.Debug.Log ("FOV: "+PerformanceTimer.Timer.GetMillis("FOV")+"ms");
		}



		System.Collections.IEnumerator CreateChunksAsync (HashSet<GridLocation> toCreate) {
			yield return null;
			//PerformanceTimer.Timer.StartTimer ("FOV");
			int tileCount = toCreate.Count;
			int perFrame = Mathf.CeilToInt(tileCount / 30f);
			int cnt = 1;
			foreach(GridLocation pos in toCreate) {
				CreateChunk (pos);
				if( cnt % perFrame == 0 ) {
					//UnityEngine.Debug.Log ("FOV: "+PerformanceTimer.Timer.GetMillis("FOV")+"ms");
					yield return null;
				}
				cnt++;
			}
			//PerformanceTimer.Timer.StopTimer ("FOV");
		}
		TileChunk CreateChunk(GridLocation pos) {
			GameObject go = (GameObject)GameObject.Instantiate (empty);
			TileChunk chunk = go.AddComponent<TileChunk> ();
			chunk.Initialize (pos, chunkSize, (p, t) => GetTile (p, t), ReleaseTile);
			chunk.transform.SetParent (parent);
			chunk.SetActive (true);
			chunks [ new ImmutableGridLocation( pos ) ] = chunk;
			chunk.UpdateVisibility (grid, null);
			return chunk;
		}


		public void Notify(MapChangedEvent evt) {
			GridLocation pos = evt.location;
			GetChunkWith (pos).UpdateTile (pos);
		}

		public void UpdateVisibility(VisibilityGrid visible, GridLocation camPosition) {
			//PerformanceTimer.Timer.StartTimer ("FOV");
			LayeredGrid.Bounds newBounds = GetCameraBounds (camPosition, visionRangeX, visionRangeY, true);
			GridLocation position = new GridLocation (0, 0, 0);
			for (int l = newBounds.lmin; l <= newBounds.lmax; l++) {
				for (int x = newBounds.xmin; x <= newBounds.xmax; x++) {
					for (int y = newBounds.ymin; y <= newBounds.ymax; y++) {
						position.Set (l, x, y);
						//GridLocation position = new GridLocation (l, x, y);
						if (chunks.ContainsKey (position)) {
							TileChunk chunk = chunks [position];
							if (chunk != null) {
								chunk.UpdateVisibility (grid, visible);
							}
						}
					}
				}
			}
			//PerformanceTimer.Timer.StopTimer ("FOV");
			//UnityEngine.Debug.Log ("FOV: "+PerformanceTimer.Timer.GetMillis("FOV")+"ms");
		}

		LayeredGrid.Bounds GetCameraBounds(GridLocation position, int rangeX, int rangeY, bool toTop) {
			LayeredGrid.Bounds b = new LayeredGrid.Bounds ();
			b.xmin = ToChunk(position.X - rangeX);
			b.xmax = ToChunk(position.X + rangeX);
			b.ymin = ToChunk(position.Y - rangeY);
			b.ymax = ToChunk(position.Y + rangeY);
			if (position.Layer >= 0) {
				b.lmin = 0;
				b.lmax = toTop ? grid.TopLayer : position.Layer;
			} else {
				b.lmin = position.Layer;
				b.lmax = position.Layer;
			}
			return b;
		}

	}
}
