﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;

namespace Map.TileMap {
	public interface TileManager {
		
		void UpdateCamera (GridLocation camPosition, GridLocation playerPosition);
		void UpdateVisibility (VisibilityGrid visible, GridLocation camPosition);

	}
}
