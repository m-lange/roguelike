﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using Map.LayeredGrid.Algorithm.Impl;
using TurnSystem;
using Game.Parts;

namespace Map {
	public class GridVisibilityManager {
		
		LayeredGrid<TileInfo> grid;
		SpatialEntityManager spatialManager;
		GridVisibility visibility;
		TileTypeInfo[] tileInfo;
		int visionRangeX;
		int visionRangeY;

		VisibilityGrid visible;
		int currentValue = 0;
		int currentIndex = 0;
		VisibilityGrid[] allGrids;

		//Dictionary<GridLocation, Visible> visible = new Dictionary<GridLocation, Visible> ();

		public VisibilityGrid Visible {
			get { return visible; }
		}

		public GridVisibilityManager ( LayeredGrid<TileInfo> grid, SpatialEntityManager spatialManager, TileTypeInfo[] tileInfo, int visionRangeX, int visionRangeY ) {
			this.grid = grid;
			this.spatialManager = spatialManager;
			this.tileInfo = tileInfo;
			this.visionRangeX = visionRangeX;
			this.visionRangeY = visionRangeY;

			visibility = new GridVisibility (grid, CalcViewThrough, GetBlockingEntities, CalcIsWall, CalcIsIlluminated);

			allGrids = new VisibilityGrid[8];
			for (int i = 0; i < allGrids.Length; i++) {
				allGrids[i] = new VisibilityGrid(0, 0, grid.BottomLayer, 2 * visionRangeX + 1, 2 * visionRangeY + 1, grid.LayerCount);
			}
		}

		public void RecalcVisible(GridLocation position, float radius) {
			//PerformanceTimer.Timer.StartTimer ("FOV");
			currentValue += 2;
			currentIndex = (currentIndex + 1) % allGrids.Length;
			//UnityEngine.Debug.Log(currentIndex);
			visible = allGrids[currentIndex];
			visibility.FindVisible (position, visionRangeX, visionRangeY, radius, visible, currentValue);
			//PerformanceTimer.Timer.StopTimer ("FOV");
			//UnityEngine.Debug.Log ("FOV: "+PerformanceTimer.Timer.GetMillis("FOV")+"ms, "+Visible.Count+" tiles.");
		}
		public bool CanSee(GridLocation source, GridLocation target, float maxDist, bool ignoreLOS) {
			return visibility.CanSee (source, target, maxDist, ignoreLOS);
		}
		public List<GridLocation> GetLOS(GridLocation source, GridLocation target) {
			return visibility.GetLOS (source, target);
		}

		bool CalcIsIlluminated (GridLocation target) {
			return target.Layer >= 0;
		}

		bool CalcViewThrough(GridLocation source, GridLocation target, int row, bool blockingEntity) {
			TileInfo vTarget = grid.Get(target);
			TileTypeInfo targetInfo = tileInfo[vTarget.Type];
			if( source.Layer == target.Layer ) {
				return (! targetInfo.IsViewBlocking) && (! blockingEntity);
			} else if(source.Layer > target.Layer) {
				int diff = source.Layer - target.Layer;
				if( (targetInfo.IsViewBlocking || blockingEntity) && row+1 > diff ) {
					return false;
				}
				bool floor = false;
				for(int i=source.Layer; i>target.Layer; i--) {
					TileInfo vSource = grid.Get(i, target.X, target.Y);
					if( ! vSource.Equals(grid.NoData) ) {
						floor = true;
						break;
					}
				}
				return ! floor;
			} else {
				TileInfo vSource = grid.Get(source.Layer, target.X, target.Y);
				TileTypeInfo sourceInfo = tileInfo[vSource.Type];
				if( targetInfo.IsViewBlocking || sourceInfo.IsViewBlocking ) {
					return false;
				}
				bool floor = false;
				for(int i=source.Layer+1; i<=target.Layer; i++) {
					TileInfo vSource2 = grid.Get(i, target.X, target.Y);
					if( ! vSource2.Equals(grid.NoData) ) {
						floor = true;
						break;
					}
				}
				return ! floor;
			}
		}

		bool CalcIsWall(GridLocation target) {
			int vTarget = grid.Get(target).Type;
			TileTypeInfo targetInfo = tileInfo[vTarget];
			return targetInfo.IsViewBlocking;
		}


		HashSet<GridLocation> GetBlockingEntities(GridLocation source, int rangeX, int rangeY) {
			HashSet<GridLocation> result = new HashSet<GridLocation> ();
			Bounds bounds = new Bounds (grid.BottomLayer, grid.TopLayer, 
				source.X - rangeX, source.Y - rangeY, 
				source.X + rangeX, source.Y + rangeY);

			foreach( LocationPart vp in spatialManager.GetInBounds<LocationPart>( LocationPart.partId, bounds, (e) => e.HasPart(ViewBlockingPart.partId) ) ) {
				result.Add (vp.Location);
			}
			return result;
		}
	}
}
