﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using Map.LayeredGrid.Algorithm.Impl;
using TurnSystem;
using Game.Parts;

namespace Map {
	public class VisibilityManager {
		
		LayeredGrid<TileInfo> grid;
		SpatialEntityManager spatialManager;
		Visibility visibility;
		TileTypeInfo[] tileInfo;
		int visionRangeX;
		int visionRangeY;

		Dictionary<GridLocation, Visible> visible = new Dictionary<GridLocation, Visible> ();

		public Dictionary<GridLocation, Visible> Visible {
			get { return visible; }
		}

		public VisibilityManager(LayeredGrid<TileInfo> grid, SpatialEntityManager spatialManager, TileTypeInfo[] tileInfo, int visionRangeX, int visionRangeY) {
			this.grid = grid;
			this.spatialManager = spatialManager;
			this.tileInfo = tileInfo;
			this.visionRangeX = visionRangeX;
			this.visionRangeY = visionRangeY;

			visibility = new RecursiveShadowCastingVisibility<TileInfo> (grid, CalcViewThrough, GetBlockingEntities, CalcIsWall, CalcIsIlluminated);
		}

		public void RecalcVisible(GridLocation position, float radius) {
			//PerformanceTimer.Timer.StartTimer ("FOV");
			visible = visibility.FindVisible (position, visionRangeX, visionRangeY, radius);
			//PerformanceTimer.Timer.StopTimer ("FOV");
			//UnityEngine.Debug.Log ("FOV: "+PerformanceTimer.Timer.GetMillis("FOV")+"ms, "+Visible.Count+" tiles.");
		}
		public bool CanSee(GridLocation source, GridLocation target, float maxDist, bool ignoreLOS) {
			return visibility.CanSee (source, target, maxDist, ignoreLOS);
		}
		public List<GridLocation> GetLOS(GridLocation source, GridLocation target) {
			return visibility.GetLOS (source, target);
		}

		bool CalcIsIlluminated (GridLocation target) {
			return target.Layer >= 0;
		}

		bool CalcViewThrough(GridLocation source, GridLocation target, int row, bool blockingEntity) {
			TileInfo vTarget = grid.Get(target);
			TileTypeInfo targetInfo = tileInfo[vTarget.Type];
			if( source.Layer == target.Layer ) {
				return (! targetInfo.IsViewBlocking) && (! blockingEntity);
			} else if(source.Layer > target.Layer) {
				int diff = source.Layer - target.Layer;
				if( (targetInfo.IsViewBlocking || blockingEntity) && row+1 > diff ) {
					return false;
				}
				bool floor = false;
				for(int i=source.Layer; i>target.Layer; i--) {
					TileInfo vSource = grid.Get(i, target.X, target.Y);
					if( ! vSource.Equals(grid.NoData) ) {
						floor = true;
						break;
					}
				}
				return ! floor;
			} else {
				TileInfo vSource = grid.Get(source.Layer, target.X, target.Y);
				TileTypeInfo sourceInfo = tileInfo[vSource.Type];
				if( targetInfo.IsViewBlocking || sourceInfo.IsViewBlocking ) {
					return false;
				}
				bool floor = false;
				for(int i=source.Layer+1; i<=target.Layer; i++) {
					TileInfo vSource2 = grid.Get(i, target.X, target.Y);
					if( ! vSource2.Equals(grid.NoData) ) {
						floor = true;
						break;
					}
				}
				return ! floor;
			}
		}

		bool CalcIsWall(GridLocation target) {
			int vTarget = grid.Get(target).Type;
			TileTypeInfo targetInfo = tileInfo[vTarget];
			return targetInfo.IsViewBlocking;
		}


		HashSet<GridLocation> GetBlockingEntities(GridLocation source, int rangeX, int rangeY) {
			HashSet<GridLocation> result = new HashSet<GridLocation> ();
			Bounds bounds = new Bounds (grid.BottomLayer, grid.TopLayer, 
				source.X - rangeX, source.Y - rangeY, 
				source.X + rangeX, source.Y + rangeY);

			foreach( LocationPart vp in spatialManager.GetInBounds<LocationPart>( LocationPart.partId, bounds, (e) => e.HasPart(ViewBlockingPart.partId) ) ) {
				result.Add (vp.Location);
			}
			return result;
		}
	}
}
