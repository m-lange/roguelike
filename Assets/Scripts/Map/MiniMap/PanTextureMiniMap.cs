﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using TurnSystem;
using Game.Parts;

using UnityEngine;
using UnityEngine.UI;


namespace Map.MiniMap {
	public class PanTextureMiniMap : MiniMap {

		LayeredGrid<TileInfo> grid;
		SpatialEntityManager spatialManager;
		Texture2D[] textures;
		TileApprearanceInfo[] tileAppearance;

		RawImage image;
		RectTransform playerMarker;
		RectTransform frame;

		int currentLayer;
		float clipWidth;
		float clipHeight;
		int zoom;
		ImmutableGridLocation currentCenter = new ImmutableGridLocation(0,0,0);


		//Color[][] pix; 

		public PanTextureMiniMap(LayeredGrid<TileInfo> grid, SpatialEntityManager spatialManager, TileApprearanceInfo[] tileAppearance, RawImage image, RectTransform playerMarker, RectTransform frame, int zoom) {
			this.grid = grid;
			this.spatialManager = spatialManager;
			this.tileAppearance = tileAppearance;
			this.image = image;
			this.playerMarker = playerMarker;
			this.frame = frame;
			this.zoom = zoom;

			LayeredGrid.Bounds bounds = grid.Bounds;
			textures = new Texture2D[grid.LayerCount]; 
			Color32 black = new Color32 (0, 0, 0, 255);
			for (int i = 0; i < textures.Length; i++) {
				Texture2D texture = new Texture2D (bounds.GetWidth ()+2, bounds.GetHeight ()+2, TextureFormat.ARGB32, false);
				texture.filterMode = FilterMode.Point;
				texture.wrapMode = TextureWrapMode.Clamp;
				textures [i] = texture;
				Color32[] pix = texture.GetPixels32 ();

				for (int j = 0; j < pix.Length; j++) {
					pix [j] = black;
				}
				texture.SetPixels32 (pix);
				texture.Apply ();
			}
			//pix = new Color[textures.Length][];
			RectTransform rect = image.gameObject.GetComponent<RectTransform> ();

			clipWidth = rect.sizeDelta.x / (float) (bounds.GetWidth () + 2);
			clipHeight = rect.sizeDelta.y / (float) (bounds.GetHeight () + 2);

			currentLayer = -1;
			ShowLayer (0);
		}


		public int Zoom {
			get { return zoom; }
		}

		public GridLocation CurrentCenter {
			get { return currentCenter; }
		}

		public void SetZoom(int zoom) {
			this.zoom = zoom; 
			UpdateZoom();
		}

		public void ShowLayer (int layer) {
			if (layer != currentLayer) {
				currentLayer = layer;
				image.texture = textures[ currentLayer - grid.BottomLayer ];
			}
		}

		public void UpdateLocation (GridLocation position) {
			currentCenter = new ImmutableGridLocation( position );
			UpdateZoom ();
		}

		void UpdateZoom () {
			LayeredGrid.Bounds bounds = grid.Bounds;
			int xll = bounds.xmin;
			int yll = bounds.ymin;

			int w = bounds.GetWidth () + 2;
			int h = bounds.GetHeight () + 2;

			float cw = clipWidth / zoom;
			float ch = clipHeight / zoom;
			float xllUV = ((currentCenter.X - xll + 1.5f) / (float)w) - cw / 2f;
			float yllUV = ((currentCenter.Y - yll + 1.5f) / (float)h) - ch / 2f;
			image.uvRect = new Rect (xllUV, yllUV, cw, ch);

			GridLocation player = GameData.PlayerLocation.Location;
			int dx = (player.X - currentCenter.X) * zoom;
			int dy = (player.Y - currentCenter.Y) * zoom;

			playerMarker.anchoredPosition = new Vector2(dx, dy);

			frame.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 2 * GameData.VisionRangeX * zoom);
			frame.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,   2 * GameData.VisionRangeY * zoom);
		}
		/*
		public void UpdateVisibility (GridLocation position, Dictionary<GridLocation, Visible> visible) {
			LayeredGrid.Bounds bounds = grid.Bounds;
			int xll = bounds.xmin;
			int yll = bounds.ymin;

			int w = bounds.GetWidth() + 2;


			Color32[][] pix = new Color32[textures.Length][]; 
			for (int i = 0; i < textures.Length; i++) {
				Texture2D tex = textures [i];
				pix [i] = tex.GetPixels32 ();
			}

			foreach (GridLocation pos in visible.Keys) {
				int x = (pos.X - xll) + 1;
				int y = (pos.Y - yll) + 1;
				int idx = y * w + x;
				Color32[] col = pix [pos.Layer - grid.BottomLayer];

				// TODO keep?
				MiniMapPart mapPart = spatialManager.OneAt<MiniMapPart> (pos); 
				if(mapPart == null) {
					col[idx] = tileAppearance [grid [pos].Type].MapColor;
				} else {
					col[idx] = mapPart.MapColor;
				}
			}

			for (int i = 0; i < textures.Length; i++) {
				//Debug.Log (position.Layer +" "+ (i + grid.BottomLayer));
				if (position != null && position.Layer == i + grid.BottomLayer) {
					int x = (position.X - xll) + 1;
					int y = (position.Y - yll) + 1;
					int idx = y * w + x;
					pix [i][idx] = new Color32 (0,0,255,255);
				}
				Texture2D tex = textures [i];
				tex.SetPixels32 (pix[i]);
				tex.Apply ();
			}
			UpdateLocation(position);
		}
		*/
		public void UpdateVisibility (GridLocation position, VisibilityGrid visible) {
			LayeredGrid.Bounds bounds = grid.Bounds;
			int rx = GameData.VisionRangeX;
			int ry = GameData.VisionRangeY;
			//int xll = Mathf.Max (position.X - rx, bounds.xmin);
			//int yll = Mathf.Max (position.Y - ry, bounds.ymin);
			int xll = position.X - rx;
			int yll = position.Y - ry;

			int w = 2 * rx + 1;
			int h = 2 * ry + 1;

			if (xll + w > bounds.xmax)
				w = bounds.xmax - xll;
			if (yll + h > bounds.ymax)
				h = bounds.ymax - yll;

			if (xll < bounds.xmin) {
				w -= bounds.xmin - xll;
				xll = bounds.xmin;
			}
			if (yll < bounds.ymin) {
				h -= bounds.ymin - yll;
				yll = bounds.ymin;
			}

			int lmin = position.Layer - grid.BottomLayer;
			int lmax = lmin;
			int lBase = grid.BottomLayer;
			if (position.Layer >= 0) {
				lmin = -lBase;
				lmax = grid.TopLayer - lBase;
			}

			Texture2D tex;
			Color[] col;
			int vis;
			GridLocation loc;
			//MiniMapPart mapPart;
			GridLocation tempLoc = new GridLocation (0, 0, 0);
			//Color[][] pix = new Color[textures.Length][]; 
			for (int i = lmin; i <= lmax; i++) {
				tex = textures [i];
				col = tex.GetPixels (xll + 1, yll + 1, w, h);

				int layer = i + lBase;
				int xmax = xll + w;
				int ymax = yll + h;
				for (int xx = xll; xx < xmax; xx++) {
					for (int yy = yll; yy < ymax; yy++) {
						//if (visible.Contains (layer, xx, yy)) {
						vis = visible.GetVisibility (layer, xx, yy);
						if (vis >= VisibilityGrid.TILE_VISIBLE) {
							int x = (xx - xll);
							int y = (yy - yll);
							int idx = y * w + x;
							tempLoc.Set (layer, xx, yy);
							col [idx] = tileAppearance [grid [tempLoc].Type].MapColor;
							/*
							mapPart = spatialManager.OneAt<MiniMapPart> (MiniMapPart.partId, tempLoc); 
							if (mapPart == null) {
								col [idx] = tileAppearance [grid [tempLoc].Type].MapColor;
							} else {
								col [idx] = mapPart.MapColor;
							}
							*/
						}
					}
				}
				foreach (MiniMapPart mmp in spatialManager.GetInBounds<MiniMapPart>(MiniMapPart.partId, 
					new Map.LayeredGrid.Bounds(layer, layer, xll, yll, xll+w-1, yll+h-1), null)) {
					loc = mmp.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
					vis = visible.GetVisibility (loc.Layer, loc.X, loc.Y);
					if (vis >= VisibilityGrid.TILE_VISIBLE) {
						int x = (loc.X - xll);
						int y = (loc.Y - yll);
						int idx = y * w + x;
						col [idx] = mmp.MapColor;
					}
				}

				tex = textures [i];
				tex.SetPixels (xll+1,yll+1,w,h,col);
				tex.Apply ();
				col = null;
			}
			/*
			foreach (GridLocation pos in visible.Keys) {
				int x = (pos.X - xll);
				int y = (pos.Y - yll);
				//UnityEngine.Debug.Log(x+" "+y);
				int idx = y * w + x;
				col = pix [pos.Layer - grid.BottomLayer];

				// TODO keep?
				mapPart = spatialManager.OneAt<MiniMapPart> (MiniMapPart.partId, pos); 
				if(mapPart == null) {
					col[idx] = tileAppearance [grid [pos].Type].MapColor;
				} else {
					col[idx] = mapPart.MapColor;
				}
			}
			//List<MiniMapPart> mapParts = spatialManager.GetInBounds<MiniMapPart>( new Map.LayeredGrid.Bounds(xll, yll, xll+w, yll+w) ); 

			for (int i = lmin; i <= lmax; i++) {
				tex = textures [i];
				tex.SetPixels (xll+1,yll+1,w,h,pix[i]);
				tex.Apply ();
				pix[i] = null;
			}
			*/
			UpdateLocation(position);
		}
		public void UpdateSeen (GridLocation position) {
			LayeredGrid.Bounds bounds = grid.Bounds;
			int xll = bounds.xmin;
			int yll = bounds.ymin;

			int w = bounds.GetWidth() + 2;

			Color32[][] pix = new Color32[textures.Length][]; 
			for (int i = 0; i < textures.Length; i++) {
				Texture2D tex = textures [i];
				pix [i] = tex.GetPixels32 ();
			}


			int lmin = grid.BottomLayer;
			int lmax = grid.TopLayer;
			GridLocation pos = new GridLocation (0,0,0);
			for (int l = lmin; l <= lmax; l++) {
				Grid<TileInfo>[] layer = grid.GetLayer (l);
				Color32[] col = pix [l - grid.BottomLayer];
				foreach (Grid<TileInfo> subGrid in layer) {
					for (int x = subGrid.Xll; x < subGrid.Xll + subGrid.Width; x++) {
						for (int y = subGrid.Yll; y < subGrid.Yll + subGrid.Height; y++) {
							pos.Set (l, x, y);
							TileInfo info = subGrid [x, y];
							if (info.Seen) {
								int xx = (x - xll) + 1;
								int yy = (y - yll) + 1;
								int idx = yy * w + xx;
								MiniMapPart mapPart = spatialManager.OneAt<MiniMapPart> (MiniMapPart.partId, pos);  
								if(mapPart == null) {
									col[idx] = tileAppearance [info.Type].MapColor;
								} else {
									col[idx] = mapPart.MapColor;
									//Debug.Log(mapPart.MapColor);
								}
							}
						}
					}
				}
			}
			for (int i = 0; i < textures.Length; i++) {
			/*
				if (position != null && position.Layer == i + grid.BottomLayer) {
					int x = (position.X - xll) + 1;
					int y = (position.Y - yll) + 1;
					int idx = y * w + x;
					pix [i][idx] = new Color32 (0,0,255,255);
				}
				*/
				Texture2D tex = textures [i];
				tex.SetPixels32 (pix[i]);
				tex.Apply ();
			}
			UpdateLocation(position);
		}
	}
}
