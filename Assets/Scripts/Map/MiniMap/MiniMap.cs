﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;

namespace Map.MiniMap {
	public interface MiniMap {

		void UpdateVisibility (GridLocation position, VisibilityGrid visible);
		void ShowLayer (int layer);
		void UpdateSeen (GridLocation position);

		int Zoom {
			get;
		}
		void SetZoom(int zoom);
	}
}