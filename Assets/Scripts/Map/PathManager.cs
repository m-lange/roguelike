﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using Map.LayeredGrid.Algorithm.Impl;
using TurnSystem;
using Game.Parts;
using Game.Rules;

namespace Map {
	public class PathManager {
		
		LayeredGrid<TileInfo> grid;
		SpatialEntityManager spatialManager;
		TileTypeInfo[] tileInfo;
		PathFinding pathfinder;


		public PathManager(LayeredGrid<TileInfo> grid, SpatialEntityManager spatialManager, TileTypeInfo[] tileInfo) {
			this.grid = grid;
			this.spatialManager = spatialManager;
			this.tileInfo = tileInfo;

			pathfinder = new AStarPathFinding<TileInfo> (grid, GetCost, GameRules.DefaultCost, GameRules.DiagonalCost);
		}

		public List<GridLocation> FindPath (GridLocation source, GridLocation target, int accessLevel, float costLimit, bool allowStairs, bool ignoreIsSeen) {
			if (IsBlocked (target, accessLevel, true, ignoreIsSeen)) {
				return null;
			}
			return pathfinder.FindPath (source, target, accessLevel, costLimit * GameRules.DefaultCost, allowStairs, ignoreIsSeen);
		}

		public float GetCost (GridLocation source, GridLocation target, int accessLevel, bool isTarget, bool ignoreSeen)
		{
			if (IsBlocked (target, accessLevel, isTarget, ignoreSeen)) {
				return -1;
			}
			if (source.X - target.X != 0 && source.Y - target.Y != 0) {
				return GameRules.DiagonalCost;
			}
			return GameRules.DefaultCost;
		}
		
		public IEnumerable GetNeighbours (GridLocation current, bool allowStairs, GridLocation result) {
			return pathfinder.GetNeighbours(current, allowStairs, result);
		}

		TileInfo info;
		TileTypeInfo tti;
		bool IsBlocked (GridLocation target, int accessLevel, bool isTarget, bool ignoreSeen) {
			info = grid [target];
			if ((!ignoreSeen) && (!info.Seen)) {
				return true;
			}
			tti = tileInfo [info.Type];
			if (tti.IsWallLevel(accessLevel)) {
				return true;
			}

			if (!isTarget) {
				if (spatialManager.IsAt<MoveBlockingPart> (MoveBlockingPart.partId, target)) {
					return true;
				}
				/*
				MoveBlockingPart blockAt = spatialManager.OneAt<MoveBlockingPart> (target);
				if(blockAt != null && ! blockAt.Entity.HasPart<ActorPart>()) {
					return true;
				}
				*/
			}
			return false;
		}

		bool IsBlockedByTerrain(GridLocation target, bool ignoreSeen) {
			TileInfo info = grid [target];
			TileTypeInfo tt = tileInfo [info.Type];
			if ( (tt.IsWalkable) || ((!ignoreSeen) && (!info.Seen)) ) {
				return true;
			}
			return false;
		}

	}
}
