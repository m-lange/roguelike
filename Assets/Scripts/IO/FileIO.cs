﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace IO {
	public class FileIO {

		readonly static string newline = System.Environment.NewLine;

	    public static string ReadFile(string file) {
	        string text = null;
	        text = System.IO.File.ReadAllText(file);
	        return text;
	    }
	    public static void WriteText(string file, string text, bool append=false) {
	        if (append) {
	            System.IO.File.AppendAllText(file, text);
	        } else {
	            System.IO.File.WriteAllText(file, text);
	        }
		}
	    public static void WriteMatrix<T> (string file, T[,] matrix, string sep) {
			StringBuilder b = new StringBuilder ();
			for (int y = matrix.GetLength (1) - 1; y >= 0; y--) {
				for (int x = 0; x < matrix.GetLength (0); x++) {
					b.Append( matrix[x,y] );
					if(sep != null) {
						b.Append( sep );
					}
				}
				b.Append( newline );
			}

			System.IO.File.WriteAllText(file, b.ToString());
	    }
	}
}