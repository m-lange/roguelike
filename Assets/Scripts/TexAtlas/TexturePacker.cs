﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TexAtlas {
	public class TexturePacker {


		public TexturePacker () {
			
		}

		public Texture2D CreateAtlas (
			string name,
			string[] textureNames, 
			Color32[] additionalColors, 
			int atlasSize, 
			int textureSize, 
			int padding,
			out Rect[] uv) {

			int numTextures = textureNames.Length * (1 + additionalColors.Length);
			int texPerRow = atlasSize / (textureSize + 2 * padding);

			int maxRows = atlasSize / textureSize;
			int numRows = (1 + additionalColors.Length) * Mathf.CeilToInt (textureNames.Length / (float)texPerRow);

			if (numRows > maxRows) {
				throw new ArgumentException("Textures don't fit on atlas of size "+atlasSize);
			}
			//Debug.Log (numTextures);
			//Debug.Log (texPerRow);
			//Debug.Log (numRows);

			Texture2D atlas = new Texture2D (atlasSize, atlasSize, TextureFormat.ARGB32, true);
			atlas.wrapMode = TextureWrapMode.Clamp;
			atlas.anisoLevel = 1;
			atlas.filterMode = FilterMode.Bilinear;

			Texture2D[] textures = ReadTextures (textureNames, textureSize);

			Color32[] pix = atlas.GetPixels32 ();

			uv = new Rect[numTextures];
			float uvSize = textureSize / (float) atlasSize;

			int cnt = 0;
			for (int t = 0; t < textures.Length; t++) {
				Texture2D tex = textures [t];

				int col = (t % texPerRow);
				int xll = col * (textureSize + 2 * padding) + padding;

				int idx1, idx2;
				Color32[] subPix = tex.GetPixels32 ();
				for (int c = 0; c < additionalColors.Length + 1; c++) {
					int row = (t / texPerRow) * (1 + additionalColors.Length) + c;
					int yll = row * (textureSize + 2 * padding) + padding;
					Color32 mul = (c == 0) ? new Color32() : additionalColors[c-1];


					uv[cnt] = new Rect( xll / (float) atlasSize, yll / (float) atlasSize, uvSize, uvSize );
					for (int x = -padding; x < textureSize + padding; x++) {
						for (int y = -padding; y < textureSize + padding; y++) {
							int xx = x;
							int yy = y;
							if (xx < 0)
								xx = 0;
							if (yy < 0)
								yy = 0;
							if (xx >= textureSize)
								xx = textureSize - 1;
							if (yy >= textureSize)
								yy = textureSize - 1;

							idx1 = ToIndex (xx, yy, textureSize);
							idx2 = ToIndex (x + xll, y + yll, atlasSize);
							if (c == 0) {
								pix [idx2] = subPix [idx1];
							} else {
								pix [idx2] = MulColors(subPix [idx1], mul);
							}
						}
					}
					cnt++;
				}
			}
			atlas.SetPixels32( pix );
			atlas.Apply();

			byte[] data = atlas.EncodeToPNG();
			System.IO.File.WriteAllBytes( GlobalParameters.Instance.TexturePath+"/"+name+".png", data );

			return atlas;
		}

		Color32 MulColors(Color32 c1, Color32 c2) {
			c1.r = (byte) ((c1.r * c2.r) / 255);
			c1.g = (byte) ((c1.g * c2.g) / 255);
			c1.b = (byte) ((c1.b * c2.b) / 255);
			c1.a = (byte) ((c1.a * c2.a) / 255);
			return c1;
		}

		int ToIndex (int x, int y, int width) {
			return y * width + x;
		}

		Texture2D[] ReadTextures (string[] textureNames, int textureSize) {
			Texture2D[] textures = new Texture2D[textureNames.Length];
			for (int tex = 0; tex < textures.Length; tex++) {
				//Texture2D texture = (Texture2D) Resources.Load (textureNames [tex]);
				byte[] bytes = System.IO.File.ReadAllBytes (GlobalParameters.Instance.TexturePath+"/"+textureNames [tex]+".png");
				Texture2D texture = new Texture2D(1,1);
				texture.LoadImage(bytes);
				if (texture.width != textureSize || texture.height != textureSize) {
					throw new ArgumentException( "Texture "+textureNames [tex]+" is not of size "+textureSize+"x"+textureSize+"." );
				}
				textures[tex] = texture;
			}
			return textures;
		}
				
	}
}
