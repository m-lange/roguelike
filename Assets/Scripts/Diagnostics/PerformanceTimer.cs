﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PerformanceTimer : MonoBehaviour {

    private static PerformanceTimer timer;
    public static PerformanceTimer Timer {
        get { return timer; }
    }

    public bool isActive = true;
    //public int measureSteps = 60;

    private Dictionary<string, Stopwatch> watches = new Dictionary<string, Stopwatch>();

	// Use this for initialization
	void Awake () {
        timer = this;
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if(isActive && Time.frameCount % measureSteps == 0) {
            foreach( KeyValuePair<string, Stopwatch> entry in watches) {
                UnityEngine.Debug.Log(entry.Key+": "+ (entry.Value.Elapsed.TotalMilliseconds / measureSteps)+ " ms");
                entry.Value.Reset();
            }
        }
        */
	}
    
    public void StartTimer(string key) {
        if (isActive) {
            Stopwatch sw;
            if (watches.ContainsKey(key)) {
                sw = watches[key];
            } else {
                sw = new Stopwatch();
                watches[key] = sw;
            }
			sw.Reset ();
            sw.Start();
        }
    }
    public void StopTimer(string key) {
        if (watches.ContainsKey(key)) {
            watches[key].Stop();
        }
    }

	public void Reset(string key) {
		watches[key].Reset();;
	}
	public double GetMillis(string key) {
		if (watches.ContainsKey (key)) {
			return watches [key].Elapsed.TotalMilliseconds;
		} else {
			return -1;
		}
	}
}
