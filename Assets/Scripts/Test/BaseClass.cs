﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseClass {
	
}

public class BaseClass<T> : BaseClass where T : BaseClass {

	public static int testVar = 0;
	
}

public class Test {
	static void test<T> () where T : BaseClass<T> {
		
	}
}
