﻿using System;
using System.Collections;
using System.Collections.Generic;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using Map.LayeredGrid.Algorithm.Impl;
using Map.LayeredGrid.Impl;
using Map.LayeredGrid.IO;
using ComponentSystem;
using EventSystem;
using TurnSystem;
using Game;
using Game.AI.Managers;
using Game.Data;
using Game.Events;
using Game.EventListeners;
using Game.Factories;
using Game.Parts;
using Game.Serialization;
using Game.Visual;
using Game.Records;
using UI;
using UnityEngine;
using UnityEngine.UI;

public abstract class GameData {

	static Entity player;
	static LocationPart playerLocation;
	static ActorPart playerActor;
	static HealthPointsPart playerHp;
	static Map.LayeredGrid.Bounds updateBounds;

	static Entity camera;
	static Camera cam;
	static LocationPart cameraLocation;

	static PanTextureMiniMap miniMap;
	static SimpleTileManager tileManager;
	static TurnManager turnManager;
	static SpatialEntityManager spatialManager;
	static GridVisibilityManager visibility;
	static PathManager pathManager;
	static LayeredGrid<TileInfo> map;
	static EventManager eventManager;
	static PatrolPointManager partolPointManager;
	static GameOptions options;

	static KillRecord killRecord;

	static Factions factions;

	static CharacterMapping characterMapping;

	static int visionRangeX;
	static int visionRangeY;
	static int updateRadius;

	static bool isDebugMode;

	static bool saveOnQuit = true;

	static HashSet<string> identifiedNames;

	//public static List<MapEntity> mapEntities;
	//public static List<Entity> proceduralEntities;

	public static void Initialize (
		string mapFile, 
				//EntityVisual playerVisual, 
				//CameraVisual cameraVisual, 
		GridLocation startLocation,
		RawImage mapImage,
		RectTransform playerMarker,
		RectTransform mapFrame,
		RectTransform uiPanel,
		RectTransform miniMapPanel,
		RectTransform miniMapPanelInner,
		MessageWindow messageWindow,
		InventoryWindow inventoryWindow,
		InfoWindow statsWindow,
		float aspectRatio,
		string saveGame, bool isSavePoint,
		MapGen.MapProgressListener progressListener) {

		options = ReadOptions();
		RandUtils.SetSeed( GlobalParameters.Instance.RandomSeed );

		visionRangeY = GlobalParameters.Instance.visionRange;
		//visionRangeX = Mathf.CeilToInt ( visionRangeY * Screen.width / (float) Screen.height );
		visionRangeX = Mathf.CeilToInt (visionRangeY * aspectRatio);
		updateRadius = GlobalParameters.Instance.updateRadius;

		identifiedNames = new HashSet<string> ();
		List<MapEntity> mapEntities = null;
		List<Entity> proceduralEntities = null;
		//mapEntities = null;

		factions = FactionFactory.CreateFactions ();
		eventManager = new EventManager ();

		int stairsUp = Terra.TileInfoId (Terra.StairsUp);
		int stairsDown = Terra.TileInfoId (Terra.StairsDown);
		if (saveGame == null) {
			if (GlobalParameters.Instance.mapFile == "PROCEDURAL") {
				map = new MapGen.MapGenerator (progressListener).Create (out proceduralEntities);
			} else {
				map = SimpleLayeredGridReader<TileInfo>.Read (GlobalParameters.Instance.MapPath, 
					GlobalParameters.Instance.mapFile, 
					(tp, seen) => {
						return new TileInfo ((byte)tp, seen);
					},
					(info) => { 
						if (info.Type == stairsUp)
							return 1;
						if (info.Type == stairsDown)
							return -1;
						return 0;
					},
					Terra.TileInfoIndexMap, out mapEntities, out characterMapping);
			}

		} else {
			string savePoint = isSavePoint ? "_savepoint" : "";
			map = SimpleLayeredGridReader<TileInfo>.Read (GlobalParameters.Instance.SavePath, 
				saveGame + savePoint + GlobalParameters.Instance.MapExtension, 
				(tp, seen) => {
					return new TileInfo ((byte)tp, seen);
				},
				(info) => { 
					if (info.Type == stairsUp)
						return 1;
					if (info.Type == stairsDown)
						return -1;
					return 0;
				},
				Terra.TileInfoIndexMap, out mapEntities, out characterMapping);
		}


		spatialManager = new SpatialEntityManager (map, GlobalParameters.Instance.entityChunkSize, visionRangeX + 2, visionRangeY + 2);
		eventManager.Listen<EntityMovedEvent> (spatialManager);

		turnManager = new TurnManager (spatialManager);


		tileManager = new SimpleTileManager (map, 
			Terra.TileInfo, 
			//Terra.TileAppearance, 
			GlobalParameters.Instance.mapChunkSize, 
			visionRangeX + 2, visionRangeY + 2, 
			visionRangeX + 5, visionRangeY + 5, 
			GlobalParameters.Instance.LayerHeight,
			false);
		eventManager.Listen<MapChangedEvent> (tileManager);

		miniMap = new PanTextureMiniMap (map, spatialManager, Terra.TileAppearance, mapImage, playerMarker, mapFrame, GlobalParameters.Instance.MiniMapZoom);
		visibility = new GridVisibilityManager (map, spatialManager, Terra.TileInfo, visionRangeX, visionRangeY);
		pathManager = new PathManager (map, spatialManager, Terra.TileInfo);


		VisualUpdaterListener visUpdater = new VisualUpdaterListener (visibility);
		eventManager.Listen<EntityMovedEvent> (visUpdater);
		eventManager.Listen<RemoveEntityEvent> (turnManager);

		EntityCreatorListener creatorListener = new EntityCreatorListener (turnManager);
		eventManager.Listen<CreateEntityEvent> (creatorListener);

		StackMergerListener stackListener = new StackMergerListener (spatialManager);
		eventManager.Listen<EntityMovedEvent> (stackListener);

		MessageDispatchListener messageDispatcher = new MessageDispatchListener (spatialManager);
		eventManager.Listen<GameMessageEvent> (messageDispatcher);

		BattleListener battle = new BattleListener ();
		eventManager.Listen<AttackEvent> (battle);
		RangedBattleListener rangedBattle = new RangedBattleListener (map, spatialManager);
		eventManager.Listen<RangedAttackEvent> (rangedBattle);

		EntityDestroyedEffectListener destroyListener = new EntityDestroyedEffectListener ();
		eventManager.Listen<DestroyEntityEvent> (destroyListener);

		ConsumptionListener consumptionListener = new ConsumptionListener ();
		eventManager.Listen<ConsumeEvent> (consumptionListener);

		HighlightCellListener highlightListener = new HighlightCellListener (Prefabs.CellHighlight, Prefabs.RangeHighlight);
		eventManager.Listen<HighlightCellEvent> (highlightListener);
		eventManager.Listen<HighlightRangeEvent> (highlightListener);

		killRecord = new KillRecord ();

		if (saveGame == null) {
			if (mapEntities == null) {
				player = CreateInitialEntities (proceduralEntities);
			} else {
				player = CreateInitialEntities (mapEntities);
			}

			if (player == null) {
				player = CreatureFactory.CreatePlayer (new GridLocation (startLocation), true);
			}
			playerLocation = player.GetPart<LocationPart> ();
			playerActor = player.GetPart<ActorPart> ();
			playerHp = player.GetPart<HealthPointsPart> ();

			camera = CreatureFactory.CreateCamera (new GridLocation (playerLocation.Location), true);
			cameraLocation = camera.GetPart<LocationPart> ();

			DeserializeDeaths ();
		} else {
			string savePoint = isSavePoint ? "_savepoint" : "";

			string xml = IO.FileIO.ReadFile (GlobalParameters.Instance.SavePath + "/" + saveGame + savePoint + GlobalParameters.Instance.SaveExtension);
			SaveGame serial = GameSerializer.Deserialize<SaveGame> (xml);

			DeserializeGame (serial);

			if (isSavePoint) {
				System.IO.File.Delete( GlobalParameters.Instance.SavePath + "/" + saveGame + savePoint + GlobalParameters.Instance.SaveExtension );
				System.IO.File.Delete( GlobalParameters.Instance.SavePath + "/" + saveGame + savePoint + GlobalParameters.Instance.MapExtension );
			}
		}
		UpdateBounds ();

		
		FontSizeListener fontListener = new FontSizeListener ( uiPanel, miniMapPanel, miniMapPanelInner, messageWindow, inventoryWindow, statsWindow );
		eventManager.Listen<ChangeFontSizeEvent> (fontListener);

		CamUpdaterListener camUpdater = new CamUpdaterListener ( camera, player, /*map,*/ tileManager, spatialManager, miniMap, visibility, visionRangeX, visionRangeY);
		EventManager.Listen<EntityMovedEvent> (camUpdater);
		EventManager.Listen<CameraLayerChangedEvent> (camUpdater);
		EventManager.Listen<UpdateVisibilityEvent> (camUpdater);
		EventManager.Listen<UpdateVisibilityDelayedEvent> (camUpdater);

		turnManager.AddUpdatable (camUpdater);
		turnManager.AddUpdatable (stackListener);


		partolPointManager = new PatrolPointManager(map);
		partolPointManager.GeneratePoints( "path", (ti) => ti.Type == MapGen.TownGenerator.FLOOR_PATH, 500 );

		cam = UnityEngine.Camera.main;

		turnManager.Initialize ();

		//EventManager.Notify (new UpdateVisibilityEvent (player.GetPart<LocationPart>().Location));
		LocationPart playerLoc = player.GetPart<LocationPart>();
		((CameraVisual)camera.GetPart<VisualPart> ().Visual).SetLocationForced (playerLoc.Location);
		miniMap.UpdateSeen(playerLoc.Location);
		tileManager.UpdateCamera (playerLoc.Location, playerLoc.Location);
		spatialManager.UpdateSeen();
		EventManager.Notify (new UpdateVisibilityEvent (playerLoc.Location));

		GlobalParameters.Instance.StartCoroutine( InitializeVisibilityDelayed() );

		//IncreaseHeap();

		//Debug.Log (turnManager.EntityCount);

		//EventManager.Notify (new EntityMovedEvent(player, playerLoc, new GridLocation(playerLoc.Location.Layer, playerLoc.Location.X+1, playerLoc.Location.Y)));
	}

	static IEnumerator InitializeVisibilityDelayed () {
		yield return new WaitForSeconds(2f);
		EventManager.Notify (new UpdateVisibilityEvent (playerLocation.Location));
	}

	static void IncreaseHeap () {
		int cnt = 1024*512;
		var tmp = new System.Object[cnt];
		for (int i = 0; i < cnt; i++)
            tmp[i] = new byte[1024];
        tmp = null;
	}
	static GameOptions ReadOptions () {
		if (!System.IO.File.Exists (GlobalParameters.Instance.OptionsPath)) {
			return new GameOptions();
		}
		string xml = IO.FileIO.ReadFile (GlobalParameters.Instance.OptionsPath);
		return GameSerializer.Deserialize<GameOptions> (xml);
	}

	static void DeserializeGame (SaveGame serial) {
		foreach (SaveEntity e in serial.Entities) {
			Entity entity = e.CreateEntity (true);
			EventManager.Notify (new CreateEntityEvent (entity));

			AssignInventoryEntities (entity);

			if (entity.HasPart (PlayerPart.partId)) {
				player = entity;
				playerLocation = player.GetPart<LocationPart> ();
				playerActor = player.GetPart<ActorPart> ();
				playerHp = player.GetPart<HealthPointsPart> ();

				//Debug.Log("PLAYER");
			} else if (entity.HasPart<CameraPart> ()) {
				camera = entity;
				cameraLocation = entity.GetPart<LocationPart> ();
			}
		}

		turnManager.TurnCounter = serial.CurrentTurn;
		turnManager.CurrentEntity = serial.CurrentEntity;

		HerbFactory.PopulateNames (serial.HerbFactories, serial.HerbNames);
		BookFactory.PopulateNames (serial.BookFactories, serial.BookNames);

		identifiedNames.Clear ();
		if (serial.IdentifiedNames != null) {
			foreach (string identified in serial.IdentifiedNames) {
				identifiedNames.Add(identified);
			}
		}

		killRecord.Kills.Clear();

		if (serial.Kills != null) {
			foreach (string kill in serial.Kills) {
				string[] tokens = kill.Split('|');
				killRecord.Kills[tokens[0]] = int.Parse(tokens[1]);
			}
		}
	}

	static void DeserializeDeaths () {
		SaveDeathHistory hist;
		if (!System.IO.File.Exists (GlobalParameters.Instance.DeathsPath)) {
			return;
		}

		string xml = IO.FileIO.ReadFile (GlobalParameters.Instance.DeathsPath);
		hist = GameSerializer.Deserialize<SaveDeathHistory> (xml);

		List<SaveDeath> deaths = hist.deaths;
		for (int i = 0; i < deaths.Count; i++) {
			SaveDeath e = deaths [i];
			GridLocation pos = new GridLocation (e.location);
			TileInfo inf = map.Get (pos);
			TileTypeInfo tti = Terra.TileInfo [inf.Type];
			int cnt = 0;
			while ( cnt < 250 ) {
				if (!map.IsNoData (inf) && tti.IsWalkable && !tti.IsWall) {
					break;
				}
				pos.Set (e.location.Layer, e.location.X + RandUtils.NextInt (-15, 16), e.location.Y + RandUtils.NextInt (-15, 16));
				inf = map.Get (pos);
				tti = Terra.TileInfo [inf.Type];

				cnt++;
			}
			CreatureFactory.CreatePlayerGhost (pos, true, e);
			if (i == deaths.Count - 1) {
				CreatureFactory.CreateHumanCarcass(pos, true);
			}
		}
	}


	public static void AssignInventoryEntities (Entity entity) {
		InventoryPart inv = entity.GetPart<InventoryPart> (InventoryPart.partId);
		if (inv != null) {
			foreach( PickablePart pick in inv.Items ) {
				Entity item = pick.Entity;
				pick.CarrierEntity = entity.Id;
				turnManager.EntityIds[item.Id] = item;
				AssignInventoryEntities(item);
			}
		}
	}

	public static void RevealMap() {
		for (int l = map.BottomLayer; l <= map.TopLayer; l++) {
			Grid<TileInfo>[] layers = map.GetLayer (l);
			foreach (Grid<TileInfo> grid in layers) {
				for (int x = grid.Xll; x < grid.Xll + grid.Width; x++) {
					for (int y = grid.Yll; y < grid.Yll + grid.Height; y++) {
						grid.Get(x, y).Seen = true;
					}
				}
			}
		}
		miniMap.UpdateSeen(playerLocation.Location);
		tileManager.UpdateCamera (playerLocation.Location, playerLocation.Location);
		spatialManager.UpdateSeen();
		EventManager.Notify (new UpdateVisibilityEvent (playerLocation.Location));
	}

	public static void SetPlayerImmortal () {
		PlayerHp.Immortal = true;
	}

	public static Entity CreateInitialEntities(List<MapEntity> mapEntities) {
		if (mapEntities.Count == 0) {
			return null;
		}
		Entity player = null;
		Entity entity = null;
		TileTypeInfo[] tti = Terra.TileInfo;
		foreach (MapEntity me in mapEntities) {
			Func<GridLocation, bool, Entity> creator = EntityFactory.GetCreator (me.entityType);

			if (me.location == null) {
				HashSet<string> tSet = new HashSet<string> ();
				if (me.types != null) {
					foreach (string t in me.types) {
						tSet.Add (t);
					}
				}
				List<GridLocation> locs = map.GetAll (me.layer, (info) => {
					if (map.IsNoData (info)) {
						return false;
					}
					TileTypeInfo tt = tti [info.Type];
					if (me.types == null) {
						return tt.IsWalkable;
					}
					return tSet.Contains (tt.Id);
				});
				if (locs.Count > 0) {
					
					for (int i = 0; i < me.entityCount; i++) {
						//int l = me.layer - map.BottomLayer;
						entity = creator (RandUtils.Select (locs), true);
						if (entity.HasPart<PlayerPart> ()) {
							player = entity;
						}
					}
				}
			} else {
				for (int i = 0; i < me.entityCount; i++) {
					entity = creator (me.location, true);
					if (entity.HasPart<PlayerPart> ()) {
						player = entity;
					}
				}
			}
		}
		return player;
	}

	public static Entity CreateInitialEntities(List<Entity> entities) {
		if (entities.Count == 0) {
			return null;
		}
		Entity player = null;
		TileTypeInfo[] tti = Terra.TileInfo;
		foreach (Entity entity in entities) {
			EventManager.Notify( new CreateEntityEvent(entity) );
			if (entity.HasPart<PlayerPart> ()) {
				player = entity;
			}
		}
		return player;
	}

	public static void UpdateBounds () {
		GridLocation loc = playerLocation.Location;
		if (updateBounds == null) {
			updateBounds = new Map.LayeredGrid.Bounds();
		}
		updateBounds.lmin = loc.Layer >= 0 ? -1 : loc.Layer - 1;
		updateBounds.lmax = loc.Layer >= 0 ? Map.TopLayer : loc.Layer + 1;
		updateBounds.xmin = loc.X - updateRadius;
		updateBounds.xmax = loc.X + updateRadius;
		updateBounds.ymin = loc.Y - updateRadius;
		updateBounds.ymax = loc.Y + updateRadius;
	}

	public static bool IsPlayer (Entity e) {
		return e == player;
	}
	public static bool IsPlayer (ActorPart a) {
		return a == playerActor;
	}


	public static Entity Player {
		get { return player; }
	}
	public static ActorPart PlayerActor {
		get { return playerActor; }
	}
	public static HealthPointsPart PlayerHp {
		get { return playerHp; }
	}
	public static LocationPart PlayerLocation {
		get { return playerLocation; }
	}
	public static Map.LayeredGrid.Bounds PlayerBounds {
		get { return updateBounds; }
	}

	public static Entity Camera {
		get { return camera; }
	}

	public static Camera Cam {
		get { return cam; }
	}

	public static LocationPart CameraLocation {
		get { return cameraLocation; }
	}

	public static Factions Factions {
		get { return factions; }
	}

	public static CharacterMapping CharacterMapping {
		get { return characterMapping; }
	}

	public static LayeredGrid<TileInfo> Map {
		get { return map; }
	}

	public static EventManager EventManager {
		get { return eventManager; }
	}

	public static TurnManager TurnManager {
		get {
			return turnManager;
		}
	}


	public static GridVisibilityManager Visibility {
		get { return visibility; }
	}

	public static PathManager PathManager {
		get { return pathManager; }
	}

	public static SpatialEntityManager SpatialManager {
		get { return spatialManager; }
	}

	public static PatrolPointManager PartolPointManager {
		get { return partolPointManager; }
	}

	public static PanTextureMiniMap MiniMap {
		get { return miniMap; }
	}

	public static GameOptions Options {
		get { return options; }
	}


	public static KillRecord KillRecord {
		get { return killRecord; }
		set { killRecord = value; }
	}

	public static int VisionRangeX {
		get { return visionRangeX; }
	}
	public static int VisionRangeY {
		get { return visionRangeY; }
	}
	public static bool IsDebugMode {
		get { return isDebugMode; }
		set { isDebugMode = value; }
	}
	public static HashSet<string> IdentifiedNames {
		get { return identifiedNames; }
	}

	public static bool SaveOnQuit {
		get { return saveOnQuit; }
		set { saveOnQuit = value; }
	}

	public static void Identify (Entity e) {
		NamePart p = e.GetPart<NamePart> (NamePart.partId);
		if (p != null && !identifiedNames.Contains (p.NameIdentified)) {
			identifiedNames.Add( p.Name +"|"+ p.NameIdentified );
		}
	}
	public static bool IsIdentified (Entity e) {
		NamePart p = e.GetPart<NamePart> (NamePart.partId);
		if (p != null) {
			return identifiedNames.Contains ( p.Name +"|"+ p.NameIdentified );
		}
		return false;
	}
	public static string GetIdentifiedName (Entity e) {
		NamePart p = e.GetPart<NamePart> (NamePart.partId);
		if (p != null && p.NameIdentified != null && identifiedNames.Contains ( p.Name +"|"+ p.NameIdentified )) {
			return p.NameIdentified;
		}
		return null;
	}

	public static void SaveGame(bool isSavePoint) {
		if (TurnManager != null) {
			string name = GlobalParameters.Instance.PlayerName;
			string ser = GameSerializer.Serialize (new SaveGame (), false);
			string file = name;
			if(isSavePoint) file += "_savepoint";
			IO.FileIO.WriteText (GlobalParameters.Instance.SavePath + "/" + file + GlobalParameters.Instance.SaveExtension, ser);
			SimpleLayeredGridReader<TileInfo>.Write (
				(BasicLayeredGrid<TileInfo>)map, 
				GlobalParameters.Instance.SavePath, file + GlobalParameters.Instance.MapExtension, 
				Terra.TileInfo, Terra.DefaultMapping);

		}
	}
	public static void SaveDeath () {
		SaveDeathHistory hist;
		if (System.IO.File.Exists (GlobalParameters.Instance.DeathsPath)) {
			string xml = IO.FileIO.ReadFile (GlobalParameters.Instance.DeathsPath);
			hist = GameSerializer.Deserialize<SaveDeathHistory> (xml);
		} else {
			hist = new SaveDeathHistory ();
		}
		hist.AddPlayer( player );
		string ser = GameSerializer.Serialize (hist, false);
		IO.FileIO.WriteText (GlobalParameters.Instance.DeathsPath, ser);
	}
	public static void SaveOptions () {
		string ser = GameSerializer.Serialize (options, true);
		IO.FileIO.WriteText (GlobalParameters.Instance.OptionsPath, ser);
	}

	public static Vector3 GridToWorld(GridLocation loc) {
		return new Vector3 (loc.X, loc.Y, loc.Layer * -GlobalParameters.Instance.LayerHeight);
	}
	public static GridLocation WorldToGrid(Vector3 vec) {
		return new GridLocation ((int)(vec.z / -GlobalParameters.Instance.LayerHeight), Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y));
	}

	public static bool IsInViewBounds(GridLocation location, int buffer=0) {
		GridLocation camPos = playerLocation.Location;
		if (camPos.Layer >= 0) {
			if (location.Layer < 0)
				return false;
		} else {
			if (location.Layer != camPos.Layer)
				return false;
		}
		return location.X >= camPos.X - visionRangeX && location.X <= camPos.X + visionRangeX 
			&& location.Y >= camPos.Y - visionRangeY && location.Y <= camPos.Y + visionRangeY;
	}
}
