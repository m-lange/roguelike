﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Parts.Sub;
using UnityEngine;
using ComponentSystem;
using Game.Events;
using Game.Parts;
using Game.Data;
using Game.Util;

namespace Game.Rules {
	public class GameRules {

		public enum SatiationStatus {
			Starving, Weak, Hungry, Normal, Satiated, Oversatiated, Bursting
		};
		public enum BurdeningStatus {
			Unburdened = 0, Burdened, Stressed, Strained, Overtaxed, Overloaded
		};
		

		public static readonly int NoCost = -1;
		public static readonly int DefaultCost = 12;
		public static readonly int DiagonalCost = 17;

		public static readonly float PlayerIlluminationDistance = 1.5f;

		public static readonly float MaxThrowingDistance = 10;
		public static readonly float MaxThrowPerStrength = 1000;
		public static readonly float NonProjectileDamagePerMass = 0.001f;
		public static readonly float RangedNonTargetHitProb = 0.5f;

		public static readonly float ProjectileDamageProb = 0.5f;
		public static readonly float LauncherDamageProb = 0.2f;
		public static readonly float WeaponDamageProb = 0.5f;
		public static readonly float ArmorDamageProb = 0.5f;
		public static readonly float ToolDamageProb = 0.5f;

		public static readonly float DefaultUnlockCost = 3f * DefaultCost;
		public static readonly float DefaultTinningCost = 5f * DefaultCost;
		public static readonly float DefaultHarvestCost = 2f * DefaultCost;

		public static readonly int UntinnedLimitedPresence = 100;
		public static readonly string UntinnedLimitedPresenceMessage = "rots away";

		public static readonly int SmallCarcassLimitedPresence = 250;
		public static readonly int MediumCarcassLimitedPresence = 500;
		public static readonly int LargeCarcassLimitedPresence = 1000;


		public static readonly int MinStatsLevel = 1;
		public static readonly int MaxStatsLevel = 100;
		public static readonly int MinSkillLevel = 0;
		public static readonly int MaxSkillLevel = 10;
		public static readonly int PointsForLevelGain = 1000;
		public static readonly int PointsForSkillGain = 1000;
		public static readonly float SkillGainPerHit = 100f;
		public static readonly float SkillGainPerHitRanged = 100f;
		public static readonly float StatsGainPerKill = 5f;

		public static readonly int StomachContentPerTurn = 1;

		public static readonly float WeakStomachContent = 0.1f;
		public static readonly float HungryStomachContent = 0.2f;
		public static readonly float SatiatedStomachContent = 0.6f;
		public static readonly float OversatiatedStomachContent = 0.9f;

		public static readonly int LoseConstitutionPointsWeak = 5;
		public static readonly int LoseConstitutionPointsHungry = 2;
		public static readonly int LoseConstitutionPointsSatiated = 1;
		public static readonly int LoseConstitutionPointsOversatiated = 5;
		public static readonly int GainConstitutionPointsNormal = 1;

		public static readonly int EatNutritionPerTurn = 50;
		public static readonly int DefaultTinNutritionValue = 200;
		public static readonly int DefaultBreadNutritionValue = 200;



		public static readonly float CarryMassPerStrength = 5000;
		public static readonly float BurdenedMass = 0.5f;
		public static readonly float StressedMass = 0.65f;
		public static readonly float StrainedMass = 0.8f;
		public static readonly float OvertaxedMass = 0.9f;
		public static readonly float OverloadedMass = 1.0f;


		public static readonly float BuyPriceGeneral = 1.1f;
		public static readonly float BuyPriceSpecialized = 1f;
		public static readonly float SellPriceGeneral = 0.7f;
		public static readonly float SellPriceSpecialized = 0.8f;
		public static readonly float LimitedUseFixedPriceFraction = 0.5f;
		public static readonly int TraderUpdateInterval = 1000;
		public static readonly int DefaultTraderItems = 10;
		public static readonly int BookTraderItems = 5;
		public static readonly int TraderMoneyMin = 1500;
		public static readonly int TraderMoneyMax = 2500;
		public static readonly int TraderMoneyInc = 100;

		public static readonly float DefaultMessageRadius = 20;
		
		
		public static float CalcRelStatsInv (int level) {
			return (GameRules.MaxStatsLevel - level) / (float) GameRules.MaxStatsLevel;
		}
		public static float CalcRelSkillInv (int skill) {
			return (GameRules.MaxSkillLevel - skill) / (float) GameRules.MaxSkillLevel;
		}
		

		public static int CalcHpPointsRegeneration (ActorPart actor, HealthPointsPart hp) {
			if (hp.RecoverPointsPerTurn > 0) {
				return hp.RecoverPointsPerTurn;
			}
			if (actor.Stats.BurdeningStatus == BurdeningStatus.Unburdened) {
				return RandUtils.RoundRandom (5 * actor.Constitution);
			} else if (actor.Stats.BurdeningStatus == BurdeningStatus.Burdened) {
				return 0;
			} else if (actor.Stats.BurdeningStatus == BurdeningStatus.Stressed) {
				return -20;
			} else {
				return -50;
			}
		}
		public static float CalcJumpingDist (ActorPart actor) {
			return 3.2f;
		}
		public static int CalcUnlockCost (ActorPart actor) {
			return Mathf.CeilToInt(0.01f * (100 - actor.Dexterity) * DefaultUnlockCost);
		}
		public static int CalcTinningCost (ActorPart actor) {
			return Mathf.CeilToInt(0.01f * (100 - actor.Dexterity) * DefaultTinningCost);
		}
		public static int CalcConsumptionCost (ActorPart actor, ConsumablePart consumable) {
			return Mathf.CeilToInt(Game.Rules.GameRules.DefaultCost * consumable.NutritionValue / (float) EatNutritionPerTurn);
		}
		public static int CalcHarvestCost (ActorPart actor, PlantPart plant) {
			return Mathf.CeilToInt(0.01f * (100 - actor.Dexterity) * DefaultHarvestCost);
		}

		public static int CalcAttackCost (ActorPart attacker, EquipmentPart weapon) {
			int baseCost = DefaultCost * attacker.Stats.AttackDuration;
			if (weapon != null && weapon.AttackDuration > 0) {
				baseCost = DefaultCost * weapon.AttackDuration;
				if (weapon.Skill != Skills.None) {
					int skill = attacker.Stats.Skill[ (int) weapon.Skill ];
					baseCost = Mathf.CeilToInt( baseCost * (0.25f + 0.075f * (10 - skill)) );
				}
			}
			baseCost = Mathf.CeilToInt( baseCost * (0.5f + 0.005f * (100 - attacker.Dexterity)) );
			return baseCost;
		}

		public static int CalcReloadCost (ActorPart attacker, PickablePart thrown, bool launcher) {
			int baseCost = DefaultCost;
			ProjectilePart pro = thrown.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (pro != null) {
				baseCost = DefaultCost * (launcher ? pro.ReloadTurnsFired : pro.ReloadTurns);
				if (pro.Skill != Skills.None && (pro.LauncherType == LauncherType.None || launcher)) {
					int skill = attacker.Stats.Skill[ (int) pro.Skill ];
					baseCost = Mathf.CeilToInt( baseCost * (0.25f + 0.075f * (10 - skill)) );
				}
			}
			baseCost = Mathf.CeilToInt( baseCost * (0.5f + 0.005f * (100 - attacker.Dexterity)) );
			return baseCost;
		}

		public static bool CalcIsHit (ActorPart attacker, ActorPart defender, EquipmentPart weapon) {
			return RandUtils.NextBool( CalcHitChance(attacker, defender, weapon) );
		}
		public static float CalcHitChance (ActorPart attacker, ActorPart defender, EquipmentPart weapon) {
			//float prob = 0.0002f * attacker.Dexterity * (100 - defender.Defense);
			int skill = (weapon == null) ? MaxSkillLevel : attacker.Stats.Skill[(int) weapon.Skill];
			return 0.01f * (100 - defender.Defense) * (0.5f + 0.5f * skill / (float) MaxSkillLevel);
		}
		public static bool CalcIsHitRanged (ActorPart attacker, PickablePart thrown, float range, bool isTarget, float baseProb) {
			return CalcIsHitRanged(attacker, null, thrown, range, isTarget, baseProb, false);
		}
		public static bool CalcIsHitRanged (ActorPart attacker, ActorPart defender, PickablePart thrown, float relDist, bool isTarget, float baseProb, bool launcher) {
			return RandUtils.NextBool( CalcHitChanceRanged(attacker, defender, thrown, relDist, isTarget, baseProb, launcher) );
		}
		public static float CalcHitChanceRanged (ActorPart attacker, ActorPart defender, PickablePart thrown, float relDist, bool isTarget, float baseProb, bool launcher) {
			if( relDist > 1 ) return 0f;
			float rangeScale = (relDist < 0.5) ? 1f : 0.5f + (1 - relDist);
			float prob = rangeScale * baseProb; // * 0.02f * attacker.Dexterity;
			ProjectilePart pro = thrown.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (pro != null) {
				if (pro.Skill != Skills.None && (pro.LauncherType == LauncherType.None || launcher)) {
					//prob += attacker.Stats.Skill[ (int) pro.Skill ] * 5;
					prob *= (0.5f + 0.5f * attacker.Stats.Skill[ (int) pro.Skill ] / (float) MaxSkillLevel);
				}
			}
			if (defender != null) {
				prob *= 0.01f * (100 - defender.Defense);
			}
			if (!isTarget) {
				prob *= RangedNonTargetHitProb;
			}
			return prob;
		}
		
		
		public static int CalcDamage (ActorPart attacker, ActorPart defender, EquipmentPart weapon) {
			float dmg = CalcMaxDamage(attacker, defender, weapon);
			return RandUtils.RoundRandom((RandUtils.NextFloat( 0.5f ) + 0.5f) * dmg);
		}
		public static float CalcMaxDamage (ActorPart attacker, ActorPart defender, EquipmentPart weapon) {
			int skill = (weapon == null) ? 0 : attacker.Stats.Skill[(int) weapon.Skill];
			float a = attacker.Attack * (1f + skill / (float) MaxSkillLevel);
			int attType = (weapon == null) ? attacker.Stats.AttackType : weapon.AttackType;
			int b = defender.Armor.ByType[attType];
			float dmg = a * (1f - 0.01f * b);
			return dmg;
		}


		public static bool HasDamageRanged (PickablePart thrown, bool launcher) {
			ProjectilePart pro = thrown.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (pro != null) {
				int a = launcher ? pro.DamageFired : pro.Damage;
				return a > 0;
			} else {
				return true;
			}
		}
		public static int CalcDamageRanged (ActorPart attacker, ActorPart defender, PickablePart thrown, bool launcher) {
			float dmg = CalcMaxDamageRanged(attacker, defender, thrown, launcher);
			return RandUtils.RoundRandom((RandUtils.NextFloat( 0.5f ) + 0.5f) * dmg);
		}
		
		public static float CalcMaxDamageRanged (ActorPart attacker, ActorPart defender, PickablePart thrown, bool launcher) {
			float a = 0;
			int b = 0;
			int[] armor = defender.Armor.ByType;
			ProjectilePart p = thrown.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (p != null) {
				a = launcher ? p.DamageFired : p.Damage;
				if (p.Skill != Skills.None && (p.LauncherType == LauncherType.None || launcher)) {
					a *= 1f + attacker.Stats.Skill[ (int) p.Skill ] / (float) MaxSkillLevel;
				}
				b = armor[ p.AttackType ];
			} else {
				a = thrown.Mass * GameRules.NonProjectileDamagePerMass;
				b = armor[ (int) AttackTypes.Impact ];
			}
			//float dmg = a * a / (float )(2 * b);
			float dmg = a * (1f - 0.01f * b);
			return dmg;
		}

		public static int CalcRange (ActorPart actor, PickablePart thrown, bool hasLauncher) {
			float range = 0;
			ProjectilePart p = thrown.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (p != null) {
				range = hasLauncher ? p.RangeFired : p.Range;
				if (range > 0) {
					if (p.Skill != Skills.None && (p.LauncherType == LauncherType.None || hasLauncher)) {
						range *= 1f + actor.Stats.Skill[ (int) p.Skill ] / 10f;
					}
					return (int) range;
				}
			}
			float maxMass = actor.Strength * GameRules.MaxThrowPerStrength;
			float minMass = 100;
			float mass = thrown.Mass;
			range = GameRules.MaxThrowingDistance - ((GameRules.MaxThrowingDistance - 1) * (mass - minMass) / (maxMass - minMass));
			return (int) range;
		}

		public static int CalcDamageFallen (ActorPart actor, int height) {
			int mass = 0;
			InventoryPart inv = actor.Entity.GetPart<InventoryPart>(InventoryPart.partId);
			if (inv != null) {
				mass = inv.GetMass() / 1000;
			}
			return 10 * height + RandUtils.NextInt( Mathf.CeilToInt(0.25f * mass * height) );
		}

		public static int CalcSkillPoints (ActorPart attacker, ProjectilePart projectile, bool launcher) {
			if (projectile.Skill != Skills.None && (projectile.LauncherType == LauncherType.None || launcher)) {
				return RandUtils.RoundRandom(SkillGainPerHitRanged * CalcRelSkillInv(attacker.Stats.Skill[(int) projectile.Skill]));
			}
			return 0;
		}

		public static int CalcSkillPoints (ActorPart attacker, EquipmentPart attackWeapon) {
			if (attackWeapon != null && attackWeapon.Skill != Skills.None) {
				return RandUtils.RoundRandom(SkillGainPerHit * CalcRelSkillInv(attacker.Stats.Skill[(int) attackWeapon.Skill]));
			}
			return 0;
		}



		public static int CalcMaxMass (ActorPart actor) {
			return (int) (actor.Strength * CarryMassPerStrength);
		}


		public static int CalcPrice (PickablePart pick, bool sell, bool specialized) {
			float price = pick.Price;
			LimitedUsePart lu = pick.Entity.GetPart<LimitedUsePart> (LimitedUsePart.partId);
			if (lu != null) {
				if (!lu.BreaksAtRandom) {
					float remaining = lu.UsesRemaining / (float)lu.MaxUses;
					price = LimitedUseFixedPriceFraction * price + (1 - LimitedUseFixedPriceFraction) * price * remaining;
				}
			} else {
				LightSourcePart ls = pick.Entity.GetPart<LightSourcePart> (LightSourcePart.partId);
				if (ls != null) {
					float remaining = ls.TurnsRemaining / (float)ls.MaxTurns;
					if (ls.DisappearWhenEmpty) {
						price = price * remaining;
					} else {
						price = LimitedUseFixedPriceFraction * price + (1 - LimitedUseFixedPriceFraction) * price * remaining;
					}
				}
			}
			float frac = (sell)
							? ((specialized) ? SellPriceSpecialized : SellPriceGeneral)
							: ((specialized) ? BuyPriceSpecialized : BuyPriceGeneral );
			
			return Mathf.RoundToInt (price * frac);
		}



		public static void LimitedUseDamage (ActorPart actor, Entity entity, float prob, bool remove) {
			LimitedUsePart lu = entity.GetPart<LimitedUsePart> (LimitedUsePart.partId);
			if (lu != null) {
				LimitedUseDamage(actor, lu, prob, remove);
			}
		}
		public static void LimitedUseDamage (ActorPart actor, LimitedUsePart damage, float prob, bool remove) {
			if ((prob >= 1 || RandUtils.NextBool (prob))) {
				LimitedUseDamage(actor, damage, 1, remove);
			}
		}
		public static void LimitedUseDamage (ActorPart actor, Entity entity, int value, bool remove) {
			LimitedUsePart lu = entity.GetPart<LimitedUsePart> (LimitedUsePart.partId);
			if (lu != null) {
				LimitedUseDamage(actor, lu, value, remove);
			}
		}
		public static void LimitedUseDamage (ActorPart actor, LimitedUsePart damage, int value, bool remove) {
			bool destroyed = damage.Damage (value);
			if (destroyed) {
				if (GameData.IsPlayer (actor) && damage.Message != null) {
					string text = "The " + TextUtil.GetName (damage) + " " + damage.Message + ".";
					GameData.EventManager.Notify (new ShowWarningEvent (actor.Entity, text));
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, text));
					Sounds.Play(Sounds.Crack);
				}
				if (remove) {
					GameData.EventManager.Notify (new RemoveEntityEvent (damage.Entity, true));
					GameData.EventManager.Notify (new DestroyEntityEvent (damage.Entity, actor.Entity));
				}
			}
		}
	}
}
