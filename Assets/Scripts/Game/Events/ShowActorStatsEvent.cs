﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowActorStatsEvent : GameEvent {

		public ActorPart actor;
		public string title;

		public ShowActorStatsEvent( ActorPart actor, string title ) {
			this.actor = actor;
			this.title = title;
		}
	}
}
