﻿using ComponentSystem;
using EventSystem;

namespace Game.Events {
	public class RemoveEntityEvent : GameEvent {

		public Entity entity;
		public bool permanent;

		public RemoveEntityEvent( Entity entity, bool permanent ) {
			this.entity = entity;
			this.permanent = permanent;
		}
		
	}
}
