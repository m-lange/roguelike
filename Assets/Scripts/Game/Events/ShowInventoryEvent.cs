﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowInventoryEvent : GameEvent {

		public Entity entity;
		public string title;
		public OrderedDict<string, PickablePart> inventory;
		public Func<PickablePart, bool> filter;
		public PickablePart selected;

		public ShowInventoryEvent( Entity entity, string title, OrderedDict<string, PickablePart> inventory, Func<PickablePart, bool> filter, PickablePart selected ) {
			this.entity = entity;
			this.title = title;
			this.inventory = inventory;
			this.filter = filter;
			this.selected = selected;
		}
		public ShowInventoryEvent( Entity entity, string title, OrderedDict<string, PickablePart> inventory) 
		: this(entity, title, inventory, null, null) {

		}
	}
}
