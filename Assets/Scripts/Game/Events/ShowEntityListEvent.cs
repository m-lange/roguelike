﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowEntityListEvent : GameEvent {

		public Entity entity;
		public string title;
		public List<Entity> entities;
		public Func<Entity, bool> filter;

		public ShowEntityListEvent( Entity entity, string title, List<Entity> entities, Func<Entity, bool> filter ) {
			this.entity = entity;
			this.title = title;
			this.entities = entities;
			this.filter = filter;
		}
	}
}
