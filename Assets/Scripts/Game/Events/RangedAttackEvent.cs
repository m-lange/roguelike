﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class RangedAttackEvent : GameEvent {

		public ActorPart attacker;
		public GridLocation target;
		public PickablePart projectile;
		public int range;
		public EquipmentPart launcher;

		public RangedAttackEvent( ActorPart attacker, GridLocation target, PickablePart projectile, int range, EquipmentPart launcher ) {
			this.attacker = attacker;
			this.target = target;
			this.projectile = projectile;
			this.range = range;
			this.launcher = launcher;
		}
	}
}
