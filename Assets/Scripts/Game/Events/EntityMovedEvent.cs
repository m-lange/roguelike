﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class EntityMovedEvent : GameEvent {

		public readonly Entity entity;
		public readonly LocationPart location;
		public readonly GridLocation oldLocation;

		public EntityMovedEvent( Entity entity, LocationPart location, GridLocation oldLocation ) {
			this.entity = entity;
			this.location = location;
			this.oldLocation = oldLocation;
		}
	}
}
