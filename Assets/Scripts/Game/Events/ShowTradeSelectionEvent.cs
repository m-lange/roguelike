﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowTradeSelectionEvent : GameEvent {

		public Entity entity;
		public string title;
		public OrderedDict<string, PickablePart> inventory;
		public bool sell;
		public bool specialized;

		public ShowTradeSelectionEvent( Entity entity, string title, OrderedDict<string, PickablePart> inventory, bool sell, bool specialized ) {
			this.entity = entity;
			this.title = title;
			this.inventory = inventory;
			this.sell = sell;
			this.specialized = specialized;
		}
	}
}
