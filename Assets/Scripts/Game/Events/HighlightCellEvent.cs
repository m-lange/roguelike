﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class HighlightCellEvent : GameEvent {

		public Entity entity;
		public GridLocation location;

		public HighlightCellEvent( Entity entity, GridLocation location ) {
			this.entity = entity;
			this.location = location;
		}
	}
}
