﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class UpdateVisibilityDelayedEvent : GameEvent {

		public GridLocation sourceLocation;

		public UpdateVisibilityDelayedEvent( GridLocation sourceLocation ) {
			this.sourceLocation = sourceLocation;
		}
	}
}
