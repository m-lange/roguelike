﻿using ComponentSystem;
using EventSystem;

namespace Game.Events {
	public class CreateEntityEvent : GameEvent {

		public Entity entity;

		public CreateEntityEvent( Entity entity ) {
			this.entity = entity;
		}
		
	}
}
