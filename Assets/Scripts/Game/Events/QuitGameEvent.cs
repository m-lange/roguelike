﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class QuitGameEvent : GameEvent {

		public bool deleteSaveGame;

		public QuitGameEvent( bool deleteSaveGame ) {
			this.deleteSaveGame = deleteSaveGame;
		}
	}
}
