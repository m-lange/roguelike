﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Game.Messages;

namespace Game.Events {
	public class GameMessageEvent : GameEvent {

		public Entity entity;
		public Message message;

		public GameMessageEvent( Entity entity, Message message ) {
			this.entity = entity;
			this.message = message;
		}
	}
}
