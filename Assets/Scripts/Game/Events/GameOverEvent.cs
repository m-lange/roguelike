﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class GameOverEvent : GameEvent {

		public Entity entity;

		public GameOverEvent( Entity entity ) {
			this.entity = entity;
		}
	}
}
