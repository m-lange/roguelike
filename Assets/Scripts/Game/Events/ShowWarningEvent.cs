﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowWarningEvent : GameEvent {

		public Entity entity;
		public string text;

		public ShowWarningEvent( Entity entity, string text ) {
			this.entity = entity;
			this.text = text;
		}
	}
}
