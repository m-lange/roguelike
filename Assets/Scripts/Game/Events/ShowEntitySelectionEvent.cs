﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowEntitySelectionEvent : GameEvent {

		public Entity entity;
		public string title;
		public OrderedDict<string, Entity> inventory;
		public Func<Entity, bool> filter;

		public ShowEntitySelectionEvent( Entity entity, string title, OrderedDict<string, Entity> inventory, Func<Entity, bool> filter ) {
			this.entity = entity;
			this.title = title;
			this.inventory = inventory;
			this.filter = filter;
		}
	}
}
