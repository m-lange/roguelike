﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class AttackEvent : GameEvent {

		public ActorPart attacker;
		public ActorPart defender;
		public bool isPassive;

		public AttackEvent( ActorPart attacker, ActorPart defender, bool isPassive ) {
			this.attacker = attacker;
			this.defender = defender;
			this.isPassive = isPassive;
		}
	}
}
