﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class UpdateVisibilityEvent : GameEvent {

		public GridLocation sourceLocation;

		public UpdateVisibilityEvent( GridLocation sourceLocation ) {
			this.sourceLocation = sourceLocation;
		}
	}
}
