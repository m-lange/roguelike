﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class MessageEvent : GameEvent {

		public Entity entity;
		public string message;

		public MessageEvent( Entity entity, string message ) {
			this.entity = entity;
			this.message = message;
		}
	}
}
