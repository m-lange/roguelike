﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class HighlightRangeEvent : GameEvent {

		public Entity entity;
		public GridLocation location;
		public float range;

		public HighlightRangeEvent( Entity entity, GridLocation location, float range ) {
			this.entity = entity;
			this.location = location;
			this.range = range;
		}
	}
}
