﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ConsumeEvent : GameEvent {

		public ActorPart actor;
		public ConsumablePart consumable;

		public ConsumeEvent( ActorPart actor, ConsumablePart consumable ) {
			this.actor = actor;
			this.consumable = consumable;
		}
	}
}
