﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ShowTextSelectionEvent : GameEvent {

		public Entity entity;
		public string title;
		public string[] entries;
		public Entity selected;

		public ShowTextSelectionEvent( Entity entity, string title, string[] entries, Entity selected ) {
			this.entity = entity;
			this.title = title;
			this.entries = entries;
			this.selected = selected;
		}
	}
}
