﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Events {
	public class MapChangedEvent : GameEvent {

		public GridLocation location;

		public MapChangedEvent( GridLocation location ) {
			this.location = location;
		}
	}
}
