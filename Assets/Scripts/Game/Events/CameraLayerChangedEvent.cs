﻿using System.Collections;
using System.Collections.Generic;

using Game.Parts;
using EventSystem;

namespace Game.Events {
	public class CameraLayerChangedEvent : GameEvent {

		public int delta;

		public CameraLayerChangedEvent( int delta ) {
			this.delta = delta;
		}
	}
}
