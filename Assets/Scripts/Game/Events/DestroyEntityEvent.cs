﻿using ComponentSystem;
using EventSystem;

namespace Game.Events {
	public class DestroyEntityEvent : GameEvent {

		public Entity entity;
		public Entity destroyer;

		public DestroyEntityEvent( Entity entity, Entity destroyer ) {
			this.entity = entity;
			this.destroyer = destroyer;
		}
		
	}
}
