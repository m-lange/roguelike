﻿using System;
using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using EventSystem;
using Game.Parts;

namespace Game.Events {
	public class ChangeFontSizeEvent : GameEvent {
		
		public bool increase;

		public ChangeFontSizeEvent( bool increase ) {
			this.increase = increase;
		}
	}
}
