﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(HerbUsable))]
	public class HerbUsable : Usable {

		public bool IsTargetless { get{ return false; } }
		public bool AllowItems { get{ return true; } }
		public bool AllowDirections { get{ return true; } }
		public string Use { get{ return "transer effects"; } }

		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (useWith != null) {
				if (tool.HasPart<ConsumablePart>() && useWith.HasPart<ConsumablePart> () && ! useWith.HasPart<TinnedPart> ()) {
					ConsumablePart src = tool.GetPart<ConsumablePart> ();
					ConsumablePart trg = useWith.GetPart<ConsumablePart> ();
					return new CopyConsumableEffectsAction (actor, src, trg, true);
				}
			}
			return null;
		}

		
	}
}
