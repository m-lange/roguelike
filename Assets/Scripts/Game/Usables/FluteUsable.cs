﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Messages;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(FluteUsable))]
	public class FluteUsable : Usable {

		public bool IsTargetless { get{ return true; } }
		public bool AllowItems { get{ return false; } }
		public bool AllowDirections { get{ return false; } }
		public string Use { get{ return "play music"; } }

		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			return new SendMessageAction ( 
						actor, 
						new PlayFluteMessage(actor.Entity, 10, true, actor.Entity.GetPart<LocationPart>().Location),
						3 * GameRules.DefaultCost,
						"You play the flute.", Sounds.Flute );
		}

		
	}
}
