﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(OnOffUsable))]
	public class OnOffUsable : Usable {

		public bool IsTargetless { get{ return true; } }
		public bool AllowItems { get{ return false; } }
		public bool AllowDirections { get{ return false; } }
		public string Use { get{ return "switch on and off" ;} }

		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (tool != null) {
				OnOffPart ls = tool.GetPart<OnOffPart>(OnOffPart.partId);
				if (ls != null) {
					return new ToggleOnOffAction (actor, ls);
				}
			}
			return null;
		}

		
	}
}
