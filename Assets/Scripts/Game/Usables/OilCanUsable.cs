﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(OilCanUsable))]
	public class OilCanUsable : Usable {

		public bool IsTargetless { get{ return false; } }
		public bool AllowItems { get{ return true; } }
		public bool AllowDirections { get{ return true; } }
		public string Use { get{ return "refill light sources"; } }


		//int chargeTurns;

		public OilCanUsable (  ) {
			//this.chargeTurns = chargeTurns;
		}


		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (useWith != null) {
				LightSourcePart ls = useWith.GetPart<LightSourcePart>(LightSourcePart.partId);
				if (ls != null && ! ls.DisappearWhenEmpty) {
					return new RefillLightSourceAction (actor, tool, ls);
				}
			}
			return null;
		}
		/*
		[DataMember]
		public int ChargeTurns {
			get { return chargeTurns; }
			set { chargeTurns = value; }
		}
		*/
	}
}
