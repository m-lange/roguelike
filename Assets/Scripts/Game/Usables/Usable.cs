﻿using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Parts;

namespace Game.Usables {
	public interface Usable {

		//UseEffectRule[] Rules { get; }
		bool IsTargetless { get; }
		bool AllowItems { get; }
		bool AllowDirections { get; }
		string Use { get; }

		Action GetAction(ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo);
	}
}
