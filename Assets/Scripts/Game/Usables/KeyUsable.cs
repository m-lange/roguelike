﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions.Actor.Secondary;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;
using Game.Data;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(KeyUsable))]
	public class KeyUsable : Usable {
		
		//static UseEffectRule[] rules;

		//public UseEffectRule[] Rules { get{ return rules; } }
		public bool IsTargetless { get{ return false; } }
		public bool AllowItems { get{ return true; } }
		public bool AllowDirections { get{ return true; } }
		public string Use { get{ return "unlock doors and chests"; } }


		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (useWith != null) {
				if (useWith.HasPart<ContainerPart> ()) {
					ContainerPart cont = useWith.GetPart<ContainerPart> ();
					if (cont.IsLocked) {
						return new UnlockContainerAction (actor, cont, tool);
					} else if(cont.Lockable) {
						return new LockContainerAction (actor, cont, tool);
					}
				}
			} if (locationInfo != null) {
				TileTypeInfo typeInfo = Terra.TileInfo [locationInfo.Type];
				if( typeInfo.Id == Terra.DoorClosed ) {
					return new LockDoorAction(actor, location, tool);
				} else if( typeInfo.Id == Terra.DoorLocked ) {
					return new UnlockDoorAction(actor, location, tool);
				} else if( typeInfo.Id == Terra.WindowClosed ) {
					return new LockWindowAction(actor, location, tool);
				} else if( typeInfo.Id == Terra.WindowLocked ) {
					return new UnlockWindowAction(actor, location, tool);
				}
			}
			return null;
		}
		/*
		static KeyUsable() {
			rules = new UseEffectRule[] {
				new UseEffectRule("Door", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if(info != null) {
						TileTypeInfo typeInfo = Terra.TileInfo[info.Type];
						if( typeInfo.Id == Terra.DoorClosed ) {
							return new LockDoorEffect(actor.Entity, loc);
						} else if( typeInfo.Id == Terra.DoorLocked ) {
							return new UnlockDoorEffect(actor.Entity, loc);
						}
					}
					return null;
				}),
				new UseEffectRule("Window", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if(info != null) {
						TileTypeInfo typeInfo = Terra.TileInfo[info.Type];
						if( typeInfo.Id == Terra.WindowClosed ) {
							return new LockWindowEffect(actor.Entity, loc);
						} else if( typeInfo.Id == Terra.WindowLocked ) {
							return new UnlockWindowEffect(actor.Entity, loc);
						}
					}
					return null;
				})
			};
		}
		*/
		
	}
}
