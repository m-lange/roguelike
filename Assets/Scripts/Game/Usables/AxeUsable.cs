﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(AxeUsable))]
	public class AxeUsable : Usable {

		static int damage = 50;
		//static UseEffectRule[] rules;

		//public UseEffectRule[] Rules { get{ return rules; } }
		public bool IsTargetless { get{ return false; } }
		public bool AllowItems { get{ return true; } }
		public bool AllowDirections { get{ return true; } }
		public string Use { get{ return "destroy things"; } }

		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (locationInfo != null) {
				TileTypeInfo typeInfo = Terra.TileInfo [locationInfo.Type];
				if (typeInfo.Id == Terra.DoorLocked
				    || typeInfo.Id == Terra.DoorClosed) {
					return new DestroyDoorAction (actor, location, tool, 0.01f * actor.Strength);
				} else if (typeInfo.Id == Terra.WindowLocked
				           || typeInfo.Id == Terra.WindowClosed) {
					return new DestroyWindowAction (actor, location, tool, 0.9f);
				}
			} else {
				if (useWith != null) {
					if (useWith.HasPart<ActorPart> ()) {
						return new AttackAction (actor, useWith.GetPart<ActorPart> (), GameData.IsPlayer(actor) );
					} else if (useWith.HasPart<DestructablePart> ()) {
						DestructablePart dest = useWith.GetPart<DestructablePart> ();
						return new DestructionAction (actor, dest, tool, RandUtils.RoundRandom(0.01f * damage * actor.Strength));
					}
				}
			}
			return null;
		}

		/*
		static AxeUsable() {
			rules = new UseEffectRule[] {
				new UseEffectRule("Door", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if(info != null) {
						TileTypeInfo typeInfo = Terra.TileInfo[info.Type];
						if( typeInfo.Id == Terra.DoorLocked 
							|| typeInfo.Id == Terra.DoorClosed ) {
							return new DestroyDoorEffect(actor.Entity, loc, 0.333f);
						}
					}
					return null;
				}),
				new UseEffectRule("Window", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if(info != null) {
						TileTypeInfo typeInfo = Terra.TileInfo[info.Type];
						if( typeInfo.Id == Terra.WindowLocked 
							|| typeInfo.Id == Terra.WindowClosed ) {
							return new DestroyWindowEffect(actor.Entity, loc, 0.9f);
						}
					}
					return null;
				}),
				new UseEffectRule("Damage", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if( target != null && target.HasPart<DestructablePart>() ) {
						DestructablePart dest = target.GetPart<DestructablePart>();
						//self.Cost = Mathf.CeilToInt(dest.Hp / (float) damage);
						//return new DestructionEffect(actor.Entity, dest, dest.Hp);
						return new DestructionEffect(actor.Entity, dest, damage);
					}
					return null;
				}),
				new UseEffectRule("Attack", GameRules.DefaultCost, (self, actor, tool, target, loc, info) => {
					if( target != null && target.HasPart<ActorPart>() ) {
						return new AttackEffect(actor, target.GetPart<ActorPart>());
					}
					return null;
				})
			};
		}
		*/
		
	}
}
