﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using UnityEngine;

using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions;
using Game.Actions.Actor;
using Game.Actions.Actor.Secondary;
using Game.Data;
using Game.Parts;
using Game.Effects;
using Game.Events;
using Game.Rules;


namespace Game.Usables {

	[DataContract]
	[KnownType(typeof(TinOpenerUsable))]
	public class TinOpenerUsable : Usable {

		public bool IsTargetless { get{ return false; } }
		public bool AllowItems { get{ return true; } }
		public bool AllowDirections { get{ return false; } }
		public string Use { get{ return "open tin cans"; } }

		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			if (useWith != null) {
				if (useWith.HasPart<TinnedPart> ()) {
					TinnedPart dest = useWith.GetPart<TinnedPart> ();
					return new UntinningAction (actor, dest, tool);
				}
			}
			return null;
		}

		
	}
}
