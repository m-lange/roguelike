﻿using System;
using ComponentSystem;
using Game.Effects;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Usables {
	public class UseEffectRule {

		int cost;
		string name;
		public string Name { get{ return name; } }
		public int Cost { 
			get{ return cost; } 
			set{ cost = value; } 
		}


		public delegate Effect Rule(UseEffectRule self, ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo);

		Rule rule;
	
		public UseEffectRule(string name, int cost, Rule rule) {
			this.name = name;
			this.cost = cost;
			this.rule = rule;
		}

		public Effect Apply(ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			return rule(this, actor, tool, useWith, location, locationInfo );
		}
		
	}
}
