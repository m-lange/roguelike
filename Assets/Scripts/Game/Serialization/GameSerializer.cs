﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Xml.Linq;
using ComponentSystem;
using Game.ActorEffects;
using Game.ActorEffects.Continuous;
using Game.AI;
using Game.Destructables;
using Game.EntityEffects;
using Game.Usables;
using UnityEngine;
using Game.Parts;

namespace Game.Serialization {
	public class GameSerializer {
		
		public static string Serialize (object obj, bool pretty) {
			using (MemoryStream memoryStream = new MemoryStream ()) {
				
				DataContractSerializer serializer = new DataContractSerializer (obj.GetType (), KnownTypes());
				serializer.WriteObject (memoryStream, obj);
				string xml = Encoding.UTF8.GetString (memoryStream.ToArray ());
				if (pretty) {
					xml = PrettyXml(xml);
				}
				return xml;
			}
		}

		public static object Deserialize(string xml, Type toType)
		{
			
			using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
			{
				XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(memoryStream, Encoding.UTF8, new XmlDictionaryReaderQuotas(), null);
				
				DataContractSerializer serializer = new DataContractSerializer(toType, KnownTypes());
				return serializer.ReadObject(reader);
			}

		}

		public static T Deserialize<T>(string rawXml)
		{
			using (XmlReader reader = XmlReader.Create(new StringReader(rawXml)))
			{
				DataContractSerializer formatter0 = 
					new DataContractSerializer(typeof(T), KnownTypes());
				return (T)formatter0.ReadObject(reader);
			}
		}
		
		static List<Type> KnownTypes() {
			List<Type> known = new List<Type>();
			known.AddRange( SubTypes(typeof(Part)) );
			known.AddRange( SubTypes(typeof(ActorAI)) );
			known.AddRange( SubTypes(typeof(Usable)) );
			known.AddRange( SubTypes(typeof(Destructable)) );
			known.AddRange( SubTypes(typeof(ActorEffect)) );
			known.AddRange( SubTypes(typeof(ContinuousEffect)) );
			known.AddRange( SubTypes(typeof(EntityEffect)) );
			return known;
		}
		static List<Type> SubTypes(Type tp) {
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			List<Type> known = new List<Type>();
			//Debug.Log("----- "+tp+" ------");
			foreach(Type t in types) {
				if( (t.IsSubclassOf(tp) || tp.IsAssignableFrom(t)) && ! t.IsAbstract) {
					known.Add(t);
					//Debug.Log(t);
				}
			}
			return known;
		}

		static string PrettyXml(string xml) {
		    var stringBuilder = new StringBuilder();

		    var element = XElement.Parse(xml);

		    var settings = new XmlWriterSettings();
		    settings.OmitXmlDeclaration = true;
		    settings.Indent = true;
		    settings.NewLineOnAttributes = true;

		    using (var xmlWriter = XmlWriter.Create(stringBuilder, settings))
		    {
		        element.Save(xmlWriter);
		    }

		    return stringBuilder.ToString();
		}

	}
}
