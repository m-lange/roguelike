﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

using ComponentSystem;
using Game.Parts;
using Game.Parts.Sub;
using Map.LayeredGrid;

namespace Game.Serialization {

	[DataContract]
	public class SaveDeath {

		[DataMember]
		public string name;
		[DataMember]
		public ActorStats stats;
		[DataMember]
		public HealthPointsPart hp;
		[DataMember]
		public SaveEntity[] inventory;
		[DataMember]
		public GridLocation location;

		public SaveDeath() {

		}
		public SaveDeath ( string name, Entity entity ) {
			this.name = name;
			if (entity.HasPart (ActorPart.partId)) {
				stats = entity.GetPart<ActorPart> (ActorPart.partId).Stats;
			}
			if (entity.HasPart (HealthPointsPart.partId)) {
				hp = entity.GetPart<HealthPointsPart> (HealthPointsPart.partId);
			}
			if (entity.HasPart (LocationPart.partId)) {
				location = entity.GetPart<LocationPart> (LocationPart.partId).LocationForSerialization;
			}
			if (entity.HasPart (InventoryPart.partId)) {
				InventoryPart inv = entity.GetPart<InventoryPart> (InventoryPart.partId);
				List<SaveEntity> items = new List<SaveEntity>();
				foreach (PickablePart item in inv.Items) {
					NamePart np = item.Entity.GetPart<NamePart> (NamePart.partId);
					if (np != null && np.NameIdentified == null && ! item.Entity.HasPart(ContainerPart.partId)) {
						items.Add( new SaveEntity(item.Entity) );
					}
				}
				inventory = items.Count == 0 ? null : items.ToArray();
			}
		}
	}
}
