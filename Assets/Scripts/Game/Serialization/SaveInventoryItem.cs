﻿using System.Runtime.Serialization;

namespace Game.Serialization {

	[DataContract(Namespace="SaveGame")]
	public class SaveInventoryItem {

		[DataMember]
		public string Key;

		[DataMember]
		public SaveEntity Entity;

		public SaveInventoryItem ( ) {

		}
		/*
		public SaveInventoryItem(string key, SaveEntity entity) {			
			this.Key = key;
			this.Entity = entity;
		}
		*/

	}
}
