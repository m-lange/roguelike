﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

using ComponentSystem;
using Game.Factories;
using Game.Parts;

namespace Game.Serialization {

	[DataContract]
	public class SaveDeathHistory {

		[IgnoreDataMember]
		public List<SaveDeath> deaths;

		public SaveDeathHistory () {
			deaths = new List<SaveDeath>();
		}

		public void AddPlayer (Entity player) {
			deaths.Add( new SaveDeath(GlobalParameters.Instance.PlayerName, player) );
		}

		[DataMember]
		public SaveDeath[] DeathsForSerialization {
			get { return deaths.Count == 0 ? null : deaths.ToArray(); }
			set { deaths = value == null ? new List<SaveDeath>() : new List<SaveDeath>(value); }
		}
	}
}
