﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

using ComponentSystem;
using Game.Factories;

namespace Game.Serialization {

	[DataContract(Namespace="SaveGame")]
	public class SaveGame {

		[DataMember]
		public SaveEntity[] Entities;

		[DataMember]
		public int CurrentEntity;

		[DataMember]
		public int CurrentTurn;

		[DataMember]
		public string[] HerbFactories;
		[DataMember]
		public string[] HerbNames;

		[DataMember]
		public string[] BookFactories;
		[DataMember]
		public string[] BookNames;

		[DataMember]
		public string[] IdentifiedNames;

		[DataMember]
		public string[] Kills;

		public SaveGame () {
			CurrentTurn = GameData.TurnManager.TurnCounter;
			CurrentEntity = GameData.TurnManager.CurrentEntity;
			Entities = new SaveEntity[GameData.TurnManager.Entities.Count];
			int i = 0;
			foreach (Entity entity in GameData.TurnManager.Entities) {
				Entities [i] = new SaveEntity (entity);
				i++;
			}

			Dictionary<string, string> herbs = HerbFactory.Names;
			HerbFactories = new string[herbs.Count];
			HerbNames = new string[herbs.Count];
			int cnt = 0;
			foreach (KeyValuePair<string, string> kv in herbs) {
				HerbFactories [cnt] = kv.Key;
				HerbNames [cnt] = kv.Value;
				cnt++;
			}


			Dictionary<string, string> books = BookFactory.Names;
			BookFactories = new string[books.Count];
			BookNames = new string[books.Count];
			cnt = 0;
			foreach (KeyValuePair<string, string> kv in books) {
				BookFactories [cnt] = kv.Key;
				BookNames [cnt] = kv.Value;
				cnt++;
			}


			if (GameData.IdentifiedNames.Count > 0) {
				IdentifiedNames = new string[GameData.IdentifiedNames.Count];
				GameData.IdentifiedNames.CopyTo (IdentifiedNames);
			}

			if (GameData.KillRecord.Kills.Count > 0) {
				Kills = new string[GameData.KillRecord.Kills.Count];
				cnt = 0;
				foreach (KeyValuePair<string, int> kv in GameData.KillRecord.Kills) {
					Kills [cnt] = kv.Key+"|"+kv.Value;
					cnt++;
				}
			}
		}
		
	}
}
