﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

using ComponentSystem;

namespace Game.Serialization {

	[DataContract]
	[KnownType(typeof(SaveEntity))]
	public class SaveEntity {

		[DataMember]
		public long id;
		[DataMember]
		public bool isActive;
		[DataMember]
		public bool updatable;
		[DataMember]
		public List<Part> Parts;

		public SaveEntity () {

		}
		public SaveEntity ( Entity entity ) {
			id = entity.Id;
			isActive = entity.IsActive;
			updatable = entity.Updatable;
			Parts = new List<Part> ();
			//foreach (KeyValuePair<Type, Part> kv in entity.Parts) {
			foreach (Part p in entity.Parts.Values) {
				if (p != null) {
					Parts.Add (p);
				}
			}
		}

		public Entity CreateEntity(bool useId) {
			Entity entity = useId ? new Entity (id, updatable) : new Entity (updatable);
			foreach (Part p in Parts) {
				entity.Attach (p);
			}
			return entity;
		}
		
	}
}
