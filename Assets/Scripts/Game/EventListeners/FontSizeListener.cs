﻿using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using ComponentSystem;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using EventSystem;
using Game.Parts;
using Game.Events;
using TurnSystem;
using UnityEngine.UI;

namespace Game.EventListeners {
	public class FontSizeListener :
							GameEventListener<ChangeFontSizeEvent> {

		RectTransform panel;
		RectTransform miniMap;
		RectTransform miniMapInner;
		Text messages;
		Text inventory;
		Text stats;

		public FontSizeListener(RectTransform panel, RectTransform miniMap, RectTransform miniMapInner, MessageWindow messages, InventoryWindow inventory, InfoWindow stats) {
			this.panel = panel;
			this.miniMap = miniMap;
			this.miniMapInner = miniMapInner;
			this.messages = messages.GetComponent<Text>();
			this.inventory = inventory.GetComponent<Text>();
			this.stats = stats.GetComponent<Text>();
			
		}

		public void Notify (ChangeFontSizeEvent evt) {
			int size = messages.fontSize;
			if( evt.increase ) size += 1;
			else size -= 1;
			
			float width = (26.6667f * size);
			/*
			panel.sizeDelta = new Vector2( width, panel.sizeDelta.y );
			miniMap.sizeDelta = new Vector2( width-10, miniMap.sizeDelta.y );
			miniMapInner.sizeDelta = new Vector2( width-12, miniMap.sizeDelta.y );
			*/
			messages.fontSize = size;
			inventory.fontSize = size;
			stats.fontSize = size;
		}
	}
}
