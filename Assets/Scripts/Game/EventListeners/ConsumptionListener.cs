﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ComponentSystem;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using EventSystem;
using Game.Parts;
using Game.Events;
using TurnSystem;

namespace Game.EventListeners {
	public class ConsumptionListener :
							GameEventListener<ConsumeEvent> {



		public ConsumptionListener() {
		}

		public void Notify (ConsumeEvent evt) {
			int energy = evt.consumable.NutritionValue;
			evt.actor.AddEnergy (energy);
			if (evt.consumable.Effects != null) {
				foreach (ActorEffects.ActorEffect effect in evt.consumable.Effects) {
					if (effect.Execute (evt.actor)) {
						if (GameData.IsPlayer (evt.actor) && effect.Message != null) {
							GameData.EventManager.Notify (new MessageEvent (evt.actor.Entity, effect.Message));	
						}
					}
				}
			}
		}
	}
}
