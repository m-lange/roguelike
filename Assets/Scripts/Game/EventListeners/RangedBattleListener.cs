﻿using System.Collections;
using System.Collections.Generic;
using Game.EntityEffects;
using UnityEngine;
using ComponentSystem;
using EventSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Actions.Interface;
using Game.Events;
using Game.EventListeners;
using Game.Parts;
using Game.Destructables;
using Game.Effects;
using Game.Rules;
using Game.ActorEffects;
using Game.Util;
using Game.Messages;
using TurnSystem;

namespace Game.EventListeners {
	public class RangedBattleListener : GameEventListener<RangedAttackEvent> {

		LayeredGrid<TileInfo> map;
		SpatialEntityManager spatialManager;

		List<Transform> indicators;

		public RangedBattleListener (LayeredGrid<TileInfo> map, SpatialEntityManager spatialManager) {
			this.map = map;
			this.spatialManager = spatialManager;

			indicators = new List<Transform>();
		}

		public void Notify (RangedAttackEvent evt) {
			ActorPart att = evt.attacker;
			bool playerIsAtt = GameData.IsPlayer (att); 

			LocationPart source = att.Entity.GetPart<LocationPart> (LocationPart.partId);
			if (source == null) {
				return;
			}

			List<GridLocation> locs = GameData.Visibility.GetLOS (source.Location, evt.target);
			locs.Add (evt.target);



			TileInfo info;
			TileTypeInfo tti;
			bool hit = false;
			GridLocation endLocation = new GridLocation (source.Location);

			foreach (GridLocation loc in locs) {
				info = map.Get (loc);
				tti = Terra.TileInfo [info.Type]; 
				if (tti.IsWall) {
					if (tti.Id == Terra.WindowClosed || tti.Id == Terra.WindowLocked) {
						hit = true;
						if (!ThrowAtWindow (att, loc, evt.projectile)) {
							break;
						}
					} else {
						if (playerIsAtt) {
							GameData.EventManager.Notify (new MessageEvent (att.Entity, 
								"The " + TextUtil.GetName (evt.projectile) + " hits a " + tti.Name + "."));
							Sounds.Play (Sounds.RangedHit);
						}
						hit = true;
						break;
					}
				}
				float baseHitProb = CalcBaseHitProb (source.Location, evt.target, loc);
				List<MoveBlockingPart> blocking = new List<MoveBlockingPart> (spatialManager.GetAt<MoveBlockingPart> (MoveBlockingPart.partId, loc));
				List<ActorPart> actorsHere = new List<ActorPart> (spatialManager.GetAt<ActorPart> (ActorPart.partId, loc));
				float relDist = source.Location.Distance (loc) / (float)evt.range;
				if (blocking.Count > 0) {
					if (actorsHere.Count == 0) {
						if (ThrowAtObstacles (att, blocking, evt.projectile, relDist, loc.Equals (evt.target), baseHitProb)) {
							hit = true;
							break;
						}
					}
				}
				endLocation.Set (loc);
				if (actorsHere.Count > 0) {
					if (ThrowAtActors (att, actorsHere, evt.projectile, relDist, evt.launcher != null, loc.Equals (evt.target), baseHitProb)) {
						hit = true;
						break;
					}
				} else if( loc == locs[ locs.Count-1 ] ) {
					List<OnHitEffectPart> effectsHere = new List<OnHitEffectPart> (spatialManager.GetAt<OnHitEffectPart> (OnHitEffectPart.partId, loc));
					if( effectsHere.Count > 0 ) {
						if (ThrowAtEffect (att, effectsHere, evt.projectile, relDist, loc.Equals (evt.target), baseHitProb)) {
							hit = true;
							break;
						}
					}
				}
			}

			if ((!hit) && playerIsAtt) {
				GameData.EventManager.Notify (new MessageEvent (att.Entity, "The " + TextUtil.GetName (evt.projectile) + " misses."));
				Sounds.Play (Sounds.RangedMiss);
			}
			GridLocation floorPos = MapUtil.GetFloor (map, endLocation);
			if (floorPos == null) {
				GameData.EventManager.Notify (new RemoveEntityEvent (evt.projectile.Entity, true));
				GameData.EventManager.Notify (new MessageEvent (att.Entity, "The " + TextUtil.GetName (evt.projectile) + " disappears."));
			} else {
				evt.projectile.Entity.GetPart<LocationPart> (LocationPart.partId).Location = floorPos;
			}

			GameRules.LimitedUseDamage (att, evt.projectile.Entity, GameRules.ProjectileDamageProb, true);
			if (evt.launcher != null) {
				GameRules.LimitedUseDamage (att, evt.launcher.Entity, GameRules.LauncherDamageProb, true);
			}
			ShowIndicator( source.Location, endLocation );
			//def.Attacked (att);

		}


		void ShowIndicator (GridLocation start, GridLocation end) {
			Transform indicator = null;
			for (int i = 0; i < indicators.Count; i++) {
				if (!indicators [i].gameObject.activeSelf) {
					indicator = indicators [i];
					break;
				}
			}
			if (indicator == null) {
				indicator = ( (GameObject) GameObject.Instantiate( Resources.Load( Prefabs.RangedIndicator ) ) ).transform;
				indicators.Add(indicator);
			}
			Vector3 startPos = GameData.GridToWorld(start);
			Vector3 endPos = GameData.GridToWorld(end);
			Vector3 dir = endPos - startPos;
			indicator.position = startPos;
			indicator.localScale = new Vector3(1, dir.magnitude, 1 );
			indicator.up = dir;
			indicator.gameObject.SetActive(true);
			GlobalParameters.Instance.StartCoroutine( HideIndicator(indicator, 0.6f) );
		}

		IEnumerator HideIndicator (Transform indicator, float seconds) {
			yield return new WaitForSeconds(seconds);
			indicator.gameObject.SetActive(false);
		}


		void LevelUp (ActorPart attacker, ActorPart defender) {
			attacker.GainDexterity( defender.Defense * 10 );
		}
		void LevelUp (ActorPart attacker, int value) {
			attacker.GainDexterity( value );
		}


		void GainSkill (ActorPart attacker, PickablePart projectile, bool launcher) {
			ProjectilePart p = projectile.Entity.GetPart<ProjectilePart> (ProjectilePart.partId);
			if (p != null) {
				if (p.Skill != Skills.None && (p.LauncherType == LauncherType.None || launcher)) {
					attacker.Stats.GainSkill (p.Skill, GameRules.CalcSkillPoints(attacker, p, launcher) );
				}
			}
		}

		bool ThrowAtActors (ActorPart attacker, List<ActorPart> defenders, PickablePart projectile, float relDist, bool launcher, bool isTarget, float baseHitProb) {
			ActorPart defender = RandUtils.Select (defenders);
			bool isHit = GameRules.CalcIsHitRanged (attacker, defender, projectile, relDist, isTarget, baseHitProb, launcher);

			bool hasDamage = GameRules.HasDamageRanged (projectile, launcher);
			bool consumed = false;
			ConsumablePart consumable = null;
			if (isHit || isTarget) {
				if (projectile.Entity.HasPart (ConsumablePart.partId) && !projectile.Entity.HasPart (TinnedPart.partId)) {
					consumable = projectile.Entity.GetPart<ConsumablePart> (ConsumablePart.partId);
					bool poison = false;
					if (consumable.Effects != null) {
						foreach (ActorEffect eff in consumable.Effects) {
							if (eff is PoisonEffect || eff is AttachSlowPoisonEffect) {
								poison = true;
							}
						}
					}
					if (!poison) {
						if (consumable.IsVegetarian) {
							consumed = defender.Notify (new HitByVegetarianFoodMessage (attacker.Entity, 0, false, null));
						} else {
							consumed = defender.Notify (new HitByNonVegetarianFoodMessage (attacker.Entity, 0, false, null));
						}
					}
				}
				if ((!consumed) && hasDamage) {
					GameData.EventManager.Notify (new GameMessageEvent (attacker.Entity, 
						new ActorAttackedMessage (attacker.Entity, GameRules.DefaultMessageRadius, true, defender.Entity.GetPart<LocationPart> (LocationPart.partId).Location, attacker, defender)));
				}
			}
			if (consumed && consumable != null) {
				defender.UseActionPoints (GameRules.DefaultCost);

				GameData.EventManager.Notify (new RemoveEntityEvent (projectile.Entity, true));
				GameData.EventManager.Notify (new ConsumeEvent (defender, consumable));
				GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "The " + TextUtil.GetName (defender) + " eats the " + TextUtil.GetName (projectile)));
				if (GameData.IsPlayer (attacker) || GameData.IsPlayer (defender)) {
					Sounds.Play (Sounds.RangedMiss);
					Sounds.Play (Sounds.Eat);
				}
				return true;
			}

			if (isHit) {

				int damage = GameRules.CalcDamageRanged (attacker, defender, projectile, launcher);

				if (GameData.IsPlayer (attacker)) {
					string message = "The " + TextUtil.GetName (projectile) + " hits the " + TextUtil.GetName (defender)+".";
					if(GameData.IsDebugMode) message += " (" + damage + ")";
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, message));
				} else if (GameData.IsPlayer (defender)) {
					string message = "You are hit by " + TextUtil.GetNameIndef (projectile)+".";
					if(GameData.IsDebugMode) message += " (" + damage + ")";
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, message));
				}
				if (damage > 0) {
					bool destroyed = defender.Entity.HasPart (HealthPointsPart.partId) 
						? defender.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).DealDamage (damage)
						: true;

					LevelUp (attacker, 5);
					GainSkill (attacker, projectile, launcher);

					if (destroyed) {

						LevelUp (attacker, defender);

						if (GameData.IsPlayer (defender)) {
							GameData.EventManager.Notify (new MessageEvent (defender.Entity, 
								"You are killed by the " + TextUtil.GetName (projectile) + "-trowing " + TextUtil.GetName (attacker)));
							//att.SetNextAction ( new GameOverAction(att, "Game over!") );
						} else {
							DestroyEntity (defender, attacker.Entity);
							GameData.KillRecord.AddKill(defender);
							if (GameData.IsPlayer (attacker)) {
								GameData.EventManager.Notify (new MessageEvent (attacker.Entity, 
									"You kill the " + TextUtil.GetName (defender))); 
							}
						}
					}
				}
				if (hasDamage) {
					defender.Attacked (attacker);
				}
				if (GameData.IsPlayer (attacker) || GameData.IsPlayer (defender)) {
					Sounds.Play (Sounds.RangedHit);
				}
				return true;
			} else {
				if (GameData.IsPlayer (defender)) {
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "You are almost hit by " + TextUtil.GetNameIndef (projectile)));
					Sounds.Play (Sounds.RangedMiss);
				}
			}
			return false;
		}
		bool ThrowAtWindow (ActorPart attacker, GridLocation location, PickablePart projectile) {
			float probability = projectile.Mass * 0.01f;
			if (GameData.IsPlayer(attacker)) {
				GameData.EventManager.Notify (new GameMessageEvent(attacker.Entity, 
					new UnauthorizedAccessMessage(attacker.Entity, GameRules.DefaultMessageRadius, false, location) ) );
			}
			if (RandUtils.NextFloat () < probability) {
				GameData.Map.Set(location, new TileInfo (Terra.TileInfoId (Terra.WindowDestroyed)) );
				GameData.EventManager.Notify (new MapChangedEvent (location));
				GameData.EventManager.Notify (new UpdateVisibilityEvent (location));
				GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "The "+TextUtil.GetName(projectile)+" destroys the window."));
				if (GameData.IsPlayer (attacker)) {
					Sounds.Play (Sounds.RangedMiss);
					Sounds.Play (Sounds.WindowDestroy);
				}
				return true;
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, "The "+TextUtil.GetName(projectile)+" hits the window."));
				return false;
			}
		}
		bool ThrowAtObstacles (ActorPart attacker, List<MoveBlockingPart> obstacles, PickablePart projectile, float relDist, bool isTarget, float baseHitProb) {
			bool isHit = GameRules.CalcIsHitRanged(attacker, projectile, relDist, isTarget, baseHitProb);
			if (isHit) {
				if (GameData.IsPlayer(attacker)) {
					MoveBlockingPart obs = RandUtils.Select (obstacles);
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "The " + TextUtil.GetName (projectile) + " hits " + TextUtil.GetNameIndef (obs) + "."));
					Sounds.Play (Sounds.RangedHit);
				}
				return true;
			}
			return false;
		}
		bool ThrowAtEffect (ActorPart attacker, List<OnHitEffectPart> obstacles, PickablePart projectile, float relDist, bool isTarget, float baseHitProb) {
			bool isHit = GameRules.CalcIsHitRanged(attacker, projectile, relDist, isTarget, baseHitProb);
			if (isHit) {
				OnHitEffectPart obs = RandUtils.Select (obstacles);
				if (GameData.IsPlayer(attacker)) {
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "The " + TextUtil.GetName (projectile) + " hits " + TextUtil.GetNameIndef (obs) + "."));
					Sounds.Play (Sounds.RangedHit);
				}
				foreach( EntityEffect effect in obs.Effects ) {
					if (effect.Execute (obs.Entity)) {
						if ((GameData.IsPlayer (obs.Entity) || GameData.IsPlayer (attacker.Entity)) && effect.Message != null) {
							GameData.EventManager.Notify (new MessageEvent (obs.Entity, effect.Message));	
						}
					}
				}
				return true;
			}
			return false;
		}
		float CalcBaseHitProb (GridLocation source, GridLocation target, GridLocation location) {
			float dist = PointToLineDist (source, target, location);
			if (dist > 0.5f) {
				return 0;
			}
			return 1 - (2 * dist);
		}
		float PointToLineDist (GridLocation l1, GridLocation l2, GridLocation p) {
			float val = Mathf.Abs( (l2.Y - l1.Y)*p.X - (l2.X - l1.X)*p.Y + l2.X*l1.Y - l2.Y*l1.X );
			return val / Mathf.Sqrt( Mathf.Pow(l1.X - l2.X, 2) + Mathf.Pow(l1.Y - l2.Y, 2) );
		}

		void DestroyEntity(ActorPart actor, Entity attacker) {

			GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
			GameData.EventManager.Notify (new DestroyEntityEvent (actor.Entity, attacker));
			if (actor.Entity.HasPart (LocationPart.partId)) {
				GameData.EventManager.Notify (new UpdateVisibilityDelayedEvent (actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location));
			}
		}

	}
}
