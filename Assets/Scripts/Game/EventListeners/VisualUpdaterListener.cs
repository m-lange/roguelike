﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Events;
using Game.EventListeners;
using Game.Parts;
using Map;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;

namespace Game.EventListeners {
	public class VisualUpdaterListener : GameEventListener<EntityMovedEvent> {

		GridVisibilityManager visibility;

		public VisualUpdaterListener(GridVisibilityManager visibility) {
			this.visibility = visibility;
		}

		public void Notify(EntityMovedEvent evt) {
			Entity entity = evt.entity;
			VisualPart vis = entity.GetPart<VisualPart> (VisualPart.partId);
			if (vis != null) {
				vis.SetLocation (evt.location.Location);
				vis.SetVisible (GameData.IsInViewBounds(evt.location.Location, 0) && visibility.Visible.IsCenterVisible(evt.location.Location) );
				
			}
		}
	}
}
