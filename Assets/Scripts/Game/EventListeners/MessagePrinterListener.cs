﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Game.Events;
using Game.EventListeners;
using Game.Parts;

namespace Game.EventListeners {
	public class MessagePrinterListener : GameEventListener<MessageEvent> {

		public void Notify(MessageEvent evt) {
			UnityEngine.Debug.Log ("MSG: " + evt.message);
		}
	}
}
