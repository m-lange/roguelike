﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ComponentSystem;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using EventSystem;
using Game.Parts;
using Game.Events;
using Game.Rules;
using TurnSystem;

namespace Game.EventListeners {
	public class CamUpdaterListener 
						  : TickUpdatable,
							GameEventListener<EntityMovedEvent>, 
							GameEventListener<CameraLayerChangedEvent>, 
							GameEventListener<UpdateVisibilityEvent>, 
							GameEventListener<UpdateVisibilityDelayedEvent> {

		//VisualPart camVisual;
		LocationPart cameraLocation;
		LocationPart playerLocation;

		//LayeredGrid<TileInfo> map;
		MiniMap miniMap;
		TileManager tileManager;
		SpatialEntityManager spatialManager;
		GridVisibilityManager visibility;


		bool delayedUpdates = false;

		//bool delayedVisibility = false;
		//bool delayedRepaint = false;

		public CamUpdaterListener(Entity cam, Entity player, 
						//LayeredGrid<TileInfo> map,
						TileManager tileManager, SpatialEntityManager spatialManager, 
			MiniMap miniMap, GridVisibilityManager visibility,
						int visionRangeX, int visionRangeY) {
			//this.camVisual = cam.GetPart<VisualPart>();
			//this.map = map;
			this.cameraLocation = cam.GetPart<LocationPart>(LocationPart.partId);
			this.playerLocation = player.GetPart<LocationPart>(LocationPart.partId);
			this.tileManager = tileManager;
			this.spatialManager = spatialManager;
			this.miniMap = miniMap;
			this.visibility = visibility;
		}

		public void Notify(EntityMovedEvent evt) {
			Entity entity = evt.entity;
			if (GameData.IsPlayer(entity)) {

				cameraLocation.Location = evt.location.Location;
				UpdateCamera ();
				RecalcVisibility ();
				GameData.UpdateBounds();
				//delayedRepaint = true;

				GlobalParameters.Instance.StartCoroutine( UpdateVisibilityAsync() );
				//UpdateVisibility ();

			} else if (entity == GameData.Camera) {
				UpdateMap ();
			} 
			/*else if (entity.HasPart<ViewBlockingPart> ()) {
				if (GameData.IsInViewBounds (evt.location.Location)) {
					UpdateVisibility ();
				}
			}*/
		}

		public void Notify(CameraLayerChangedEvent evt) {
			cameraLocation.Location.Layer += evt.delta;
			UpdateCamera ();
			UpdateMap ();
			//UpdateVisibility ();
		}

		public void Notify(UpdateVisibilityEvent evt) {
			if (GameData.IsInViewBounds (evt.sourceLocation)) {
				RecalcVisibility ();
				UpdateVisibility ();
			}
		}
		public void Notify(UpdateVisibilityDelayedEvent evt) {
			if (GameData.IsInViewBounds (evt.sourceLocation)) {
				delayedUpdates = true;
			}
		}

		public void UpdateTick () {
			if (delayedUpdates) {
				RecalcVisibility ();
				UpdateVisibility ();
				delayedUpdates = false;
				//delayedRepaint = false;
			} /*else if (delayedRepaint) {
				UpdateVisibility ();
				delayedRepaint = false;
			} */
		}


		float CalcIlluminationRange () {
			InventoryPart inv = GameData.Player.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv == null) {
				return GameRules.PlayerIlluminationDistance; 
			}
			float dist = GameRules.PlayerIlluminationDistance;
			LightSourcePart ls;

			List<PickablePart> items = inv.Items;
			PickablePart p;
			int cnt = items.Count;
			for(int i=0; i<cnt; i++) {
				p = items[i];
				ls = p.Entity.GetPart<LightSourcePart> (LightSourcePart.partId);
				if (ls != null) {
					if (ls.IsOn && ls.Range > dist) {
						dist = ls.Range;
					}
				}
			}
			return dist;
		}


		void UpdateCamera() {
			//GlobalParameters.Instance.EventManager.Notify ( new EntityMovedEvent(camVisual.Entity, cameraLocation) );
		}
		void UpdateMap() {
			miniMap.ShowLayer ( cameraLocation.Location.Layer );
			tileManager.UpdateCamera (cameraLocation.Location, playerLocation.Location);
		}
		void RecalcVisibility() {
			visibility.RecalcVisible( playerLocation.Location, CalcIlluminationRange () );
			//foreach (GridLocation pos in visibility.Visible.Keys) {
			//	map [pos].Seen = true;
			//}
		}
		void UpdateVisibility() {
			tileManager.UpdateVisibility (visibility.Visible, cameraLocation.Location);
			spatialManager.UpdateVisibility (visibility.Visible, cameraLocation.Location);
			miniMap.UpdateVisibility (playerLocation.Location, visibility.Visible);
		}


		System.Collections.IEnumerator UpdateVisibilityAsync() {
			VisibilityGrid prevVisible = visibility.Visible;
			GridLocation camLoc = new ImmutableGridLocation(cameraLocation.Location);
			GridLocation playerLoc = new ImmutableGridLocation(playerLocation.Location);
			yield return null;
			yield return null;
			tileManager.UpdateVisibility (prevVisible, camLoc);
			yield return null;
			yield return null;
			spatialManager.UpdateVisibility (prevVisible, camLoc);
			yield return null;
			yield return null;
			miniMap.UpdateVisibility (playerLoc, prevVisible);
		}

	}
}
