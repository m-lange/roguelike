﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using Map.LayeredGrid;
using Game.Actions.Interface;
using Game.Events;
using Game.EventListeners;
using Game.Parts;
using Game.Destructables;
using Game.Effects;
using Game.Util;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Data;
using TurnSystem;

namespace Game.EventListeners {
	public class BattleListener : GameEventListener<AttackEvent> {

		public void Notify (AttackEvent evt) {
			ActorPart att = evt.attacker;
			ActorPart def = evt.defender;
			bool playerAtt = GameData.IsPlayer (att.Entity);
			bool playerDef = GameData.IsPlayer (def.Entity);

			EquipmentPart attackWeapon = null;
			InventoryPart inv = att.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				attackWeapon = inv.GetEquipped (Equip.HandPrim);
			}


			bool isHit = GameRules.CalcIsHit (att, def, attackWeapon);

			def.Attacked (att);
			//if (playerAtt) {
			GameData.EventManager.Notify (new GameMessageEvent (att.Entity, 
				new ActorAttackedMessage (att.Entity, GameRules.DefaultMessageRadius, true, def.Entity.GetPart<LocationPart> (LocationPart.partId).Location, att, def)));
			//}
			bool destroyed = false;

			if (isHit) {
				int damage = GameRules.CalcDamage (att, def, attackWeapon);
				if (playerAtt) {
					string message = "You hit the " + TextUtil.GetName (def)+".";
					if(GameData.IsDebugMode) message += " (" + damage + ")";
					GameData.EventManager.Notify (new MessageEvent (att.Entity, message)); 
					Sounds.Play (Sounds.Hit1);
				} else if (playerDef) {
					string message = "The " + TextUtil.GetName (att) + " hits.";
					if(GameData.IsDebugMode) message += " (" + damage + ")";
					GameData.EventManager.Notify (new MessageEvent (att.Entity, message));  
					Sounds.Play (Sounds.Hit2);
				}
				GainSkill (att, attackWeapon);
				HealthPointsPart hp = def.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId);
				destroyed = hp != null 
					? hp.DealDamage (damage)
					: true;
				if (destroyed) {
					LevelUp (att, def, attackWeapon);

					if (playerDef) {
						GameData.EventManager.Notify (new MessageEvent (def.Entity, 
							"You are killed by the " + TextUtil.GetName (att)));
						//att.SetNextAction ( new GameOverAction(att, "Game over!") );
					} else {
						DestroyEntity (def, att.Entity);
						if (playerAtt) {
							GameData.KillRecord.AddKill (def);
							GameData.EventManager.Notify (new MessageEvent (att.Entity, 
								"You kill the " + TextUtil.GetName (def))); 
						}
					}
				} else {
					//if (attackWeapon == null) {
					foreach (ActorEffect effect in att.AttackEffects) {
						if (effect.Execute (def)) {
							if (GameData.IsPlayer (def) && effect.Message != null) {
								GameData.EventManager.Notify (new MessageEvent (def.Entity, effect.Message));	
							}
						}
					}
					//}
				}

				if (attackWeapon != null) {
					GameRules.LimitedUseDamage (att, attackWeapon.Entity, GameRules.WeaponDamageProb, true);
				}
				ArmorDamage (def);

			} else {
				if (playerAtt) {
					GameData.EventManager.Notify (new MessageEvent (att.Entity, 
						"You miss the " + TextUtil.GetName (def) + ".")); 
					Sounds.Play (Sounds.Miss);
				} else if (playerDef) {
					GameData.EventManager.Notify (new MessageEvent (att.Entity, 
						"The " + TextUtil.GetName (att) + " misses.")); 
					Sounds.Play (Sounds.Miss);
				}
			}

			if (!destroyed) {
				if (def.Stats.HasPassiveAttack && !evt.isPassive) {
					Notify (new AttackEvent (def, att, true));
				}
			}
		}

		void ArmorDamage (ActorPart defender) {
			InventoryPart inv = defender.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				List<EquipmentPart> equipment = inv.Filter<EquipmentPart> ((eq) => eq.IsEquiped && eq.ArmorBonus.Any());
				int cnt = equipment.Count;
				for (int i = 0; i < cnt; i++) {
					EquipmentPart eq = equipment [i];
					LimitedUsePart lu = eq.Entity.GetPart<LimitedUsePart> (LimitedUsePart.partId);
					if (lu != null) {
						GameRules.LimitedUseDamage(defender, lu, GameRules.ArmorDamageProb, true);
					}
				}
			}
		}

		void LevelUp (ActorPart attacker, ActorPart defender, EquipmentPart attackWeapon) {
			HealthPointsPart hp = attacker.Entity.GetPart<HealthPointsPart>(HealthPointsPart.partId);
			if (hp != null) {
				hp.GainMaxHpPoints( 1000 / hp.MaxHp );
			}
			attacker.GainStrength ( (int) (defender.Strength * GameRules.StatsGainPerKill * GameRules.CalcRelStatsInv(attacker.Stats.BaseStrength) ) );
			attacker.GainDefense ( (int) (defender.Attack * GameRules.StatsGainPerKill * GameRules.CalcRelStatsInv(attacker.Stats.BaseDefense) ) );
			if (attackWeapon == null) {
				attacker.GainAttack ( (int) (defender.Attack * GameRules.StatsGainPerKill * GameRules.CalcRelStatsInv(attacker.Stats.BaseAttack) ) );
			}
		}

		void GainSkill (ActorPart attacker, EquipmentPart attackWeapon) {
			if (attackWeapon != null && attackWeapon.Skill != Skills.None) {
				attacker.Stats.GainSkill (attackWeapon.Skill, GameRules.CalcSkillPoints(attacker, attackWeapon) );
			}
		}

		void DestroyEntity(ActorPart actor, Entity attacker) {

			GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
			GameData.EventManager.Notify (new DestroyEntityEvent (actor.Entity, attacker));

			LocationPart loc = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
			if (loc != null) {
				//requiresUpdate = target.Entity.GetPart<LocationPart> ().Location;
				GameData.EventManager.Notify (new UpdateVisibilityDelayedEvent (loc.Location));
			}
		}

	}
}
