﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using EventSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Events;
using Game.EventListeners;
using Game.Parts;
using Game.Destructables;
using Game.Effects;

namespace Game.EventListeners {
	public class EntityDestroyedEffectListener : GameEventListener<DestroyEntityEvent> {

		public void Notify (DestroyEntityEvent evt) {
			InventoryPart inv = evt.entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				while ( inv.Items.Count > 0 ) {
					PickablePart dropped = inv.RemoveAt (0, -1);
					if (dropped != null) {
						GameData.EventManager.Notify ( new CreateEntityEvent(dropped.Entity) ); 
					}
				}
			}

			GridLocation requiresUpdate = null;
			//Action action = null;
			DestructionEffectPart destr = evt.entity.GetPart<DestructionEffectPart> (DestructionEffectPart.partId);
			if (destr != null) {
				DestructionEffectRule rule = null;
				Effect effect = destr.Destroy (evt.destroyer, out rule);
				if (effect != null) {
					//action = 
					effect.Execute (out requiresUpdate);
				}
			}
		}
	}
}
