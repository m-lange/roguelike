﻿using System.Collections;
using System.Collections.Generic;
using EventSystem;
using Map.LayeredGrid;
using Game.Events;
using Game.Parts;

using UnityEngine;

namespace Game.EventListeners {
	public class HighlightCellListener 
				: GameEventListener<HighlightCellEvent>,
					GameEventListener<HighlightRangeEvent> {

		GameObject highlightGO;
		Transform highlightTransform;
		GameObject rangeGO;
		Transform rangeTransform;


		public HighlightCellListener(string highlightPrefab, string rangeHighlightPrefab) {
			highlightGO = (GameObject)GameObject.Instantiate ( Resources.Load(highlightPrefab) );
			highlightTransform = highlightGO.transform;
			highlightGO.SetActive (false);

			rangeGO = (GameObject)GameObject.Instantiate ( Resources.Load(rangeHighlightPrefab) );
			rangeTransform = rangeGO.transform;
			rangeGO.SetActive (false);
		}

		public void Notify(HighlightCellEvent evt) {
			if (evt.location == null) {
				highlightGO.SetActive (false);
			} else {
				highlightTransform.position = GameData.GridToWorld (evt.location);
				highlightGO.SetActive (true);

				//Debug.Log( GameData.Visibility.CanSee(evt.entity.GetPart<LocationPart>().Location, evt.location, 20, false) );
			}
		}
		public void Notify(HighlightRangeEvent evt) {
			if (evt.location == null) {
				rangeGO.SetActive (false);
			} else {
				rangeTransform.position = GameData.GridToWorld (evt.location);
				rangeTransform.localScale = new Vector3(evt.range*2, evt.range*2, 1);
				rangeGO.SetActive (true);
			}
		}
	}
}
