﻿using ComponentSystem;
using Map.LayeredGrid;
using TurnSystem;
using EventSystem;
using Game.Events;
using Game.Messages;
using Game.Parts;

namespace Game.EventListeners {
	public class MessageDispatchListener :
							GameEventListener<GameMessageEvent> {

		SpatialEntityManager spatialManager;


		public MessageDispatchListener(SpatialEntityManager spatialManager) {
			this.spatialManager = spatialManager;
		}

		public void Notify (GameMessageEvent evt) {
			Message message = evt.message;
			float rad = message.Radius;
			int intRad = (int)rad;
			GridLocation loc = message.Location;
			//UnityEngine.Debug.Log("DISPATCH "+loc);
			Bounds bounds = new Bounds (loc.Layer, loc.Layer, loc.X - intRad, loc.Y - intRad, loc.X + intRad, loc.Y + intRad);
			foreach (ActorPart actor in spatialManager.GetInBounds<ActorPart>(ActorPart.partId, bounds, null)) {
				LocationPart actorLoc = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				if (loc.Distance (actorLoc.Location) < rad) {
					actor.Notify(message);
				}
			}
		}

	}
}
