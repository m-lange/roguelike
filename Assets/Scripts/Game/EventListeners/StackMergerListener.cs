﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ComponentSystem;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using EventSystem;
using Game.Parts;
using Game.Events;
using TurnSystem;

namespace Game.EventListeners {
	public class StackMergerListener 
						  : TickUpdatable,
						GameEventListener<EntityMovedEvent> {

		SpatialEntityManager spatialManager;
		Dictionary<GridLocation, StackablePart> addedStacks;

		public StackMergerListener(SpatialEntityManager spatialManager) {
			this.spatialManager = spatialManager;
			addedStacks = null; //new Dictionary<GridLocation, StackablePart>();
		}

		public void Notify (EntityMovedEvent evt) {
			StackablePart st = evt.entity.GetPart<StackablePart> (StackablePart.partId);
			if (st != null) {
				foreach (StackablePart stack in spatialManager.GetAt<StackablePart>(StackablePart.partId, evt.location.Location)) {
					if (stack.CanStack (st)) {
						stack.Stack (st);
						GameData.EventManager.Notify (new RemoveEntityEvent (evt.entity, true));
						return;
					}
				}
				if (addedStacks != null) {

					//foreach (KeyValuePair<GridLocation, StackablePart> kv in addedStacks) {
					if (addedStacks.ContainsKey (evt.location.Location)) {
						StackablePart stack = addedStacks [evt.location.Location];
						if (stack.CanStack (st)) {
							stack.Stack (st);
							GameData.EventManager.Notify (new RemoveEntityEvent (evt.entity, true));
							return;
						}
						/*
						if (kv.Key.Equals (evt.location.Location)) {
							if (kv.Value.CanStack (st)) {
								kv.Value.Stack (st);
								GameData.EventManager.Notify (new RemoveEntityEvent (evt.entity, true));
								return;
							}
						}
						*/
					}
				}
				//Debug.Log("Adding at "+loc.Location);
				if (addedStacks == null) {
					addedStacks = new Dictionary<GridLocation, StackablePart>();
				}
				addedStacks [evt.location.Location] = st;
			}
		}
		public void UpdateTick() {
			//addedStacks.Clear();
			addedStacks = null; //new Dictionary<GridLocation, StackablePart>();
		}
	}
}
