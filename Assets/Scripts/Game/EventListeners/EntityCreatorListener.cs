﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ComponentSystem;
using Map;
using Map.TileMap;
using Map.MiniMap;
using Map.LayeredGrid;
using EventSystem;
using Game.Parts;
using Game.Events;
using TurnSystem;

namespace Game.EventListeners {
	public class EntityCreatorListener :
							GameEventListener<CreateEntityEvent> {

		TurnManager turnManager;
		//SpatialEntityManager spatialManager;


		public EntityCreatorListener(TurnManager turnManager) {
			this.turnManager = turnManager;
			//this.spatialManager = spatialManager;

		}

		public void Notify (CreateEntityEvent evt) {
		/*
			if (evt.entity.HasPart<StackablePart> () && evt.entity.HasPart<LocationPart> ()) {
				StackablePart st = evt.entity.GetPart<StackablePart> ();

				UnityEngine.Debug.Log("Creating "+st.Count);

				LocationPart loc = evt.entity.GetPart<LocationPart> ();
				foreach (StackablePart stack in spatialManager.GetAt<StackablePart>(loc.Location)) {
					if (stack.CanStack (st)) {
						stack.Stack (st);
						return;
					}
				}
				foreach (KeyValuePair<GridLocation, StackablePart> kv in addedStacks) {
					if (kv.Key.Equals (loc.Location)) {
						if (kv.Value.CanStack (st)) {
							kv.Value.Stack (st);
							return;
						}
					}
				}
				addedStacks[loc.Location] = st;
			}
			*/
			turnManager.AddEntity ( evt.entity );
		}
	}
}
