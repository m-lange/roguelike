﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class ConsumablesFactory {

		public static Entity CreateBread (GridLocation loc, bool addToGame) {
			Entity tin = new Entity (true);
			tin.Attach (new NamePart ("bread"));
			tin.Attach (new LocationPart (new GridLocation (loc), false));
			tin.Attach (new VisualPart ("Items/Item", "Items/BreadTex", false, true));
			tin.Attach (new ProjectilePart (3, 1, 1,    0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			//tin.Attach (new StackablePart ("TinnedMeat", 1, 1000));
			tin.Attach (new ConsumablePart ( GameRules.DefaultBreadNutritionValue, true, false ));
			tin.Attach (new PickablePart (1000, 5, TradeClass.Consumables));

			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(tin) );
			}
			return tin;
		}

		public static Entity CreateTinnedMeat (GridLocation loc, bool addToGame) {
			Entity tin = new Entity (true);
			tin.Attach (new NamePart (TextUtil.Tinned + " meat"));
			tin.Attach (new LocationPart (new GridLocation (loc), false));
			tin.Attach (new VisualPart ("Items/Item", "Items/TinTex", false, true));
			tin.Attach (new ProjectilePart (5, 5, 1,    0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			//tin.Attach (new StackablePart ("TinnedMeat", 1, 1000));
			tin.Attach (new ConsumablePart ( GameRules.DefaultTinNutritionValue, false, false ));
			tin.Attach (new TinnedPart () );
			tin.Attach (new PickablePart (500, 10, TradeClass.Consumables));


			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(tin) );
			}
			return tin;
		}
		public static Entity CreateTinned (GridLocation loc, bool addToGame, TinnablePart toTin) {
			Entity tin = new Entity (true);
			tin.Attach (new NamePart (TextUtil.Tinned + " " + TextUtil.GetName (toTin)));
			tin.Attach (new LocationPart (new GridLocation (loc), false));
			tin.Attach (new VisualPart ("Items/Item", "Items/TinTex", false, true));
			tin.Attach (new ProjectilePart (5, 5, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			tin.Attach (new TinnedPart () );

			int price = 1;
			if (toTin.Entity.HasPart<PickablePart> ()) {
				price = toTin.Entity.GetPart<PickablePart> ().Price;
			}
			if (toTin.Entity.HasPart<ConsumablePart> ()) {
				ConsumablePart cons = toTin.Entity.GetPart<ConsumablePart> ();
				ConsumablePart consNew = new ConsumablePart ( cons.NutritionValue / 2, cons.IsVegetarian, cons.IsHerb );
				if(cons.Effects != null && cons.Effects.Length > 0) {
					ActorEffect[] eff = new ActorEffect[cons.Effects.Length];
					cons.Effects.CopyTo(eff, 0);
					consNew.Effects = eff;
				}
				tin.Attach (consNew);
				tin.Attach (new PickablePart (500, price, TradeClass.Consumables));
			} else {
				tin.Attach (new PickablePart (500, price, TradeClass.Stuff));
			}
			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(tin) );
			}
			return tin;
		}
	}
}
