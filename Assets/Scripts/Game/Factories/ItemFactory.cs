﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Game.EntityEffects;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class ItemFactory {


		public static Entity CreateStone (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("stone"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/StoneTex", false, true));
			item.Attach (new PickablePart (200, 0, TradeClass.None));
			item.Attach (new ProjectilePart (8, 4, 1,   12, 10, 1, LauncherType.Sling, AttackTypes.Impact, Skills.Sling));
			item.Attach (new StackablePart ("Stone", 1, 1000));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateKey (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (false);
			entity.Attach (new NamePart ("key"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/KeyTex", false, true));
			entity.Attach (new PickablePart (100, 100, TradeClass.Tools));
			entity.Attach (new UsablePart (new KeyUsable ()));
			entity.Attach (new DestructablePart ());
			entity.Attach (new HealthPointsPart (150, 150, 0));
			entity.Attach (new LimitedUsePart (250, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}

			return entity;
		}

		public static Entity CreateTinningKit (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (false);
			entity.Attach (new NamePart ("tinning kit"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/TinningKitTex", false, true));
			entity.Attach (new PickablePart (1000, 200, TradeClass.Tools));
			entity.Attach (new UsablePart (new TinningKitUsable ()));
			entity.Attach (new DestructablePart ());
			entity.Attach (new HealthPointsPart (100, 100, 0));
			entity.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}

			return entity;
		}

		public static Entity CreateTinOpener (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (false);
			entity.Attach (new NamePart ("tin opener"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/TinOpenerTex", false, true));
			entity.Attach (new PickablePart (200, 40, TradeClass.Tools));
			entity.Attach (new UsablePart (new TinOpenerUsable ()));
			entity.Attach (new DestructablePart ());
			entity.Attach (new HealthPointsPart (100, 100, 0));
			entity.Attach (new LimitedUsePart (250, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}

			return entity;
		}

		public static Entity CreateFlute (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (false);
			entity.Attach (new NamePart ("flute"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/FluteTex", false, true));
			entity.Attach (new PickablePart (300, 50, TradeClass.Tools));
			entity.Attach (new UsablePart (new FluteUsable ()));
			entity.Attach (new DestructablePart ());
			entity.Attach (new HealthPointsPart (100, 100, 0));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}

			return entity;
		}

		public static Entity CreateCandle (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("candle"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/CandleTex", false, true));
			entity.Attach (new PickablePart (200, 10, TradeClass.Lights));
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new LightSourcePart (3.5f, 500, true) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}

			return entity;
		}

		public static Entity CreateOilLamp (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("oil lamp"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/OilLampTex", false, true));
			entity.Attach (new PickablePart (500, 100, TradeClass.Lights));
			entity.Attach (new DestructablePart ());
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new LightSourcePart (5.5f, 1000, false) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateOilLatern (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("oil latern"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/OilLaternTex", false, true));
			entity.Attach (new PickablePart (1000, 250, TradeClass.Lights));
			entity.Attach (new DestructablePart ());
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new LightSourcePart (8.5f, 1000, false) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateTorch (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("torch"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/TorchTex", false, true));
			entity.Attach (new PickablePart (1000, 25, TradeClass.Lights));
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new LightSourcePart (12.5f, 200, true) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateOilCan (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (false);
			entity.Attach (new NamePart ("oil can"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/OilCanTex", false, true));
			entity.Attach (new PickablePart (2000, 60, TradeClass.Tools));
			entity.Attach (new UsablePart (new OilCanUsable() ));
			entity.Attach (new LimitedUsePart (5000, false, "is empty"));

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateBearTrap (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("bear trap"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/BearTrapTex", false, true));
			entity.Attach (new PickablePart (3000, 150, TradeClass.Tools));
			entity.Attach (new DestructablePart ());
			entity.Attach (new OnHitEffectPart(new SwitchOffEffect()));
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new ContactEffectPart( false, new InjuryEffect(5), new AttachStickToEntityEffect(entity, 10), new MessageEffect("The trap snaps shut.", Sounds.Trap, false, 15) ) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}
		
		public static Entity CreateBoobyTrap (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("booby trap"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/BoobyTrapTex", false, true));
			entity.Attach (new PickablePart (1000, 100, TradeClass.Tools));
			entity.Attach (new DestructablePart ());
			entity.Attach (new OnHitEffectPart(new SwitchOffEffect()));
			entity.Attach (new UsablePart (new OnOffUsable() ));
			entity.Attach (new OnOffPart (false) );
			entity.Attach (new ContactEffectPart( true, new InjuryEffect(20), new MessageEffect("The booby trap explodes.", Sounds.Boom, false, 20) ) );

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateGrapplingHook (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("grappling hook"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", "Items/GrapplingHookTex", false, true));
			entity.Attach (new PickablePart (2000, 500, TradeClass.Tools));
			entity.Attach (new UsablePart (new GrapplingHookUsable() ));
			entity.Attach (new ProjectilePart (6, 8, 2,   0, 0, 1, LauncherType.None, AttackTypes.Impact, Skills.None));
			
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}


		public static Entity CreateWoodLog(GridLocation loc, bool addToGame) {
			Entity log = new Entity (false);
			log.Attach ( new NamePart("wood log") );
			log.Attach ( new LocationPart( new GridLocation(loc), false ) );
			log.Attach ( new VisualPart( "Items/Item", "Items/WoodLogTex", false, true ) );
			log.Attach ( new PickablePart(20000, 10, TradeClass.Stuff) );
			log.Attach ( new EquipmentPart(Equip.HandPrim, LauncherType.None, Skills.None, 0, 0, 0, 5, 2, AttackTypes.Impact, 0, new Armor()) );
			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(log) );
			}
			return log;
		}


		public static Entity CreateGold (GridLocation loc, int amount, bool addToGame) {
			Entity gold = CreateGold(loc, false);
			gold.GetPart<StackablePart>().Count = amount;
			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(gold) );
			}
			return gold;
		}
		public static Entity CreateGold(GridLocation loc, bool addToGame) {
			Entity gold = new Entity (false);
			gold.Attach ( new NamePart("gold piece") );
			gold.Attach ( new LocationPart( new GridLocation(loc), false ) );
			gold.Attach ( new VisualPart( "Items/Item", "Items/GoldTex", false, true ) );
			gold.Attach ( new PickablePart(3, 1, TradeClass.None) );
			gold.Attach (new StackablePart ("Gold", 1, 1000000));
			gold.Attach (new ProjectilePart (8, 0, 1,    12, 1, 1, LauncherType.Sling, AttackTypes.Impact, Skills.Sling));
			gold.Attach (new MoneyPart() );
			if (addToGame) { 
				GameData.EventManager.Notify ( new CreateEntityEvent(gold) );
			}
			return gold;
		}

		public static Entity CreateGold10(GridLocation loc, bool addToGame) {
			return CreateGold(loc, 10, addToGame);
		}
		public static Entity CreateGold100(GridLocation loc, bool addToGame) {
			return CreateGold(loc, 100, addToGame);
		}
		public static Entity CreateGold1000(GridLocation loc, bool addToGame) {
			return CreateGold(loc, 1000, addToGame);
		}


	}
}
