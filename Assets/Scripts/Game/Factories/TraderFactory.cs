﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class TraderFactory {

		public static Entity CreateMobileTrader (GridLocation loc, bool addToGame) {
			return CreateGeneralTrader(loc, addToGame, false);
		}
		public static Entity CreateStationaryTrader (GridLocation loc, bool addToGame) {
			return CreateGeneralTrader(loc, addToGame, true);
		}
		
		protected static void FillInventory(Entity entity, GridLocation loc, string[] items, int count) {
			InventoryPart inv = entity.GetPart<InventoryPart> ();
			for (int i = 0; i < count; i++) {
				string creator = RandUtils.Select( items );
				EntityFactory.AddToInventory(inv, EntityFactory.GetCreator( creator )(loc, false), false );
			}
		}

		public static Entity CreateGeneralTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("trader", "Creatures/TraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( false, EntityFactory.TraderItems, new TradeClass[]{ TradeClass.All } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateWeaponTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("arms trader", "Creatures/WeaponTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.WeaponTraderItems, new TradeClass[]{ TradeClass.Weapon } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateArmorTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("armor trader", "Creatures/ArmorTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.ArmorTraderItems, new TradeClass[]{ TradeClass.Armor } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateToolTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("tool trader", "Creatures/ToolTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.ToolTraderItems, new TradeClass[]{ TradeClass.Tools, TradeClass.Lights } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateProvisionTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("provision trader", "Creatures/ProvisionTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.ProvisionTraderItems, new TradeClass[]{ TradeClass.Consumables } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateHerbTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("herb trader", "Creatures/HerbTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.HerbTraderItems, new TradeClass[]{ TradeClass.Herbs } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.DefaultTraderItems);
			return entity;
		}

		public static Entity CreateBookTrader (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("book trader", "Creatures/BookTraderTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, EntityFactory.BookTraderItems, new TradeClass[]{ TradeClass.Books } );
			entity.Attach (trader);
			FillInventory(entity, loc, trader.TradeItems, GameRules.BookTraderItems);
			return entity;
		}

		public static Entity CreateAnatomist (GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = CreateDefaultTrader("anatomist", "Creatures/AnatomistTex", loc, addToGame, stationary);
			TraderPart trader = new TraderPart ( true, new string[]{}, new TradeClass[]{ TradeClass.HumanCarcass }, new TradeClass[]{ TradeClass.None } );
			entity.Attach (trader);
			return entity;
		}

		public static Entity CreateDefaultTrader (string name, string texture, GridLocation loc, bool addToGame, bool stationary) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart (name));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Creatures/Creature", texture, false, stationary));
			entity.Attach (new ActorPart (FactionFactory.Villagers, 
				new RandomWalkAI (!stationary, true, 15, 20, 50, Terra.ACCESS_CLOSED, true, null),  
				new ActorStats(             speed:             12,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           20,
			                              	armor:             new Armor(20, 20, 20),
			                              	maxStomachContent: 0,
			                              	canStarve:         true,
			                              	hasPassiveAttack:  false)));
			entity.Attach (new InventoryPart (GlobalParameters.letters.Length, stationary ? 240000 : 80000, 1f));
			entity.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("HumanCarcass", 1)));
			entity.Attach (new MoveBlockingPart ());
			entity.Attach (new HealthPointsPart (20, 20, 0));

			InventoryPart inv = entity.GetPart<InventoryPart> ();
			Entity money = ItemFactory.CreateGold(loc, false);
			money.GetPart<StackablePart>().Count = RandUtils.NextInt( GameRules.TraderMoneyMin, GameRules.TraderMoneyMax+1 );
			EntityFactory.AddToInventory(inv, money, false);

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}
	}
}
