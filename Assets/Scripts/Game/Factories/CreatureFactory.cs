﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Game.ActorEffects.Continuous;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;
using Game.Serialization;

namespace Game.Factories {
	public class CreatureFactory {

		public static Entity CreatePlayer (GridLocation loc, bool addToGame) {
			Entity player = new Entity (true);
			player.Attach (new NamePart ("You"));
			player.Attach (new LocationPart (new GridLocation (loc), false));
			player.Attach (new PlayerPart ());
			player.Attach (new VisualPart ("Creatures/Creature", "Creatures/PlayerTex", false, true));
			player.Attach (new ActorPart (FactionFactory.Player, null, 
			                   new ActorStats(
			                              	speed:             12,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           20,
			                              	armor:             new Armor(20,20,20),
			                              	maxStomachContent: 2000,
			                              	canStarve:         true,
			                              	hasPassiveAttack:  false)));
			player.Attach (new MoveBlockingPart ());
			player.Attach (new InventoryPart ( GlobalParameters.letters.Length , -1, 1f));
			player.Attach (new HealthPointsPart (20, 20, 0));

			InventoryPart inv = player.GetPart<InventoryPart>();

			EntityFactory.AddToInventory(inv, ItemFactory.CreateGold100(loc, false), false);
			EntityFactory.AddToInventory(inv, WeaponFactory.CreateClub(loc, false), true);
			EntityFactory.AddToInventory(inv, WeaponFactory.CreateSling(loc, false), true);
			EntityFactory.AddToInventory(inv, ItemFactory.CreateFlute(loc, false), true);

			//EntityFactory.AddToInventory(inv, HerbFactory.CreateSavePointHerbTuft(loc, false), false);

			if (addToGame) { GameData.EventManager.Notify (new CreateEntityEvent (player)); }
			return player;
		}


		public static Entity CreatePlayerGhost (GridLocation loc, bool addToGame, SaveDeath saved) {
			Entity player = new Entity (true);
			player.Attach (new NamePart ("ghost of "+saved.name));
			player.Attach (new LocationPart (new GridLocation (loc), false));
			player.Attach (new VisualPart ("Creatures/Creature", "Creatures/PlayerGhostTex", false, false));

			saved.stats.CanStarve = false;
			ActorPart actor = new ActorPart (FactionFactory.WildPredators, 
				new RandomWalkAI (true, true, 15, 20, 50, 
					Terra.ACCESS_CLOSED, true, 
					null),
				saved.stats);
			player.Attach (actor);
			player.Attach (new MoveBlockingPart ());
			player.Attach (new InventoryPart (GlobalParameters.letters.Length, -1, 1f));
			player.Attach (new HealthPointsPart (saved.hp.MaxHp, saved.hp.MaxHp, 0));

			InventoryPart inv = player.GetPart<InventoryPart> ();
			foreach (SaveEntity e in saved.inventory) {
				Entity entity = e.CreateEntity(false);
				PickablePart pick = entity.GetPart<PickablePart>(PickablePart.partId);
				inv.AddItem(pick);
			}

			GameData.AssignInventoryEntities(player);

			if (addToGame) { GameData.EventManager.Notify (new CreateEntityEvent (player)); }
			return player;
		}


		public static Entity CreateCamera (GridLocation loc, bool addToGame) {
			Entity camera = new Entity (true);
			camera.Attach (new LocationPart (new GridLocation (loc), false));
			camera.Attach (new VisualPart ("Camera", null, true, true));
			camera.Attach (new CameraPart ());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (camera));
			}
			return camera;
		}

		public static Entity CreateAngryCiticen (GridLocation loc, bool addToGame) {
			Entity villager = CreateCiticen (loc, addToGame);
			ActorPart actor = villager.GetPart<ActorPart>();
			actor.SetFaction( FactionFactory.AngryVillagers );
			actor.ActorAi = new ChasePlayerAI (false, true, 15, 20, 50, Terra.ACCESS_CLOSED, true, null); 
			return villager;
		}

		public static Entity CreateCiticen (GridLocation loc, bool addToGame) {
			Entity villager = new Entity (true);
			villager.Attach (new NamePart ("citizen"));
			villager.Attach (new LocationPart (new GridLocation (loc), false));
			villager.Attach (new VisualPart ("Creatures/Creature", "Creatures/HumanRedTex", false, false));
			villager.Attach (new ActorPart (FactionFactory.Villagers, new RandomWalkAI (true, true, 15, 20, 50, 
								Terra.ACCESS_CLOSED, true, 
								new Dictionary<Type, ActorEffect>{ 
									{ typeof(ActorAttackedMessage), new TurnAgressiveOrFleeEffect(0.5f) },
									{ typeof(UnauthorizedAccessMessage), new TurnAgressiveEffect() }
								}),
								new ActorStats(
			                              	speed:             12,
			                              	strength:          30,
			                              	constitution:      20,
			                              	dexterity:         25,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           30,
			                              	armor:             new Armor(20,20,20),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			villager.Attach (new InventoryPart ( GlobalParameters.letters.Length , 80000, 1f));
			villager.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("HumanCarcass", 1)));
			villager.Attach (new MoveBlockingPart ());
			villager.Attach (new HealthPointsPart (20, 20, 0));

			InventoryPart inv = villager.GetPart<InventoryPart> (InventoryPart.partId);
			if (RandUtils.NextBool(0.1f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateGold100 (loc, false), false );
			} 
			if (RandUtils.NextBool(0.2f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateKey (loc, false), false );
			}
			if (RandUtils.NextBool(0.01f)) {
				EntityFactory.AddToInventory(inv, ContainerFactory.CreateTrolley (loc, false), false );
			}
			
			if (RandUtils.NextBool(0.6f)) {
				float r = RandUtils.NextFloat();
				if (r < 0.333) {
					EntityFactory.AddToInventory(inv, WeaponFactory.CreateAxe (loc, false), true );
				} else if (r < 0.666) {
					EntityFactory.AddToInventory(inv, WeaponFactory.CreateClub (loc, false), true );
				} else {
					EntityFactory.AddToInventory(inv, WeaponFactory.CreateSpear (loc, false), true );
				}
			}
			if (RandUtils.NextBool(0.33f)) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateDaggers (loc, RandUtils.NextInt(2,6), false), false, true );
			}
			if (RandUtils.NextBool(0.1f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateTinOpener (loc, false ), false );
			}
			if (RandUtils.NextBool(0.025f)) {
				EntityFactory.AddToInventory(inv, EntityFactory.GetCreator(RandUtils.Select(EntityFactory.BookTraderItems))(loc, false), false );
			}
			
			AssignSkills(villager, inv, 0, 2);
			
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (villager));
			}
			return villager;
		}


		public static Entity CreateSoldier (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("soldier"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Creatures/Creature", "Creatures/HumanGreenTex", false, false));

			entity.Attach (new ActorPart (FactionFactory.Soldiers, new RandomWalkAI (true, true, 15, 20, 50, 
								Terra.ACCESS_CLOSED, true, 
								new Dictionary<Type, ActorEffect>{ 
									{ typeof(ActorAttackedMessage), new TurnAgressiveEffect() },
									{ typeof(UnauthorizedAccessMessage), new TurnAgressiveEffect() }
								}), 
								new ActorStats(
			                              	speed:             12,
			                              	strength:          30,
			                              	constitution:      20,
			                              	dexterity:         25,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           30,
			                              	armor:             new Armor(20,20,20),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			
			/*
			entity.Attach (new ActorPart (FactionFactory.Soldiers, new PatrolAI (true, 15, 20, 50, 
								Terra.ACCESS_CLOSED, true, 
								new Dictionary<Type, ActorEffect>{ 
									{ typeof(ActorAttackedMessage), new TurnAgressiveEffect() },
									{ typeof(UnauthorizedAccessMessage), new TurnAgressiveEffect() }
								}), 
				new ActorStats(12, 35, 22, 30, 5, 35, 5, 0, false, false)));
			*/
			entity.Attach (new InventoryPart (GlobalParameters.letters.Length, 100000, 1f));
			entity.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("HumanCarcass", 1)));
			entity.Attach (new MoveBlockingPart ());
			entity.Attach (new HealthPointsPart (30, 30, 0));

			InventoryPart inv = entity.GetPart<InventoryPart> ();
			
			// Weapon
			float r = RandUtils.NextFloat ();
			if (r < 0.2f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateAxe (loc, false), true);
			} else if (r < 0.4f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateShortSword (loc, false), true);
			} else if (r < 0.6f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateLongSword (loc, false), true);
			} else if (r < 0.8f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateMace (loc, false), true);
			} else {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateHalberd (loc, false), true);
			}
			
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateRoundShield(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateLongShield(loc, false), true);
			}
			r = RandUtils.NextFloat ();
			if (r < 0.333f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateLeatherArmor(loc, false), true);
			} else if (r < 0.666f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateChainMail(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreatePlateMail(loc, false), true);
			}
			
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateHelmet(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateCloseHelmet(loc, false), true);
			}
			
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateShoes(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateBoots(loc, false), true);
			}

			r = RandUtils.NextFloat ();
			if (r < 0.3f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateDaggers(loc, RandUtils.NextInt(2,6), false), false, true);
			} else if (r < 0.5f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateBow(loc, false), true);
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateArrows(loc, RandUtils.NextInt(5,10), false), false, true);
			} else if (r < 0.7f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateCrossbow(loc, false), true);
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateCrossbowBolts(loc, RandUtils.NextInt(5,10), false), false, true);
			}
			
			AssignSkills(entity, inv, 2, 6);
			
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateBrigand (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("brigand"));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Creatures/Creature", "Creatures/HumanBrownTex", false, false));
			entity.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 15, 20, 50, Terra.ACCESS_CLOSED, true, null),  
								new ActorStats(
			                              	speed:             12,
			                              	strength:          30,
			                              	constitution:      20,
			                              	dexterity:         25,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           30,
			                              	armor:             new Armor(20,20,20),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			
			entity.Attach (new InventoryPart (GlobalParameters.letters.Length, 100000, 1f));
			entity.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("HumanCarcass", 1)));
			entity.Attach (new MoveBlockingPart ());
			entity.Attach (new HealthPointsPart (30, 30, 0));

			InventoryPart inv = entity.GetPart<InventoryPart> ();
			
			// Weapon
			float r = RandUtils.NextFloat ();			
			if (r < 0.1f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateClub (loc, false), true);
			} else if (r < 0.2f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateAxe (loc, false), true);
			} else if (r < 0.3f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateSpear (loc, false), true);
			} else if (r < 0.4f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateShortSword (loc, false), true);
			} else if (r < 0.6f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateLongSword (loc, false), true);
			} else if (r < 0.8f) {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateMace (loc, false), true);
			} else {
				EntityFactory.AddToInventory (inv, WeaponFactory.CreateHalberd (loc, false), true);
			}
			
			// Light
			r = RandUtils.NextFloat();
			if (r < 0.2) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateOilLatern (loc, false), false );
				if (RandUtils.NextBool(0.25f)) {
					EntityFactory.AddToInventory(inv, ItemFactory.CreateOilCan (loc, false), false );
				}
			} else if (r < 0.5) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateOilLamp (loc, false), false );
				if (RandUtils.NextBool(0.25f)) {
					EntityFactory.AddToInventory(inv, ItemFactory.CreateOilCan (loc, false), false );
				}
			} else if (r < 0.75) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateTorch (loc, false), false );
			} else {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateCandle (loc, false), false );
				EntityFactory.AddToInventory(inv, ItemFactory.CreateCandle (loc, false), false );
				EntityFactory.AddToInventory(inv, ItemFactory.CreateCandle (loc, false), false );
			}
			
			// Tools
			if (RandUtils.NextBool(0.1f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateBearTrap (loc, false), false );
			}
			if (RandUtils.NextBool(0.05f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateGrapplingHook (loc, false), false );
			}
			if (RandUtils.NextBool(0.01f)) {
				EntityFactory.AddToInventory(inv, ContainerFactory.CreateTrolley (loc, false), false );
			}
			if (RandUtils.NextBool(0.8f)) {
				for(int i=0; i<RandUtils.NextInt(1, 7); i++) {
					EntityFactory.AddToInventory(inv, ConsumablesFactory.CreateTinnedMeat (loc, false), false );
				}
			}
			
			// Shield
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateRoundShield(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateLongShield(loc, false), true);
			}
			
			// Armor
			r = RandUtils.NextFloat ();
			if (r < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateLeatherArmor(loc, false), true);
			} else if (r < 0.75f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateChainMail(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreatePlateMail(loc, false), true);
			}

			// Helmet
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateHelmet(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateCloseHelmet(loc, false), true);
			}
			
			// Shoes
			if (RandUtils.NextFloat () < 0.5f) {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateShoes(loc, false), true);
			} else {
				EntityFactory.AddToInventory(inv, ArmorFactory.CreateBoots(loc, false), true);
			}
			
			// Ranged
			r = RandUtils.NextFloat ();
			if (r < 0.25f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateDaggers(loc, RandUtils.NextInt(3,5), false), false, true);
			} else if (r < 0.5f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateBow(loc, false), true);
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateArrows(loc, RandUtils.NextInt(5,10), false), false, true);
			} else if (r < 0.75f) {
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateCrossbow(loc, false), true);
				EntityFactory.AddToInventory(inv, WeaponFactory.CreateCrossbowBolts(loc, RandUtils.NextInt(5,10), false), false, true);
			}
			
			// Gold
			if (RandUtils.NextBool(0.2f)) {
				EntityFactory.AddToInventory(inv, ItemFactory.CreateGold100 (loc, false), false );
				if (RandUtils.NextBool(0.5f)) {
					EntityFactory.AddToInventory(inv, ItemFactory.CreateGold100 (loc, false), false );
				}
			} 
			
			AssignSkills(entity, inv, 2, 5);

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}

		public static Entity CreateHumanCarcass (GridLocation loc, bool addToGame) {
			Entity villager = new Entity (true);
			villager.Attach (new NamePart ("human carcass"));
			villager.Attach (new LocationPart (new GridLocation (loc), false));
			villager.Attach (new VisualPart ("Items/Item", "Creatures/HumanGrayCarcassTex", false, true));
			villager.Attach (new PickablePart (50000, 100, TradeClass.HumanCarcass));
			villager.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			villager.Attach (new ConsumablePart(500, false, false));
			villager.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (villager));
			}

			return villager;
		}



		public static Entity CreateGolem (GridLocation loc, bool addToGame) {
			Entity wolf = new Entity (true);
			wolf.Attach (new NamePart ("golem"));
			wolf.Attach (new LocationPart (new GridLocation (loc), false));
			wolf.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 20, 20, 50, Terra.ACCESS_CLOSED, true, null),  
								new ActorStats(
			                              	speed:             4,
			                              	strength:          40,
			                              	constitution:      20,
			                              	dexterity:         10,
			                              	attackNoWeapon:    30,
			                              	attackDuration:    5,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           20,
			                              	armor:             new Armor(0,50,50),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			wolf.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("GolemCarcass", 1)));
			wolf.Attach (new HealthPointsPart (50, 50, 0));
			wolf.Attach (new MoveBlockingPart ());
			wolf.Attach (new VisualPart ("Creatures/Creature", "Creatures/GolemTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (wolf));
			}
			return wolf;
		}
		public static Entity CreateGolemCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("golem carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/GolemCarcassTex", false, true));
			carcass.Attach (new LimitedPresencePart (GameRules.LargeCarcassLimitedPresence, "rots away"));
			carcass.Attach (new MoveBlockingPart ());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}



		public static Entity CreateSheep (GridLocation loc, bool addToGame) {

			Entity sheep = new Entity (true);

			sheep.Attach (new NamePart ("sheep"));
			sheep.Attach (new LocationPart (new GridLocation (loc), false));
			sheep.Attach (new ActorPart (FactionFactory.DomesticPrey, new RandomWalkAI (true, false, 10, 10, 25, Terra.ACCESS_OPEN, false,
								new Dictionary<Type, ActorEffect>{
									{typeof(ActorAttackedMessage), new TurnToFleeEffect() },
									{typeof(HitByVegetarianFoodMessage), new NullEffect() }
								}),  
									new ActorStats(
			                              	speed:             8,
			                              	strength:          5,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Impact,
			                              	defense:           10,
			                              	armor:             new Armor(50,20,0),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			sheep.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("SheepCarcass", 1)));
			sheep.Attach (new HealthPointsPart (10, 10, 0));
			sheep.Attach (new MoveBlockingPart ());
			sheep.Attach (new VisualPart ("Creatures/Creature", "Creatures/SheepTex", false, false));

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (sheep));
			}

			return sheep;
		}
		public static Entity CreateSheepCarcass (GridLocation loc, bool addToGame) {
			Entity sheepCarcass = new Entity (true);
			sheepCarcass.Attach (new NamePart ("sheep carcass"));
			sheepCarcass.Attach (new LocationPart (new GridLocation (loc), false));
			sheepCarcass.Attach (new VisualPart ("Items/Item", "Creatures/SheepCarcassTex", false, true));
			sheepCarcass.Attach (new PickablePart (30000, 50, TradeClass.AnimalCarcass));
			sheepCarcass.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			sheepCarcass.Attach (new ConsumablePart(300, false, false));
			sheepCarcass.Attach (new TinnablePart());
			//sheepCarcass.Attach ( new DestructionEffectPart(new SpawnEntitiesDestructable("WolfCarcass", 1)) );
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (sheepCarcass));
			}

			return sheepCarcass;
		}

		public static Entity CreateWolf (GridLocation loc, bool addToGame) {
			Entity wolf = new Entity (true);
			wolf.Attach (new NamePart ("wolf"));
			wolf.Attach (new LocationPart (new GridLocation (loc), false));
			wolf.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 10, 15, 25, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect>{
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}),     
								new ActorStats(
			                              	speed:             8,
			                              	strength:          10,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    15,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           20,
			                              	armor:             new Armor(25,25,25),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			wolf.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("CanineCarcass", 1)));
			wolf.Attach (new HealthPointsPart (25, 25, 0));
			wolf.Attach (new MoveBlockingPart ());
			wolf.Attach (new VisualPart ("Creatures/Creature", "Creatures/WolfTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (wolf));
			}
			return wolf;
		}
		public static Entity CreateDog (GridLocation loc, bool addToGame) {
			Entity wolf = new Entity (true);
			wolf.Attach (new NamePart ("dog"));
			wolf.Attach (new LocationPart (new GridLocation (loc), false));
			wolf.Attach (new ActorPart (FactionFactory.WildPredators, 
				new ChasePlayerAI (true, true, 10, 15, 25, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect>{
									{typeof(HitByNonVegetarianFoodMessage), new TurnNeutralEffect( new RandomWalkAI(true, true, 10, 15, 25, Terra.ACCESS_OPEN, true, null ) ) }
								}),    
								new ActorStats(
			                              	speed:             8,
			                              	strength:          10,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    10,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           20,
			                              	armor:             new Armor(25,25,25),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			wolf.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("CanineCarcass", 1)));
			wolf.Attach (new HealthPointsPart (25, 25, 0));
			wolf.Attach (new MoveBlockingPart ());
			wolf.Attach (new VisualPart ("Creatures/Creature", "Creatures/DogTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (wolf));
			}
			return wolf;
		}

		public static Entity CreateCanineCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("canine carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/CanineCarcassTex", false, true));
			carcass.Attach (new PickablePart (30000, 50, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			carcass.Attach (new ConsumablePart(200, false, false));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}


		public static Entity CreateCat (GridLocation loc, bool addToGame) {
			Entity cat = new Entity (true);
			cat.Attach (new NamePart ("cat"));
			cat.Attach (new LocationPart (new GridLocation (loc), false));

			cat.Attach (new ActorPart (FactionFactory.WildPredators, 
				new ChasePlayerAI (true, true, 12, 18, 50, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect>{
									{typeof(HitByNonVegetarianFoodMessage), 
									new TurnNeutralEffect( new RandomWalkAI(true, true, 12, 18, 50, Terra.ACCESS_OPEN, true, null ) ) }
								}),    
								new ActorStats(
			                              	speed:             12,
			                              	strength:          10,
			                              	constitution:      25,
			                              	dexterity:         16,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    1,
			                              	attackType:        AttackTypes.Blade,
			                              	defense:           50,
			                              	armor:             new Armor(20,30,50),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			/*
			cat.Attach (new ActorPart (FactionFactory.WildPredators, new RandomWalkAI (true, false, 12, 18, 50, Terra.ACCESS_OPEN, false,
								new Dictionary<Type, ActorEffect>{
									{typeof(HitByVegetarianFoodMessage), new NullEffect() }
								}),  
				new ActorStats(14, 10, 25, 16, 15, 15, 10, 0, false)));
			*/

			cat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("FelineCarcass", 1)));
			cat.Attach (new HealthPointsPart (35, 35, 0));
			cat.Attach (new MoveBlockingPart ());
			cat.Attach (new VisualPart ("Creatures/Creature", "Creatures/CatTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (cat));
			}
			return cat;
		}

		public static Entity CreateFelineCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("feline carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/FelineCarcassTex", false, true));
			carcass.Attach (new PickablePart (10000, 25, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			carcass.Attach (new ConsumablePart(150, false, false));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}

		public static Entity CreateRat (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("rat"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/RatTex", false, false));
			
			
			rat.Attach (new ActorPart (FactionFactory.WildPredators, 
								new ChasePlayerAI (true, true, 10, 15, 25, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect>{
									{typeof(PlayFluteMessage), new TurnNeutralEffect( new RandomWalkAI(true, true, 10, 15, 25, Terra.ACCESS_OPEN, true, null ) ) },
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}),  
								new ActorStats(
			                              	speed:             10,
			                              	strength:          10,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    2,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           30,
			                              	armor:             new Armor(0,20,40),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
				
				/*
			rat.Attach (new ActorPart (FactionFactory.WildPredators, 
								new FleeFromPlayerAI (true, true, 10, 15, 25, Terra.ACCESS_OPEN, true, null),  
				new ActorStats(10, 10, 20, 20, 5, 1, 20, 8, 0, false, false)));
				*/
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("RatCarcass", 1)));
			rat.Attach (new HealthPointsPart (15, 15, 0));
			rat.Attach (new MoveBlockingPart ());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		public static Entity CreateRatCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("rat carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/RatCarcassTex", false, true));
			carcass.Attach (new PickablePart (5000, 1, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.SmallCarcassLimitedPresence, "rots away"));
			carcass.Attach (new ConsumablePart(100, false, false));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}


		public static Entity CreateGiantSpider (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("giant spider"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 15, 20, 25, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect> {
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() },
									{typeof(PlayFluteMessage), new TurnToFleeEffect() }
								}), 
								new ActorStats(
			                              	speed:             10,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    20,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Blade,
			                              	defense:           30,
			                              	armor:             new Armor(60,60,20),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false)));
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("GiantSpiderCarcass", 1)));
			rat.Attach (new HealthPointsPart (20, 20, 0));
			rat.Attach (new MoveBlockingPart ());
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/GiantSpiderTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		public static Entity CreateGiantSpiderCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("giant spider carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/GiantSpiderCarcassTex", false, true));
			carcass.Attach (new PickablePart (20000, 10, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			// TODO poison
			carcass.Attach (new ConsumablePart(250, false, false, new PoisonEffect(25)));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}

		public static Entity CreateSnake (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("snake"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 15, 20, 25, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect> {
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}), 
								new ActorStats(
			                              	speed:             8,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    10,
			                              	attackDuration:    3,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           20,
			                              	armor:             new Armor(50,0,25),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  false),
							new AttachSlowPoisonEffect(2, 20, 5) ));
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("SnakeCarcass", 1)));
			rat.Attach (new HealthPointsPart (10, 10, 0));
			rat.Attach (new MoveBlockingPart ());
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/SnakeTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		public static Entity CreateSnakeCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("snake carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/SnakeCarcassTex", false, true));
			carcass.Attach (new PickablePart (3000, 10, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.SmallCarcassLimitedPresence, "rots away"));
			// TODO poison
			carcass.Attach (new ConsumablePart(100, false, false )); //, new PoisonEffect(20)));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}


		public static Entity CreateSnappingTurtle (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("snapping turtle"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 10, 20, 50, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect> {
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}), 
								new ActorStats(
			                              	speed:             2,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    10,
			                              	attackDuration:    5,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           10,
			                              	armor:             new Armor(80,80,50),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  true), 
						new StunEffect(10, 25)));
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("SnappingTurtleCarcass", 1)));
			rat.Attach (new HealthPointsPart (30, 30, 0));
			rat.Attach (new MoveBlockingPart ());
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/SnappingTurtleTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		public static Entity CreateSnappingTurtleCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("snapping turtle carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/SnappingTurtleCarcassTex", false, true));
			carcass.Attach (new PickablePart (20000, 50, TradeClass.AnimalCarcass));
			//carcass.Attach (new LimitedPresencePart (GameRules.MediumCarcassLimitedPresence, "rots away"));
			carcass.Attach (new ConsumablePart(500, false, false));
			carcass.Attach (new TinnedPart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}

		public static Entity CreateToadQueen (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("toad queen"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 10, 20, 30, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect> {
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}),
						new ActorStats(
			                              	speed:             6,
			                              	strength:          10,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           30,
			                              	armor:             new Armor(20,20,50),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  true), 
						new AttachConfusionEffect(33, 8)));
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("Toad", 3)));
			rat.Attach (new HealthPointsPart (15, 15, 0));
			rat.Attach (new MoveBlockingPart ());
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/ToadQueenTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		public static Entity CreateToad (GridLocation loc, bool addToGame) {
			Entity rat = new Entity (true);
			rat.Attach (new NamePart ("toad"));
			rat.Attach (new LocationPart (new GridLocation (loc), false));
			rat.Attach (new ActorPart (FactionFactory.WildPredators, new ChasePlayerAI (true, true, 10, 20, 30, Terra.ACCESS_OPEN, true,
								new Dictionary<Type, ActorEffect> {
									{typeof(HitByNonVegetarianFoodMessage), new NullEffect() }
								}),
						new ActorStats(
			                              	speed:             6,
			                              	strength:          10,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    5,
			                              	attackDuration:    2,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           30,
			                              	armor:             new Armor(20,20,50),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  true), 
						new AttachConfusionEffect(33, 8)));
			rat.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("ToadCarcass", 1)));
			rat.Attach (new HealthPointsPart (15, 15, 0));
			rat.Attach (new MoveBlockingPart ());
			rat.Attach (new VisualPart ("Creatures/Creature", "Creatures/ToadTex", false, false));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (rat));
			}
			return rat;
		}
		
		public static Entity CreateToadCarcass (GridLocation loc, bool addToGame) {
			Entity carcass = new Entity (true);
			carcass.Attach (new NamePart ("toad carcass"));
			carcass.Attach (new LocationPart (new GridLocation (loc), false));
			carcass.Attach (new VisualPart ("Items/Item", "Creatures/ToadCarcassTex", false, true));
			carcass.Attach (new PickablePart (1000, 200, TradeClass.AnimalCarcass));
			carcass.Attach (new LimitedPresencePart (GameRules.SmallCarcassLimitedPresence, "rots away"));
			carcass.Attach (new ConsumablePart(250, false, false, new AttachConfusionEffect(33, 25)));
			carcass.Attach (new TinnablePart());
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (carcass));
			}
			return carcass;
		}



		public static Entity CreateSundew (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("sundew", "healing"));
			entity.Attach (new LocationPart (new GridLocation (loc), true));
			entity.Attach (new ActorPart (FactionFactory.WildPredators, new RandomWalkAI (false, false, 0, 0, 0, Terra.ACCESS_OPEN, false, null), 
							new ActorStats(
			                              	speed:             0,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    3,
			                              	attackDuration:    3,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           0,
			                              	armor:             new Armor(40,40,70),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  true), 
							new AttachStickToEntityEffect(entity, 999999)));
			entity.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("SundewTuft", 1)));
			entity.Attach (new HealthPointsPart (20, 20, 100));
			entity.Attach (new MoveBlockingPart ());
			entity.Attach (new VisualPart ("Creatures/Creature", "Creatures/SundewTex", false, true));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}
		public static Entity CreateSundewTuft (GridLocation loc, bool addToGame) {
			Entity entity = HerbFactory.CreateSimpleTuft(
							"sundew",
							"healing",
							"Creatures/SundewCarcassTex",
							loc, addToGame, 100);
			entity.Attach (new ConsumablePart ( 1, true, true, new HealingEffect(10) ));
			return entity;
		}


		public static Entity CreateVenusFlytrap (GridLocation loc, bool addToGame) {
			Entity entity = new Entity (true);
			entity.Attach (new NamePart ("venus flytrap", "poison resistance"));
			entity.Attach (new LocationPart (new GridLocation (loc), true));
			entity.Attach (new ActorPart (FactionFactory.WildPredators, new RandomWalkAI (false, false, 0, 0, 0, Terra.ACCESS_OPEN, false, null), 
							new ActorStats(
			                              	speed:             0,
			                              	strength:          20,
			                              	constitution:      20,
			                              	dexterity:         20,
			                              	attackNoWeapon:    8,
			                              	attackDuration:    3,
			                              	attackType:        AttackTypes.Pierce,
			                              	defense:           0,
			                              	armor:             new Armor(40,40,70),
			                              	maxStomachContent: 0,
			                              	canStarve:         false,
			                              	hasPassiveAttack:  true), 
							new AttachStickToEntityEffect(entity, 999999)));
			entity.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("VenusFlytrapTuft", 1)));
			entity.Attach (new HealthPointsPart (20, 20, 100));
			entity.Attach (new MoveBlockingPart ());
			entity.Attach (new VisualPart ("Creatures/Creature", "Creatures/VenusFlytrapTex", false, true));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}
		public static Entity CreateVenusFlytrapTuft (GridLocation loc, bool addToGame) {
			Entity entity = HerbFactory.CreateSimpleTuft(
							"venus flytrap",
							"poison resistance",
							"Creatures/VenusFlytrapCarcassTex",
							loc, addToGame, 200);
			entity.Attach (new ConsumablePart ( 1, true, true, new AttachPoisonResistanceEffect(100) ));
			return entity;
		}



		public static Entity CreateTree (GridLocation loc, bool addToGame) {
			Entity tree = new Entity (false);

			tree.Attach (new NamePart ("tree"));
			tree.Attach (new LocationPart (new GridLocation (loc), true));
			tree.Attach (new VisualPart ("Creatures/Tree", "Creatures/TreeTex", false, true));
			tree.Attach (new ViewBlockingPart ());
			tree.Attach (new MoveBlockingPart ());
			tree.Attach (new MiniMapPart (new Color (0, 0.8f, 0)));
			tree.Attach (new DestructablePart ());
			tree.Attach (new DestructionEffectPart (new SpawnEntitiesDestructable ("WoodLog", 5)));
			tree.Attach (new HealthPointsPart (100, 100, 0));

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (tree));
			}

			return tree;
		}



		static void AssignSkills(Entity entity, InventoryPart inventory, int min, int max) {
			EquipmentPart weapon = inventory.GetEquipped(Equip.HandPrim);
			PickablePart proj = inventory.GetQuivered();
			if( weapon != null && weapon.Skill != Skills.None ) {
				entity.GetPart<ActorPart>(ActorPart.partId).Stats.Skill[ (int) weapon.Skill ] = RandUtils.NextInt(min, max+1);
			}
			if( proj != null && proj.Entity.HasPart(ProjectilePart.partId) ) {
				ProjectilePart pp = proj.Entity.GetPart<ProjectilePart>(ProjectilePart.partId);
				if(pp.Skill != Skills.None) {
					entity.GetPart<ActorPart>(ActorPart.partId).Stats.Skill[ (int) pp.Skill ] = RandUtils.NextInt(min, max+1);
				}
			}
		}





	}
}
