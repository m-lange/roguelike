﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class EntityFactory {
		
		public static Color MapMarkerColor = new Color (0f, 0f, 1f);
		public static Color MapMarkerHighlightColor = new Color (0.8f, 0f, 0.8f);

		public static readonly string[] TraderItems = new string[] {
			"Club",
			"Axe",
			"ShortSword",
			"LongSword",
			"Mace",
			"Spear",
			"Halberd",
			"Dagger", "Dagger",
			"Arrow", "Arrow", "Arrow", "Arrow",
			"Bow",
			"CrossbowBolt", "CrossbowBolt",
			"Crossbow",
			"Sling",
			"Helmet",
			"CloseHelmet",
			"LongShield",
			"RoundShield",
			"Shoes",
			"Boots",
			"LeatherArmor",
			"ChainMail",
			"PlateMail",
			"Key",
			"TinOpener",
			"TinningKit",
			"Flute",
			"TinnedMeat", "TinnedMeat", "TinnedMeat",
			"Bread", "Bread", "Bread",
			"Chest",
			"Box",
			"Bag",
			"Trolley",
			"BookOfHerbs",
			"BookOfMushrooms",
			"BookOfGrasses",
			"BookOfShrubs",
			"BookOfAnimals",
			"BookOfCarnivorousPlants",
			"BookOfWeapons",
			"BookOfArmor",
			"BookOfTools",
			"Candle", "Candle", "Candle",
			"OilLamp",
			"OilLatern",
			"Torch", "Torch",
			"OilCan", "OilCan",
			"BearTrap",
			"BoobyTrap", "BoobyTrap",
			"GrapplingHook"
		};

		public static readonly string[] WeaponTraderItems = new string[] {
			"Club",
			"Axe",
			"ShortSword",
			"LongSword",
			"Mace",
			"Spear",
			"Halberd",
			"Dagger", "Dagger",
			"Arrow", "Arrow", "Arrow", "Arrow",
			"Bow",
			"CrossbowBolt", "CrossbowBolt",
			"Crossbow",
			"Sling",
			"BoobyTrap"
		};

		public static readonly string[] ArmorTraderItems = new string[] {
			"Helmet",
			"CloseHelmet",
			"LongShield",
			"RoundShield",
			"Shoes",
			"Boots",
			"LeatherArmor",
			"ChainMail",
			"PlateMail"
		};

		public static readonly string[] ToolTraderItems = new string[] {
			"Key",
			"TinOpener",
			"TinningKit",
			"Flute",
			"Candle", "Candle", "Candle",
			"OilLamp",
			"OilLatern",
			"Torch", "Torch",
			"OilCan", "OilCan",
			"BearTrap",
			"BoobyTrap", "BoobyTrap",
			"GrapplingHook",
			"Chest",
			"Box",
			"Bag",
			"Trolley"
		};

		public static readonly string[] ProvisionTraderItems = new string[] {
			"TinnedMeat",
			"Bread"
		};

		public static readonly string[] HerbTraderItems = HerbFactory.Tufts.ToArray();

		public static readonly string[] BookTraderItems = new string[] {
			"BookOfHerbs",
			"BookOfMushrooms",
			"BookOfShrubs",
			"BookOfGrasses",
			"BookOfAnimals",
			"BookOfCarnivorousPlants",
			"BookOfWeapons",
			"BookOfArmor",
			"BookOfTools"
		};


		static readonly Dictionary<string, Func<GridLocation, bool, Entity>> creators;


		public static void AddToInventory(InventoryPart inv, Entity item, bool equip, bool quiver = false) {
			inv.AddItem (item.GetPart<PickablePart> (PickablePart.partId));
			if (equip && item.HasPart (EquipmentPart.partId)) {
				inv.Equip (item.GetPart<EquipmentPart> ());
			}
			if (quiver && item.HasPart (PickablePart.partId)) {
				inv.Quiver (item.GetPart<PickablePart> ());
			}
		}


		public static Func<GridLocation, bool, Entity> GetCreator (string type) {
			string name = "Create" + type;
			if (!creators.ContainsKey (name)) {
				throw new ArgumentException("Entity creator for "+type+" does not exist.");
			}
			return creators [name];
		}
		public static bool HasCreator (string type) {
			string name = "Create" + type;
			return creators.ContainsKey (name);
		}


		static EntityFactory() {

			creators = new Dictionary<string, Func<GridLocation, bool, Entity>> ();

			Type[] types = new Type[] {
				typeof(CreatureFactory),
				typeof(TraderFactory),
				typeof(WeaponFactory),
				typeof(ArmorFactory),
				typeof(ItemFactory),
				typeof(ConsumablesFactory),
				typeof(HerbFactory),
				typeof(ContainerFactory),
				typeof(BookFactory),
				typeof(VisualFactory)
			};
			foreach(Type tp in types) {
				foreach (MethodInfo method in tp.GetMethods()) {
					ParameterInfo[] par = method.GetParameters();
					if (method.IsStatic 
						&& method.IsPublic 
						&& method.ReturnType == typeof(Entity)
						&& par.Length == 2
						&& par[0].ParameterType == typeof(GridLocation)
						&& par[1].ParameterType == typeof(bool) ) {
						creators [method.Name] = (loc, addToGame) => (Entity) method.Invoke (null, new object[]{ loc, addToGame });
					}
				}
			}

		}

	}
}
