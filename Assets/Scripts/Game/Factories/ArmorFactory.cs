﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class ArmorFactory {

		public static Entity CreateLeatherArmor (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("leather armor"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/LeatherArmorTex", false, true));
			item.Attach (new PickablePart (4000, 250, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.BodyArmor, LauncherType.None, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(30, 30, 30) ));
			item.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreateChainMail (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("chain mail"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/ChainMailTex", false, true));
			item.Attach (new PickablePart (12000, 1000, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.BodyArmor, LauncherType.None, Skills.None, 0, 0, -1, -1, 0, AttackTypes.Impact, 0, new 
			                                Armor(30, 50, 70) ));
			item.Attach (new LimitedUsePart (250, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreatePlateMail (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("plate mail"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/PlateMailTex", false, true));
			item.Attach (new PickablePart (18000, 500, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.BodyArmor, LauncherType.None, Skills.None, 0, 0, -2, -2, 0, AttackTypes.Impact, 0, 
			                                new Armor(70, 70, 50) ));
			item.Attach (new LimitedUsePart (500, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}


		public static Entity CreateShoes (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("pair of shoes"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/ShoesTex", false, true));
			item.Attach (new PickablePart (2000, 100, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.Feet, LauncherType.None, Skills.None, 3, 0, 0, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(10, 10, 10) ));
			item.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreateBoots (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("pair of boots"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/BootsTex", false, true));
			item.Attach (new PickablePart (3000, 200, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.Feet, LauncherType.None, Skills.None, 2, 0, 0, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(15, 15, 15) ));
			item.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateHelmet (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("helmet"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/HelmetTex", false, true));
			item.Attach (new PickablePart (2000, 100, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.Head, LauncherType.None, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(25, 15, 15) ));
			item.Attach (new LimitedUsePart (250, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateCloseHelmet (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("close helmet"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/CloseHelmetTex", false, true));
			item.Attach (new PickablePart (3000, 500, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.Head, LauncherType.None, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(25, 25, 25) ));
			item.Attach (new LimitedUsePart (250, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateRoundShield (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("round shield"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/RoundShieldTex", false, true));
			item.Attach (new PickablePart (3000, 200, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.HandSec, LauncherType.None, Skills.None, -1, 0, -1, 0, 0, AttackTypes.Impact, 0, 
			                                new Armor(30, 30, 30) ));
			item.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreateLongShield (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("long shield"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/LongShieldTex", false, true));
			item.Attach (new PickablePart (5000, 300, TradeClass.Armor));
			item.Attach (new EquipmentPart (Equip.HandSec, LauncherType.None, Skills.None, -2, 0, -1, -1, 0, AttackTypes.Impact, 0, 
			                                new Armor(50, 50, 50) ));
			item.Attach (new LimitedUsePart (100, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}


	}
}
