﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Game.EntityEffects;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class VisualFactory {


		public static Entity CreateMapMarker (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("map marker"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Visuals/MapMarkerTex", false, true));
			item.Attach (new MapMarkerPart(""));
			item.Attach (new MiniMapPart (EntityFactory.MapMarkerColor));
			
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

	}
}
