﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using ComponentSystem;
using Game.Parts.Sub;
using Map.LayeredGrid;
using Game.ActorEffects;
using Game.ActorEffects.Continuous;
using Game.Data;
using Game.Events;
using Game.Usables;
using Game.Parts;
using Game.Util;

namespace Game.Factories {
	public class HerbFactory {

		public const string TuftGenerator = "Tuft";

		static readonly string[] NameList = new string[] {
			"green herb",
			"blue herb",
			"purple herb",
			"large-flowered herb",

			"green shrub",
			"purple shrub",
			"defoliated shrub",
			"hanging shrub",

			"low grass",
			"tall grass",
			"flowering grass",

			"blue mushroom",
			"spotted mushroom",
			"trumpet mushroom",
			"rusty mushroom"
		};

		/// <summary>
		/// Mapping generator -> name
		/// </summary>
		public static Dictionary<string, string> Names = new Dictionary<string, string>();
		/// <summary>
		/// Mapping name -> generator
		/// </summary>
		public static Dictionary<string, string> Generators = new Dictionary<string, string>();
		/// <summary>
		/// List of tuft generators
		/// </summary>
		public static List<string> Tufts = new List<string>();


		public static Entity CreateNutritionHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"nutrition",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateNutritionHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"nutrition",
							loc, addToGame, 25);
			herb.Attach (new ConsumablePart ( 25, true, true ));
			return herb;
		}


		public static Entity CreatePoisonHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"poison",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreatePoisonHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"poison",
							loc, addToGame, 25);
			herb.Attach (new ConsumablePart ( 1, true, true, new PoisonEffect(10) ));
			return herb;
		}

		public static Entity CreateSlowPoisonHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"slow poison",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateSlowPoisonHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"slow poison",
							loc, addToGame, 25);
			herb.Attach (new ConsumablePart ( 1, true, true, new AttachSlowPoisonEffect(2, 20, 0) ));
			return herb;
		}



		public static Entity CreateConfusionHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"confusion",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateConfusionHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"confusion",
							loc, addToGame, 100);
			herb.Attach (new ConsumablePart ( 1, true, true, new AttachConfusionEffect(33, 100) ));
			return herb;
		}


		public static Entity CreateStunningHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"stun",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateStunningHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"stun",
							loc, addToGame, 100);
			herb.Attach (new ConsumablePart ( 1, true, true, new StunEffect(10, 0) ));
			return herb;
		}



		public static Entity CreateAntiPoisonHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"anti poison",
							loc, addToGame, 
							1, 200, 2, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateAntiPoisonHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"anti poison",
							loc, addToGame, 250);
			herb.Attach (new ConsumablePart ( 1, true, true, new AntiPoisonEffect() ));
			return herb;
		}



		public static Entity CreatePoisonResistanceHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"poison resistance",
							loc, addToGame, 
							1, 200, 2, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreatePoisonResistanceHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"poison resistance",
							loc, addToGame, 200);
			herb.Attach (new ConsumablePart ( 1, true, true, new AttachPoisonResistanceEffect(100) ));
			return herb;
		}




		public static Entity CreateHealingHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"healing",
							loc, addToGame, 
							1, 200, 5, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateHealingHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"healing",
							loc, addToGame, 100);
			herb.Attach (new ConsumablePart ( 1, true, true, new HealingEffect(10) ));
			return herb;
		}

		public static Entity CreateExtraHealingHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"healing+",
							loc, addToGame, 
							1, 200, 1, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateExtraHealingHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"healing+",
							loc, addToGame, 250);
			herb.Attach (new ConsumablePart ( 1, true, true, new ExtraHealingEffect(10) ));
			return herb;
		}


		public static Entity CreateFullHealingHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"full healing",
							loc, addToGame, 
							1, 200, 2, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateFullHealingHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"full healing",
							loc, addToGame, 200);
			herb.Attach (new ConsumablePart ( 1, true, true, new FullHealingEffect() ));
			return herb;
		}

		public static Entity CreateGainStrengthHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"strength",
							loc, addToGame, 
							1, 200, 1, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateGainStrengthHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"strength",
							loc, addToGame, 200);
			herb.Attach (new ConsumablePart ( 1, true, true, new ModifyStrengthEffect( 1 ) ));
			return herb;
		}

		public static Entity CreateGainConstitutionHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb(
							"constitution",
							loc, addToGame, 
							1, 200, 1, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateGainConstitutionHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"constitution",
							loc, addToGame, 150);
			herb.Attach (new ConsumablePart ( 1, true, true, new ModifyConstitutionEffect( 1 ) ));
			return herb;
		}

		public static Entity CreateSpeedHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb (
							"speed boost",
							loc, addToGame, 
							1, 200, 2, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateSpeedHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"speed boost",
							loc, addToGame, 250);
			herb.Attach (new ConsumablePart ( 1, true, true, new AttachModifyStatsEffect( "speed boost", "You feel fast.", "+3x100T",
			                                                                             3, 0, 0, 0, 0, new Armor(),  100 ) ));
			return herb;
		}


		public static Entity CreateBerserkHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb (
							"berserk",
							loc, addToGame, 
							1, 200, 2, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateBerserkHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
							"berserk",
							loc, addToGame, 250);
			herb.Attach (new ConsumablePart ( 1, true, true, new AttachModifyStatsEffect( "berserk", "You berserk.", "100T",
					2, 10, -2, 10, 20, new Armor(),   100 ) ));
			return herb;
		}


		public static Entity CreateSavePointHerb (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultHerb (
							"reincarnation",
							loc, addToGame, 
							1, 200, 1, 
							Terra.FloorMeadow,
							Terra.FloorForest,
							Terra.FloorField);
			return herb;
		}
		public static Entity CreateSavePointHerbTuft (GridLocation loc, bool addToGame) {
			Entity herb = CreateDefaultTuft(
				"reincarnation",
							loc, addToGame, 1000);
			herb.Attach (new ConsumablePart ( 1, true, true, new SavePointEffect()));
			return herb;
		}




		static Entity CreateDefaultHerb (string nameIdentified, GridLocation loc, bool addToGame, int maxHarvest, int durability, int growthProb100, params string[] terrains) {
			string method = FactoryUtil.GetMethodName (2);
			string name = Names [method];
			string generator = FactoryUtil.MethodToGenerator(method)+TuftGenerator;
			Entity herb = new Entity (true);
			herb.Attach (new NamePart (name, nameIdentified));
			herb.Attach (new LocationPart (new GridLocation (loc), false));
			herb.Attach (new VisualPart ("Items/Item", FactoryUtil.GetTexturePath ("Herbs", name), false, true));
			herb.Attach (new PickablePart (500, 10, TradeClass.Herbs));
			herb.Attach (new ProjectilePart (4, 0, 1,    0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			herb.Attach (new DestructablePart ());
			herb.Attach (new HealthPointsPart (10, 10, 100));

			int[] terr = new int[terrains.Length];
			for (int i = 0; i < terr.Length; i++) {
				terr[i] = Terra.TileInfoId(terrains[i]);
			}
			herb.Attach (new PlantPart ( generator, maxHarvest, durability, 100, growthProb100, "rots away", terr) );
			if (addToGame) { 
					GameData.EventManager.Notify ( new CreateEntityEvent(herb) );
			}
				return herb;
		}

		static Entity CreateDefaultTuft (string nameIdentified, GridLocation loc, bool addToGame, int price) {
			string method = GetBaseMethodName (2);
			string name = Names [method];
			Entity herb = new Entity (false);
			herb.Attach (new NamePart (TextUtil.TuftName + name, nameIdentified));
			herb.Attach (new LocationPart (new GridLocation (loc), false));
			herb.Attach (new VisualPart ("Items/Item", FactoryUtil.GetTexturePath ("Herbs", name), false, true));
			herb.Attach (new PickablePart (50, price, TradeClass.Herbs));
			herb.Attach (new StackablePart ( FactoryUtil.MethodToGenerator(FactoryUtil.GetMethodName (2)), 1, 100 ));
			herb.Attach (new ProjectilePart (3, 0, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			herb.Attach (new UsablePart( new HerbUsable() ));
			herb.Attach (new DestructablePart ());
			herb.Attach (new HealthPointsPart (1, 1, 0));
			return herb;
		}


		public static Entity CreateSimpleTuft (string name, string nameIdentified, string texture, GridLocation loc, bool addToGame, int price) {
			//string method = GetBaseMethodName (2);
			Entity herb = new Entity (false);
			herb.Attach (new NamePart (TextUtil.TuftName + name, nameIdentified));
			herb.Attach (new LocationPart (new GridLocation (loc), false));
			herb.Attach (new VisualPart ("Items/Item", texture, false, true));
			herb.Attach (new PickablePart (50, price, TradeClass.Herbs));
			herb.Attach (new StackablePart ( FactoryUtil.MethodToGenerator(FactoryUtil.GetMethodName (2)), 1, 100 ));
			herb.Attach (new ProjectilePart (3, 0, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			herb.Attach (new UsablePart( new HerbUsable() ));
			herb.Attach (new DestructablePart ());
			herb.Attach (new HealthPointsPart (1, 1, 0));
			return herb;
		}

		public static void PopulateNames (string[] factories, string[] names) {
			Names.Clear ();
			Tufts.Clear ();
			for (int i = 0; i < factories.Length; i++) {
				Names[factories[i]] = names[i];
				Tufts.Add( factories[i].Replace("Create","")+TuftGenerator );
				Generators[names[i]] = factories[i].Replace("Create","");
			}
		}

		static HerbFactory () {
			if (Names.Count > 0) {
				return;
			}
			List<string> names = new List<string> ();
			foreach (MethodInfo method in typeof(HerbFactory).GetMethods()) {
				ParameterInfo[] par = method.GetParameters ();
				if (method.IsStatic
				    && method.IsPublic
					&& ! method.Name.EndsWith(TuftGenerator)
				    && method.ReturnType == typeof(Entity)
				    && par.Length == 2
				    && par [0].ParameterType == typeof(GridLocation)
				    && par [1].ParameterType == typeof(bool)) {

					names.Add (method.Name);
				}
			}
			if (names.Count > NameList.Length) {
				throw new ArgumentException ("Not enough herb names!");
			}
			names = RandUtils.Shuffle (names);
			List<string> nameList = RandUtils.Shuffle (NameList);
			for (int i = 0; i < names.Count; i++) {
				Names[names[i]] = nameList[i];
				Tufts.Add(names[i].Replace("Create","")+TuftGenerator);
				Generators[nameList[i]] = names[i].Replace("Create","");
			}
		}

		static string GetBaseMethodName (int depth) {
			return new StackTrace(true).GetFrame(depth).GetMethod().Name.Replace(TuftGenerator, "");
		}

	}
}
