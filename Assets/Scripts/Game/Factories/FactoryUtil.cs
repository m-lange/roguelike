﻿using System.Diagnostics;
using Game.Util;

namespace Game.Factories {
	public class FactoryUtil {


		public static string GetMethodName (int depth) {
			return new StackTrace(true).GetFrame(depth).GetMethod().Name;
		}
		public static string MethodToGenerator (string methodName) {
			return methodName.Replace("Create", "");
		}
		public static string NameToGenerator (string name) {
			string[] parts = name.Split (new char[]{' ', '-'});
			for (int i = 0; i < parts.Length; i++) {
				parts[i] = TextUtil.Capitalize(parts[i]);
			}
			return string.Join("", parts);
		}

		public static string GetTexturePath (string folder, string name) {
			string[] parts = name.Split (new char[]{' ', '-'});
			for (int i = 0; i < parts.Length; i++) {
				parts[i] = TextUtil.Capitalize(parts[i]);
			}
			return folder+"/"+string.Join("", parts) + "Tex";
		}


	}
}
