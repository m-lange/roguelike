﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class ContainerFactory {

		public static Entity CreateChest (GridLocation loc, bool addToGame) {
			Entity item = new Entity (true);
			item.Attach (new NamePart ("chest"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Containers/ChestTex", false, true));
			item.Attach (new PickablePart (30000, 500, TradeClass.Containers));
			item.Attach (new InventoryPart(GlobalParameters.letters.Length-1, 500000, 1f ) );
			item.Attach (new ContainerPart(true, true) );
			item.Attach (new BulkyPart() );
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateChest (GridLocation loc, bool addToGame, int gold) {
			Entity item = CreateChest(loc, addToGame);
			InventoryPart inv = item.GetPart<InventoryPart>();
			EntityFactory.AddToInventory(inv, ItemFactory.CreateGold(loc, gold, false), false);
			return item;
		}

		public static Entity CreateBox (GridLocation loc, bool addToGame) {
			Entity item = new Entity (true);
			item.Attach (new NamePart ("box"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Containers/BoxTex", false, true));
			item.Attach (new PickablePart (10000, 50, TradeClass.Containers));
			item.Attach (new InventoryPart(GlobalParameters.letters.Length-1, 100000, 1f ) );
			item.Attach (new ContainerPart(false, false) );
			item.Attach (new BulkyPart() );
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}


		public static Entity CreateBag (GridLocation loc, bool addToGame) {
			Entity item = new Entity (true);
			item.Attach (new NamePart ("bag"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Containers/BagTex", false, true));
			item.Attach (new PickablePart (500, 25, TradeClass.Containers));
			item.Attach (new InventoryPart(GlobalParameters.letters.Length-1, 50000, 1f ) );
			item.Attach (new ContainerPart(false, false) );
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateTrolley (GridLocation loc, bool addToGame) {
			Entity item = new Entity (true);
			item.Attach (new NamePart ("trolley"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Containers/TrolleyTex", false, true));
			item.Attach (new PickablePart (3000, 400, TradeClass.Containers));
			item.Attach (new InventoryPart(GlobalParameters.letters.Length-1, 200000, 0.25f ) );
			item.Attach (new ContainerPart(false, false) );
			item.Attach (new BulkyPart() );
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
	}
}
