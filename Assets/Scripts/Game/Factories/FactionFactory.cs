﻿using System.Collections;
using System.Collections.Generic;
using Game.Data;

namespace Game.Factories {
	public class FactionFactory {

		public static readonly string Neutral = "Neutral";
		public static readonly string Player = "Player";
		public static readonly string Villagers = "Villagers";
		public static readonly string AngryVillagers = "AngryVillagers";
		public static readonly string Soldiers = "Soldiers";
		public static readonly string DomesticPrey = "DomesticPrey";
		public static readonly string DomesticPredator = "DomesticPredator";
		public static readonly string WildPrey = "WildPrey";
		public static readonly string WildPredators = "WildPredators";
	
		public static Factions CreateFactions() {
			Factions factions = new Factions (
				Neutral,
				Player,
				Villagers,
				AngryVillagers,
				Soldiers,
				DomesticPrey,
				DomesticPredator,
				WildPrey,
				WildPredators
			);

			factions.SetNeutralWithAll (Neutral);

			factions.SetFriendlyWith (Villagers, Soldiers);
			factions.SetFriendlyWith (AngryVillagers, Soldiers);
			factions.SetFriendlyWith (Villagers, AngryVillagers);
			factions.SetFriendlyWith (Player, DomesticPrey);
			factions.SetFriendlyWith (Villagers, DomesticPrey);
			factions.SetFriendlyWith (Villagers, DomesticPredator);
			factions.SetFriendlyWith (AngryVillagers, DomesticPrey);
			factions.SetFriendlyWith (AngryVillagers, DomesticPredator);
			factions.SetFriendlyWith (Soldiers, DomesticPrey);
			factions.SetFriendlyWith (Soldiers, DomesticPredator);

			factions.SetHostileWith (Player, AngryVillagers);
			factions.SetHostileWith (Player, WildPrey);
			factions.SetHostileWith (Player, WildPredators);
			factions.SetHostileWith (Player, DomesticPredator);

			factions.SetHostileWith (WildPredators, DomesticPrey);

			return factions;
		}
	}
}
