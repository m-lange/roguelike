﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;
using Game.AI;
using Game.Util;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Usables;
using Game.Destructables;

namespace Game.Factories {
	public class WeaponFactory {

		public static Entity CreateClub (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("club"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/ClubTex", false, true));
			item.Attach (new PickablePart (6000, 50, TradeClass.Weapon));
			item.Attach (new ProjectilePart (6, 8, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.ClubAndMace, 0, 0, 0, 15, 3, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateAxe (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("axe"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/AxeTex", false, true));
			item.Attach (new PickablePart (4000, 100, TradeClass.Weapon, TradeClass.Tools));
			item.Attach (new ProjectilePart (8, 15, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.AxeRanged));
			item.Attach (new UsablePart (new AxeUsable ()));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.Axe, 0, 0, 0, 20, 2, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateShortSword (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("short sword"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/ShortSwordTex", false, true));
			item.Attach (new PickablePart (1500, 600, TradeClass.Weapon));
			item.Attach (new ProjectilePart (6, 8, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.Sword, 0, 0, 0, 25, 2, AttackTypes.Blade, 0, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateLongSword (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("long sword"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/LongSwordTex", false, true));
			item.Attach (new PickablePart (2000, 1000, TradeClass.Weapon));
			item.Attach (new ProjectilePart (6, 8, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.Sword, 0, 0, 0, 30, 2, AttackTypes.Blade, 0, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateMace (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("mace"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/MaceTex", false, true));
			item.Attach (new PickablePart (5000, 800, TradeClass.Weapon));
			item.Attach (new ProjectilePart (6, 12, 1,   0, 0, 0, LauncherType.None, AttackTypes.Impact, Skills.None));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.ClubAndMace, 0, 0, 0, 40, 3, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateSpear (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("spear"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/SpearTex", false, true));
			item.Attach (new PickablePart (1000, 300, TradeClass.Weapon));
			item.Attach (new ProjectilePart (10, 20, 2,    0, 0, 0, LauncherType.None, AttackTypes.Pierce, Skills.SpearRanged) );
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.SpearAndHalberd, 0, 0, 0, 20, 2, AttackTypes.Pierce, 20, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		
		public static Entity CreateHalberd (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("halberd"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/HalberdTex", false, true));
			item.Attach (new PickablePart (3000, 800, TradeClass.Weapon));
			item.Attach (new ProjectilePart (6, 10, 2,    0, 0, 0, LauncherType.None, AttackTypes.Pierce, Skills.None) );
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.SpearAndHalberd, 0, 0, 0, 30, 3, AttackTypes.Pierce, 30, new Armor() ));
			item.Attach (new LimitedUsePart (1000, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}


		public static Entity CreateDagger (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("dagger"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/DaggerTex", false, true));
			item.Attach (new PickablePart (250, 50, TradeClass.Weapon));
			item.Attach (new ProjectilePart (10, 15, 1,    0, 0, 0, LauncherType.None, AttackTypes.Pierce, Skills.DaggerRanged));
			item.Attach (new StackablePart ("Dagger", 1, 1000));
			item.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.Dagger, 0, 0, 0, 10, 1, AttackTypes.Pierce, 0, new Armor() ));
			item.Attach (new LimitedUsePart (100, true, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateDaggers (GridLocation loc, int count, bool addToGame) {
			Entity item = CreateDagger (loc, addToGame);
			StackablePart stack = item.GetPart<StackablePart> (StackablePart.partId);
			if (stack != null) {
				stack.Count = count;
			}
			return item;
		}

		public static Entity CreateArrow (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("arrow"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/ArrowTex", false, true));
			item.Attach (new PickablePart (60, 20, TradeClass.Weapon));
			item.Attach (new ProjectilePart (4, 2, 1,    15, 20, 2, LauncherType.Bow, AttackTypes.Pierce, Skills.Bow));
			item.Attach (new StackablePart ("Arrow", 1, 1000));
			item.Attach (new LimitedUsePart (25, true, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreateArrows (GridLocation loc, int count, bool addToGame) {
			Entity item = CreateArrow (loc, addToGame);
			StackablePart stack = item.GetPart<StackablePart> (StackablePart.partId);
			if (stack != null) {
				stack.Count = count;
			}
			return item;
		}

		public static Entity CreateCrossbowBolt (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("crossbow bolt"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/CrossbowBoltTex", false, true));
			item.Attach (new PickablePart (30, 25, TradeClass.Weapon));
			item.Attach (new ProjectilePart (4, 3, 1,    20, 30, 3, LauncherType.Crossbow, AttackTypes.Pierce, Skills.Crossbow));
			item.Attach (new StackablePart ("CrossbowBolt", 1, 1000));
			item.Attach (new LimitedUsePart (25, true, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}
		public static Entity CreateCrossbowBolts (GridLocation loc, int count, bool addToGame) {
			Entity item = CreateCrossbowBolt (loc, addToGame);
			StackablePart stack = item.GetPart<StackablePart> (StackablePart.partId);
			if (stack != null) {
				stack.Count = count;
			}
			return item;
		}

		public static Entity CreateBow (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("bow"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/BowTex", false, true));
			item.Attach (new PickablePart (2000, 500, TradeClass.Weapon));
			item.Attach (new EquipmentPart (Equip.Ranged, LauncherType.Bow, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (200, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateCrossbow (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("crossbow"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/CrossbowTex", false, true));
			item.Attach (new PickablePart (4000, 1000, TradeClass.Weapon));
			item.Attach (new EquipmentPart (Equip.Ranged, LauncherType.Crossbow, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (200, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

		public static Entity CreateSling (GridLocation loc, bool addToGame) {
			Entity item = new Entity (false);
			item.Attach (new NamePart ("sling"));
			item.Attach (new LocationPart (new GridLocation (loc), false));
			item.Attach (new VisualPart ("Items/Item", "Items/SlingTex", false, true));
			item.Attach (new PickablePart (1000, 50, TradeClass.Weapon));
			item.Attach (new EquipmentPart (Equip.Ranged, LauncherType.Sling, Skills.None, 0, 0, 0, 0, 0, AttackTypes.Impact, 0, new Armor() ));
			item.Attach (new LimitedUsePart (500, false, "breaks"));
			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (item));
			}
			return item;
		}

	}
}
