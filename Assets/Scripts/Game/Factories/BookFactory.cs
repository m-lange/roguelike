﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using ComponentSystem;
using Game.Parts.Sub;
using Map.LayeredGrid;
using Game.ActorEffects;
using Game.ActorEffects.Continuous;
using Game.Data;
using Game.Events;
using Game.Usables;
using Game.Parts;
using Game.Util;

namespace Game.Factories {
	public class BookFactory {

		static readonly string[] NameList = new string[] {
			"blue book",
			"red book",
			"green book",
			"purple book",
			"golden book",
			"black book",
			"cyan book",
			"white book",
			"silver book"
		};

		/// <summary>
		/// Mapping generator -> name
		/// </summary>
		public static Dictionary<string, string> Names = new Dictionary<string, string>();

		public static Entity CreateBookOfHerbs (GridLocation loc, bool addToGame) {
			return CreateDefaultBook(
				"book of herbs",
				500,
				loc, addToGame,
				2,
				new string[] {
					"green herb",
					"tuft of green herb",
					"blue herb",
					"tuft of blue herb",
					"purple herb",
					"tuft of purple herb",
					"large-flowered herb",
					"tuft of large-flowered herb"
				},
				new string[] {
					HerbFactory.Generators["green herb"],
					HerbFactory.Generators["green herb"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["blue herb"],
					HerbFactory.Generators["blue herb"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["purple herb"],
					HerbFactory.Generators["purple herb"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["large-flowered herb"],
					HerbFactory.Generators["large-flowered herb"]+HerbFactory.TuftGenerator
				}
			);
		}

		public static Entity CreateBookOfMushrooms (GridLocation loc, bool addToGame) {
			return CreateDefaultBook(
				"book of mushrooms",
				500,
				loc, addToGame,
				2,
				new string[] {
					"blue mushroom",
					"tuft of blue mushroom",
					"spotted mushroom",
					"tuft of spotted mushroom",
					"trumpet mushroom",
					"tuft of trumpet mushroom",
					"rusty mushroom",
					"tuft of rusty mushroom"
				},
				new string[] {
					HerbFactory.Generators["blue mushroom"],
					HerbFactory.Generators["blue mushroom"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["spotted mushroom"],
					HerbFactory.Generators["spotted mushroom"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["trumpet mushroom"],
					HerbFactory.Generators["trumpet mushroom"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["rusty mushroom"],
					HerbFactory.Generators["rusty mushroom"]+HerbFactory.TuftGenerator
				}
			);
		}

		public static Entity CreateBookOfShrubs (GridLocation loc, bool addToGame) {
			return CreateDefaultBook(
				"book of shrubs",
				500,
				loc, addToGame,
				2,
				new string[] {
					"green shrub",
					"tuft of green shrub",
					"purple shrub",
					"tuft of purple shrub",
					"defoliated shrub",
					"tuft of defoliated shrub",
					"hanging shrub",
					"tuft of hanging shrub"
				},
				new string[] {
					HerbFactory.Generators["green shrub"],
					HerbFactory.Generators["green shrub"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["purple shrub"],
					HerbFactory.Generators["purple shrub"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["defoliated shrub"],
					HerbFactory.Generators["defoliated shrub"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["hanging shrub"],
					HerbFactory.Generators["hanging shrub"]+HerbFactory.TuftGenerator
				}
			);
		}

		public static Entity CreateBookOfGrasses (GridLocation loc, bool addToGame) {
			return CreateDefaultBook(
				"book of grasses",
				500,
				loc, addToGame,
				2,
				new string[] {
					"low grass",
					"tuft of low grass",
					"tall grass",
					"tuft of tall grass",
					"flowering grass",
					"tuft of flowering grass"
				},
				new string[] {
					HerbFactory.Generators["low grass"],
					HerbFactory.Generators["low grass"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["tall grass"],
					HerbFactory.Generators["tall grass"]+HerbFactory.TuftGenerator,
					HerbFactory.Generators["flowering grass"],
					HerbFactory.Generators["flowering grass"]+HerbFactory.TuftGenerator
				}
			);
		}


		public static Entity CreateBookOfCarnivorousPlants (GridLocation loc, bool addToGame) {
			return CreateDefaultBook(
				"book of carnivorous plants",
				500,
				loc, addToGame,
				2,
				new string[] {
					"sundew",
					"tuft of sundew",
					"venus flytrap",
					"tuft of venus flytrap"
				},
				new string[] {
					"Sundew",
					"SundewTuft",
					"VenusFlytrap",
					"VenusFlytrapTuft"
				}
			);
		}

		public static Entity CreateBookOfAnimals (GridLocation loc, bool addToGame) {
			string[] names = new string[] {
					"rat",
					"rat carcass",
					"dog",
					"canine carcass",
					"wolf",
					"canine carcass",
					"cat",
					"feline carcass",
					"giant spider",
					"giant spider carcass",
					"snake",
					"snake carcass",
					"snapping turtle",
					"snapping turtle carcass",
					"toad",
					"toad carcass",
					"toad queen",
					"toad queen carcass"
				};
			string[] generators = new List<string>(names).Select( (n) => FactoryUtil.NameToGenerator(n) ).ToArray();
			return CreateDefaultBook(
				"book of animals",
				500,
				loc, addToGame,
				2,
				names,
				generators
			);
		}

		public static Entity CreateBookOfWeapons (GridLocation loc, bool addToGame) {
			string[] names = new string[] {
					"Axe",
					"Club",
					"Mace",
					"ShortSword",
					"LongSword",
					"Spear",
					"Halberd",
					"Dagger",
					"Bow",
					"Arrow",
					"Crossbow",
					"CrossbowBolt",
					"Sling"
				};
			string[] generators = new List<string>(names).Select( (n) => FactoryUtil.NameToGenerator(n) ).ToArray();
			return CreateDefaultBook(
				"book of weapons",
				500,
				loc, addToGame,
				1,
				names,
				generators
			);
		}

		public static Entity CreateBookOfArmor (GridLocation loc, bool addToGame) {
			string[] names = new string[] {
					"Helmet",
					"CloseHelmet",
					"LongShield",
					"RoundShield",
					"Shoes",
					"Boots",
					"LeatherArmor",
					"ChainMail",
					"PlateMail"
				};
			string[] generators = new List<string>(names).Select( (n) => FactoryUtil.NameToGenerator(n) ).ToArray();
			return CreateDefaultBook(
				"book of armor",
				500,
				loc, addToGame,
				1,
				names,
				generators
			);
		}
		
		public static Entity CreateBookOfTools (GridLocation loc, bool addToGame) {
			string[] names = new string[] {
					"Key",
					"TinOpener",
					"TinningKit",
					"Flute",
					"BearTrap",
					"BoobyTrap",
					"GrapplingHook",
					"Candle",
					"OilLamp",
					"OilLatern",
					"Torch",
					"OilCan",
					"Bag",
					"Box",
					"Chest",
					"Trolley"
				};
			string[] generators = new List<string>(names).Select( (n) => FactoryUtil.NameToGenerator(n) ).ToArray();
			return CreateDefaultBook(
				"book of tools",
				500,
				loc, addToGame,
				1,
				names,
				generators
			);
		}
		
		public static Entity CreateDefaultBook (string nameIdentified, int price, GridLocation loc, bool addToGame, int correlated, string[] names, string[] generators) {
			string method = FactoryUtil.GetMethodName (2);
			string name = Names [method];

			Entity entity = new Entity (true);
			entity.Attach (new NamePart (name, nameIdentified));
			entity.Attach (new LocationPart (new GridLocation (loc), false));
			entity.Attach (new VisualPart ("Items/Item", FactoryUtil.GetTexturePath ("Books", name), false, true));

			entity.Attach (new PickablePart (5000, price, TradeClass.Books));
			entity.Attach (new StackablePart ( FactoryUtil.MethodToGenerator(FactoryUtil.GetMethodName (2)), 1, 100 ));
			entity.Attach (new EquipmentPart (Equip.HandPrim, LauncherType.None, Skills.None, 0, 0, 0, 10, 3, AttackTypes.Impact, 0, new Armor(0, 0, 0) ));

			entity.Attach (new BookPart (names, generators, correlated));

			if (addToGame) {
				GameData.EventManager.Notify (new CreateEntityEvent (entity));
			}
			return entity;
		}


		public static void PopulateNames (string[] factories, string[] names) {
			Names.Clear ();
			for (int i = 0; i < factories.Length; i++) {
				Names[factories[i]] = names[i];
			}
		}

		static BookFactory () {
			if (Names.Count > 0) {
				return;
			}
			List<string> names = new List<string> ();
			foreach (MethodInfo method in typeof(BookFactory).GetMethods()) {
				ParameterInfo[] par = method.GetParameters ();
				if (method.IsStatic
				    && method.IsPublic
				    && method.ReturnType == typeof(Entity)
				    && par.Length == 2
				    && par [0].ParameterType == typeof(GridLocation)
				    && par [1].ParameterType == typeof(bool)) {

					names.Add (method.Name);
				}
			}
			if (names.Count > NameList.Length) {
				throw new ArgumentException ("Not enough book names!");
			}
			names = RandUtils.Shuffle (names);
			List<string> nameList = RandUtils.Shuffle (NameList);
			for (int i = 0; i < names.Count; i++) {
				Names[names[i]] = nameList[i];
			}
		}
	}
}
