﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;
using Game.Events;

namespace Game.EntityEffects {

	[DataContract]
	[KnownType(typeof(SwitchOffEffect))]
	public class SwitchOffEffect : EntityEffect {
		
		public override string Name{ get { return "switch off"; } }
		public override string DisplayValue{ get { return ""; } }
		public override string Message{ get { return "Something switched off."; } }
		
		public SwitchOffEffect() {
		}
		
		public override bool Execute (Entity entity) {
			if ( entity.HasPart(OnOffPart.partId) ) {
				OnOffPart part = entity.GetPart<OnOffPart>(OnOffPart.partId);
				if( part.IsOn ) {
					part.IsOn = false;
					return true;
				}
			}
			return false;
		}

	}
}
