﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Game.Parts;

namespace Game.EntityEffects {

	[DataContract]
	public abstract class EntityEffect {
		
		public abstract string Name{ get; }
		public abstract string DisplayValue{ get; }
		public abstract string Message{ get; }

		int lastExecuted = int.MinValue;

		[DataMember]
		public int LastExecuted {
			get {return lastExecuted;}
			set {lastExecuted = value;}
		}

		public abstract bool Execute(Entity entity);

	}
}
