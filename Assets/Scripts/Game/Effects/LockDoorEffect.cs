﻿using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Events;

namespace Game.Effects {
	public class LockDoorEffect : Effect {

		Entity entity;
		GridLocation target;

		public LockDoorEffect(Entity entity, GridLocation target) {
			this.entity = entity;
			this.target = target;
		}

		public Action Execute(out GridLocation requiresUpdate) {
			requiresUpdate = null;
			TileInfo info = GameData.Map [target];
			TileTypeInfo tInfo = Terra.TileInfo [info.Type];
			if (tInfo.Id == Terra.DoorClosed) {
				GameData.Map [target] = new TileInfo (Terra.TileInfoId (Terra.DoorLocked));
				GameData.EventManager.Notify (new MapChangedEvent (target));
				GameData.EventManager.Notify (new UpdateVisibilityEvent (target));
				GameData.EventManager.Notify (new MessageEvent (entity, "You lock the door."));
			}

			return null;
		}
	}
}
