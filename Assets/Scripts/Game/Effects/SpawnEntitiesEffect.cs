﻿using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Parts;
using Game.Events;
using Game.Destructables;
using Game.Factories;

namespace Game.Effects {
	public class SpawnEntitiesEffect : Effect {

		string entityType;
		int entityCount;
		GridLocation location;
		long carrierEntity;

		public SpawnEntitiesEffect(string entityType, int entityCount, GridLocation location, long carrierEntity) {
			this.entityType = entityType;
			this.entityCount = entityCount;
			this.location = location;
			this.carrierEntity = carrierEntity;
		}

		public Action Execute (out GridLocation requiresUpdate) {
			requiresUpdate = null;
			System.Func<GridLocation, bool, Entity> creator = EntityFactory.GetCreator (entityType);

			Entity carrier = carrierEntity < 0 ? null : GameData.TurnManager.GetEntity (carrierEntity);
			InventoryPart inv = (carrier != null && carrier.HasPart(InventoryPart.partId))
				? carrier.GetPart<InventoryPart> (InventoryPart.partId)
				: null;

			for (int i = 0; i < entityCount; i++) {
				Entity e = creator (location, false);
				if (inv != null && e.HasPart (PickablePart.partId)) {
					PickablePart pick = e.GetPart<PickablePart> (PickablePart.partId);
					if (inv.CanAdd (pick)) {
						inv.AddItem(pick);
						continue;
					}
				}
				GameData.EventManager.Notify( new CreateEntityEvent(e) );
			}
			return null;
		}
	}
}
