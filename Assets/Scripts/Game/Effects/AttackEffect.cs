﻿using TurnSystem;
using Game.Parts;
using Game.Events;
using Game.Actions.Actor;
using Map.LayeredGrid;

namespace Game.Effects {
	public class AttackEffect : Effect {

		ActorPart actor;
		ActorPart target;

		public AttackEffect(ActorPart actor, ActorPart target) {
			this.actor = actor;
			this.target = target;
		}

		public Action Execute(out GridLocation requiresUpdate) {
			requiresUpdate = null;
			return new AttackAction(actor, target, GameData.IsPlayer(actor));
		}
	}
}
