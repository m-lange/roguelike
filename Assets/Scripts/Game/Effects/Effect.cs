﻿using TurnSystem;
using Map.LayeredGrid;


namespace Game.Effects {
	public interface Effect {

		Action Execute(out GridLocation requiresUpdate);

	}
}
