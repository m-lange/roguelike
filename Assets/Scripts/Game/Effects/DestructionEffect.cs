﻿using ComponentSystem;
using TurnSystem;
using Game.Parts;
using Game.Events;
using Game.Destructables;
using Game.Util;
using Map.LayeredGrid;

namespace Game.Effects {
	public class DestructionEffect : Effect {

		Entity destroyer;
		DestructablePart target;
		int damage;

		public DestructionEffect(Entity destroyer, DestructablePart target, int damage) {
			this.destroyer = destroyer;
			this.target = target;
			this.damage = damage;
		}

		public Action Execute(out GridLocation requiresUpdate) {
			requiresUpdate = null;
			string name = TextUtil.GetName (target);
			bool destroyed = target.Entity.HasPart<HealthPointsPart>() 
				? target.Entity.GetPart<HealthPointsPart>().DealDamage( damage )
				: true;
			Action action = null;
			if (destroyed) {
				GameData.EventManager.Notify (new RemoveEntityEvent (target.Entity, true));
				GameData.EventManager.Notify (new DestroyEntityEvent (target.Entity, destroyer));
				GameData.EventManager.Notify (new MessageEvent (null, "You destroy the "+name));
				if (target.Entity.HasPart<LocationPart> ()) {
					//requiresUpdate = target.Entity.GetPart<LocationPart> ().Location;
					GameData.EventManager.Notify (new UpdateVisibilityDelayedEvent (target.Entity.GetPart<LocationPart> ().Location));
				}
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, "You hit the "+name));
			}
			return action;
		}
	}
}
