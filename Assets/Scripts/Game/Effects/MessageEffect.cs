﻿using TurnSystem;
using Game.Parts;
using Game.Events;
using Game.Actions.Actor;
using Map.LayeredGrid;

namespace Game.Effects {
	public class MessageEffect : Effect {

		ActorPart actor;
		string message;

		public MessageEffect(ActorPart actor, string message) {
			this.actor = actor;
			this.message = message;
		}

		public Action Execute(out GridLocation requiresUpdate) {
			requiresUpdate = null;
			GameData.EventManager.Notify (new MessageEvent (actor.Entity, message));
			return null;
		}
	}
}
