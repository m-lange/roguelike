﻿using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Data;
using Game.Parts;
using Game.Events;

namespace Game.Effects {
	public class DestroyDoorEffect : Effect {

		Entity entity;
		GridLocation target;
		float probability;

		public DestroyDoorEffect(Entity entity, GridLocation target, float probability) {
			this.entity = entity;
			this.target = target;
			this.probability = probability;
		}

		public Action Execute(out GridLocation requiresUpdate) {
			requiresUpdate = null;
			if (RandUtils.NextFloat () < probability) {
				GameData.Map.Set(target, new TileInfo (Terra.TileInfoId (Terra.FloorWood)) );
				GameData.EventManager.Notify (new MapChangedEvent (target));
				GameData.EventManager.Notify (new UpdateVisibilityEvent (target));
				GameData.EventManager.Notify (new MessageEvent (entity, "You destroy the door."));
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, "You hit the door."));
			}
			return null;
		}
	}
}
