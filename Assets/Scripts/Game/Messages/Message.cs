﻿using ComponentSystem;
using Map.LayeredGrid;
using Game.Parts;

namespace Game.Messages {
	public abstract class Message {

		Entity entity; 
		float radius; 
		bool requireLoS;
		GridLocation location; 


		private Message () { }

		protected Message ( Entity entity, float radius, bool requireLoS, GridLocation location ) {
			this.entity = entity;
			this.radius = radius;
			this.requireLoS = requireLoS;
			this.location = location;
		}

		public abstract bool IsRelevant(ActorPart actor);

		public Entity Entity {
			get { return entity; }
		}
		public float Radius {
			get { return radius; }
		}

		public bool RequireLoS {
			get { return requireLoS; }
		}

		public GridLocation Location {
			get { return location; }
		}
	}
}
