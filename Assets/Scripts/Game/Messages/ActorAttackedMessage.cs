﻿using ComponentSystem;
using Map.LayeredGrid;
using Game.Parts;

namespace Game.Messages {
	public class ActorAttackedMessage : Message {

		ActorPart attacker;
		ActorPart defender;

		public ActorAttackedMessage ( Entity entity, float radius, bool requireLoS, GridLocation location, ActorPart attacker, ActorPart defender )
		: base(entity, radius, requireLoS, location) {
			this.attacker = attacker;
			this.defender = defender;
			//UnityEngine.Debug.Log("MESSAGE");
		}

		public ActorPart Attacker {
			get { return attacker; }
		}
		public ActorPart Defender {
			get { return defender; }
		}

		public override bool IsRelevant(ActorPart actor) {
			return ( GameData.IsPlayer(Attacker) && GameData.Factions.IsFriendly (actor.Faction, Defender.Faction))
					|| (GameData.IsPlayer(Defender) && GameData.Factions.IsFriendly (actor.Faction, Attacker.Faction));
			
		}
	}
}
