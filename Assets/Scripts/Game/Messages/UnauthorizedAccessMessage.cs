﻿using ComponentSystem;
using Map.LayeredGrid;
using Game.Parts;

namespace Game.Messages {
	public class UnauthorizedAccessMessage : Message {


		public UnauthorizedAccessMessage ( Entity entity, float radius, bool requireLoS, GridLocation location )
			: base(entity, radius, requireLoS, location) {

		}


		public override bool IsRelevant(ActorPart actor) {
			return GameData.IsPlayer(Entity);
		}
	}
}
