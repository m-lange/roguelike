﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(ModifyConstitutionEffect))]
	public class ModifyConstitutionEffect : ActorEffect {
		
		static readonly string gainName = "gain constitution";
		static readonly string loseName = "lose constitution";
		static readonly string gainMessage = "You feel good.";
		static readonly string loseMessage = "You feel bad.";
		
		public override string Name{ get { return value < 0 ? loseName : gainName; } }
		public override string DisplayValue{ get { return System.Math.Abs(value).ToString(); } }
		public override string Message{ get { return value < 0 ? loseMessage : gainMessage; } }

		int value;

		public ModifyConstitutionEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}


		public override bool Execute(ActorPart actor) {
			actor.Stats.BaseConstitution += value;
			return true;
		}

	}
}
