﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;
using Game.Events;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(PoisonEffect))]
	public class PoisonEffect : ActorEffect {
		
		public override string Name{ get { return "poison"; } }
		public override string DisplayValue{ get { return value.ToString(); } }
		public override string Message{ get { return "You are poisoned."; } }

		int value;

		public PoisonEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}

		public override bool Execute (ActorPart actor) {
			if ((!actor.HasEffect<PoisonResistanceEffect> ()) && actor.Entity.HasPart (HealthPointsPart.partId)) {
				bool destroyed = actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).DealDamage (value);
				if (GameData.IsPlayer (actor)) {
					Sounds.Play (Sounds.Vomit);
				} else {
					if (destroyed) {
						GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
						GameData.EventManager.Notify (new DestroyEntityEvent (actor.Entity, null));
					}
				}
				return true;
			}
			return false;
		}

	}
}
