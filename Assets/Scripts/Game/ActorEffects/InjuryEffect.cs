﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Events;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(InjuryEffect))]
	public class InjuryEffect : ActorEffect {
		
		public override string Name{ get { return "injury"; } }
		public override string DisplayValue{ get { return value.ToString(); } }
		public override string Message{ get { return "You are injured."; } }

		int value;

		public InjuryEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}


		public override bool Execute(ActorPart actor) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				bool destroyed = actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).DealDamage (value);
				if ( destroyed && ! GameData.IsPlayer (actor) ) {
					GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
					GameData.EventManager.Notify (new DestroyEntityEvent (actor.Entity, null));
				}
			}
			return true;
		}

	}
}
