﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(TurnToFleeEffect))]
	public class TurnToFleeEffect : ActorEffect {
		
		public override string Name{ get { return "flee"; } }
		public override string DisplayValue{ get { return "true"; } }
		public override string Message{ get { return "It flees."; } }

		public TurnToFleeEffect() {
		}

		public override bool Execute (ActorPart actor) {
			actor.AgressiveAgainstPlayer = -1;
			if (actor.ActorAi != null) {
				actor.SetNextAction(null);
			}
			return true;
		}

	}
}
