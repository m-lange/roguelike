﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;
using Game.Rules;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(StunEffect))]
	public class StunEffect : ActorEffect {
		
		public override string Name{ get { return "stun"; } }
		public override string DisplayValue{ get { return turns.ToString()+"T"; } }
		public override string Message{ get { return "You are stunned."; } }

		int turns;
		int reloadTurns;

		public StunEffect(int turns, int reloadTurns) {
			this.turns = turns;
			this.reloadTurns = reloadTurns;
		}

		[DataMember]
		public int Turns {
			get { return turns; }
			set { this.turns = value; }
		}
		[DataMember]
		public int ReloadTurns {
			get { return reloadTurns; }
			set { this.reloadTurns = value; }
		}


		public override bool Execute (ActorPart actor) {
			int t = GameData.TurnManager.TurnCounter;
			if (reloadTurns <= 0 || (LastExecuted + reloadTurns <= t)) {
				actor.UseActionPoints (turns * actor.Speed); 
				LastExecuted = t;
				return true;
			}
			return false;
		}

	}
}
