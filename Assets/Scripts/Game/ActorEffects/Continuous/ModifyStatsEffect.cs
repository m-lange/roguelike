﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.Parts.Sub;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	[KnownType(typeof(ModifyStatsEffect))]
	public class ModifyStatsEffect : ContinuousEffect {

		public override string Name{ 
			get { return name; } 
		}

		string name;
		int speedBonus;
		int strengthBonus;
		int dexterityBonus;
		int defenseBonus;
		Armor armorBonus;
		int attackBonus;

		public ModifyStatsEffect(string name, int speedBonus, int strengthBonus, int dexterityBonus, int attackBonus, int defenseBonus, Armor armorBonus, int duration) : base(duration) {
			this.name = name;
			this.speedBonus = speedBonus;
			this.strengthBonus = strengthBonus;
			this.dexterityBonus = dexterityBonus;
			this.attackBonus = attackBonus;
			this.defenseBonus = defenseBonus;
			this.armorBonus = armorBonus;
		}


		[DataMember]
		public string NameForSerialization{ 
			get { return name; } 
			set { name = value; } 
		}
		[DataMember]
		public int SpeedBonus {
			get { return speedBonus; }
			set { speedBonus = value; }
		}

		[DataMember]
		public int StrengthBonus {
			get { return strengthBonus; }
			set { strengthBonus = value; }
		}

		[DataMember]
		public int DexterityBonus {
			get { return dexterityBonus; }
			set { dexterityBonus = value; }
		}

		[DataMember]
		public int DefenseBonus {
			get { return defenseBonus; }
			set { defenseBonus = value; }
		}

		[DataMember]
		public Armor ArmorBonus {
			get { return armorBonus; }
			set { armorBonus = value; }
		}

		[DataMember]
		public int AttackBonus {
			get { return attackBonus; }
			set { attackBonus = value; }
		}


		public override void OnUpdate (ActorPart actor) {
		}
		public override void OnAttach (ActorPart actor) {
		}
		public override void OnDetach (ActorPart actor) {
		}

	}
}
