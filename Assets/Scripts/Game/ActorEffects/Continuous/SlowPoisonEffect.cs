﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Events;
using Game.Parts;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	[KnownType(typeof(SlowPoisonEffect))]
	public class SlowPoisonEffect : ContinuousEffect {

		public override string Name{ get { return "poisoned"; } }

		int value;

		public SlowPoisonEffect(int value, int duration) : base(duration) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}

		public override void OnUpdate (ActorPart actor) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				bool destroyed = actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).DealDamage (value);
				if ( destroyed && ! GameData.IsPlayer (actor) ) {
					GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
					GameData.EventManager.Notify (new DestroyEntityEvent (actor.Entity, null));
				}
			}
		}
		public override void OnAttach (ActorPart actor) {
		}
		public override void OnDetach (ActorPart actor) {
		}

	}
}
