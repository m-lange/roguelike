﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.Util;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	[KnownType(typeof(StickToEntityEffect))]
	public class StickToEntityEffect : ContinuousEffect {

		public override string Name{ get { return "immobilized by "+EntityName; } }

		long entityId;
		string entityName;

		public StickToEntityEffect(long stickTo) : base(int.MaxValue) {
			this.entityId = stickTo;
			entityName = null;
		}

		[DataMember]
		public long EntityId {
			get { return entityId; }
			set { this.entityId = value; }
		}

		[DataMember]
		public string EntityName {
			get { 
				if( entityName == null ) {
					if( GameData.TurnManager.EntityIds.ContainsKey (entityId) ) {
						Entity e = GameData.TurnManager.EntityIds[entityId];
						entityName = TextUtil.GetNameIndef(e, false);
					} else {
						entityName = "something";
					}
				}
				return entityName; 
			}
			set { this.entityName = value; }
		}
		
		public override void OnUpdate (ActorPart actor) {
			if (!GameData.TurnManager.EntityIds.ContainsKey (entityId)) {
				Duration = 0;
			} else {
				Entity e = GameData.TurnManager.EntityIds[entityId];
				if( e.HasPart(OnOffPart.partId) && ! e.GetPart<OnOffPart>(OnOffPart.partId).IsOn ) {
					Duration = 0;
				}
			}
		}
		public override void OnAttach (ActorPart actor) {
		}
		public override void OnDetach (ActorPart actor) {
		}

	}
}
