﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	[KnownType(typeof(PoisonResistanceEffect))]
	public class PoisonResistanceEffect : ContinuousEffect {

		public override string Name{ get { return "poison resistance"; } }


		public PoisonResistanceEffect(int duration) : base(duration) {

		}

		public override void OnUpdate (ActorPart actor) {
		}
		public override void OnAttach (ActorPart actor) {
		}
		public override void OnDetach (ActorPart actor) {
		}

	}
}
