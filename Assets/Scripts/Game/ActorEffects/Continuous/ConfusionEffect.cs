﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	[KnownType(typeof(ConfusionEffect))]
	public class ConfusionEffect : ContinuousEffect {

		public override string Name{ get { return "confused"; } }

		int wrongMoveProb;

		public ConfusionEffect(int wrongMoveProb, int duration) : base(duration) {
			this.wrongMoveProb = wrongMoveProb;
		}

		[DataMember]
		public int WrongMoveProb {
			get { return wrongMoveProb; }
			set { this.wrongMoveProb = value; }
		}

		public override void OnUpdate (ActorPart actor) {
		}
		public override void OnAttach (ActorPart actor) {
		}
		public override void OnDetach (ActorPart actor) {
		}

	}
}
