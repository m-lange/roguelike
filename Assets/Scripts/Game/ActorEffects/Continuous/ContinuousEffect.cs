﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects.Continuous {

	[DataContract]
	//[KnownType(typeof(ContinuousEffect))]
	public abstract class ContinuousEffect {

		public abstract string Name{ get; }

		int duration;

		protected ContinuousEffect ( int duration ) {
			this.duration = duration;
		}

		[DataMember]
		public int Duration {
			get { return duration; }
			set { this.duration = value; }
		}

		public abstract void OnUpdate(ActorPart actor);
		public abstract void OnAttach(ActorPart actor);
		public abstract void OnDetach(ActorPart actor);

	}
}
