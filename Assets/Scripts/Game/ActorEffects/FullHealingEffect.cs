﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(FullHealingEffect))]
	public class FullHealingEffect : ActorEffect {
		
		public override string Name{ get { return "full healing"; } }
		public override string DisplayValue{ get { return "100%"; } }
		public override string Message{ get { return "You feel healthy."; } }


		public FullHealingEffect() {
		}

		public override bool Execute(ActorPart actor) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).FullHeal();
			}
			return true;
		}

	}
}
