﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Events;
using Game.Parts;
using Game.Rules;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(MessageEffect))]
	public class MessageEffect : ActorEffect {
		
		public override string Name{ get { return "message"; } }
		public override string DisplayValue{ get { return ""; } }
		public override string Message{ get { return ""; } }
	
		string messageText;
		string sound;
		bool needsVision;
		int soundRange;
		

		public MessageEffect(string messageText, string sound, bool needsVision, int soundRange) {
			this.needsVision = needsVision;
			this.soundRange = soundRange;
			this.messageText = messageText;
			this.sound = sound;
		}

		[DataMember]
		public string MessageText {
			get { return messageText; }
			set { this.messageText = value; }
		}

		[DataMember]
		public string Sound {
			get { return sound; }
			set { this.sound = value; }
		}

		[DataMember]
		public int SoundRange {
			get { return soundRange; }
			set { this.soundRange = value; }
		}
		
		[DataMember]
		public bool NeedsVision {
			get { return needsVision; }
			set { this.needsVision = value; }
		}


		public override bool Execute(ActorPart actor) {
			if (actor.Entity.HasPart (LocationPart.partId)) {
				LocationPart loc = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				LocationPart playerLoc = GameData.Player.GetPart<LocationPart> (LocationPart.partId);
				
				if( needsVision ) {
					if( GameData.Visibility.Visible.IsTileVisible(loc.Location) ) {
						SendMessage();
					}
				} else {
					if( loc.Location.Distance3d(playerLoc.Location) <= soundRange || GameData.Visibility.Visible.IsTileVisible(loc.Location) ) {
						SendMessage();
					}
				}
			}
			return true;
		}
		
		void SendMessage() {
			if(messageText != null) GameData.EventManager.Notify (new MessageEvent (null, messageText));
			if(sound != null) Sounds.Play (sound);
		}

	}
}
