﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(TurnAgressiveOrFleeEffect))]
	public class TurnAgressiveOrFleeEffect : ActorEffect {
		
		public override string Name{ get { return "agressive or flee"; } }
		public override string DisplayValue{ get { return "true"; } }
		public override string Message{ get { return "It becomes agressive."; } }

		float fleeProb;
		
		public TurnAgressiveOrFleeEffect(float fleeProb) {
			this.fleeProb = fleeProb;
		}

		public override bool Execute (ActorPart actor) {
			if( RandUtils.NextBool(fleeProb) ) {
				actor.AgressiveAgainstPlayer = -1;
			} else {
				actor.AgressiveAgainstPlayer = 1;
			}
			if (actor.ActorAi != null) {
				actor.SetNextAction(null);
			}
			return true;
		}

	}
}
