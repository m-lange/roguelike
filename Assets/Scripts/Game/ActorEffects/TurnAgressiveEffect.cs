﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(TurnAgressiveEffect))]
	public class TurnAgressiveEffect : ActorEffect {
		
		public override string Name{ get { return "agressive"; } }
		public override string DisplayValue{ get { return "true"; } }
		public override string Message{ get { return "It becomes agressive."; } }

		public TurnAgressiveEffect() {
		}

		public override bool Execute (ActorPart actor) {
			actor.AgressiveAgainstPlayer = 1;
			if (actor.ActorAi != null) {
				actor.SetNextAction(null);
			}
			return true;
		}

	}
}
