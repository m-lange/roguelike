﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(HealingEffect))]
	public class HealingEffect : ActorEffect {
		
		public override string Name{ get { return "healing"; } }
		public override string DisplayValue{ get { return value.ToString(); } }
		public override string Message{ get { return "You feel better."; } }

		int value;

		public HealingEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}


		public override bool Execute(ActorPart actor) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).Heal(value);
			}
			return true;
		}

	}
}
