﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AttachConfusionEffect))]
	public class AttachConfusionEffect : ActorEffect {
		
		public override string Name{ get { return "confusion"; } }
		public override string DisplayValue{ get { return wrongMoveProb.ToString()+"% x "+turns.ToString()+"T"; } }
		public override string Message{ get { return "You are confused."; } }

		int wrongMoveProb;
		int turns;

		public AttachConfusionEffect(int wrongMoveProb, int turns) {
			this.wrongMoveProb = wrongMoveProb;
			this.turns = turns;
		}

		[DataMember]
		public int WrongMoveProb {
			get { return wrongMoveProb; }
			set { this.wrongMoveProb = value; }
		}
		[DataMember]
		public int Turns {
			get { return turns; }
			set { this.turns = value; }
		}

		public override bool Execute (ActorPart actor) {
			actor.AttachEffect( new ConfusionEffect(wrongMoveProb, turns) );
			return true;
		}

	}
}
