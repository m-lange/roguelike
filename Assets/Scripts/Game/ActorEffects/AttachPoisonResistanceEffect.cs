﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AttachPoisonResistanceEffect))]
	public class AttachPoisonResistanceEffect : ActorEffect {
		
		public override string Name{ get { return "poison resistance"; } }
		public override string DisplayValue{ get { return turns.ToString()+"T"; } }
		public override string Message{ get { return null; } }

		int turns;

		public AttachPoisonResistanceEffect(int turns) {
			this.turns = turns;
		}

		[DataMember]
		public int Turns {
			get { return turns; }
			set { this.turns = value; }
		}

		public override bool Execute (ActorPart actor) {
			actor.AttachEffect( new PoisonResistanceEffect(turns) );
			return true;
		}

	}
}
