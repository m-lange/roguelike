﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AntiPoisonEffect))]
	public class AntiPoisonEffect : ActorEffect {
		
		public override string Name{ get { return "anti-poison"; } }
		public override string DisplayValue{ get { return ""; } }
		public override string Message{ get { return null; } }

		public AntiPoisonEffect() {
		}

		public override bool Execute (ActorPart actor) {
			SlowPoisonEffect eff = actor.GetEffect<SlowPoisonEffect>();
			if (eff != null) {
				actor.DetachEffect(eff);
				return true;
			}
			return false;
		}

	}
}
