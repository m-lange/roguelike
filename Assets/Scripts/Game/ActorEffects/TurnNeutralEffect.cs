﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.AI;
using Game.Parts;
using Game.Data;
using Game.Factories;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(TurnNeutralEffect))]
	public class TurnNeutralEffect : ActorEffect {
		
		public override string Name{ get { return "neutral"; } }
		public override string DisplayValue{ get { return "true"; } }
		public override string Message{ get { return "It becomes neutral."; } }

		ActorAI ai;

		public TurnNeutralEffect(ActorAI ai) {
			this.ai = ai;
		}

		[DataMember]
		public ActorAI Ai {
			get { return ai; }
			set { ai = value; }
		}

		public override bool Execute (ActorPart actor) {
			if (actor.AgressiveAgainstPlayer < 1) {
				actor.Faction = GameData.Factions.Get (FactionFactory.Neutral).id;
				if (actor.ActorAi != null && ai != null) {
					actor.ActorAi = ai;
				}
			}
			return true;
		}

	}
}
