﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;
using Game.ActorEffects.Continuous;
using Game.Parts.Sub;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AttachModifyStatsEffect))]
	public class AttachModifyStatsEffect : ActorEffect {

		public override string Name{ get { return name; } }
		public override string DisplayValue{ get { return value; } }
		public override string Message{ get { return message; } }

		string name;
		string message;
		string value;

		int speedBonus;
		int strengthBonus;
		int dexterityBonus;
		int defenseBonus;
		Armor armorBonus;
		int attackBonus;
		int duration;

		public AttachModifyStatsEffect(string name, string message, string value, int speedBonus, int strengthBonus, int dexterityBonus, int attackBonus, int defenseBonus, Armor armorBonus, int duration) {
			this.name = name;
			this.message = message;
			this.value = value;
			this.speedBonus = speedBonus;
			this.strengthBonus = strengthBonus;
			this.dexterityBonus = dexterityBonus;
			this.attackBonus = attackBonus;
			this.defenseBonus = defenseBonus;
			this.armorBonus = armorBonus;
			this.duration = duration;
		}


		public override bool Execute(ActorPart actor) {
			actor.AttachEffect( new ModifyStatsEffect(name, speedBonus, strengthBonus, dexterityBonus, attackBonus, defenseBonus, armorBonus, duration) );
			return true;
		}



		[DataMember]
		public string NameForSerialization{ 
			get { return name; } 
			set { name = value; } 
		}
		[DataMember]
		public string MessageForSerialization{ 
			get { return message; } 
			set { message = value; } 
		}
		[DataMember]
		public string ValueForSerialization{ 
			get { return value; } 
			set { this.value = value; } 
		}
		[DataMember]
		public int SpeedBonus {
			get { return speedBonus; }
			set { speedBonus = value; }
		}

		[DataMember]
		public int StrengthBonus {
			get { return strengthBonus; }
			set { strengthBonus = value; }
		}

		[DataMember]
		public int DexterityBonus {
			get { return dexterityBonus; }
			set { dexterityBonus = value; }
		}

		[DataMember]
		public int DefenseBonus {
			get { return defenseBonus; }
			set { defenseBonus = value; }
		}

		[DataMember]
		public Armor ArmorBonus {
			get { return armorBonus; }
			set { armorBonus = value; }
		}

		[DataMember]
		public int AttackBonus {
			get { return attackBonus; }
			set { attackBonus = value; }
		}

		[DataMember]
		public int Duration {
			get { return duration; }
			set { this.duration = value; }
		}

	}
}
