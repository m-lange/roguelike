﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(ExtraHealingEffect))]
	public class ExtraHealingEffect : ActorEffect {
		
		public override string Name{ get { return "extra healing"; } }
		public override string DisplayValue{ get { return value.ToString()+"+1"; } }
		public override string Message{ get { return "You feel much better."; } }

		int value;


		public ExtraHealingEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}


		public override bool Execute(ActorPart actor) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).ExtraHeal(value);
			}
			return true;
		}

	}
}
