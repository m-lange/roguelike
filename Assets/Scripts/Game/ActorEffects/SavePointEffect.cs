﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(SavePointEffect))]
	public class SavePointEffect : ActorEffect {
		
		public override string Name{ get { return "reincarnation"; } }
		public override string DisplayValue{ get { return ""; } }
		public override string Message{ get { return "You feel eternity."; } }

		public SavePointEffect() {
		}

		public override bool Execute(ActorPart actor) {
			GameData.SaveGame(true);
			return true;
		}

	}
}
