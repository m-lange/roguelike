﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AttachSlowPoisonEffect))]
	public class AttachSlowPoisonEffect : ActorEffect {
		
		public override string Name{ get { return "slow poison"; } }
		public override string DisplayValue{ get { return value.ToString()+" x "+turns.ToString()+"T"; } }
		public override string Message{ get { return "You are poisoned."; } }

		int value;
		int turns;
		int reloadTurns;

		public AttachSlowPoisonEffect(int value, int turns, int reloadTurns) {
			this.value = value;
			this.turns = turns;
			this.reloadTurns = reloadTurns;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}
		[DataMember]
		public int Turns {
			get { return turns; }
			set { this.turns = value; }
		}
		[DataMember]
		public int ReloadTurns {
			get { return reloadTurns; }
			set { this.reloadTurns = value; }
		}

		public override bool Execute (ActorPart actor) {
			int t = GameData.TurnManager.TurnCounter;
			if (reloadTurns <= 0 || (LastExecuted + reloadTurns <= t)) {
				LastExecuted = t;
				if (!actor.HasEffect<PoisonResistanceEffect> ()) {
					actor.AttachEffect (new SlowPoisonEffect (value, turns));
					return true;
				}
			}
			return false;
		}

	}
}
