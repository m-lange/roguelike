﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(ModifyStrengthEffect))]
	public class ModifyStrengthEffect : ActorEffect {
		static readonly string gainName = "gain strength";
		static readonly string loseName = "lose strength";
		static readonly string gainMessage = "You feel strong.";
		static readonly string loseMessage = "You feel weak.";
		
		public override string Name{ get { return value < 0 ? loseName : gainName; } }
		public override string DisplayValue{ get { return System.Math.Abs(value).ToString(); } }
		public override string Message{ get { return value < 0 ? loseMessage : gainMessage; } }

		int value;

		public ModifyStrengthEffect(int value) {
			this.value = value;
		}

		[DataMember]
		public int Value {
			get { return value; }
			set { this.value = value; }
		}


		public override bool Execute(ActorPart actor) {
			actor.Stats.BaseStrength += value;
			return true;
		}

	}
}
