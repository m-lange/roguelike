﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Game.Parts;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(NullEffect))]
	public class NullEffect : ActorEffect {
		
		public override string Name{ get { return "null"; } }
		public override string DisplayValue{ get { return "-"; } }
		public override string Message{ get { return "Nothing happens."; } }


		public NullEffect() {
		}


		public override bool Execute(ActorPart actor) {

			return true;
		}

	}
}
