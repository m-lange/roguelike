﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.ActorEffects.Continuous;
using Game.Util;

namespace Game.ActorEffects {

	[DataContract]
	[KnownType(typeof(AttachStickToEntityEffect))]
	public class AttachStickToEntityEffect : ActorEffect {
		
		public override string Name{ get { return "immobilization"; } }
		public override string DisplayValue{ get { return ""; } }
		public override string Message{ get { return "You are immobilized by "+EntityName+"."; } }


		long entityId;
		int reloadTurns;
		string entityName;

		public AttachStickToEntityEffect(Entity stickTo, int reloadTurns) {
			this.entityId = stickTo.Id;
			this.reloadTurns = reloadTurns;
			entityName = null;
		}

		[DataMember]
		public long EntityId {
			get { return entityId; }
			set { this.entityId = value; }
		}
		[DataMember]
		public int ReloadTurns {
			get { return reloadTurns; }
			set { this.reloadTurns = value; }
		}
		[DataMember]
		public string EntityName {
			get { 
				if( entityName == null ) {
					if( GameData.TurnManager.EntityIds.ContainsKey (entityId) ) {
						Entity e = GameData.TurnManager.EntityIds[entityId];
						entityName = TextUtil.GetNameIndef(e, false);
					} else {
						entityName = "something";
					}
				}
				return entityName; 
			}
			set { this.entityName = value; }
		}


		public override bool Execute (ActorPart actor) {
			int t = GameData.TurnManager.TurnCounter;
			if (reloadTurns <= 0 || (LastExecuted + reloadTurns <= t)) {
				actor.AttachEffect( new StickToEntityEffect( entityId ) );
				LastExecuted = t;
				return true;
			}
			return false;
		}

	}
}
