﻿using System.Collections.Generic;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Game.Data;
using Collections;
using UnityEngine;

namespace Game.Actions.Actor {
	public class InspectNearbyAction : Action {

		/*static readonly string help = 
			"Cancel with Esc.";*/

		ActorPart actor;
		GridLocation location;
		InventoryPart inventory;
		bool confirmed = false;
		OrderedDict<string, Entity> inspectables;
		Entity inspect;

		public InspectNearbyAction(ActorPart actor, GridLocation location) {
			this.actor = actor;
			this.location = location;
			if(location != null) {
				confirmed = true;
			}
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			
			if (location != null && confirmed) {
				if(inspectables != null) {
					if(inspect != null) {
						GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, Inspect(inspect, inventory), inspectables, null) );
						inspect = null;
						return ActionResult.PENDING;
					} else {
						GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to inspect?", inspectables, null) );
						return ActionResult.PENDING;
					}
				} else {
					if ((!GameData.Visibility.Visible.Contains (location)) 
							|| !GameData.Visibility.Visible.IsCenterVisible (location)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't see there!"));
						GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
						GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
						return ActionResult.FAILURE;
					}
					inspectables = new OrderedDict<string, Entity> ();
					int i = 0;
					foreach (NamePart item in GameData.SpatialManager.GetAt<NamePart> (location)) {
						if (i >= GlobalParameters.letters.Length) {
							break;
						}
						char c = GlobalParameters.letters [i];
						inspectables [c.ToString ()] = item.Entity;
						i++;
					}
					if(inspectables.Count == 1) {
						inspect = inspectables[0];
					} else if(inspectables.Count == 0) {
						GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing to inspect there. Inspect where?") );
						inspectables = null;
						inspect = null;
						confirmed = false;
						return ActionResult.PENDING;
					}
					if(inspect != null) {
						GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, Inspect(inspect, inventory), inspectables, null) );
						inspect = null;
						if(inspectables.Count == 1) {
							GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
							return ActionResult.SUCCESS;
						} else {
							return ActionResult.PENDING;
						}
					} else {
						GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to inspect?", inspectables, null) );
						return ActionResult.PENDING;
					}
				}
			} 
			
			if (location == null) {
				if (!actor.Entity.HasPart<LocationPart> ()) {
					return ActionResult.FAILURE;
				}
				LocationPart loc = actor.Entity.GetPart<LocationPart> ();
				location = loc.Location;
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Inspect where? (directions/mouse/?)"));
			}
			GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, location));
			return ActionResult.PENDING;
		}

		BookPart GetBook (InventoryPart inventory, string name) {
			return inventory.FilterOne<BookPart>( (b) => b.Contains(name) );
		}

		string Inspect (Entity item, InventoryPart inventory) {
			string name = TextUtil.GetNameSimple (item);
			BookPart book = GetBook (inventory, name);
			if (book != null) {
				foreach (Entity e in book.GetAll(name)) {
					GameData.Identify (e);
					//Debug.Log("Identified "+TextUtil.GetName(e));
				}
			}
			bool isIdentified = book != null || GameData.IsIdentified(item);
			return TextUtil.EntityInfo(actor, item, isIdentified);
		}

		public int GetCost () {
			return Game.Rules.GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if( ! confirmed ) {
				if (Input.GetMouseButtonDown (0)) {
					location = InputUtil.GetMousePosition ( GameData.CameraLocation.Location, Input.mousePosition );
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
	
				Direction direction = InputUtil.HandleDirectionXYInput ();
	
				if (direction != null) {
					location = direction.Transform (location);
					actor.SetNextAction (this);
					return true;
				} else {
					if (location != null && Input.GetKeyDown (KeyCode.Return)) {
						confirmed = true;
						actor.SetNextAction (this);
						return true;
					}
				}
			} else if(inspectables != null) {
				Entity item = InputUtil.HandleSelectionInput (inspectables, null);
				if (item != null) {
					inspect = item;
					actor.SetNextAction (this);
					return true;
				}
			}
			
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				return true;
			}
			return false;
		}

	}
}
