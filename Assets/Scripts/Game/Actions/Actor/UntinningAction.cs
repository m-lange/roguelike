﻿using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.Events;
using Game.Rules;
using Game.Util;
using Game.Factories;
using Map;
using Map.LayeredGrid;
using TurnSystem;

namespace Game.Actions.Actor {
	public class UntinningAction : Action {

		ActorPart actor;
		TinnedPart target;
		Entity tool;

		public UntinningAction(ActorPart actor, TinnedPart target, Entity tool) {
			this.actor = actor;
			this.target = target;
			this.tool = tool;
		}

		public ActionResult Execute () {
			Entity entity = target.Entity;
			entity.Detach (target);

			if (!entity.HasPart (LimitedPresencePart.partId)) {
				LimitedPresencePart lp = new LimitedPresencePart (GameRules.UntinnedLimitedPresence, GameRules.UntinnedLimitedPresenceMessage);
				entity.Attach (lp);
			}
			if (GameData.IsPlayer (actor)) {
				string name = TextUtil.GetName (entity);
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You open the " + name + "."));
				if (name.Contains (TextUtil.Tinned)) {
					name = name.Replace (TextUtil.Tinned, TextUtil.TinnedOpen);
				} else {
					name = "opened "+name;
				}
				entity.GetPart<NamePart> ().Name = name;
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", actor.Entity.GetPart<InventoryPart> (InventoryPart.partId).ToDict()));
				Sounds.Play( Sounds.Untinning );
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.CalcTinningCost(actor);
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
