﻿
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Events;
using Game.Parts;
using Game.Util;
using UnityEngine;

namespace Game.Actions.Actor {
	public class OpenAction : Action {

		ActorPart actor;
		Direction direction;

		public OpenAction( ActorPart actor, Direction direction ) {
			this.actor = actor;
			this.direction = direction;
		}

		public ActionResult Execute () {
			if (direction != null) {

				LocationPart source = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				GridLocation pos = new GridLocation (source.Location.Layer + direction.dl,
					                   source.Location.X + direction.dx,
					                   source.Location.Y + direction.dy);

				TileInfo info = GameData.Map.Get(pos);
				TileTypeInfo typeInfo = Terra.TileInfo [info.Type];

				if (typeInfo.Id == Terra.DoorLocked) {
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "The door is locked!"));
						Sounds.Play (Sounds.DoorLocked);
					}
					return ActionResult.SUCCESS;
				} else if (typeInfo.Id == Terra.WindowLocked) {
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "The window is locked!"));
						Sounds.Play (Sounds.DoorLocked);
					}
					return ActionResult.SUCCESS;
				} else if (typeInfo.Id == Terra.DoorClosed) {
					GameData.Map.Set(pos, new TileInfo (Terra.TileInfoId (Terra.DoorOpen)) );
					GameData.EventManager.Notify (new MapChangedEvent (pos));
					GameData.EventManager.Notify (new UpdateVisibilityEvent (pos));
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You open the door."));
						Sounds.Play (Sounds.DoorOpen);
					}
					return ActionResult.SUCCESS;
				} else if (typeInfo.Id == Terra.WindowClosed) {
					GameData.Map.Set(pos, new TileInfo (Terra.TileInfoId (Terra.WindowOpen)) );
					GameData.EventManager.Notify (new MapChangedEvent (pos));
					GameData.EventManager.Notify (new UpdateVisibilityEvent (pos));
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You open the window."));
						Sounds.Play (Sounds.DoorOpen);
					}
					return ActionResult.SUCCESS;
				}

				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing to open here!") );
				return ActionResult.FAILURE;
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Open in which direction?") );
				return ActionResult.PENDING;
			}
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			direction = InputUtil.HandleDirectionXYInput ();

			if (direction != null) {
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.N) || Input.GetKeyDown (KeyCode.Escape)) {
				return true;
			}
			return false;
		}
	}
}
