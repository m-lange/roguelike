﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class ConsumeAction : Action {

		static readonly string help = 
			"Consume/eat something lying at your feet or from your inventory.\n"+
			"If something consumable lies at your feet, but you want to access your inventory, press Esc.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		GridLocation location;
		Entity consumable;
		OrderedDict<string, Entity> consumables;
		InventoryPart inventory;
		bool consumeHere = true;
		bool isForced = false;

		int cost = Game.Rules.GameRules.DefaultCost;


		public ConsumeAction(ActorPart actor,  ConsumablePart consumable) {
			this.actor = actor;
			this.consumable = consumable == null ? null : consumable.Entity;
			if (actor.Entity.HasPart (InventoryPart.partId)) {
				inventory = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart (LocationPart.partId)) {
				location = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			} else {
				location = null;
			}
		}

		public ActionResult Execute () {
			if (location == null) {
				return ActionResult.FAILURE;
			}
			if (actor.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to eat!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}
			if (consumeHere && consumables == null) {
				consumables = new OrderedDict<string, Entity> ();
				int i = 0;
				foreach (ConsumablePart item in GameData.SpatialManager.GetAt<ConsumablePart> (location)) {
					if (i >= GlobalParameters.letters.Length) {
						break;
					}
					if (!item.Entity.HasPart<TinnedPart> ()) {
						char c = GlobalParameters.letters [i];
						consumables [c.ToString ()] = item.Entity;
						i++;
					}
				}
				if (consumables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("There are consumables. Eat them?")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to eat?", consumables, null));
					return ActionResult.PENDING;
				} else {
					consumeHere = false;
				}
			}

			if (consumables == null && inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to eat!")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.FAILURE;
			}

			if (consumable == null) {
				consumables = inventory.FilterEntities<ConsumablePart> ((e) => ! e.HasPart<TinnedPart>());
				if (consumables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to eat? (a-Z*?)")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to eat?", consumables, (e) => e.HasPart<ConsumablePart> ()));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to eat."));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return ActionResult.FAILURE;
				}
			} else {
				if (consumable.HasPart<ConsumablePart> ()) {
					ConsumablePart cons = consumable.GetPart<ConsumablePart> ();
					if ( ! isForced ) {
						int energy = cons.NutritionValue;
						if (actor.Stats.StomachContent + energy > actor.Stats.MaxStomachContent) {
							GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You may die from oversatiation. Continue eating? (y/n)"));
							return ActionResult.PENDING;
						}
					}
					Consume( cons );
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
					return ActionResult.SUCCESS;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't eat that "+TextUtil.GetName(consumable) ));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
					return ActionResult.FAILURE;
				}
			}
		}

		void Consume (ConsumablePart cons) {
			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You eat the " + TextUtil.GetNameSimple (cons) + "."));
				Sounds.Play (Sounds.Eat);
			}
			cost = GameRules.CalcConsumptionCost(actor, cons);

			if (cons.Entity.HasPart<StackablePart> ()) {
				StackablePart stack = cons.Entity.GetPart<StackablePart> ();
				if (stack.Count > 1) {
					stack.Count -= 1;
					stack.SetContainerDirty();
					GameData.EventManager.Notify (new ConsumeEvent (actor, cons));
					return;
				}
			}
			GameData.EventManager.Notify (new RemoveEntityEvent (cons.Entity, true));
			GameData.EventManager.Notify (new ConsumeEvent (actor, cons));
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}
			if (consumable != null && !isForced) {
				if (Input.GetKeyDown (KeyCode.Y)) {
					isForced = true;
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.N)) {
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return true;
				}
				return false;
			} else {
				Entity dropItem = InputUtil.HandleSelectionInput (consumables, null);
				if (dropItem != null) {
					consumable = dropItem;
					actor.SetNextAction (this);
					return true;
				}
				if (Input.GetKeyDown (KeyCode.Escape)) {
					if (consumeHere) {
						consumeHere = false;
						actor.SetNextAction (this);
					} else {
						GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					}
					return true;
				}
			}
			return false;
		}

	}
}
