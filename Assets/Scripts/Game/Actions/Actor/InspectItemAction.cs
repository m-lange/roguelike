﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Game.Data;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class InspectItemAction : Action {

		static readonly string help = 
			"Cancel with Esc.";

		ActorPart actor;

		GridLocation location;
		PickablePart inspect;
		OrderedDict<string, PickablePart> inspectables;
		InventoryPart inventory;
		bool inspectHere = true;

		public InspectItemAction(ActorPart actor,  PickablePart inspect) {
			this.actor = actor;
			this.inspect = inspect;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart<LocationPart> ()) {
				location = actor.Entity.GetPart<LocationPart> ().Location;
			} else {
				location = null;
			}
		}

		public ActionResult Execute () {
			if (location == null) {
				return ActionResult.FAILURE;
			}
			if (inventory == null) {
				return ActionResult.FAILURE;
			}

			if (inspectHere && inspectables == null) {
				inspectables = new OrderedDict<string, PickablePart> ();
				int i = 0;
				foreach (PickablePart item in GameData.SpatialManager.GetAt<PickablePart> (location)) {
					if (i >= GlobalParameters.letters.Length) {
						break;
					}
					char c = GlobalParameters.letters [i];
					inspectables [c.ToString ()] = item;
					i++;
				}
				if (inspectables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("There is something. Inspect it?")));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to inspect?", inspectables));
					return ActionResult.PENDING;
				} else {
					inspectHere = false;
				}
			}

			if (inspectables == null && inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to inspect!")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.FAILURE;
			}

			if (inspect == null) {
				inspectables = inventory.ToDict();
				if (inspectables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to inspect? (a-Z*?)")));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to inspect?", inspectables));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to inspect."));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return ActionResult.FAILURE;
				}
			} else {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, Inspect(inspect, inventory), inspectables, null, inspect) );
				inspect = null;
				return ActionResult.PENDING;
			}
		}

		BookPart GetBook (InventoryPart inventory, string name) {
			return inventory.FilterOne<BookPart>( (b) => b.Contains(name) );
		}

		string Inspect (PickablePart item, InventoryPart inventory) {
			string name = TextUtil.GetNameSimple (item);
			BookPart book = GetBook (inventory, name);
			if (book != null) {
				foreach (Entity e in book.GetAll(name)) {
					GameData.Identify (e);
					//Debug.Log("Identified "+TextUtil.GetName(e));
				}
			}
			bool isIdentified = book != null || GameData.IsIdentified(item.Entity);
			return TextUtil.EntityInfo(actor, item.Entity, isIdentified);
		}

		public int GetCost () {
			return Game.Rules.GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				//actor.SetNextAction (this);
				return false;
			}
			PickablePart dropItem = InputUtil.HandleSelectionInput (inspectables, null);
			if (dropItem != null) {
				inspect = dropItem;
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (inspectHere) {
					inspectHere = false;
					actor.SetNextAction (this);
				} else {
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				}
				return true;
			}
			return false;
		}

	}
}
