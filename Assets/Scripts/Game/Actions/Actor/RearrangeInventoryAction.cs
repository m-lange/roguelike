﻿using UnityEngine;

using Collections;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Effects;
using Game.Util;
using Game.Usables;

namespace Game.Actions.Actor {
	public class RearrangeInventoryAction : Action {

		static readonly string help = 
			"First, select what to arrange using the inventory letters.\n"+
			"With something selected, type a letter to assign it to the item.\n"+
			"Use the direction keys to move the item up/down, and confirm with enter.\n"+
			"After that, you can continue with another item.\n"+
			"Finish/go back/cancel with Esc.";


		ActorPart actor;
		InventoryPart inventory;
		PickablePart item;


		public RearrangeInventoryAction(ActorPart actor, PickablePart item) {
			this.actor = actor;
			this.item = item;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute() {

			if (inventory == null) {
				return ActionResult.FAILURE;
			}

			if (item == null) {
				//GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Arrange what? (a-Z)")));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Arrange what?", inventory.ToDict(), null, null ) );
			} else {
				string name = TextUtil.GetNameIndef(item);
				//GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Arrange "+name)));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Move or re-assign "+name, inventory.ToDict(), null, item ) );
			}	
			return ActionResult.PENDING;

			//return ActionResult.SUCCESS;
		}
		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}

			if (item == null) {
				PickablePart pick = InputUtil.HandleSelectionInput (inventory.ToDict(), null);
				if (pick != null) {
					item = pick;
					actor.SetNextAction (this);
					return true;
				}
			} else {
				if (Input.GetKeyDown (KeyCode.Keypad8)) {
					inventory.MoveItemUp (item);
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.Keypad2)) {
					inventory.MoveItemDown (item);
					actor.SetNextAction (this);
					return true;
				}
				string character = InputUtil.GetInputCharacter ();
				if (character != null && character.Length > 0 && GlobalParameters.letterSet.Contains (character [0])) {
					inventory.ReassignItem (character [0], item);
					item = null;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Return)) {
				item = null;
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return true;
			}
			return false;
		}
	}
}
