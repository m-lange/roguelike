﻿using ComponentSystem;
using Game.Data;
using Map.LayeredGrid;
using TurnSystem;
using Game.ActorEffects.Continuous;
using Game.Events;
using Game.Parts;
using Game.Rules;
using Game.Util;
using UnityEngine;

namespace Game.Actions.Actor {
	public class AttackAction : Action {

		ActorPart attacker;
		ActorPart defender;
		bool IsPlayer;
		bool confirmed;
		int cost = GameRules.DefaultCost;

		public AttackAction(ActorPart attacker, ActorPart defender, bool IsPlayer) {
			this.attacker = attacker;
			this.defender = defender;
			this.IsPlayer = IsPlayer;
			this.confirmed = false;
		}

		public ActionResult Execute() {
			if (attacker.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(attacker) ) {
					GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "You carry too much to use a attack!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}

			bool hostile = GameData.Factions.IsHostile (attacker.Faction, defender.Faction);
			bool friendly = GameData.Factions.IsFriendly (attacker.Faction, defender.Faction);
			if (hostile || confirmed 
					|| ((GameData.IsPlayer(attacker) || GameData.IsPlayer(defender)) && (attacker.AgressiveAgainstPlayer != 0 || defender.AgressiveAgainstPlayer != 0)) 
					|| attacker.HasEffect<ConfusionEffect>() ) {
				EquipmentPart attackWeapon = null;
				InventoryPart inv = attacker.Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv != null) {
					attackWeapon = inv.GetEquipped (Equip.HandPrim);
				}
				cost = GameRules.CalcAttackCost(attacker, attackWeapon);
				GameData.EventManager.Notify (new AttackEvent(attacker, defender, false));
				return ActionResult.SUCCESS;
			}

			if (!IsPlayer) {
				return ActionResult.SUCCESS;
			}
			string name = TextUtil.GetName (defender);
			string text = friendly ? "peaceful" : "neutral";
			GameData.EventManager.Notify (new MessageEvent (attacker.Entity, "Really attack this "+text+" "+name+"? (y/n)"));

			return ActionResult.PENDING;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			string inp = InputUtil.GetInputCharacter ();
			if (inp != null) {
				if (inp == "y") {
					confirmed = true;
					attacker.SetNextAction (this);
					return true;
				} else if (inp == "n") {
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				return true;
			}
			return false;
		}
	}
}
