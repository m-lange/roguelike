﻿using System.Collections;
using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class PickAction : Action {

		static readonly string help = 
			"Use 'p' to pick up everything here, and 'P' to select a single item.\n"+
			"Things to pick up are listed in the inventory window.\n"+
			"Type * to pick up everything. Type a character to pick up one list entry.\n"+
			"Type a number, followed by a character to pick up a certain amount from a list entry.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		List<PickablePart> pick;
		OrderedDict<string, PickablePart> pickableHere;
		bool pickAll;

		string count = "";

		public PickAction(ActorPart actor,  List<PickablePart> pick, bool pickAll) {
			this.actor = actor;
			this.pick = pick;
			this.pickAll = pickAll;
			this.pickableHere = null;
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<InventoryPart> ()) {
				return ActionResult.FAILURE;
			}
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}
			LocationPart loc = actor.Entity.GetPart<LocationPart> ();
			if (pick == null) {
				pickableHere = new OrderedDict<string, PickablePart> ();
				int i = 0;
				foreach (PickablePart item in GameData.SpatialManager.GetAt<PickablePart> (loc.Location)) {
					if (i >= GlobalParameters.letters.Length) {
						break;
					}
					char c = GlobalParameters.letters [i];
					pickableHere [c.ToString ()] = item;
					i++;
				}
				if (pickableHere.Count == 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to pick here"));
					return ActionResult.FAILURE;
				} else {
					if (pickAll) {
						InventoryPart inventory = actor.Entity.GetPart<InventoryPart> ();
						return Pick(inventory, pickableHere.Values);
					} else {
						if (pickableHere.Count == 1
						    && !(pickableHere [0].Entity.HasPart<StackablePart> () && pickableHere [0].Entity.GetPart<StackablePart> ().Count > 1)) {
							InventoryPart inventory = actor.Entity.GetPart<InventoryPart> ();
							ActionResult res = Pick (inventory, pickableHere [0], -1);
							GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
							return res;
						} else {
							char[] letters = GlobalParameters.letters;
							// TODO overflow!
							GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to pick? ({0}-{1}*?)", letters [0], letters [pickableHere.Count - 1])));
							GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to pick?", pickableHere));
							return ActionResult.PENDING;
						}
					}
				}
			} else {
				InventoryPart inventory = actor.Entity.GetPart<InventoryPart> ();
				return Pick(inventory, pick);
				/*
				ActionResult result = ActionResult.FAILURE;
				int pCount = (pick.Count > 1) ? -1
					: (count.Length == 0 ? -1 : int.Parse(count));
				foreach (PickablePart pickable in pick) {
					if (Pick (inventory, pickable, pCount) == ActionResult.SUCCESS) {
						result = ActionResult.SUCCESS;
					}
				}
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.Items, null) );
				return result;
				*/
			}
		}

		ActionResult Pick (InventoryPart inventory, ICollection pick) {
			ActionResult result = ActionResult.FAILURE;
			int pCount = (pick.Count > 1) ? -1
				: (count.Length == 0 ? -1 : int.Parse(count));
			foreach (object o in pick) {
				PickablePart pickable = (PickablePart) o;
				if (Pick (inventory, pickable, pCount) == ActionResult.SUCCESS) {
					result = ActionResult.SUCCESS;
				}
			}
			GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
			return result;
		}

		ActionResult Pick (InventoryPart inventory, PickablePart toPick, int count) {
			if (inventory.CanAdd (toPick)) {

				if (toPick.Entity.HasPart<StackablePart> ()) {
					StackablePart stack = toPick.Entity.GetPart<StackablePart> ();
					if (count <= 0 || count >= stack.Count) {
						if (inventory.CanAdd (toPick, stack.Count)) {
							GameData.EventManager.Notify ( new RemoveEntityEvent(toPick.Entity, false) ); 
							inventory.AddItem (toPick);
							GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Picking up {0}", TextUtil.GetNameIndef(toPick.Entity) ) ));
							return ActionResult.SUCCESS;
						} else {
							return ActionResult.FAILURE;
						}
					}

					Entity pick = stack.Unstack(false);
					StackablePart child = pick.GetPart<StackablePart> ();
					child.Count += count - 1;
					stack.Count -= count - 1;

					inventory.AddItem (pick.GetPart<PickablePart> ());
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Picking up {0}", TextUtil.GetNameCount (toPick, count))));
					return ActionResult.SUCCESS;
				}


				GameData.EventManager.Notify ( new RemoveEntityEvent(toPick.Entity, false) ); 
				inventory.AddItem (toPick);
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Picking up {0}", TextUtil.GetNameIndef(toPick.Entity) ) ));
				return ActionResult.SUCCESS;
			} else {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("The {0} is too heavy.", TextUtil.GetName(toPick.Entity)) ));
				return ActionResult.FAILURE;
			}
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {

			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				//actor.SetNextAction (this);
				return false;
			}

			string input = InputUtil.GetInputCharacter ();
			if (input != null
					&&  count.Length < 6
					&& input.Length > 0 && GlobalParameters.numbers.Contains (input [0])) {
				count += input [0].ToString();
			}

			PickablePart pickItem = InputUtil.HandleSelectionInput ( pickableHere, null );
			if (pickItem != null) {
				pick = new List<PickablePart> ();
				pick.Add (pickItem);
				actor.SetNextAction (this);
				return true;
			} else {
				if (InputUtil.GetInputCharacter() == "*") {
					pick = new List<PickablePart> ();
					foreach (PickablePart p in pickableHere.Values) {
						pick.Add (p);
					}
					actor.SetNextAction (this);
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				return true;
			}
			return false;
		}

	}
}
