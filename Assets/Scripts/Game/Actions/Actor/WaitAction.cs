﻿using System.Runtime.Serialization;
using Map.LayeredGrid;
using TurnSystem;

namespace Game.Actions.Actor {


	[DataContract]
	public class WaitAction : Action {

		public static readonly WaitAction Instance = new WaitAction();

		WaitAction() {
			
		}

		public ActionResult Execute() {
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
