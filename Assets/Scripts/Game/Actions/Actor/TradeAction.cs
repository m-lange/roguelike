﻿using System.Collections.Generic;
using ComponentSystem;
using Collections;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Events;
using Game.Parts;
using Game.Util;
using Game.Rules;
using UnityEngine;

namespace Game.Actions.Actor {
	public class TradeAction : Action {

		static readonly int UNKNOWN = 0;
		static readonly int BUY = 1;
		static readonly int SELL = 2;

		ActorPart actor;
		Direction direction;
		TraderPart trader;

		InventoryPart inventory;

		int buyOrSell;
		OrderedDict<string, PickablePart> wares;
		PickablePart ware;
		string countInput = "";
		bool confirmed = false;


		public TradeAction( ActorPart actor, TraderPart trader ) {
			this.actor = actor;
			this.trader = trader;
			this.direction = null;
			this.ware = null;
			this.buyOrSell = UNKNOWN;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			if (confirmed) {
				int cnt = countInput.Length == 0 ? -1 : int.Parse (countInput);
				Trade (ware, cnt);

				confirmed = false;
				ware = null;
				countInput = "";
				buyOrSell = UNKNOWN;
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict ()));
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Buy or sell? (b/s)"));
				return ActionResult.PENDING;
			} else if (ware != null) {
				int cnt = countInput.Length == 0 ? -1 : int.Parse (countInput);
				int price = CheckTrade (ware, ref cnt);
				if (price > 0) {
					string bs = buyOrSell == BUY ? "Buy" : "Sell";
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, bs + " " + TextUtil.GetNameCount (ware, cnt) + " for " + price + " gold? (y/n)"));
					return ActionResult.PENDING;
				} else {
					string who = (buyOrSell == BUY) ? "You" : ("The " + TextUtil.GetName (trader));
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, who + " can't afford " + TextUtil.GetNameCount (ware, cnt)));
					ware = null;
					countInput = "";
					string bs = buyOrSell == BUY ? "buy" : "sell";
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "What to " + bs + "?"));
					GameData.EventManager.Notify (new ShowTradeSelectionEvent (actor.Entity, "What to " + bs + "?", wares, buyOrSell == SELL, trader.IsSpecialized));
					return ActionResult.PENDING;
				}
			} else if (trader != null) {
				if (buyOrSell == UNKNOWN) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Buy or sell? (b/s)"));
					return ActionResult.PENDING;
				} else if (buyOrSell == BUY) {
					if (WaresToBuy ()) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "What to buy?"));
						GameData.EventManager.Notify (new ShowTradeSelectionEvent (actor.Entity, "What to buy?", wares, false, trader.IsSpecialized));
						return ActionResult.PENDING;
					} else {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to buy."));
						return ActionResult.FAILURE;
					}
				} else if (buyOrSell == SELL) {
					if (WaresToSell ()) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "What to sell?"));
						GameData.EventManager.Notify (new ShowTradeSelectionEvent (actor.Entity, "What to sell?", wares, true, trader.IsSpecialized));
						return ActionResult.PENDING;
					} else {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to sell."));
						return ActionResult.FAILURE;
					}
				} else {
					return ActionResult.FAILURE;
				}
			} else if (direction != null) {
				LocationPart source = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				GridLocation pos = direction.Transform(source.Location);

				TraderPart trader = GameData.SpatialManager.OneAt<TraderPart> (TraderPart.partId, pos);

				if (trader != null) {
					this.trader = trader;
					return ActionResult.Alternative (this);
				}

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "No trader here!"));
				return ActionResult.FAILURE;
			} else {
				List<TraderPart> traders = GetTraders (actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location);
				if (traders.Count == 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "No trader nearby!"));
					return ActionResult.FAILURE;
				} else if (traders.Count == 1) {
					this.trader = traders [0];
					return ActionResult.Alternative (this);
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Trade in which direction?"));
					return ActionResult.PENDING;
				}
			}
		}

		List<TraderPart> GetTraders (GridLocation center) {
			List<TraderPart> result = new List<TraderPart>();
			TraderPart temp = null;
			GridLocation loc = new GridLocation (0, 0, 0);
			foreach (Direction dir in Direction.HORIZONTAL) {
				dir.Transform (center, loc);
				temp = GameData.SpatialManager.OneAt<TraderPart> (TraderPart.partId, loc);
				if (temp != null) {
					result.Add( temp );
				}
			}
			return result;
		}

		int CheckTrade (PickablePart ware, ref int count) {
			bool sell = buyOrSell == SELL;
			int price = 0;
			if (ware.Entity.HasPart (StackablePart.partId)) {
				StackablePart stack = ware.Entity.GetPart<StackablePart> (StackablePart.partId);
				if (count > stack.Count || count <= 0) {
					count = stack.Count;
				}
				price = GameRules.CalcPrice( ware, sell, trader.IsSpecialized ) * count;
			} else {
				count = 1;
				price = GameRules.CalcPrice( ware, sell, trader.IsSpecialized );
			}
			if (sell) {
				InventoryPart tInv = trader.Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (!tInv.CanAdd (ware)) {
					return -1;
				}

				MoneyPart mp = tInv.FilterOne<MoneyPart> (null);
				if (mp == null) {
					return -1;
				}
				int money = mp.Entity.GetPart<StackablePart> (StackablePart.partId).Count;
				if (price >= money) {
					return -1;
				}
			} else {
				if (!inventory.CanAdd (ware)) {
					return -1;
				}

				MoneyPart mp = inventory.FilterOne<MoneyPart> (null);
				if (mp == null) {
					return -1;
				}
				int money = mp.Entity.GetPart<StackablePart> (StackablePart.partId).Count;
				if (price > money) {
					return -1;
				}
			}
			return price;
		}

		void Trade (PickablePart ware, int count) {
			bool sell = buyOrSell == SELL;
			int price = 0;
			if (ware.Entity.HasPart(StackablePart.partId)) {
				StackablePart stack = ware.Entity.GetPart<StackablePart> (StackablePart.partId);
				if (count > stack.Count || count <= 0) {
					count = stack.Count;
				}
				price = GameRules.CalcPrice( ware, sell, trader.IsSpecialized ) * count;
			} else {
				count = 1;
				price = GameRules.CalcPrice( ware, sell, trader.IsSpecialized );
			}
			InventoryPart tInv = trader.Entity.GetPart<InventoryPart> ();
			MoneyPart mp = inventory.FilterOne<MoneyPart> (null);
			MoneyPart tmp = tInv.FilterOne<MoneyPart> (null);
			if (sell) {
				PickablePart pick = inventory.RemoveItem (ware, count);
				GameData.EventManager.Notify (new RemoveEntityEvent (pick.Entity, false)); 
				tInv.AddItem (pick);

				PickablePart money = tInv.RemoveItem (tmp.Entity.GetPart<PickablePart> (), price);
				GameData.EventManager.Notify (new RemoveEntityEvent (money.Entity, false)); 
				inventory.AddItem (money);
			} else {
				PickablePart pick = tInv.RemoveItem (ware, count);
				GameData.EventManager.Notify (new RemoveEntityEvent (pick.Entity, false)); 
				inventory.AddItem (pick);

				PickablePart money = inventory.RemoveItem (mp.Entity.GetPart<PickablePart> (), price);
				GameData.EventManager.Notify (new RemoveEntityEvent (money.Entity, false)); 
				tInv.AddItem (money);
			}
			if (GameData.IsPlayer (actor)) {
				Sounds.Play (Sounds.Register);
			}
		}

		bool WaresToBuy () {
			List<PickablePart> traderSells = trader.Sells ();
			wares = new OrderedDict<string, PickablePart>();
			for (int i = 0; i < traderSells.Count; i++) {
				wares[ GlobalParameters.letters[i].ToString() ] = traderSells[i];
			}
			return wares.Count > 0;
		}

		bool WaresToSell () {
			wares = inventory.ToDict( inventory.Filter( (p) => trader.CanBuy(p) ) );
			return wares.Count > 0;
		}
		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (trader == null && direction == null) {
				direction = InputUtil.HandleDirectionXYInput ();
				if (direction != null) {
					actor.SetNextAction (this);
					return true;
				}
			} else if (buyOrSell == UNKNOWN) {
				if (Input.GetKeyDown (KeyCode.B)) {
					buyOrSell = BUY;
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.S)) {
					buyOrSell = SELL;
					actor.SetNextAction (this);
					return true;
				}
			} else if (ware == null) {
				string input = InputUtil.GetInputCharacter ();
				if (input != null
				    && countInput.Length < 6
				    && input.Length > 0 && GlobalParameters.numbers.Contains (input [0])) {
					countInput += input [0].ToString ();
				}

				PickablePart pick = InputUtil.HandleSelectionInput (wares, null);
				if (pick != null) {
					ware = pick;
					actor.SetNextAction (this);
					return true;
				}
			} else {
				if (Input.GetKeyDown (KeyCode.Y)) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.N)) {
					ware = null;
					countInput = "";
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return true;
			}
			return false;
		}
	}
}
