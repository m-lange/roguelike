﻿using System;
using ComponentSystem;
using Game.Parts;
using Game.Data;
using Game.Events;
using Game.Rules;
using Game.Util;
using Game.Messages;
using Game.Usables;
using Map;
using Map.LayeredGrid;
using TurnSystem;



namespace Game.Actions.Actor.Secondary {
	public class RefillLightSourceAction : TurnSystem.Action {

		ActorPart actor;
		Entity oilCan;
		//OilCanUsable oilCanUsable;
		LightSourcePart lightSource;
		int cost = Game.Rules.GameRules.DefaultCost;

		public RefillLightSourceAction(ActorPart actor, Entity oilCan, LightSourcePart lightSource) {
			this.actor = actor;
			this.oilCan = oilCan;
			//this.oilCanUsable = oilCanUsable;
			this.lightSource = lightSource;
		}

		public ActionResult Execute () {

			LimitedUsePart limUse = oilCan.GetPart<LimitedUsePart> (LimitedUsePart.partId);

			//if (oilCanUsable.ChargeTurns <= 0) {
			//	GameData.EventManager.Notify (new MessageEvent (actor.Entity, "The " + TextUtil.GetName (oilCan) + " is empty."));
			//	return ActionResult.FAILURE;
			//}
			if (limUse == null) {
				return ActionResult.FAILURE;
			}
			if (limUse.UsesRemaining <= 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "The " + TextUtil.GetName (oilCan) + " is empty."));
				return ActionResult.FAILURE;
			}

			int charges = Math.Min (limUse.UsesRemaining, lightSource.MaxTurns - lightSource.TurnsRemaining);
			//limUse.UsesRemaining -= charges;
			lightSource.TurnsRemaining += charges;
			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You refill the " + TextUtil.GetName (lightSource) + "."));
			}
			GameRules.LimitedUseDamage(actor, limUse, charges, false);

			InventoryPart inv = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inv.ToDict()));
			}
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
