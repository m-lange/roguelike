﻿using Game.Parts;
using Game.Events;
using Game.Data;
using Game.Rules;
using Game.Messages;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;

namespace Game.Actions.Actor.Secondary {
	public class DestroyWindowAction : Action {

		ActorPart actor;
		GridLocation location;
		Entity tool;
		float probability;

		public DestroyWindowAction(ActorPart actor, GridLocation location, Entity tool, float probability) {
			this.actor = actor;
			this.location = location;
			this.probability = probability;
			this.tool = tool;
		}

		public ActionResult Execute () {
			if (RandUtils.NextFloat () < probability) {
				GameData.Map.Set(location, new TileInfo (Terra.TileInfoId (Terra.WindowDestroyed)) );
				GameData.EventManager.Notify (new MapChangedEvent (location));
				GameData.EventManager.Notify (new UpdateVisibilityEvent (location));
				if (GameData.IsPlayer (actor)) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You destroy the window."));
					Sounds.Play (Sounds.Hit1);
					Sounds.Play (Sounds.WindowDestroy);
				}
			} else {
				if (GameData.IsPlayer (actor)) {
					GameData.EventManager.Notify (new MessageEvent (null, "You hit the window."));
					Sounds.Play (Sounds.Hit1);
				}
			}
			if (GameData.IsPlayer(actor)) {
				GameData.EventManager.Notify (new GameMessageEvent(actor.Entity, 
					new UnauthorizedAccessMessage(actor.Entity, GameRules.DefaultMessageRadius, false, location) ) );
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
