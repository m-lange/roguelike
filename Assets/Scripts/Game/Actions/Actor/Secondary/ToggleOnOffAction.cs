﻿using Game.Parts;
using Game.Data;
using Game.Events;
using Game.Rules;
using Game.Util;
using Game.Messages;
using Map;
using Map.LayeredGrid;
using TurnSystem;



namespace Game.Actions.Actor.Secondary {
	public class ToggleOnOffAction : Action {

		ActorPart actor;
		OnOffPart onOff;
		int cost = Game.Rules.GameRules.DefaultCost;

		public ToggleOnOffAction(ActorPart actor, OnOffPart onOff) {
			this.actor = actor;
			this.onOff = onOff;
		}

		public ActionResult Execute () {
			if (!onOff.IsOn) {
				if( onOff.Entity.HasPart(LightSourcePart.partId) && onOff.Entity.GetPart<LightSourcePart>(LightSourcePart.partId).TurnsRemaining <= 0 ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "The " + TextUtil.GetName (onOff) + " is empty."));
					return ActionResult.FAILURE;
				}
			}
			onOff.IsOn = !onOff.IsOn;
			GameData.EventManager.Notify (new UpdateVisibilityEvent (actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location));
			if (GameData.IsPlayer (actor)) {
				if (onOff.IsOn) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You activate the " + TextUtil.GetName (onOff) + "."));
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You deactivate the " + TextUtil.GetName (onOff) + "."));
				}
			}
			InventoryPart inv = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			if (inv != null) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inv.ToDict()));
			}
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
