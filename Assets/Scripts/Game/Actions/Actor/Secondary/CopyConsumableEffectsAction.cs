﻿using Game.Parts;
using Game.Data;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Game.ActorEffects;
using Game.Util;
using Map;
using Map.LayeredGrid;
using TurnSystem;

namespace Game.Actions.Actor.Secondary {
	public class CopyConsumableEffectsAction : Action {

		ActorPart actor;
		ConsumablePart source;
		ConsumablePart target;
		bool removeSource;

		int cost = Game.Rules.GameRules.DefaultCost;

		public CopyConsumableEffectsAction(ActorPart actor, ConsumablePart source, ConsumablePart target, bool removeSource) {
			this.actor = actor;
			this.source = source;
			this.target = target;
			this.removeSource = removeSource;
		}

		public ActionResult Execute () {
			if (source.Effects != null && source.Effects.Length > 0) {
				int targLength = target.Effects == null ? 0 : target.Effects.Length;
				ActorEffect[] effs = new ActorEffect[ source.Effects.Length + targLength ];
				if (targLength > 0)
					target.Effects.CopyTo (effs, 0);
				source.Effects.CopyTo (effs, targLength);
				target.Effects = effs;
			}
			if (removeSource) {
				StackablePart stack = source.Entity.GetPart<StackablePart> (StackablePart.partId);
				if (stack != null && stack.Count > 1) {
					stack.Count -= 1;
					stack.SetContainerDirty();
				} else {
					GameData.EventManager.Notify (new RemoveEntityEvent (source.Entity, true));
				}
			}
			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify( new MessageEvent(actor.Entity, "You rub the "+TextUtil.GetName(target)+" with the "+TextUtil.GetName(source) ) );
			}
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
