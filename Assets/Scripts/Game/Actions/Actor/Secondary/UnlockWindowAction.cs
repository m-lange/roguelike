﻿using Game.Parts;
using Game.Data;
using Game.Events;
using Game.Rules;
using Game.Messages;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;

namespace Game.Actions.Actor.Secondary {
	public class UnlockWindowAction : Action {

		ActorPart actor;
		GridLocation location;
		Entity tool;
		int cost = Game.Rules.GameRules.DefaultCost;

		public UnlockWindowAction(ActorPart actor, GridLocation location, Entity tool) {
			this.actor = actor;
			this.location = location;
			this.tool = tool;
		}

		public ActionResult Execute () {
			GameData.Map [location] = new TileInfo (Terra.TileInfoId (Terra.WindowClosed));
			GameData.EventManager.Notify (new MapChangedEvent (location));
			GameData.EventManager.Notify (new UpdateVisibilityEvent (location));
			GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You unlock the window."));
			cost = Game.Rules.GameRules.CalcUnlockCost(actor);

			if (GameData.IsPlayer(actor)) {
				GameData.EventManager.Notify (new GameMessageEvent(actor.Entity, 
					new UnauthorizedAccessMessage(actor.Entity, GameRules.DefaultMessageRadius, true, location) ) );
				Sounds.Play (Sounds.Lock);
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
