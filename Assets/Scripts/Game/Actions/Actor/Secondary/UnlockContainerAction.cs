﻿using Game.Parts;
using Game.Data;
using Game.Events;
using Game.Rules;
using Game.Util;
using Game.Messages;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;

namespace Game.Actions.Actor.Secondary {
	public class UnlockContainerAction : Action {

		ActorPart actor;
		ContainerPart container;
		Entity tool;
		int cost = Game.Rules.GameRules.DefaultCost;

		public UnlockContainerAction(ActorPart actor, ContainerPart container, Entity tool) {
			this.actor = actor;
			this.container = container;
			this.tool = tool;
		}

		public ActionResult Execute () {

			container.IsLocked = false;
			cost = Game.Rules.GameRules.CalcUnlockCost (actor);

			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You unlock the " + TextUtil.GetName (container) + "."));
				Sounds.Play (Sounds.Lock);
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
