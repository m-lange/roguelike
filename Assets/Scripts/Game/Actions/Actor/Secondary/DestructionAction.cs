﻿using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;

namespace Game.Actions.Actor.Secondary {
	public class DestructionAction : Action {

		ActorPart actor;
		DestructablePart target;
		Entity tool;
		int damage;

		public DestructionAction(ActorPart actor, DestructablePart target, Entity tool, int damage) {
			this.actor = actor;
			this.target = target;
			this.damage = damage;
			this.tool = tool;
		}

		public ActionResult Execute () {
			string name = TextUtil.GetName (target);
			bool destroyed = target.Entity.HasPart<HealthPointsPart>() 
				? target.Entity.GetPart<HealthPointsPart>().DealDamage( damage )
				: true;

			if (destroyed) {
				GameData.EventManager.Notify (new RemoveEntityEvent (target.Entity, true));
				GameData.EventManager.Notify (new DestroyEntityEvent (target.Entity, actor.Entity));
				GameData.EventManager.Notify (new MessageEvent (null, "You destroy the "+name));
				if (target.Entity.HasPart<LocationPart> ()) {
					GameData.EventManager.Notify (new UpdateVisibilityDelayedEvent (target.Entity.GetPart<LocationPart> ().Location));
				}
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, "You hit the "+name));
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
