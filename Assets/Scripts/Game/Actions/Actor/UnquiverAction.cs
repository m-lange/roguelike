﻿using ComponentSystem;
using Collections;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using Game.Util;

using UnityEngine;

namespace Game.Actions.Actor {
	public class UnquiverAction : Action {



		ActorPart actor;
		InventoryPart inventory;

		public UnquiverAction(ActorPart actor) {
			this.actor = actor;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			PickablePart pick = inventory.UnQuiver ();
			if (pick != null) {
				string name = TextUtil.GetNameIndef(pick);
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("You unquiver {0}.", name)));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
				return ActionResult.SUCCESS;
			} else {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing at the ready."));
				return ActionResult.FAILURE;
			}
		}



		public int GetCost () {
			return Game.Rules.GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
