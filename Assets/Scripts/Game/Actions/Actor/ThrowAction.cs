﻿using System.Collections.Generic;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Rules;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class ThrowAction : Action {

		static readonly string help = 
			"Select something to throw, then use direction keys or mouse to navigate\n"+
			"Press return to confirm. Cancel with Esc.";


		ActorPart actor;
		GridLocation actorLocation;
		GridLocation location;

		InventoryPart inventory;
		PickablePart throwable;
		EquipmentPart launcher;

		bool confirmed;

		int cost = GameRules.DefaultCost;

		public ThrowAction ( ActorPart actor, PickablePart throwable, GridLocation location ) {
			this.actor = actor;
			this.location = location;
			confirmed = this.location != null && throwable != null;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			if (throwable == null) {
				if (actor.Entity.HasPart<LocationPart> ()) {
					actorLocation = actor.Entity.GetPart<LocationPart> ().Location;
				} else {
					actorLocation = null;
				}
			}
		}

		public ActionResult Execute () {
			if (actor.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to throw!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}

			int range = 0;
			if (throwable != null) {
				launcher = inventory.GetLauncher(throwable);
				range = GameRules.CalcRange (actor, throwable, launcher != null);
				if (!CanThrow (throwable)) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "This " + TextUtil.GetName (throwable) + " is too heavy!"));
					return ActionResult.FAILURE;
				}
			}

			if (confirmed && location != null && throwable != null) {
				if (actorLocation.Distance (location) > range) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't throw that far!"));
					confirmed = false;
					return ActionResult.PENDING;
				}
				cost = GameRules.CalcReloadCost(actor, throwable, launcher != null);

				PickablePart dropped = inventory.RemoveItem(throwable, 1);
				GameData.EventManager.Notify ( new CreateEntityEvent(dropped.Entity) ); 
				GameData.EventManager.Notify (new RangedAttackEvent (actor, location, dropped, range, launcher));
				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.SUCCESS;
			}
			if (throwable == null) {
				if (inventory == null) {
					return ActionResult.FAILURE;
				}
				if (inventory.Items.Count == 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to throw!")));
					return ActionResult.FAILURE;
				}
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Throw what? (a-Z)")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Throw what?", inventory.ToDict()));
				return ActionResult.PENDING;
			}

			if (location == null) {
				if (!actor.Entity.HasPart<LocationPart> ()) {
					return ActionResult.FAILURE;
				}
				LocationPart loc = actor.Entity.GetPart<LocationPart> ();
				location = loc.Location;

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Throw where? (directions/mouse/?)"));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Throw where?", inventory.ToDict(), null, throwable));

			}
			GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, actorLocation, range));
			GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, location));


			return ActionResult.PENDING;
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				actor.SetNextAction (this);
				return true;
			}

			if (throwable == null) {
				PickablePart pick = InputUtil.HandleSelectionInput (inventory.ToDict(), null);
				if (pick != null) {
					throwable = pick;
					actor.SetNextAction (this);
					return true;
				}
			} else {
				if (Input.GetMouseButtonDown (0)) {
					location = InputUtil.GetMousePosition (GameData.CameraLocation.Location, Input.mousePosition);
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
				Direction direction = InputUtil.HandleDirectionXYInput ();
				if (direction != null) {
					location = direction.Transform (location);
					actor.SetNextAction (this);
					return true;
				} else {
					if (location != null && Input.GetKeyDown (KeyCode.Return)) {
						confirmed = true;
						actor.SetNextAction (this);
						return true;
					}
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				return true;
			}
			return false;
		}

		bool CanThrow (PickablePart thrown) {
			return throwable.Mass <= actor.Strength * GameRules.MaxThrowPerStrength;
		}
		/*
		int CalcRange (PickablePart thrown) {
			float range = 0;
			if (thrown.Entity.HasPart<ProjectilePart> ()) {
				ProjectilePart p = thrown.Entity.GetPart<ProjectilePart> ();
				range = hasLauncher ? p.RangeFired : p.Range;
				if (range > 0) {
					return (int) range;
				}
			}
			float maxMass = actor.Strength * GameRules.MaxThrowPerStrength;
			float minMass = 100;
			float mass = throwable.Mass;
			range = GameRules.MaxThrowingDistance - ((GameRules.MaxThrowingDistance - 1) * (mass - minMass) / (maxMass - minMass));
			return (int) range;
		}
		*/
	}
}
