﻿using System.Runtime.Serialization;
using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Events;
using Game.Messages;
using Game.Parts;

namespace Game.Actions.Actor {


	[DataContract]
	public class SendMessageAction : Action {

		ActorPart actor;
		Message message;
		string sound;
		int cost;
		string displayMessage;

		public SendMessageAction(ActorPart actor, Message message, int cost, string displayMessage, string sound) {
			this.actor = actor;
			this.message = message;
			this.cost = cost;
			this.displayMessage = displayMessage;
			this.sound = sound;
		}

		public ActionResult Execute () {
			if (displayMessage != null) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, displayMessage));
			}
			if (sound != null) {
				Sounds.PlayNow( sound );
			}
			GameData.EventManager.Notify( new GameMessageEvent(actor.Entity, message) );
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
