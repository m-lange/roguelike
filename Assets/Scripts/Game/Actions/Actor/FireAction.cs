﻿using System.Collections.Generic;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Rules;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class FireAction : Action {

		static readonly string help = 
			"Fire something quivered / at the ready. Use direction keys or mouse to navigate\n"+
			"Press return or f to confirm. Cancel with Esc.";


		ActorPart actor;
		GridLocation actorLocation;
		GridLocation location;

		InventoryPart inventory;
		PickablePart throwable;
		EquipmentPart launcher;

		bool confirmed;

		int cost = GameRules.DefaultCost;

		public FireAction(ActorPart actor, PickablePart throwable, GridLocation location) {
			this.actor = actor;
			this.location = location;
			this.throwable = throwable;
			confirmed = this.location != null && throwable != null;

			//if (throwable == null) {
			if (actor.Entity.HasPart (InventoryPart.partId)) {
				inventory = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart (LocationPart.partId)) {
				actorLocation = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			} else {
				actorLocation = null;
			}
			//}
		}

		public ActionResult Execute () {
			//Debug.Log("Firing "+TextUtil.GetName(throwable)+" "+location+" "+confirmed);
			if (actor.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to fire!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}

			if (throwable == null) {
				if (inventory == null) {
					return ActionResult.FAILURE;
				}
				if (inventory.Items.Count == 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to fire!")));
					return ActionResult.FAILURE;
				} else {
					throwable = inventory.GetQuivered();
					if (throwable == null) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing at the ready!")));
						return ActionResult.FAILURE;
					}
				}
			}
			int range = 0;
			if (throwable != null) {
				launcher = inventory.GetLauncher(throwable);
				range = GameRules.CalcRange (actor, throwable, launcher != null);
				if (!CanThrow (throwable)) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "This " + TextUtil.GetName (throwable) + " is too heavy!"));
					return ActionResult.FAILURE;
				}
			}

			if (confirmed && location != null && throwable != null) {
				if (actorLocation.Distance (location) > range) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't fire that far!"));
					confirmed = false;
					return ActionResult.PENDING;
				}
				//launcher = inventory.GetLauncher(throwable);
				cost = GameRules.CalcReloadCost(actor, throwable, launcher != null);

				PickablePart dropped = inventory.RemoveItem(throwable, 1);
				GameData.EventManager.Notify ( new CreateEntityEvent(dropped.Entity) ); 

				GameData.EventManager.Notify (new RangedAttackEvent (actor, location, dropped, range, launcher));

				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.SUCCESS;
			}

			if (location == null) {
				if (!actor.Entity.HasPart (LocationPart.partId)) {
					return ActionResult.FAILURE;
				}
				LocationPart loc = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				location = loc.Location;

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Fire where? (directions/mouse/?)"));
			}
			GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, actorLocation, range));
			GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, location));


			return ActionResult.PENDING;
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetMouseButtonDown (0)) {
				location = InputUtil.GetMousePosition (GameData.CameraLocation.Location, Input.mousePosition);
				confirmed = true;
				actor.SetNextAction (this);
				return true;
			}
			Direction direction = InputUtil.HandleDirectionXYInput ();
			if (direction != null) {
				location = direction.Transform (location);
				actor.SetNextAction (this);
				return true;
			} else {
				if (location != null 
					&& (Input.GetKeyDown (KeyCode.Return)) ) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				return true;
			}
			return false;
		}

		bool CanThrow (PickablePart thrown) {
			return throwable.Mass <= actor.Strength * GameRules.MaxThrowPerStrength;
		}
		/*
		int CalcRange (PickablePart thrown) {
			float range = 0;
			if (thrown.Entity.HasPart<ProjectilePart> ()) {
				ProjectilePart p = thrown.Entity.GetPart<ProjectilePart> ();
				range = hasLauncher ? p.RangeFired : p.Range;
				if (range > 0) {
					return (int) range;
				}
			}
			float maxMass = actor.Strength * GameRules.MaxThrowPerStrength;
			float minMass = 100;
			float mass = throwable.Mass;
			range = GameRules.MaxThrowingDistance - ((GameRules.MaxThrowingDistance - 1) * (mass - minMass) / (maxMass - minMass));
			return (int) range;
		}
		*/
	}
}
