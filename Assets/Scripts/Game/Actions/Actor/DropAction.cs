﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class DropAction : Action {

		static readonly string help = 
			"Things to drop are listed in the inventory window.\n"+
			"Type * to drop everything. Type a character to drop one list entry.\n"+
			"Type a number, followed by a character to drop a certain amount from a list entry.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		List<PickablePart> drop;
		InventoryPart inventory;
		LocationPart location;

		string count = "";

		public DropAction(ActorPart actor,  List<PickablePart> drop) {
			this.actor = actor;
			this.drop = drop;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart<LocationPart> ()) {
				location = actor.Entity.GetPart<LocationPart> ();
			} else {
				location = null;
			}
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			if (location == null) {
				return ActionResult.FAILURE;
			}
			if (inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to drop!")));
				return ActionResult.FAILURE;
			}

			if (drop == null) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to drop? (a-Z*?)")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to drop?", inventory.ToDict()));
				return ActionResult.PENDING;
			} else {
				ActionResult result = ActionResult.FAILURE;
				int dCount = (drop.Count > 1) ? -1
					: (count.Length == 0 ? -1 : int.Parse(count));
				foreach (PickablePart pickable in drop) {
					if (Drop (inventory, pickable, dCount) == ActionResult.SUCCESS) {
						result = ActionResult.SUCCESS;
					}
				}

				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return result;
			}
		}

		ActionResult Drop (InventoryPart inventory, PickablePart toPick, int count) {

			LocationPart locPart = toPick.Entity.GetPart<LocationPart> (LocationPart.partId);
			if (locPart != null) {
				locPart.LocationForSerialization = new GridLocation(0,0,0);
			}
			if (toPick.Entity.HasPart<StackablePart> ()) {
				if (count <= 0) {
					PickablePart dropped = inventory.RemoveItem (toPick, -1);
					if (dropped != null) {
						GameData.EventManager.Notify (new CreateEntityEvent (dropped.Entity)); 
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Dropping {0}", TextUtil.GetNameIndef (dropped))));
						return ActionResult.SUCCESS;
					} else {
						return ActionResult.FAILURE;
					}
				}

				PickablePart drop = inventory.RemoveItem (toPick, count);
				if (drop != null) {
					GameData.EventManager.Notify (new CreateEntityEvent (drop.Entity)); 
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Dropping {0}", TextUtil.GetName (drop))));
					return ActionResult.SUCCESS;
				} else {
					return ActionResult.FAILURE;
				}
			}
			PickablePart dr = inventory.RemoveItem (toPick, -1);
			if (dr != null) {
				GameData.EventManager.Notify ( new CreateEntityEvent(dr.Entity) ); 
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Dropping {0}", TextUtil.GetNameIndef(dr) ) ));
				return ActionResult.SUCCESS;
			} else {
				return ActionResult.FAILURE;
			}
				
		}

		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				//actor.SetNextAction (this);
				return false;
			}

			string input = InputUtil.GetInputCharacter ();
			if (input != null
					&&  count.Length < 6
					&& input.Length > 0 && GlobalParameters.numbers.Contains (input [0])) {
				count += input [0].ToString();
			}
			
			PickablePart dropItem = InputUtil.HandleSelectionInput ( inventory.ToDict(), null );
			if (dropItem != null) {
				drop = new List<PickablePart> ();
				drop.Add (dropItem);
				actor.SetNextAction (this);
				return true;
			} else {
				if (InputUtil.GetInputCharacter() == "*") {
					drop = new List<PickablePart> (inventory.Items);
					actor.SetNextAction (this);
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return true;
			}
			return false;
		}

	}
}
