﻿using System.Linq;
using System.Collections.Generic;
using ComponentSystem;
using Game.ActorEffects;
using Game.ActorEffects.Continuous;
using Game.Rules;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Events;
using Game.Parts;
using Game.Util;
using UnityEngine;

namespace Game.Actions.Actor {
	public class GrapplingHookAction : Action {

		ActorPart actor;
		Direction direction;
		InventoryPart inventory;

		public GrapplingHookAction( ActorPart actor, Direction direction ) {
			this.actor = actor;
			this.direction = direction;
			
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {
			
			if (actor.HasEffect<StickToEntityEffect> ()) {
				if (GameData.IsPlayer (actor)) {
					StickToEntityEffect eff = actor.GetEffect<StickToEntityEffect> ();
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You are immobilized by "+eff.EntityName+"!"));
				}
				return ActionResult.SUCCESS;
			}
			
			if (actor.Stats.BurdeningStatus > GameRules.BurdeningStatus.Unburdened) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to climb!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}
			
			if(inventory != null) {
				BulkyPart bulky = inventory.FindFirst<BulkyPart>( Part => true );
				if(bulky != null) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't climb with "+TextUtil.GetNameIndef(bulky.Entity)+"!"));
					return ActionResult.FAILURE;
				}
			}
			
			if (direction != null) {
				if( direction == Direction.UP || direction == Direction.DOWN ) {
					Entity camera = GameData.Camera;
					if (direction == Direction.UP) {
						LocationPart locPart = camera.GetPart<LocationPart> (LocationPart.partId);
						if (locPart.Location.Layer < GameData.Map.TopLayer) {
							locPart.Location = new ImmutableGridLocation(locPart.Location.Layer + 1, locPart.Location.X, locPart.Location.Y);
						}
					} else if (direction == Direction.DOWN) {
						LocationPart locPart = camera.GetPart<LocationPart> (LocationPart.partId);
						if (locPart.Location.Layer > GameData.Map.BottomLayer) {
							locPart.Location = new ImmutableGridLocation(locPart.Location.Layer - 1, locPart.Location.X, locPart.Location.Y);
						}
					}
					return ActionResult.PENDING;
				} else {
					LocationPart source = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
					LocationPart camPos = GameData.Camera.GetPart<LocationPart> (LocationPart.partId);
					GridLocation pos = new GridLocation (camPos.Location.Layer,
						                   source.Location.X + direction.dx,
						                   source.Location.Y + direction.dy);
					return Climb( source, pos );
				}
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Climb where? Select layer (+/-) and direction!") );
				return ActionResult.PENDING;
			}
		}
		
		ActionResult Climb( LocationPart loc, GridLocation target ) {
			
			if( loc.Location.Layer == target.Layer ) {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Select a different level!") );
				return ActionResult.FAILURE;
			}
			
			GridLocation bottom = (loc.Location.Layer < target.Layer) ? loc.Location : target;
			int lower = bottom.Layer;
			int upper =  (loc.Location.Layer < target.Layer) ? target.Layer : loc.Location .Layer;
						
			TileInfo info = GameData.Map.Get(target);
			TileTypeInfo typeInfo = Terra.TileInfo [info.Type];
			TileInfo ti;
			TileTypeInfo tit;
			if( typeInfo.IsWalkable || typeInfo.Id == Terra.WindowClosed || typeInfo.Id == Terra.WindowLocked ) {
				for(int l=lower+1; l<=upper; l++) {
					ti = GameData.Map.Get(l, bottom.X, bottom.Y);
					tit = Terra.TileInfo [ti.Type];
					if( tit.Id != Terra.Void ) {
						GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Unreachable place!") );
						return ActionResult.FAILURE;
					}
				}
				// Blocked?
				if (GameData.SpatialManager.IsAt<MoveBlockingPart> (MoveBlockingPart.partId, target)) {
					GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Something is blocking this place!") );
					return ActionResult.FAILURE;
				}
				
				if( typeInfo.Id == Terra.WindowClosed || typeInfo.Id == Terra.WindowLocked ) {
					GameData.Map.Set(target, new TileInfo (Terra.TileInfoId (Terra.WindowDestroyed)) );
					GameData.EventManager.Notify (new MapChangedEvent (target));
					GameData.EventManager.Notify (new UpdateVisibilityEvent (target));
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You destroy the window."));
						Sounds.Play (Sounds.WindowDestroy);
					}
				}
				
				loc.Location = target;
				
				IEnumerable<ContactEffectPart> effects = GameData.SpatialManager.GetAt<ContactEffectPart> (ContactEffectPart.partId, target);
				if (effects.Any()) {
					foreach( ContactEffectPart eff in effects ) {
						eff.OnContact(actor);
					}
				}
				
				return ActionResult.SUCCESS;
			}
			
			GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing to climb here!") );
			return ActionResult.FAILURE;
		}

		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			direction = InputUtil.HandleDirectionInput ();

			if (direction != null) {
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.N) || Input.GetKeyDown (KeyCode.Escape)) {
				return true;
			}
			return false;
		}
	}
}
