﻿using Map;
using Collections;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Effects;
using Game.Usables;
using Game.Util;
using Map.LayeredGrid;
using TurnSystem;

using UnityEngine;

namespace Game.Actions.Actor {
	public class UseWithAction : Action {

		ActorPart actor;
		InventoryPart inventory;
		OrderedDict<string, PickablePart> applicable;

		UsablePart usable;
		Entity target;
		GridLocation targetLocation;
		OrderedDict<string, Entity> atTarget;

		GridLocation requiresUpdate = null;
		int cost = 1;

		public UseWithAction(ActorPart actor, UsablePart usable, Entity target, GridLocation targetLocation) {
			this.actor = actor;
			this.usable = usable;
			this.target = target;
			this.targetLocation = targetLocation;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
				applicable = inventory.ToDict( inventory.Filter( (p) => p.Entity != usable.Entity ) );
			} else {
				inventory = null;
				applicable = null;
			}
		}

		public ActionResult Execute() {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}

			if (target != null) {
				return ApplyToTarget ();
			} else if (targetLocation != null) {
				atTarget = new OrderedDict<string, Entity> ();
				if (usable.Usable.AllowItems) {
					int i = 0;
					foreach (Entity item in GameData.SpatialManager.GetAt (targetLocation, (e) => (! GameData.IsPlayer(e)) && e != GameData.Camera)) {
						//foreach (Entity item in GameData.SpatialManager.GetAt (targetLocation, null)) {
						if (i >= GlobalParameters.letters.Length) {
							break;
						}
						char c = GlobalParameters.letters [i];
						atTarget [c.ToString ()] = item;
						i++;
					}
				}
				if (atTarget.Count == 0) {
					return ApplyToLoction ();
				} else if (atTarget.Count == 1) {
					target = atTarget [0];
					return ApplyToTarget ();
				} else {
					char[] letters = GlobalParameters.letters;
					string name = TextUtil.GetName (usable);
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Use the "+name+" with? ({0}-{1})", letters [0], letters [atTarget.Count - 1])));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent(actor.Entity, "Use the "+name+" with...", atTarget, null) );
					return ActionResult.PENDING;
				}
			} else {
				string name = TextUtil.GetName (usable);
				if (usable.Usable.AllowDirections && usable.Usable.AllowItems) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Use the " + name + " with/in direction?"));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Use the " + name + " with...", applicable));
				} else if (usable.Usable.AllowDirections) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Use the " + name + " in direction?"));
				} else if (usable.Usable.AllowItems) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Use the " + name + " with?"));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Use the " + name + " with...", applicable));
				} else {
					return ActionResult.FAILURE;
				}
				return ActionResult.PENDING;
			}

		}

		ActionResult ApplyToTarget () {
			Action action = usable.GetAction (actor, usable.Entity, target, null, null);
			GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
			if (action != null) {
				return ActionResult.Alternative(action);
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
			/*
			UseEffectRule rule = null;
			Effect effect = usable.GetEffect (actor, usable.Entity, target, null, null, out rule);
			Action action = null;

			GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null, null));
			if (effect != null) {
				cost = rule.Cost;
				GridLocation update = null;
				action = effect.Execute (out update);
				if (update != null) {
					requiresUpdate = update;
				}
				if (action != null) {
					return ActionResult.Alternative (action);
				} else {
					return ActionResult.SUCCESS;
				}
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
			*/
		}

		ActionResult ApplyToLoction() {

			Action action = usable.GetAction (actor, usable.Entity, null, targetLocation, GameData.Map.Get(targetLocation));
			GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
			if (action != null) {
				return ActionResult.Alternative(action);
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
		/*
			UseEffectRule rule = null;
			Effect effect = usable.GetEffect (actor, usable.Entity, null, targetLocation, GameData.Map[targetLocation], out rule);
			Action action = null;

			GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null, null));
			if (effect != null) {
				cost = rule.Cost;
				GridLocation update = null;
				action = effect.Execute (out update);
				if (update != null) {
					requiresUpdate = update;
				}
				if (action != null) {
					return ActionResult.Alternative (action);
				} else {
					return ActionResult.SUCCESS;
				}
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
			*/
		}


		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return requiresUpdate;
		}

		public bool HandleUserInput() {
			if (atTarget == null) {
				PickablePart item = (usable.Usable.AllowItems) ? InputUtil.HandleSelectionInput (applicable, null) : null;
				Direction dir = (usable.Usable.AllowItems) ? InputUtil.HandleDirectionInput () : null;
				if (item != null) {
					this.target = item.Entity;
					actor.SetNextAction (this);
					return true;
				} else if (dir != null && dir != Direction.UP) {
					if (actor.Entity.HasPart<LocationPart> ()) {
						GridLocation loc = actor.Entity.GetPart<LocationPart> ().Location;
						this.targetLocation = (dir == Direction.DOWN) ? new GridLocation (loc) : dir.Transform (loc);
						actor.SetNextAction (this);
					}
					return true;
				}
			} else {
				Entity item = InputUtil.HandleSelectionInput (atTarget, null);
				if (item != null) {
					this.target = item;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				return true;
			}
			return false;
		}
	}
}
