﻿using System.Linq;
using System.Collections.Generic;
using Game.ActorEffects;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.ActorEffects.Continuous;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Data;
using Game.Rules;
using Collections;
using UnityEngine;

namespace Game.Actions.Actor {
	public class JumpAction : Action {

		static readonly string help = 
			"Jump, e.g. from roof to roof.\n"+
			"Use direction keys or mouse to navigate.\n"+
			"Press return to confirm. Cancel with Esc.";


		ActorPart actor;
		GridLocation location;
		InventoryPart inventory;
		bool confirmed;

		public JumpAction(ActorPart actor, GridLocation location) {
			this.actor = actor;
			this.location = location;
			confirmed = this.location != null;
			
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {

			if (actor.HasEffect<StickToEntityEffect> ()) {
				if (GameData.IsPlayer (actor)) {
					StickToEntityEffect eff = actor.GetEffect<StickToEntityEffect> ();
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You are immobilized by "+eff.EntityName+"!")); 
				}
				return ActionResult.SUCCESS;
			}

			if (actor.Stats.BurdeningStatus > GameRules.BurdeningStatus.Unburdened) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to jump!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}
			
			if(inventory != null) {
				BulkyPart bulky = inventory.FindFirst<BulkyPart>( Part => true );
				if(bulky != null) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't jump with "+TextUtil.GetNameIndef(bulky.Entity)+"!"));
					return ActionResult.FAILURE;
				}
			}
			

			if (confirmed && location != null) {

				if ((!GameData.Visibility.Visible.Contains (location)) 
				|| !GameData.Visibility.Visible.IsCenterVisible (location)) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't jump there!"));
					GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
					GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
					return ActionResult.FAILURE;
				}

				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));

				if (Jump (location)) {
					return ActionResult.SUCCESS;
				}
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't jump there!"));
				return ActionResult.FAILURE;
			}
			if (location == null) {
				if (!actor.Entity.HasPart<LocationPart> ()) {
					return ActionResult.FAILURE;
				}
				LocationPart loc = actor.Entity.GetPart<LocationPart> ();
				location = loc.Location;

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Jump where? (directions/mouse/?)"));
			}
			LocationPart start = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
			GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, location));
			GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, start.Location, GameRules.CalcJumpingDist(actor)));

			return ActionResult.PENDING;
		}

		bool Jump (GridLocation location) {
			LocationPart start = actor.Entity.GetPart<LocationPart> ();
			if (start.Location.Layer != location.Layer
			   || start.Location.Distance (location) > GameRules.CalcJumpingDist (actor)) {
				return false;
			}
			List<GridLocation> locs = GameData.Visibility.GetLOS (start.Location, location);
			locs.Add (location);
			foreach (GridLocation loc in locs) {
				TileTypeInfo ti = Terra.TileInfo [GameData.Map.Get(loc).Type];
				if (ti.IsWall || GameData.SpatialManager.IsAt<MoveBlockingPart> (loc)) {
					return false;
				}
			}
			location = MapUtil.GetFloor (GameData.Map, location);
			if (location == null) {
				return false;
			}
			foreach (GridLocation loc in locs) {
				start.Location = loc;
			}
			
			IEnumerable<ContactEffectPart> effects = GameData.SpatialManager.GetAt<ContactEffectPart> (ContactEffectPart.partId, start.Location);
			if (effects.Any()) {
				foreach( ContactEffectPart eff in effects ) {
					eff.OnContact(actor);
				}
			}
					
			return true;
		}

		public int GetCost () {
			return GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetMouseButtonDown (0)) {
				location = InputUtil.GetMousePosition ( GameData.CameraLocation.Location, Input.mousePosition );
				confirmed = true;
				actor.SetNextAction (this);
				return true;
			}

			Direction direction = InputUtil.HandleDirectionXYInput ();

			if (direction != null) {
				location = direction.Transform (location);
				actor.SetNextAction (this);
				return true;
			} else {
				if (location != null && (Input.GetKeyDown (KeyCode.Return) || InputUtil.GetInputCharacter() == ";")) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				//GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				GameData.EventManager.Notify (new HighlightRangeEvent (actor.Entity, null, 0));
				return true;
			}
			return false;
		}
	}
}
