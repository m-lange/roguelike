﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.ActorEffects.Continuous;
using Game.Parts;
using Game.Events;
using Game.Rules;
using Game.Util;
using Game.Data;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {

	[DataContract]
	public class FollowPathAction : Action {

		static readonly string help = 
			"Go somewhere by the shortest known path.\n"+
			"Use direction keys (Numpad) and enter, or just click with the mouse.\n"+
			"Use this in safe situations only! Although you stop when running into something or being attacked, it is still risky.\n"+
			"Cancel with Esc.";


		Entity entity;
		ActorPart actor;

		long entityId;
		//GridLocation actorLocation;
		GridLocation targetLocation;
		List<GridLocation> path;
		int pathLocation;
		bool confirmed;

		bool isPlayer;
		bool forceUpdate;
		int maxSearchLength;

		int accessLevel;
		bool allowStairs;
		int cost;

		public FollowPathAction(ActorPart actor, GridLocation targetLocation, int accessLevel, bool allowStairs, bool isPlayer, bool forceUpdate, int maxSearchLength) {
			this.entityId = actor.Entity.Id;
			this.targetLocation = targetLocation;
			this.isPlayer = isPlayer;
			this.forceUpdate = forceUpdate;
			confirmed = this.targetLocation != null;

			this.accessLevel = accessLevel;
			this.allowStairs = allowStairs;
			this.maxSearchLength = maxSearchLength;
			this.cost = GameData.IsPlayer (actor) ? GameRules.NoCost : GameRules.DefaultCost;

			/*
			if (actor.Entity.HasPart<LocationPart> ()) {
				actorLocation = actor.Entity.GetPart<LocationPart> ().Location;
			} else {
				actorLocation = null;
			}
			*/
		}

		public ActionResult Execute () {
			entity = GameData.TurnManager.GetEntity (entityId);
			actor = entity.GetPart<ActorPart> (ActorPart.partId);
			GridLocation actorLocation = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;

			if (actor.HasEffect<ConfusionEffect> ()) {
				if (GameData.IsPlayer (actor)) {
					GameData.EventManager.Notify (new MessageEvent (entity, "You are too confused."));
					return ActionResult.FAILURE;
				}
				return ActionResult.SUCCESS;
			}

			if (confirmed && targetLocation != null && path == null) {
				path = GameData.PathManager.FindPath (actorLocation, targetLocation, accessLevel, maxSearchLength, allowStairs, false);
				if (path == null || path.Count < 2) {
					if (GameData.IsPlayer (actor)) {
						path = null;
						targetLocation = actorLocation;
						confirmed = false;
						return ActionResult.PENDING;
					} else {
						return ActionResult.SUCCESS;
					}
				} else {
					if (GameData.IsPlayer (actor)) {
						GameData.EventManager.Notify (new HighlightCellEvent (entity, null));
					}
				}
			}
			if (path != null) {
				if (pathLocation > 0 && !path [pathLocation].Equals (actorLocation)) {
					pathLocation -= 1;
				}
				if (pathLocation < path.Count - 1) {
					GridLocation next = path [pathLocation + 1];
					TileInfo value = GameData.Map.Get (next);
					TileTypeInfo info = Terra.TileInfo [value.Type];
					if (info.IsWallLevel (accessLevel)
					    || GameData.SpatialManager.IsAt<MoveBlockingPart> (MoveBlockingPart.partId, next)
					    || GameData.SpatialManager.IsAt<ActorPart> (ActorPart.partId, next)) {

						if (GameData.IsPlayer (actor)) {
							GameData.EventManager.Notify (new MessageEvent (entity, "You stop."));
							return ActionResult.FAILURE;
						} else {
							path = null;
							pathLocation = 0;
							actor.SetNextAction (this);
							return ActionResult.SUCCESS;
						}
					}
					Direction dir = Direction.GetFromTo (path [pathLocation], next);
					pathLocation += 1;
					actor.SetNextAction (this);
					return ActionResult.Alternative (new WalkAction (actor, dir, true, isPlayer, forceUpdate));
				} else {
					return ActionResult.SUCCESS;
				}
			}



			if (GameData.IsPlayer (actor)) {
				if (targetLocation == null) {
					targetLocation = new GridLocation (actorLocation);
					GameData.EventManager.Notify (new MessageEvent (entity, "Go where? (directions/mouse/?)"));
				}
				GameData.EventManager.Notify (new HighlightCellEvent (entity, targetLocation));
				return ActionResult.PENDING;
			} else {
				return ActionResult.SUCCESS;
			}
		}

		[DataMember]
		public long EntityId {
			get { return entityId; }
			set { entityId = value; }
		}

		[IgnoreDataMember]
		public List<GridLocation> Path {
			get { return path; }
		}

		[DataMember]
		public List<GridLocation> PathForSerialization {
			get { return path == null ? null : path.Select( (l) => new GridLocation(l) ).ToList(); }
			set { path = value == null ? new List<GridLocation>() : value.Select( (l) => new ImmutableGridLocation(l) as GridLocation ).ToList(); }
		}

		[DataMember]
		public int PathLocation {
			get { return pathLocation; }
			set { pathLocation = value; }
		}

		[DataMember]
		public bool IsPlayer {
			get { return isPlayer; }
			set { isPlayer = value; }
		}

		[DataMember]
		public bool ForceUpdate {
			get { return forceUpdate; }
			set { forceUpdate = value; }
		}

		[DataMember]
		public int AccessLevel {
			get { return accessLevel; }
			set { accessLevel = value; }
		}

		[DataMember]
		public bool AllowStairs {
			get { return allowStairs; }
			set { allowStairs = value; }
		}

		[DataMember]
		public int MaxSearchLength {
			get { return maxSearchLength; }
			set { maxSearchLength = value; }
		}

		[DataMember]
		public int Cost {
			get { return cost; }
			set { cost = value; }
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetMouseButtonDown (0)) {
				targetLocation = InputUtil.GetMousePosition (GameData.CameraLocation.Location, Input.mousePosition);
				confirmed = true;
				actor.SetNextAction (this);
				return true;
			}
			Direction direction = InputUtil.HandleDirectionXYInput ();
			if (direction != null) {
				targetLocation = direction.Transform (targetLocation);
				actor.SetNextAction (this);
				return true;
			} else {
				if (targetLocation != null 
					&& (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.F)) ) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				return true;
			}
			return false;
		}

	}
}
