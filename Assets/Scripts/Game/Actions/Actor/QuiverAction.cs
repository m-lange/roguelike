﻿using ComponentSystem;
using Collections;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using Game.Util;

using UnityEngine;

namespace Game.Actions.Actor {
	public class QuiverAction : Action {

		static readonly string help = 
			"Get items at the ready / into the quiver, for convenient throwing/firing.\n"+
			"Quivered items can be fired/thrown directly using 'f' instead of 't'.\n"+
			"Quivered items are marked with '!' in the inventory window.\n"+
			"Cancel with Esc.";

		
		ActorPart actor;
		PickablePart quiver;
		InventoryPart inventory;

		public QuiverAction(ActorPart actor, PickablePart quiver) {
			this.actor = actor;
			this.quiver = quiver;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute() {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			if (inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to quiver!")));
				return ActionResult.FAILURE;
			}

			if (quiver == null) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Quiver what? (a-Z,?)")));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Quiver what?", inventory.ToDict() ) );
				return ActionResult.PENDING;
			} else {
				if (inventory.Quiver (quiver)) {
					string name = TextUtil.GetNameIndef(quiver.Entity);
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("You have {0} at the ready.", name)));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
					return ActionResult.SUCCESS;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Doesn't work.")));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
					return ActionResult.FAILURE;
				}
			}

		}



		public int GetCost () {
			return Game.Rules.GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}

			PickablePart pick = InputUtil.HandleSelectionInput (inventory.ToDict(), null);
			if (pick != null) {
				quiver = pick;
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				return true;
			}
			return false;
		}
	}
}
