﻿
using Game.Data;
using Game.Parts;
using Game.Events;
using Map;
using Map.LayeredGrid;
using TurnSystem;

namespace Game.Actions.Actor {
	public class SearchAction : Action {

		ActorPart actor;

		public SearchAction(ActorPart actor) {
			this.actor = actor;
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}

			LocationPart loc = actor.Entity.GetPart<LocationPart> ();

			for (int i = 0; i < 2; i++) {
				GridLocation searchLoc = RandUtils.Select (Direction.HORIZONTAL).Transform (loc.Location);
				TileInfo info = GameData.Map.Get(searchLoc);
				TileTypeInfo tt = Terra.TileInfo [info.Type];
				if (tt.Id == Terra.DoorHidden) {
					GameData.Map.Set(searchLoc, new TileInfo (Terra.TileInfoId (Terra.DoorClosed)));
					GameData.EventManager.Notify (new MapChangedEvent (searchLoc));
					GameData.EventManager.Notify (new UpdateVisibilityEvent (searchLoc));
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You find a door."));
					break;
				} 
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
