﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Data;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class HarvestAction : Action {

		static readonly string help = 
			"Use this command to harvest from a herb.\n"+
			"This differs from picking up a herb, which makes you dig it out and take the entire plant with you.\n"+
			"In contrast, harvesting takes a tuft from the plant, while leaving the plant itself in place and alive.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		GridLocation location;
		Entity harvest;
		OrderedDict<string, Entity> harvestables;
		InventoryPart inventory;
		bool harvestHere = true;
		bool isForced = false;

		int cost = Game.Rules.GameRules.DefaultCost;


		public HarvestAction(ActorPart actor,  PlantPart harvest) {
			this.actor = actor;
			this.harvest = harvest == null ? null : harvest.Entity;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart<LocationPart> ()) {
				location = actor.Entity.GetPart<LocationPart> ().Location;
			} else {
				location = null;
			}
		}

		public ActionResult Execute () {
			if (location == null) {
				return ActionResult.FAILURE;
			}
			if (harvestHere && harvestables == null) {
				harvestables = new OrderedDict<string, Entity> ();
				int i = 0;
				foreach (PlantPart item in GameData.SpatialManager.GetAt<PlantPart> (location)) {
					if (i >= GlobalParameters.letters.Length) {
						break;
					}
					char c = GlobalParameters.letters [i];
					harvestables [c.ToString ()] = item.Entity;
					i++;
				}
				if (harvestables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("There are plants. Harvest them?")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to harvest?", harvestables, null));
					return ActionResult.PENDING;
				} else {
					harvestHere = false;
				}
			}

			if (harvestables == null && inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to harvest!")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.FAILURE;
			}

			if (harvest == null) {
				harvestables = inventory.FilterEntities<PlantPart> (null);
				if (harvestables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to harvest? (a-Z*?)")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to harvest?", harvestables, null));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to harvest."));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return ActionResult.FAILURE;
				}
			} else {
				if (harvest.HasPart<PlantPart> ()) {
					PlantPart plant = harvest.GetPart<PlantPart> ();
					if ( ! isForced ) {
						if (plant.HarvestRemaining <= 1) {
							GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You will overharvest. Continue? (y/n)"));
							return ActionResult.PENDING;
						}
					}
					Harvest( plant );
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
					return ActionResult.SUCCESS;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't harvest that "+TextUtil.GetName(harvest) ));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
					return ActionResult.FAILURE;
				}
			}
		}

		void Harvest (PlantPart plant) {
			cost = GameRules.CalcHarvestCost (actor, plant);
			Entity tuft = plant.Harvest ();
			if (plant.HarvestRemaining < 1) {
				GameData.EventManager.Notify (new RemoveEntityEvent (plant.Entity, true));
			}
			inventory.AddItem (tuft.GetPart<PickablePart> ());
			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You harvest the " + TextUtil.GetName (plant) + "."));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				Sounds.Play( Sounds.Harvest );
			}
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				//actor.SetNextAction (this);
				return false;
			}
			if (harvest != null && !isForced) {
				if (Input.GetKeyDown (KeyCode.Y)) {
					isForced = true;
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.N)) {
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return true;
				}
				return false;
			} else {
				Entity dropItem = InputUtil.HandleSelectionInput (harvestables, null);
				if (dropItem != null) {
					harvest = dropItem;
					actor.SetNextAction (this);
					return true;
				}
				if (Input.GetKeyDown (KeyCode.Escape)) {
					if (harvestHere) {
						harvestHere = false;
						actor.SetNextAction (this);
					} else {
						GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					}
					return true;
				}
			}
			return false;
		}

	}
}
