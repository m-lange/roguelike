﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using UnityEngine;

using Collections;
using Map.LayeredGrid;
using TurnSystem;
using Game.Parts;
using Game.Util;
using Game.Events;

namespace Game.Actions.Actor {


	[DataContract]
	public class LootThisAction : Action {
		static readonly int UNKNOWN = 0;
		static readonly int PUT_IN = 1;
		static readonly int TAKE_OUT = 2;


		ActorPart actor;
		InventoryPart lootInventory;
		InventoryPart inventory;


		int inOrOut;
		OrderedDict<string, PickablePart> wares;
		PickablePart ware;
		string countInput = "";


		public LootThisAction(ActorPart actor, InventoryPart loot) {
			this.actor = actor;
			this.lootInventory = loot;
			this.ware = null;
			this.inOrOut = UNKNOWN;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}


			if (ware != null) {
				int cnt = countInput.Length == 0 ? -1 : int.Parse (countInput);
				if (Exchange (ware, cnt)) {
					//confirmed = false;
					ware = null;
					countInput = "";
					inOrOut = UNKNOWN;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Unable to move the "+TextUtil.GetName(ware)));
				}
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Put in or take out? (i/o)"));
				return ActionResult.PENDING;
			}

			if (inOrOut == UNKNOWN) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Put in or take out? (i/o)"));
					return ActionResult.PENDING;
			} else if (inOrOut == TAKE_OUT) {
				if (WaresToTakeOut ()) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "What to take out?"));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to take out?", wares));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to take out."));
					return ActionResult.FAILURE;
				}
			} else if (inOrOut == PUT_IN) {
				if (WaresToPutIn ()) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "What to put in?"));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to put in?", wares));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to put in."));
					return ActionResult.FAILURE;
				}
			} else {
				return ActionResult.FAILURE;
			}
		}



		bool Exchange (PickablePart ware, int count) {
			bool putIn = inOrOut == PUT_IN;
			if (ware.Entity.HasPart<StackablePart> ()) {
				StackablePart stack = ware.Entity.GetPart<StackablePart> ();
				if (count > stack.Count || count <= 0) {
					count = stack.Count;
				}
			} else {
				count = 1;
			}
			if (putIn) {
				if (lootInventory.CanAdd (ware, count)) {
					PickablePart pick = inventory.RemoveItem (ware, count);
					GameData.EventManager.Notify (new RemoveEntityEvent (pick.Entity, false)); 
					lootInventory.AddItem (pick);
					return true;
				} else {
					return false;
				}
			} else {
				if (inventory.CanAdd (ware, count)) {
					PickablePart pick = lootInventory.RemoveItem (ware, count);
					GameData.EventManager.Notify (new RemoveEntityEvent (pick.Entity, false)); 
					inventory.AddItem (pick);
					return true;
				} else {
					return false;
				}
			}
		}


		bool WaresToTakeOut () {
			wares = lootInventory.ToDict( lootInventory.Filter( (p) => true ) );
			return wares.Count > 0;
		}

		bool WaresToPutIn () {
			wares = inventory.ToDict( inventory.Filter( (p) => p.Entity != lootInventory.Entity && ! p.Entity.HasPart(BulkyPart.partId) ) );
			return wares.Count > 0;
		}
		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (inOrOut == UNKNOWN) {
				if (Input.GetKeyDown (KeyCode.I)) {
					inOrOut = PUT_IN;
					actor.SetNextAction (this);
					return true;
				} else if (Input.GetKeyDown (KeyCode.O)) {
					inOrOut = TAKE_OUT;
					actor.SetNextAction (this);
					return true;
				}
			} else if (ware == null) {
				string input = InputUtil.GetInputCharacter ();
				if (input != null
				    && countInput.Length < 6
				    && input.Length > 0 && GlobalParameters.numbers.Contains (input [0])) {
					countInput += input [0].ToString ();
				}

				PickablePart pick = InputUtil.HandleSelectionInput (wares, null);
				if (pick != null) {
					ware = pick;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return true;
			}
			return false;
		}
	}
}
