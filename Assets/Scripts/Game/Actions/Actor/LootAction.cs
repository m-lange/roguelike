﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Game.Data;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class LootAction : Action {

		static readonly string help = 
			"Loot a container (take things out or put in).\n"+
			"The container can be at your feet, on in your inventory.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		GridLocation location;
		Entity loot;
		OrderedDict<string, Entity> lootables;
		InventoryPart inventory;
		bool lootHere = true;

		int cost = Game.Rules.GameRules.DefaultCost;


		public LootAction(ActorPart actor) {
			this.actor = actor;
			this.loot = null;
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			if (actor.Entity.HasPart<LocationPart> ()) {
				location = actor.Entity.GetPart<LocationPart> ().Location;
			} else {
				location = null;
			}
		}

		public ActionResult Execute () {
			if (actor.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to loot!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}

			if (location == null) {
				return ActionResult.FAILURE;
			}
			if (lootHere && lootables == null) {
				lootables = new OrderedDict<string, Entity> ();
				int i = 0;
				foreach (InventoryPart item in GameData.SpatialManager.GetAt<InventoryPart> (location)) {
					if (i >= GlobalParameters.letters.Length) {
						break;
					}
					if (item.Entity.HasPart<ContainerPart> ()) {
						char c = GlobalParameters.letters [i];
						lootables [c.ToString ()] = item.Entity;
						i++;
					}
				}
				if (lootables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("There are containers. Loot them?")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to loot?", lootables, null));
					return ActionResult.PENDING;
				} else {
					lootHere = false;
				}
			}

			if (lootables == null && inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to loot!")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.FAILURE;
			}

			if (loot == null) {
				lootables = inventory.FilterEntities<InventoryPart> ( (e) => e.HasPart<ContainerPart>() );
				if (lootables.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to loot? (a-Z*?)")));
					GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "What to loot?", lootables, null));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to loot."));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return ActionResult.FAILURE;
				}
			} else {
				if (loot.HasPart<ContainerPart> ()) {
					if (loot.GetPart<ContainerPart> ().IsLocked) {
						GameData.EventManager.Notify (new MessageEvent (actor.Entity, "That "+TextUtil.GetName(loot)+" is locked." ));
						GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
						Sounds.Play (Sounds.DoorLocked);
						return ActionResult.FAILURE;
					}
				}
				if (loot.HasPart<InventoryPart> ()) {
					InventoryPart invent = loot.GetPart<InventoryPart> ();
					return ActionResult.Alternative(new LootThisAction(actor, invent));
				} 

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't loot that "+TextUtil.GetName(loot) ));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return ActionResult.FAILURE;
			}
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				//actor.SetNextAction (this);
				return false;
			}
			Entity dropItem = InputUtil.HandleSelectionInput (lootables, null);
			if (dropItem != null) {
				loot = dropItem;
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (lootHere) {
					lootHere = false;
					actor.SetNextAction (this);
				} else {
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				}
				return true;
			}
			return false;
		}

	}
}
