﻿using System.Collections.Generic;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class LookThereAction : Action {

		static readonly string help = 
			"Use direction keys or mouse to navigate.\n"+
			"Press return or ; to confirm.\n"+
			"Cancel with Esc.";


		ActorPart actor;
		GridLocation location;
		bool confirmed;

		public LookThereAction(ActorPart actor, GridLocation location) {
			this.actor = actor;
			this.location = location;
			confirmed = this.location != null;
		}

		public ActionResult Execute() {
			if (confirmed && location != null) {

				if ((!GameData.Visibility.Visible.Contains (location)) 
				|| !GameData.Visibility.Visible.IsCenterVisible (location)) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Can't see there!"));
					GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
					return ActionResult.FAILURE;
				}


				List<Entity> entities = new List<Entity>();
				List<string> items = new List<string>();

				foreach (NamePart item in GameData.SpatialManager.GetAt<NamePart> (location)) {
					items.Add (TextUtil.GetNameIndef( item ));
					entities.Add (item.Entity);
				}
				string text = "Nothing there";
				if (items.Count > 0) {
					GameData.EventManager.Notify (new ShowEntityListEvent (actor.Entity, "You see:", entities, null));
					if (items.Count == 1) {
						text = "You see " + items [0] + ".";
					} else {
						string lastItem = items [items.Count - 1];
						items.RemoveAt (items.Count - 1);
						text = "You see " + string.Join (", ", items.ToArray ()) + " and " + lastItem + ".";
					}
				} else {
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				}
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, text));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));

				return ActionResult.SUCCESS;
			}
			if (location == null) {
				if (!actor.Entity.HasPart<LocationPart> ()) {
					return ActionResult.FAILURE;
				}
				LocationPart loc = actor.Entity.GetPart<LocationPart> ();
				location = loc.Location;

				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Look where? (directions/mouse/?)"));
			}
			GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, location));


			return ActionResult.PENDING;
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}

			if (Input.GetMouseButtonDown (0)) {
				location = InputUtil.GetMousePosition ( GameData.CameraLocation.Location, Input.mousePosition );
				confirmed = true;
				actor.SetNextAction (this);
				return true;
			}

			Direction direction = InputUtil.HandleDirectionXYInput ();

			if (direction != null) {
				location = direction.Transform (location);
				actor.SetNextAction (this);
				return true;
			} else {
				if (location != null && (Input.GetKeyDown (KeyCode.Return) || InputUtil.GetInputCharacter() == ";")) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
			}

			if (Input.GetKeyDown (KeyCode.N) || Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", null));
				GameData.EventManager.Notify (new HighlightCellEvent (actor.Entity, null));
				return true;
			}
			return false;
		}
	}
}
