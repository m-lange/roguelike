﻿using System.Linq;
using System.Collections.Generic;
using ComponentSystem;
using Game.ActorEffects;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.ActorEffects.Continuous;
using Game.Data;
using Game.Parts;
using Game.Events;
using Game.Rules;
using Game.Util;
using UnityEngine;

namespace Game.Actions.Actor {
	public class WalkAction : Action {

		ActorPart actor;
		Direction direction;
		GridLocation updateLocation = null;
		bool isPlayer;
		bool forceUpdate;
		bool allowOpenDoor;
		bool confirmed;

		int cost = GameRules.DefaultCost;

		public WalkAction(ActorPart actor, Direction direction, bool allowOpenDoor, bool isPlayer, bool forceUpdate) {
			this.actor = actor;
			this.direction = direction;
			this.forceUpdate = forceUpdate;
			this.isPlayer = isPlayer;
			this.allowOpenDoor = allowOpenDoor;
			this.confirmed = false;
		}

		public ActionResult Execute () {
			LocationPart locPart = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
			GridLocation oldPos = locPart.Location;
			GridLocation newPos = new GridLocation (oldPos);


			LayeredGrid<TileInfo> grid = GameData.Map;

			if (GameData.IsPlayer (actor)) {
				ConfusionEffect confusion = actor.GetEffect<ConfusionEffect> ();
				if (confusion != null && RandUtils.NextBool (0.01f * confusion.WrongMoveProb)) {
					direction = RandUtils.Select (Direction.HORIZONTAL);
				}
			}

			if (direction == Direction.UP) {
				GridLink link = grid.GetLink (oldPos);
				if (link != null && link.Target.Layer > oldPos.Layer) {
					newPos.Set (link.Target);
					//if(info.IsStairsUp) {
					//	newPos.Layer += 1;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "There are no stairs up!")); 
					return ActionResult.FAILURE;
				}
			} else if (direction == Direction.DOWN) {
				GridLink link = grid.GetLink (oldPos);
				if (link != null && link.Target.Layer < oldPos.Layer) {
					newPos.Set (link.Target);
					//if(info.IsStairsDown) {
					//	newPos.Layer -= 1;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "There are no stairs down!")); 
					return ActionResult.FAILURE;
				}
			} else {
				newPos.X += direction.dx;
				newPos.Y += direction.dy;
			}



			// Blocked?
			bool blocked = false;
			if (GameData.SpatialManager.IsAt<MoveBlockingPart> (MoveBlockingPart.partId, newPos)) {
				blocked = true;
				// By Actor?
				ActorPart enemy = GameData.SpatialManager.OneAt<ActorPart> (ActorPart.partId, newPos);
				if (enemy == null) {
					return isPlayer ? ActionResult.FAILURE : ActionResult.SUCCESS;
				} else {
					//UnityEngine.Debug.Log("Attack action alternative.");
					return ActionResult.Alternative (new AttackAction (actor, enemy, isPlayer));
				}
			}

			
			if (actor.HasEffect<StickToEntityEffect> ()) {
				if (GameData.IsPlayer (actor)) {
					StickToEntityEffect eff = actor.GetEffect<StickToEntityEffect> ();
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You are immobilized by "+eff.EntityName+"!"));
				}
				return ActionResult.SUCCESS;
			}

			TileInfo value = grid [newPos];
			if (GameData.Map.IsNoData (value) && !GameData.IsPlayer (actor)) {
				return ActionResult.SUCCESS;
			}

			TileTypeInfo info = Terra.TileInfo [value.Type];
			if ((grid.IsNoData (value) || info.IsWalkable) && (!blocked)) {
				GridLocation floorPos = MapUtil.GetFloor (grid, newPos);
				if (floorPos != null) {
					if (floorPos.Layer != newPos.Layer && (! confirmed) && GameData.IsPlayer(actor)) {
						GameData.EventManager.Notify( new MessageEvent(actor.Entity, "Really jump down? (y/n)") );
						return ActionResult.PENDING;
					}
					if (forceUpdate) {
						updateLocation = new GridLocation (newPos);
					}
					locPart.Location = floorPos;
					if (direction == Direction.NE || direction == Direction.SE
					    || direction == Direction.NW || direction == Direction.SW) {
						cost = Game.Rules.GameRules.DiagonalCost;
					}
					if (floorPos.Layer != newPos.Layer) {
						DamageFallen (newPos.Layer - floorPos.Layer);
						if (GameData.IsPlayer (actor)) {
							Sounds.Play(Sounds.Ouch);
						}
					}
					
					IEnumerable<ContactEffectPart> effects = GameData.SpatialManager.GetAt<ContactEffectPart> (ContactEffectPart.partId, floorPos);
					if (effects.Any()) {
						foreach( ContactEffectPart eff in effects ) {
							eff.OnContact(actor);
						}
					}
					
					return ActionResult.SUCCESS;
				}
			} else if(allowOpenDoor && 
					(info.Id == Terra.DoorClosed || info.Id == Terra.DoorLocked 
					|| info.Id == Terra.WindowClosed|| info.Id == Terra.WindowLocked)) {
				return ActionResult.Alternative( new OpenAction(actor, direction) );
			}

			return isPlayer ? ActionResult.FAILURE : ActionResult.SUCCESS;
			//return ActionResult.FAILURE;
		}

		void DamageFallen (int height) {
			if (actor.Entity.HasPart (HealthPointsPart.partId)) {
				bool isPlayer = GameData.IsPlayer(actor);
				int damage = GameRules.CalcDamageFallen (actor, height);
				if (isPlayer) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("You hit the ground.")));
				}
				if (actor.Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId).DealDamage (damage)
					&& ! isPlayer) {
					GameData.EventManager.Notify (new RemoveEntityEvent (actor.Entity, true));
				}
			}
		}

		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return updateLocation;
		}

		public bool HandleUserInput () {
			/*
			string inp = InputUtil.GetInputCharacter ();
			if (inp != null) {
				if (inp == "y") {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				} else if (inp == "n") {
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				return true;
			}
			return false;
			*/
			if (!confirmed) {
				if (Input.GetKeyDown (KeyCode.Y)) {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				}
				if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.N)) {
					return true;
				}
			}
			return false;
		}
	}
}
