﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Game.Data;
using Game.Factories;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class ReadBookAction : Action {

		static readonly string help = 
			"First, select a book to read.\n"+
			"You can then read through the chapters (select with a, b, ...).\n"+
			"Cancel/finish with Esc.";

		ActorPart actor;

		PickablePart book;
		BookPart bookPart;
		OrderedDict<string, PickablePart> books;
		InventoryPart inventory;

		int bookPage = -1;

		public ReadBookAction(ActorPart actor) {
			this.actor = actor;
			this.inventory = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
		}

		public ActionResult Execute () {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}

			if (books == null && inventory.Items.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to read!")));
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return ActionResult.FAILURE;
			}

			if (book == null) {
				books = inventory.ToDict( inventory.Filter<PickablePart> ((p) => p.Entity.HasPart (BookPart.partId)) );
				if (books.Count > 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("What to read? (a-Z,?)")));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "What to read?", books));
					return ActionResult.PENDING;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Nothing to read."));
					GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
					return ActionResult.FAILURE;
				}
			} else {
				//GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, Read(book, inventory), books, null, book) );
				//book = null;
				if (bookPage < 0) {
					GameData.Identify(book.Entity);
					bookPart = book.Entity.GetPart<BookPart> (BookPart.partId);

					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Which chapter?"));
					GameData.EventManager.Notify (new ShowTextSelectionEvent (actor.Entity, "Which chapter?", bookPart.DisplayedNames, null));
				} else {
					ReadPage( bookPage );
				}
				return ActionResult.PENDING;
			}
		}


		void ReadPage (int page) {
			string text = "";
			Entity selected = null;
			for (int i = page * bookPart.CorrelatedEntries; i < (page + 1) * bookPart.CorrelatedEntries; i++) {
				Entity e = EntityFactory.GetCreator (bookPart.ContentGenerators [i]) (new GridLocation (0, 0, 0), false);
				text += TextUtil.EntityInfo(actor, e, true);
				if(selected == null) selected = e;
			}
			GameData.EventManager.Notify (new ShowTextSelectionEvent (actor.Entity, text, bookPart.DisplayedNames, selected));
				
		}

		public int GetCost () {
			return Game.Rules.GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}
			if (book == null) {
				PickablePart dropItem = InputUtil.HandleSelectionInput (books, null);
				if (dropItem != null) {
					book = dropItem;
					actor.SetNextAction (this);
					return true;
				}
			} else {
				if (bookPage >= 0 && Input.GetKeyDown (KeyCode.Escape)) {
					bookPage = -1;
					actor.SetNextAction (this);
					return true;
				}
				
				bookPage = InputUtil.HandleIndexSelectionInput (bookPart.DisplayedNames);
				if (bookPage >= 0) {
					actor.SetNextAction (this);
					return true;
				}

			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return true;
			}
			return false;
		}

	}
}
