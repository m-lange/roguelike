﻿using ComponentSystem;
using Collections;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using Game.Util;

using UnityEngine;

namespace Game.Actions.Actor {
	public class UnequipAction : Action {

		static readonly string help = 
			"Unequip a weapon, a piece of armor etc.\n"+
			"Equiped items are marked by '+' in the inventory window.\n"+
			"Cancel with Esc.";


		ActorPart actor;
		EquipmentPart equip;
		InventoryPart inventory;
		OrderedDict<string, Entity> equipment;

		public UnequipAction(ActorPart actor, EquipmentPart equip) {
			this.actor = actor;
			this.equip = equip;

			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
				equipment = inventory.FilterEntities( 
					(e) => e.HasPart<EquipmentPart>() && e.GetPart<EquipmentPart>().IsEquiped );
			} else {
				inventory = null;
				equipment = null;
			}
		}

		public ActionResult Execute() {

			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			if (equipment.Count == 0) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to unequip!")));
				return ActionResult.FAILURE;
			}

			if (equip == null) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Unequip what? (a-Z,?)")));
				GameData.EventManager.Notify (new ShowEntitySelectionEvent(actor.Entity, "Unequip what?", equipment, null ) );
				return ActionResult.PENDING;
			} else {
				if (inventory.UnEquip (equip)) {
					string name = TextUtil.GetNameIndef(equip.Entity);
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("You unequip {0}.", name)));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
					return ActionResult.SUCCESS;
				} else {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Doesn't work.")));
					GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
					return ActionResult.FAILURE;
				}
			}

		}



		public int GetCost () {
			return Game.Rules.GameRules.DefaultCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}

			Entity pick = InputUtil.HandleSelectionInput (equipment, null);
			if (pick != null) {
				equip = pick.GetPart<EquipmentPart> ();
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				return true;
			}
			return false;
		}
	}
}
