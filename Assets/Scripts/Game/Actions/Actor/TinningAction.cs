﻿using ComponentSystem;
using Game;
using Game.Data;
using Game.Parts;
using Game.Events;
using Game.Util;
using Game.Rules;
using Game.Factories;
using Map;
using Map.LayeredGrid;
using TurnSystem;

namespace Game.Actions.Actor {
	public class TinningAction : Action {

		ActorPart actor;
		TinnablePart target;
		Entity tool;

		public TinningAction(ActorPart actor, TinnablePart target, Entity tool) {
			this.actor = actor;
			this.target = target;
			this.tool = tool;
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}

			LocationPart loc = actor.Entity.GetPart<LocationPart> ();

			GameData.EventManager.Notify (new RemoveEntityEvent (target.Entity, true));
			//Entity tinned = 
			ConsumablesFactory.CreateTinned (loc.Location, true, target);
			if (GameData.IsPlayer (actor)) {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You tin the " + TextUtil.GetName (target) + "."));
				Sounds.PlayNow( Sounds.Tinning );
			}

			if (tool != null) {
				GameRules.LimitedUseDamage(actor, tool, GameRules.ToolDamageProb, true);
			}

			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return Game.Rules.GameRules.CalcTinningCost(actor);
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
