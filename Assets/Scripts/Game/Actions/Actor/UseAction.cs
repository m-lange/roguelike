﻿using UnityEngine;

using Collections;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Effects;
using Game.Usables;
using Game.Rules;
using Game.Util;

namespace Game.Actions.Actor {
	public class UseAction : Action {

		static readonly string help = 
			"First, select a 'tool' to use/apply from the list.\n"+
			"If the 'tool' is for use with another item (e.g. a tin opener), you can then select the other item, or a diretion (NumPad) in which to use the tool.\n"+
			"Cancel with Esc.";

		ActorPart actor;
		UsablePart usable;
		InventoryPart inventory;
		OrderedDict<string, PickablePart> usables;

		GridLocation requiresUpdate = null;
		int cost = Game.Rules.GameRules.DefaultCost;

		public UseAction ( ActorPart actor, UsablePart usable ) {
			this.actor = actor;
			this.usable = usable;

			if (usable == null) {
				if (actor.Entity.HasPart<InventoryPart> ()) {
					inventory = actor.Entity.GetPart<InventoryPart> ();
					usables = inventory.ToDict( inventory.Filter ((p) => p.Entity.HasPart<UsablePart> ()) );
				} else {
					inventory = null;
					usables = null;
				}
			}
		}

		public ActionResult Execute() {
			if (actor.Stats.BurdeningStatus >= GameRules.BurdeningStatus.Overtaxed) {
				if( GameData.IsPlayer(actor) ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "You carry too much to use a tool!"));
					return ActionResult.FAILURE;
				} else {
					return ActionResult.SUCCESS;
				}
			}

			if (usable == null) {
				if (inventory == null) {
					return ActionResult.FAILURE;
				}
				if (usables.Count == 0) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Nothing to apply!")));
					return ActionResult.FAILURE;
				}
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, string.Format ("Use what? (a-Z)")));
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Use what?", usables ) );
				return ActionResult.PENDING;
			} else {
				if (usable.Usable.IsTargetless) {
					return ApplyTargetless ();
				} else {
					return ActionResult.Alternative (new UseWithAction (actor, usable, null, null));
				}
			}
			//return ActionResult.SUCCESS;
		}


		ActionResult ApplyTargetless() {
			Action action = usable.GetAction(actor, usable.Entity, null, null, null);
			GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict() ) );
			if (action != null) {
				return ActionResult.Alternative(action);
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
		/*
			UseEffectRule rule = null;
			Effect effect = usable.GetEffect (actor, usable.Entity, null, null, null, out rule);
			Action action = null;
			if (effect != null) {
				cost = rule.Cost;
				GridLocation update = null;
				action = effect.Execute (out update);
				if (update != null) {
					requiresUpdate = update;
				}
				if (action != null) {
					return ActionResult.Alternative (action);
				} else {
					return ActionResult.SUCCESS;
				}
			} else {
				GameData.EventManager.Notify ( new MessageEvent(actor.Entity, "Nothing happens.") );
				return ActionResult.FAILURE;
			}
			*/
		}



		public int GetCost () {
			return cost;
		}

		public GridLocation RequiresUpdate () {
			return requiresUpdate;
		}

		public bool HandleUserInput() {
			if (InputUtil.GetInputCharacter () == "?") {
				GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, help));
				return false;
			}

			PickablePart pick = InputUtil.HandleSelectionInput (usables, null);
			if (pick != null) {
				usable = pick.Entity.GetPart<UsablePart> ();
				actor.SetNextAction (this);
				return true;
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
				return true;
			}
			return false;
		}
	}
}
