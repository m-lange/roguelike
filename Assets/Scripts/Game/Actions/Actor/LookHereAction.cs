﻿using System.Collections.Generic;
using Map.LayeredGrid;
using TurnSystem;
using ComponentSystem;
using Game.Parts;
using Game.Events;
using Game.Util;
using Collections;

using UnityEngine;

namespace Game.Actions.Actor {
	public class LookHereAction : Action {

		ActorPart actor;

		public LookHereAction(ActorPart actor) {
			this.actor = actor;
		}

		public ActionResult Execute() {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}
			LocationPart loc = actor.Entity.GetPart<LocationPart> ();
			List<Entity> entities = new List<Entity>();
			List<string> items = new List<string>();

			foreach (NamePart item in GameData.SpatialManager.GetAt<NamePart> (loc.Location)) {
				items.Add (TextUtil.GetNameIndef( item ));
				entities.Add (item.Entity);
			}
			string text = "Nothing here";
			if(items.Count > 0) {
				GameData.EventManager.Notify (new ShowEntityListEvent (actor.Entity, "You see:", entities, null));
				if (items.Count == 1) {
					text = "You see " + items[0]+".";
				} else {
					string lastItem = items [items.Count - 1];
					items.RemoveAt (items.Count - 1);
					text = "You see " + string.Join (", ", items.ToArray ()) + " and "+lastItem+".";
				}
			} else {
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "", null));
			}
			GameData.EventManager.Notify (new MessageEvent (actor.Entity, text));

			return ActionResult.SUCCESS;
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
