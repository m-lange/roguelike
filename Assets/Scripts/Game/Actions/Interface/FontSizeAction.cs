﻿
using ComponentSystem;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;

namespace Game.Actions.Interface {
	public class FontSizeAction : Action {

		bool increase;

		public FontSizeAction(bool increase) {
			this.increase = increase;
		}

		public ActionResult Execute () {
			GameData.EventManager.Notify ( new ChangeFontSizeEvent(increase) );
			return ActionResult.SUCCESS;
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			return true;
		}
		
	}
}
