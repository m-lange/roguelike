﻿
using ComponentSystem;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;

namespace Game.Actions.Interface {
	public class CameraLayerAction : Action {

		Entity camera;
		Direction direction;

		public CameraLayerAction(Entity camera, Direction direction) {
			this.camera = camera;
			this.direction = direction;
		}

		public ActionResult Execute() {
			if (direction == Direction.UP) {
				LocationPart locPart = camera.GetPart<LocationPart> (LocationPart.partId);
				if (locPart.Location.Layer < GameData.Map.TopLayer) {
					locPart.Location = new ImmutableGridLocation(locPart.Location.Layer + 1, locPart.Location.X, locPart.Location.Y);
				}
			} else if (direction == Direction.DOWN) {
				LocationPart locPart = camera.GetPart<LocationPart> (LocationPart.partId);
				if (locPart.Location.Layer > GameData.Map.BottomLayer) {
					locPart.Location = new ImmutableGridLocation(locPart.Location.Layer - 1, locPart.Location.X, locPart.Location.Y);
				}
			}
			return ActionResult.SUCCESS;
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			return true;
		}
		
	}
}
