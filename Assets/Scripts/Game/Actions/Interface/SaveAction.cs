﻿
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Serialization;

namespace Game.Actions.Interface {
	public class SaveAction : Action {

		bool isSavePoint;

		public SaveAction(bool isSavePoint) {
			this.isSavePoint = isSavePoint;
		}

		public ActionResult Execute () {
			GameData.SaveGame (isSavePoint);
			if (!isSavePoint) {
				GameData.EventManager.Notify (new MessageEvent (null, "Game saved."));
			}
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
