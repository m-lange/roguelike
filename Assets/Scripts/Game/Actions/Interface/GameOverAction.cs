﻿using UnityEngine;

using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Events;
using Game.Serialization;
using Game.Parts;

namespace Game.Actions.Interface {
	public class GameOverAction : Action {

		ActorPart actor;
		string message;
		bool confirmed;

		public GameOverAction( ActorPart actor, string message ) {
			this.actor = actor;
			this.message = message;
			this.confirmed = false;
		}

		public ActionResult Execute() {
			if (confirmed) {
				GameData.EventManager.Notify (new QuitGameEvent (true));
				return ActionResult.SUCCESS;
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, message+"\nPress Return to quit."));
				GameData.EventManager.Notify( new ShowWarningEvent(actor.Entity, "You die!") );
				Sounds.Play(Sounds.Evil);
				return ActionResult.PENDING;
			}
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (Input.GetKeyDown(KeyCode.Return)) {
				confirmed = true;
				actor.SetNextAction (this);
				return true;
			}
			return false;
		}
	}
}
