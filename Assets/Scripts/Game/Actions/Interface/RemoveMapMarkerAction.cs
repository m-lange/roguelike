﻿using System.Linq;
using System.Collections.Generic;
using ComponentSystem;
using Game.Factories;
using Game.Util;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using UnityEngine;

namespace Game.Actions.Interface {
	public class RemoveMapMarkerAction : Action {

		ActorPart actor;

		public RemoveMapMarkerAction(ActorPart actor) {
			this.actor = actor;
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}
			
			GridLocation pos = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			IEnumerable<MapMarkerPart> markers = GameData.SpatialManager.GetAt<MapMarkerPart>(pos);
			
			if( markers.Any() ) {
				int i = 0;
				foreach( MapMarkerPart m in markers ) {
					GameData.EventManager.Notify (new RemoveEntityEvent (m.Entity, true));
					i += 1;
				}
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "Removed "+i+" marker(s)." ));
				return ActionResult.SUCCESS;
			} else {
				GameData.EventManager.Notify (new MessageEvent (actor.Entity, "No markers to remove." ));
				return ActionResult.SUCCESS;
			}
		}
		
		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			return true;
		}
		
	}
}
