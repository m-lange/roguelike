﻿using ComponentSystem;
using Map.LayeredGrid;
using TurnSystem;
using Game.Parts;
using Game.Events;

namespace Game.Actions.Interface {
	public class ShowActorStatsAction : Action {

		ActorPart actor;

		public ShowActorStatsAction(ActorPart actor) {
			this.actor = actor;
		}

		public ActionResult Execute() {
			GameData.EventManager.Notify (new ShowActorStatsEvent (actor, null));
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
