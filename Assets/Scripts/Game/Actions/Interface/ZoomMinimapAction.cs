﻿
using ComponentSystem;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;

namespace Game.Actions.Interface {
	public class ZoomMinimapAction : Action {

		Entity player;
		bool zoomIn;

		public ZoomMinimapAction(Entity player, bool zoomIn) {
			this.player = player;
			this.zoomIn = zoomIn;
		}

		public ActionResult Execute () {
			if (!player.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}

			//GridLocation loc = player.GetPart<LocationPart> ().Location;
			int zoom = GameData.MiniMap.Zoom;
			if (zoomIn && zoom < 6) {
				GameData.MiniMap.SetZoom(zoom + 1);
				return ActionResult.SUCCESS;
			} else 
			if ( (!zoomIn) && zoom > 1) {
				GameData.MiniMap.SetZoom(zoom - 1);
				return ActionResult.SUCCESS;
			}
			return ActionResult.FAILURE;
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			return true;
		}
		
	}
}
