﻿using Game.Factories;
using UnityEngine;
using ComponentSystem;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Serialization;
using Game.Parts;
using Game.Util;

namespace Game.Actions.Interface {
	public class SpecialCommandAction : Action {

		ActorPart actor;
		string command;
		bool confirmed;

		public SpecialCommandAction( ActorPart actor, string command ) {
			this.actor = actor;
			this.command = command == null ? "" : command;
			this.confirmed = false;
		}

		public ActionResult Execute () {
			if (confirmed) {
				//GameData.EventManager.Notify (new MessageEvent (null, "Executing "+command));
				ProcessCommand();
				return ActionResult.SUCCESS;
			} else {
				if (command.Length == 0) {
					GameData.EventManager.Notify (new MessageEvent (null, "Enter command"));
					GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "#" ) );
				} else {
					GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "#"+command ) );
				}
				return ActionResult.PENDING;
			}
		}

		void ProcessCommand () {
			string message = "Unknown command #" + command;
			if (command == "reveal") {
				GameData.RevealMap ();
				message = "You reveal the map.";
			} else if (command == "immortal") {
				GameData.SetPlayerImmortal ();
				message = "You are immortal.";
			} else if (command == "gold") {
				GiveGold ();
				message = "You are rich.";
			} else if (command == "debug") {
				GameData.IsDebugMode = !GameData.IsDebugMode;
				if (GameData.IsDebugMode) {
					message = "Debug mode enabled.";
				} else {
					message = "Debug mode disabled.";
				}
			} else if (command.StartsWith("create ")) {
				string name = command.Remove(0, 7);
				Entity e = CreateItem( name );
				if(e != null) {
					message = "You create "+TextUtil.GetNameIndef(e, false);
				} else {
					message = "Entity type not found: "+name;
				}
			}
			GameData.EventManager.Notify (new MessageEvent (actor.Entity, message));
			GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, ""));
		}

		void GiveGold () {
			if (actor.Entity.HasPart<InventoryPart> ()) {
				InventoryPart inv = actor.Entity.GetPart<InventoryPart> ();
				Entity gold = Game.Factories.ItemFactory.CreateGold1000( actor.Entity.GetPart<LocationPart>().Location, false );
				inv.AddItem(gold.GetPart<PickablePart>());
			}
		}
		
		Entity CreateItem(string name) {
			if( EntityFactory.HasCreator(name) ) {
				GridLocation loc = actor.Entity.GetPart<LocationPart>(LocationPart.partId).Location;
				Entity e = EntityFactory.GetCreator(name) (loc, true);
				//GameData.EventManager.Notify( new CreateEntityEvent(e) );
				return e;
			} else {
				return null;
			}
		}

		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput () {
			string inp = InputUtil.GetInputCharacter ();
			if (inp != null && inp.Length > 0 && (inp[0] == ' ' || GlobalParameters.letterSet.Contains (inp [0]))) {
				command += inp [0].ToString ();
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Backspace) && command.Length > 0) {
				command = command.Substring(0, command.Length-1);
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Return)) {
				confirmed = true;
				actor.SetNextAction(this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "" ) );
				return true;
			}
			return false;
		}
	}
}
