﻿using System.Linq;
using System.Collections.Generic;
using Collections;
using ComponentSystem;
using Game.Factories;
using Game.Util;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using UnityEngine;

namespace Game.Actions.Interface {
	public class InspectMapMarkersAction : Action {

		ActorPart actor;
		OrderedDict<string, Entity> markers;
		Entity marker;
		InventoryPart inventory;

		public InspectMapMarkersAction(ActorPart actor) {
			this.actor = actor;
			this.inventory = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
			marker = null;
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}
			
			GridLocation pos = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			if(markers == null) {
				markers = GetMarkers(pos);
				if( markers.Count == 0 ) {
					GameData.EventManager.Notify (new MessageEvent (actor.Entity, "No markers on map." ));
					return ActionResult.SUCCESS;
				}
			}
			
			if(marker == null) {
				GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, "Select a marker", markers, null));
				return ActionResult.PENDING;
			} else {
				ShowMarker(marker);
				return ActionResult.PENDING;
			}
		}
		
		void ShowMarker(Entity marker) {
			string text = TextUtil.MapMarkerInfo(actor, marker)+TextUtil.NewLine;
			GameData.EventManager.Notify (new ShowEntitySelectionEvent (actor.Entity, text, markers, null));
			LocationPart loc = marker.GetPart<LocationPart>(LocationPart.partId);
			
			GameData.MiniMap.UpdateLocation(loc.Location);
			
			//MiniMapPart mini = marker.GetPart<MiniMapPart>(MiniMapPart.partId);
			//mini.MapColor = EntityFactory.MapMarkerHighlightColor;
			
			marker.Detach<VisualPart>( marker.GetPart<VisualPart>(VisualPart.partId) );
			VisualPart vis = new VisualPart ("Items/Item", "Visuals/MapMarkerHighlightTex", false, true);
			marker.Attach (vis);
			vis.InitTexture();
			
			//GameData.EventManager.Notify (new UpdateVisibilityEvent (loc.Location));
		}
		void HideMarker(Entity marker) {
			GridLocation pos = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			LocationPart loc = marker.GetPart<LocationPart>(LocationPart.partId);
			//MiniMapPart mini = marker.GetPart<MiniMapPart>(MiniMapPart.partId);
			//mini.MapColor = EntityFactory.MapMarkerColor;
			
			GameData.MiniMap.UpdateLocation(pos);
			
			marker.Detach<VisualPart>( marker.GetPart<VisualPart>(VisualPart.partId) );
			VisualPart vis = new VisualPart ("Items/Item", "Visuals/MapMarkerTex", false, true);
			marker.Attach (vis);
			vis.InitTexture();
			
			//GameData.EventManager.Notify (new UpdateVisibilityEvent (loc.Location));
		}
		
		OrderedDict<string, Entity> GetMarkers(GridLocation pos) {
			IEnumerable<Entity> m = GameData.TurnManager.GetAll(MapMarkerPart.partId);
			IEnumerable<Entity> ord = m.OrderBy( e => e.GetPart<LocationPart>(LocationPart.partId).Location.DistanceSq(pos) );
			
			OrderedDict<string, Entity> markers = new OrderedDict<string, Entity> ();
			int i = 0;
			foreach (Entity e in ord) {
				if (i >= GlobalParameters.letters.Length) {
					break;
				}
				char c = GlobalParameters.letters [i];
				markers [c.ToString ()] = e;
				i++;
			}
			return markers;
		}
		
		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			if (marker == null) {
				Entity e = InputUtil.HandleSelectionInput (markers, null);
				if (e != null) {
					marker = e;
					actor.SetNextAction (this);
					return true;
				}
			} else {
				if (Input.GetKeyDown (KeyCode.Escape)) {
					HideMarker(marker);
					marker = null;
					actor.SetNextAction (this);
					return true;
				}
				
				Entity e = InputUtil.HandleSelectionInput (markers, null);
				if (e != null) {
					HideMarker(marker);
					marker = e;
					actor.SetNextAction (this);
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if(marker != null) {
					HideMarker(marker);					
				}
				GameData.EventManager.Notify (new ShowInventoryEvent (actor.Entity, "Your inventory", inventory.ToDict()));
				return true;
			}
			return false;
		}
		
	}
}
