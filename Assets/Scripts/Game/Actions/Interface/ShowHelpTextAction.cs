﻿using System.Collections.Generic;
using System.Linq;
using ComponentSystem;
using Game.Data;
using Game.Util;
using Map.LayeredGrid;
using TurnSystem;
using Game.Parts;
using Game.Events;
using UnityEngine;

namespace Game.Actions.Interface {
	public class ShowHelpTextAction : Action {

		ActorPart actor;
		InventoryPart inventory;
		string[] items;
		int item;

		public ShowHelpTextAction(ActorPart actor) {
			this.actor = actor;
			this.item = -1;
			
			if (actor.Entity.HasPart<InventoryPart> ()) {
				inventory = actor.Entity.GetPart<InventoryPart> ();
			} else {
				inventory = null;
			}
			items = new string[ HelpText.HelpTopics.Count ];
			HelpText.HelpTopics.Keys.CopyTo(items, 0);
		}

		public ActionResult Execute() {
			if (inventory == null) {
				return ActionResult.FAILURE;
			}
			
			if( item < 0 ) {
				GameData.EventManager.Notify (new ShowTextSelectionEvent (actor.Entity, "Select a topic"+TextUtil.NewLine, items, null));
				return ActionResult.PENDING;
			} else {
				ReadPage(item);
				return ActionResult.PENDING;
			}
		}
		
		
		void ReadPage (int page) {
			string key = HelpText.HelpTopics.Keys.OfType<string>().Skip(page).First();
			string text = "<b>"+key+"</b>" + TextUtil.NewLine + TextUtil.NewLine 
							+ HelpText.HelpTopics[page] + TextUtil.NewLine + TextUtil.NewLine
							+ "Help topics:";
			
			GameData.EventManager.Notify (new ShowTextSelectionEvent (actor.Entity, text, items, null));
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			if (item >= 0 && Input.GetKeyDown (KeyCode.Escape)) {
				item = -1;
				actor.SetNextAction (this);
				return true;
			}
			
			int sel = InputUtil.HandleIndexSelectionInput ( items );
			if( sel >= HelpText.HelpTopics.Count ) sel = -1;
			if (sel >= 0) {
				item = sel;
				actor.SetNextAction (this);
				return true;
			}
			
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new ShowInventoryEvent(actor.Entity, "Your inventory", inventory.ToDict()) );
				return true;
			}
			return false;
		}
	}
}
