﻿using ComponentSystem;
using Map.LayeredGrid;
using TurnSystem;
using Game.Parts;
using Game.Events;

namespace Game.Actions.Interface {
	public class ShowInventoryAction : Action {

		Entity entity;

		public ShowInventoryAction(Entity entity) {
			this.entity = entity;
		}

		public ActionResult Execute() {
			if (entity.HasPart<InventoryPart> ()) {
				InventoryPart inv = entity.GetPart<InventoryPart> ();
				GameData.EventManager.Notify (new ShowInventoryEvent (entity, "Your inventory", inv.ToDict()));
			}
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
