﻿using ComponentSystem;
using Map.LayeredGrid;
using TurnSystem;
using Game.Parts;
using Game.Events;

namespace Game.Actions.Interface {
	public class ShowHelpAction : Action {

		Entity entity;

		public ShowHelpAction(Entity entity) {
			this.entity = entity;
		}

		public ActionResult Execute() {
			GameData.EventManager.Notify (new ShowHelpEvent (entity, null));
			return ActionResult.SUCCESS;
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
