﻿
using ComponentSystem;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Data;
using Game.Parts;
using Game.Rules;
using Game.Events;
using Game.Util;


namespace Game.Actions.Actor {
	public class ScrollMiniMapAction : Action {

		//ActorPart actor;
		Direction direction;

		public ScrollMiniMapAction(ActorPart actor, Direction direction) {
			//this.actor = actor;
			this.direction = direction;
		}

		public ActionResult Execute () {
			GridLocation pos = GameData.MiniMap.CurrentCenter;
			int zoom = GameData.MiniMap.Zoom;
			int delta = 20 / zoom;

			GridLocation newPos = new GridLocation(pos.Layer, pos.X+direction.dx*delta, pos.Y+direction.dy*delta);
			GameData.MiniMap.UpdateLocation( newPos );

			return ActionResult.SUCCESS;
		}

		public int GetCost () {
			return GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			return true;
		}
	}
}
