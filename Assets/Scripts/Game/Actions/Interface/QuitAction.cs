﻿using UnityEngine;

using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Serialization;
using Game.Parts;
using Game.Util;
using Game.Rules;

namespace Game.Actions.Interface {
	public class QuitAction : Action {

		ActorPart actor;
		bool confirmed;

		public QuitAction( ActorPart actor ) {
			this.actor = actor;
			this.confirmed = false;
		}

		public ActionResult Execute() {
			if (confirmed) {
				GameData.EventManager.Notify (new QuitGameEvent (false));
				return ActionResult.SUCCESS;
			} else {
				GameData.EventManager.Notify (new MessageEvent (null, "Really quit? (y/n)"));
				return ActionResult.PENDING;
			}
		}


		public int GetCost () {
			return GameRules.NoCost;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}

		public bool HandleUserInput() {
			string inp = InputUtil.GetInputCharacter ();
			if (inp != null) {
				if (inp == "y") {
					confirmed = true;
					actor.SetNextAction (this);
					return true;
				} else if (inp == "n") {
					GameData.EventManager.Notify (new MessageEvent (null, "Canceled."));
					return true;
				}
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify (new MessageEvent (null, "Canceled."));
				return true;
			}
			return false;
		}
	}
}
