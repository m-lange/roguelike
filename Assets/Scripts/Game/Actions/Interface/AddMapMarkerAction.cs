﻿
using ComponentSystem;
using Game.Factories;
using Game.Util;
using Map;
using Map.LayeredGrid;
using TurnSystem;
using Game.Events;
using Game.Parts;
using UnityEngine;

namespace Game.Actions.Interface {
	public class AddMapMarkerAction : Action {

		ActorPart actor;
		string command;
		bool confirmed;

		public AddMapMarkerAction(ActorPart actor) {
			this.actor = actor;
			this.command = "";
		}

		public ActionResult Execute () {
			if (!actor.Entity.HasPart<LocationPart> ()) {
				return ActionResult.FAILURE;
			}
			
			if (confirmed) {
				if( command.Length == 0 ) {
					GameData.EventManager.Notify (new MessageEvent (null, "Enter a label for the marker!"));
					return ActionResult.PENDING;
				} else {
					ProcessCommand();
					return ActionResult.SUCCESS;
				}
			} else {
				if (command.Length == 0) {
					GameData.EventManager.Notify (new MessageEvent (null, "Enter label"));
					GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "Label: " ) );
				} else {
					GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "Label: "+command ) );
				}
				return ActionResult.PENDING;
			}
		}
		
		
		void ProcessCommand () {
			GridLocation pos = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			Entity e = VisualFactory.CreateMapMarker( pos, true );
			MapMarkerPart marker = e.GetPart<MapMarkerPart>(MapMarkerPart.partId);
			marker.Label = command;
			NamePart name = e.GetPart<NamePart>(NamePart.partId);
			name.Name = "marker: " + command;
			
			string message = "Map marker placed: " + command;
			GameData.EventManager.Notify (new MessageEvent (actor.Entity, message));
			GameData.EventManager.Notify (new ShowHelpEvent (actor.Entity, ""));
		}


		public int GetCost () {
			return -1;
		}

		public GridLocation RequiresUpdate () {
			return null;
		}
		public bool HandleUserInput() {
			string inp = InputUtil.GetInputCharacter ();
			if (inp != null && inp.Length > 0 && (inp[0] == ' ' || GlobalParameters.letterSet.Contains (inp [0]))) {
				command += inp [0].ToString ();
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Backspace) && command.Length > 0) {
				command = command.Substring(0, command.Length-1);
				actor.SetNextAction (this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Return)) {
				confirmed = true;
				actor.SetNextAction(this);
				return true;
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				GameData.EventManager.Notify( new ShowHelpEvent( actor.Entity, "" ) );
				return true;
			}
			return false;
		}
		
	}
}
