﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Game.Parts.Sub;
using Map;
using Map.LayeredGrid;
using UnityEngine;
using ComponentSystem;
using Game.ActorEffects.Continuous;
using Game.Parts;
using Game.Data;
using Game.Rules;

namespace Game.Util {
	public class TextUtil {

		public const string You = "You";
		public const string Space = " ";
		public const string HLine = "-----------------------------------";
		public static readonly string NewLine = System.Environment.NewLine;
		public static readonly string Indentation = "      ";
		public static readonly string UnEquiped = "   ";
		public static readonly string Equiped = " + ";
		public static readonly string AtTheReady = " ! ";
		public static readonly string EquipedAndAtReady = " +! ";
		public static readonly string SelectionStart = "<color=cyan>";
		public static readonly string SelectionEnd = "</color>";
		public static readonly string LightOnStart = "<color=yellow>";
		public static readonly string LightOnEnd = "</color>";
		public static readonly string IdentifiedStart = "<color=#a0ffa0>";
		public static readonly string IdentifiedEnd = "</color>";


		public static readonly string Tinned = "tinned";
		public static readonly string TinnedOpen = "open tin of";
		public static readonly string TuftName = "tuft of ";

		static readonly string vocals = "aeiouöäü";
		static readonly string indefArticle = "a ";
		static readonly string indefArticleVocal = "an ";


		public static string GetName (Entity e) {
			StackablePart stack = e.GetPart<StackablePart> (StackablePart.partId);
			NamePart name = e.GetPart<NamePart> (NamePart.partId);
			if (stack != null) {
				if (stack.Count > 1) {
					return stack.Count + " " + (name!=null ? (name.Name+"s") : "unknowns");
				}
			}
			return name!=null ? name.Name : "unknown";
		}
		public static string GetName(Part e) {
			return GetName(e.Entity);
		}

		public static string GetNameSimple (Entity e) {
			return e.HasPart<NamePart>() ? e.GetPart<NamePart>().Name : "unknown";
		}
		public static string GetNameSimple(Part e) {
			return GetNameSimple(e.Entity);
		}

		public static bool IsPlural (Entity e) {
			if (e.HasPart<StackablePart> ()) {
				StackablePart st = e.GetPart<StackablePart> ();
				if (st.Count > 1) {
					return true;
				}
			}
			return false;
		}
		public static int GetCount (Entity e) {
			if (e.HasPart<StackablePart> ()) {
				StackablePart st = e.GetPart<StackablePart> ();
				return st.Count;
			}
			return 1;
		}

		public static string GetNameIndef (Entity e, bool count = true) {
			if (count && e.HasPart<StackablePart> ()) {
				StackablePart st = e.GetPart<StackablePart> ();
				if (st.Count > 1) {
					return st.Count + " " + (e.HasPart<NamePart> () ? e.GetPart<NamePart> ().Name + "s" : "unknowns");
				}
			}
			NamePart np = e.GetPart<NamePart> (NamePart.partId);
			string name = ToIndefinite (np != null ? np.Name : "unknown");
			OnOffPart ls = e.GetPart<OnOffPart> (OnOffPart.partId);
			if (ls != null ) {
				name += ls.IsOn ? " (on)" : " (off)";
			}
			return name;
		}

		public static string GetNameIndef(Part e) {
			return GetNameIndef(e.Entity);
		}

		public static string GetNameCount (Entity e, int cnt) {
			if (cnt == 1) {
				return GetNameIndef(e, false);
			}
			return cnt + " " + (e.HasPart<NamePart>() ? e.GetPart<NamePart>().Name+"s" : "unknowns");
		}
		public static string GetNameCount(Part e, int cnt) {
			return GetNameCount(e.Entity, cnt);
		}

		public static string Get3rdPersonS(Part e) {
			return e.Entity.HasPart<NamePart>() ? (e.Entity.GetPart<NamePart>().Name == You ? "" : "s") : "";
		}

		public static string Capitalize (string str) {
			if( str.Length == 0 ) return str;
			if(str.Length == 1) return str.ToUpper();
			return str[0].ToString().ToUpper() + str.Substring(1);
		}

		public static string ToIndefinite(string name) {
			if (name == You) {
				return name;
			}
			if (vocals.Contains (name [0].ToString())) {
				return indefArticleVocal + name;
			} else {
				return indefArticle + name;
			}
		}



		public static string EntityInfo (ActorPart actor, Entity entity, bool hasBook) {
			string name = TextUtil.Capitalize (TextUtil.GetName (entity));
			string s = "<size=14>" + name + "</size>" + TextUtil.NewLine + TextUtil.NewLine;

			int padding = 25;

			PickablePart pick = entity.GetPart<PickablePart> (PickablePart.partId);
			int itemCount = GetCount(entity);

			if (entity.HasPart (ActorPart.partId)) {
				ActorPart act = entity.GetPart<ActorPart> (ActorPart.partId);
				s += "Special attacks:" + TextUtil.NewLine;
				if (hasBook) {
					if (act.AttackEffects != null && act.AttackEffects.Length > 0) {
						foreach (ActorEffects.ActorEffect effect in act.AttackEffects) {
							s += TextUtil.IdentifiedStart + TextUtil.Capitalize (effect.Name + ": ").PadRight (padding) + effect.DisplayValue + TextUtil.IdentifiedEnd + TextUtil.NewLine;
						}
					} else {
						s += "none" + TextUtil.NewLine;
					}
					if (act.Stats.HasPassiveAttack) {
						s += "Passive attack" + TextUtil.NewLine;
					}
				} else {
					s += "unknown" + TextUtil.NewLine;
				}
				s += TextUtil.NewLine;
				s += "-- Stats --------------------------" + TextUtil.NewLine;
				if (entity.HasPart<HealthPointsPart> ()) {
					HealthPointsPart hp = entity.GetPart<HealthPointsPart> ();
					s += "Health: ".PadRight (padding) + hp.MaxHp + TextUtil.NewLine;
				}
	
				s += "Speed: ".PadRight (padding) + act.Speed + TextUtil.NewLine;
				s += "Strength: ".PadRight (padding) + act.Strength + TextUtil.NewLine;
				s += "Constitution: ".PadRight (padding) + act.Constitution + TextUtil.NewLine;
				s += "Dexterity: ".PadRight (padding) + act.Dexterity + TextUtil.NewLine;
				s += "Attack: ".PadRight (padding) + act.Attack + TextUtil.NewLine;
				s += "Attack type: ".PadRight (padding) + ((AttackTypes) act.Stats.AttackType) + TextUtil.NewLine;
				s += "Attack duration: ".PadRight (padding) + act.Stats.AttackDuration + TextUtil.NewLine;
				s += "Defense: ".PadRight (padding) + act.Defense + TextUtil.NewLine;
				s += "Armor: ".PadRight (padding) + (string.Join("/", act.Armor.ByType)) + TextUtil.NewLine;
				
				s += TextUtil.NewLine;
			}

			if (entity.HasPart (ConsumablePart.partId)) {
				ConsumablePart cons = entity.GetPart<ConsumablePart> (ConsumablePart.partId);
				if (cons.NutritionValue != 0) {
					s += "Nutrition: ".PadRight (padding) + cons.NutritionValue + TextUtil.NewLine;
				}
				if (hasBook) {
					if (cons.Effects != null) {
						foreach (ActorEffects.ActorEffect effect in cons.Effects) {
							s += TextUtil.IdentifiedStart + TextUtil.Capitalize (effect.Name + ": ").PadRight (padding) + effect.DisplayValue + TextUtil.IdentifiedEnd + TextUtil.NewLine;
						}
					}
				} else {
					s += "Effects unknown." + TextUtil.NewLine;
				}
				s += (cons.IsVegetarian ? "" : "not ") + "vegetarian" + TextUtil.NewLine;
				s += TextUtil.NewLine;
			}


			if (entity.HasPart (PlantPart.partId)) {
				PlantPart plant = entity.GetPart<PlantPart> (PlantPart.partId);
				bool found = false;
				List<string> grows = new List<string> ();
				foreach (int terr in plant.GrowTerrain) {
					grows.Add (Terra.TileInfo [terr].GroundName);
					found = true;
				}
				if (found) {
					if (hasBook) {
						s += "Grows on " + string.Join (", ", grows.ToArray ()) + "." + TextUtil.NewLine + TextUtil.NewLine;
					} else {
						s += "Growth conditions unknown." + TextUtil.NewLine + TextUtil.NewLine;
					}
				}
				s += "Condition: ".PadRight (padding) + (int)(100 * plant.TurnsRemaining / plant.MaxTurnsRemaining) + "%" + TextUtil.NewLine;
				if (hasBook) {
					s += "Durability: ".PadRight (padding) + plant.MaxTurnsRemaining + TextUtil.NewLine;
					s += "Growth speed: ".PadRight (padding) + plant.GrowthProbPerUpdate + TextUtil.NewLine;
				} else {
					s += "Durability: ".PadRight (padding) + "unknown" + TextUtil.NewLine;
					s += "Growth speed: ".PadRight (padding) + "unknown" + TextUtil.NewLine;
				}
				s += TextUtil.NewLine;
			}
			if (entity.HasPart (LimitedUsePart.partId)) {
				LimitedUsePart limUse = entity.GetPart<LimitedUsePart> (LimitedUsePart.partId);
				if (!limUse.BreaksAtRandom) {
					int value = Mathf.RoundToInt (100 * limUse.UsesRemaining / (float)limUse.MaxUses);
					s += "Status: ".PadRight (padding) + value + "%" + TextUtil.NewLine;
					s += TextUtil.NewLine;
				}
			}
			if (entity.HasPart (LightSourcePart.partId)) {
				LightSourcePart light = entity.GetPart<LightSourcePart> (LightSourcePart.partId);
				s += "Light range: ".PadRight (padding) + (int) (light.Range) + TextUtil.NewLine;
				s += "Light duration: ".PadRight (padding) + light.TurnsRemaining+"/"+light.MaxTurns + TextUtil.NewLine;
				s += TextUtil.NewLine;
			}
			
			if (entity.HasPart (OnOffPart.partId)) {
				OnOffPart light = entity.GetPart<OnOffPart> (OnOffPart.partId);
				s += (light.IsOn ? "On" : "Off") + TextUtil.NewLine;
				s += TextUtil.NewLine;
			}
			
			if (entity.HasPart (EquipmentPart.partId)) {
				EquipmentPart eq = entity.GetPart<EquipmentPart> (EquipmentPart.partId);
				int speed = eq.SpeedBonus;
				int str = eq.StrengthBonus;
				int att = eq.AttackBonus;
				int dur = eq.AttackDuration;
				Armor arm = eq.ArmorBonus;
				int def = eq.DefenseBonus;
				int dex = eq.DexterityBonus;

				bool found = false;
				if (att != 0) {
					s += "Attack: ".PadRight (padding) + att + TextUtil.NewLine;
					found = true;
				}
				if (att != 0) {
					s += "Attack type: ".PadRight (padding) + ((AttackTypes) eq.AttackType) + TextUtil.NewLine;
					found = true;
				}
				if (dur != 0) {
					s += "Attack duration: ".PadRight (padding) + dur + TextUtil.NewLine;
					found = true;
				}
				if (arm.Any()) {
					s += "Armor: ".PadRight (padding) + (string.Join("/", arm.ByType)) + TextUtil.NewLine;
					found = true;
				}
				if (def != 0) {
					s += "Defense: ".PadRight (padding) + def + TextUtil.NewLine;
					found = true;
				}
				if (speed != 0) {
					s += "Speed: ".PadRight (padding) + speed + TextUtil.NewLine;
					found = true;
				}
				if (str != 0) {
					s += "Strength: ".PadRight (padding) + str + TextUtil.NewLine;
					found = true;
				}
				if (dex != 0) {
					s += "Dexterity: ".PadRight (padding) + dex + TextUtil.NewLine;
					found = true;
				}
				if (found)
					s += TextUtil.NewLine;
			}
			if (entity.HasPart (ProjectilePart.partId)) {
				ProjectilePart pro = entity.GetPart<ProjectilePart> (ProjectilePart.partId);
				if (pro.LauncherType != Game.Data.LauncherType.None) {
					s += "Launcher: ".PadRight (padding) + pro.LauncherType.ToString () + TextUtil.NewLine;
				}
				s += "Range: ".PadRight (padding) + pro.RangeFired + "/" + pro.Range + TextUtil.NewLine;
				s += "Ranged damage: ".PadRight (padding) + pro.DamageFired + "/" + pro.Damage + TextUtil.NewLine;
				s += "Reload time: ".PadRight (padding) + pro.ReloadTurnsFired + "/" + pro.ReloadTurns + TextUtil.NewLine;
				s += "Attack type: ".PadRight (padding) + ((AttackTypes) pro.AttackType) + TextUtil.NewLine;
			} else if (pick != null) {
				float damage = pick.Mass * GameRules.NonProjectileDamagePerMass;
				int range = GameRules.CalcRange (actor, pick, false);
				s += "Range: ".PadRight (padding) + "0/" + range + TextUtil.NewLine;
				s += "Ranged damage: ".PadRight (padding) + "0/" + damage + TextUtil.NewLine;
				s += "Reload time: ".PadRight (padding) + "0/1" + TextUtil.NewLine;
				s += "Attack type: ".PadRight (padding) + "Impact" + TextUtil.NewLine;
			}
			s += TextUtil.NewLine;
			if (pick != null) {
				float mass = (pick.Mass / 1000f);
				string unit = mass < 1 ? " g" : " kg";
				float massTotal = mass * itemCount;
				string unitTotal = massTotal < 1 ? " g" : " kg";
				if (mass < 1)
					mass = pick.Mass;
				if (massTotal < 1)
					massTotal = pick.Mass * itemCount;
				s += "Mass: ".PadRight (padding) + mass + unit;
				if( itemCount > 1 ) {
					s += " ("+massTotal+unitTotal+")";
				}
				s += TextUtil.NewLine;
			}

			if (entity.HasPart (UsablePart.partId)) {
				UsablePart usable = entity.GetPart<UsablePart> (UsablePart.partId);
				//s += TextUtil.NewLine + TextUtil.Capitalize (TextUtil.GetNameIndef (entity)) + verb + "further uses."+TextUtil.NewLine;
				s += TextUtil.NewLine + TextUtil.Capitalize (TextUtil.GetNameIndef (entity)) + " can be used to "+usable.Usable.Use+"."+TextUtil.NewLine;
				s += TextUtil.NewLine;
			}
			
			if (entity.HasPart (InventoryPart.partId)) {
				InventoryPart inv = entity.GetPart<InventoryPart> (InventoryPart.partId);
				s += "-- Equipment ----------------------" + TextUtil.NewLine;
				PickablePart p;
				List<PickablePart> items = inv.Items;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					EquipmentPart eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null && eq.IsEquiped) {
						s += TextUtil.GetNameIndef(eq.Entity, false) + TextUtil.NewLine;
					}
				}
				s += TextUtil.NewLine;
			}
			
			if (entity.HasPart (ActorPart.partId)) {
				ActorPart act = entity.GetPart<ActorPart> (ActorPart.partId);
				EquipmentPart yourWeapon = null;
				EquipmentPart theirWeapon = null;
				PickablePart yourProjectile = null;
				EquipmentPart yourLauncher = null;
				PickablePart theirProjectile = null;
				EquipmentPart theirLauncher = null;
				
				InventoryPart yourInv = actor.Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (yourInv != null) {
					yourWeapon = yourInv.GetEquipped (Equip.HandPrim);
					yourProjectile = yourInv.GetQuivered();
					if(yourProjectile != null) yourLauncher = yourInv.GetLauncher(yourProjectile);
				}
				InventoryPart theirInv = act.Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (theirInv != null) {
					theirWeapon = theirInv.GetEquipped (Equip.HandPrim);
					theirProjectile = theirInv.GetQuivered();
					if(theirProjectile != null) theirLauncher = theirInv.GetLauncher(theirProjectile);
				}
				float hitYou = (int) Mathf.Round(GameRules.CalcHitChance( actor, act, yourWeapon ) * 100);
				float hitThey = (int) Mathf.Round(GameRules.CalcHitChance( act, actor, theirWeapon ) * 100);
				float damageYou = (int) Math.Ceiling(GameRules.CalcMaxDamage( actor, act, yourWeapon ));
				float damageThey = (int) Math.Ceiling(GameRules.CalcMaxDamage( act, actor, theirWeapon ));
				
				float hitYouRanged = 0;
				float hitTheyRanged = 0;
				float damageYouRanged = 0;
				float damageTheyRanged = 0;
				if(yourProjectile != null) {
					hitYouRanged = (int) Mathf.Round(GameRules.CalcHitChanceRanged( actor, act, yourProjectile, 0, true, 1, yourLauncher != null ) * 100);
					damageYouRanged = (int) Math.Ceiling(GameRules.CalcMaxDamageRanged( actor, act, yourProjectile, yourLauncher != null ));
				}
				if(theirProjectile != null) {
					hitTheyRanged = (int) Mathf.Round(GameRules.CalcHitChanceRanged( act, actor, theirProjectile, 0, true, 1, theirLauncher != null ) * 100);
					damageTheyRanged = (int) Math.Ceiling(GameRules.CalcMaxDamageRanged( act, actor, theirProjectile, theirLauncher != null ));
				}
				
				s += "-- Combat stats -------------------" + TextUtil.NewLine;
				s += "Hit: ".PadRight (padding) + hitYou+"%/"+hitThey+"%" + TextUtil.NewLine;
				s += "Damage: ".PadRight (padding) + damageYou+"/"+damageThey + TextUtil.NewLine;
				s += "Hit ranged: ".PadRight (padding) + hitYouRanged+"%/"+hitTheyRanged+"%" + TextUtil.NewLine;
				s += "Damage ranged: ".PadRight (padding) + damageYouRanged+"/"+damageTheyRanged + TextUtil.NewLine;
				s += TextUtil.NewLine;
				
			}
			
			if (entity.HasPart (MapMarkerPart.partId)) {
				MapMarkerPart marker = entity.GetPart<MapMarkerPart> (MapMarkerPart.partId);
				
				s += "-- Marker -------------------------" + TextUtil.NewLine;
				s += marker.Label + TextUtil.NewLine;
				s += TextUtil.NewLine;
				
			}
			s += TextUtil.HLine+TextUtil.NewLine;

			//s += TextUtil.NewLine+"What next?";
			return s;
		}
		
		public static string CharacterInfo (ActorPart actor) {
			int padding = 22;
			Entity entity = actor.Entity;
			string s = "<size=14>You</size>" + TextUtil.NewLine;

			s += TextUtil.NewLine;
			s += "-- Stats --------------------------" + TextUtil.NewLine;
			if (entity.HasPart<HealthPointsPart> ()) {
				HealthPointsPart hp = entity.GetPart<HealthPointsPart> ();
				s += "Health: ".PadRight (padding) + hp.Hp + "/" + hp.MaxHp + TextUtil.NewLine;
			}

			s += "Speed: ".PadRight (padding) + actor.Speed + " (" + actor.Stats.BaseSpeed + ")" + TextUtil.NewLine;
			s += "Strength: ".PadRight (padding) + actor.Strength + " (" + actor.Stats.BaseStrength + "+" + actor.Stats.StrengthPoints + ")" + TextUtil.NewLine;
			s += "Constitution: ".PadRight (padding) + actor.Constitution + " (" + actor.Stats.BaseConstitution + "+" + actor.Stats.ConstitutionPoints + ")" + TextUtil.NewLine;
			s += "Dexterity: ".PadRight (padding) + actor.Dexterity + " (" + actor.Stats.BaseDexterity + "+" + actor.Stats.DexterityPoints + ")" + TextUtil.NewLine;
			s += "Attack: ".PadRight (padding) + actor.Attack + " (" + actor.Stats.BaseAttack + "+" + actor.Stats.AttackPoints + ")" + TextUtil.NewLine;
			s += "Attack duration: ".PadRight (padding) + actor.Stats.AttackDuration + TextUtil.NewLine;
			s += "Defense: ".PadRight (padding) + actor.Defense + " (" + actor.Stats.BaseDefense + "+" + actor.Stats.DefensePoints + ")" + TextUtil.NewLine;
			s += "Armor: ".PadRight (padding) + (string.Join("/", actor.Armor.ByType)) + " (" + (string.Join("/", actor.Stats.BaseArmor.ByType)) + ")" + TextUtil.NewLine;
			
			s += TextUtil.NewLine;
			s += "-- Status -------------------------" + TextUtil.NewLine;
			InventoryPart inv = entity.GetPart<InventoryPart> ();
			if (inv != null) {
				int maxMass = GameRules.CalcMaxMass ( actor ) / 1000;
				int mass = inv.GetMass () / 1000;
				s += "Load: ".PadRight (padding) + mass + " kg/" + maxMass + " kg" + TextUtil.NewLine;
			}
			int sat = (int) (100 * actor.Stats.StomachContent / (actor.Stats.MaxStomachContent * GameRules.SatiatedStomachContent));
			s += "Satiation: ".PadRight (padding) + sat + " %" + TextUtil.NewLine;

			s += TextUtil.NewLine;
			s += "-- Skills -------------------------" + TextUtil.NewLine;
			foreach (Skills skill in Enum.GetValues(typeof(Skills)).Cast<Skills>()) {
				if (skill == Skills.None) {
					continue;
				}
				int idx = (int)skill;
				s += (skill.ToString () + ": ").PadRight (padding) + actor.Stats.Skill [idx] + " (" + actor.Stats.SkillPoints [idx] + ")" + TextUtil.NewLine;
			}

			s += TextUtil.NewLine;
			s += "-- Effects ------------------------" + TextUtil.NewLine;
			if (actor.Effects.Count > 0) {
				foreach (ContinuousEffect eff in actor.Effects) {
					if (eff.Name == null) {
						continue;
					}
					s += eff.Name + TextUtil.NewLine;
				}
			} else {
				s += "none" + TextUtil.NewLine;
			}

			s += TextUtil.NewLine;
			s += "-- Kills --------------------------"+ TextUtil.NewLine;
			if (GameData.KillRecord.Kills.Count > 0) {
				foreach (KeyValuePair<string, int> kv in GameData.KillRecord.Kills) {
					s += kv.Key.PadRight (padding)+kv.Value+TextUtil.NewLine;
				}
			} else {
				s += "none" + TextUtil.NewLine;
			}
			return s;
		}
		
		
		public static string MapMarkerInfo (ActorPart actor, Entity marker) {
			int padding = 22;
			
			GridLocation pos = actor.Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			NamePart name = marker.GetPart<NamePart>(NamePart.partId);
			MapMarkerPart m = marker.GetPart<MapMarkerPart>(MapMarkerPart.partId);
			LocationPart loc = marker.GetPart<LocationPart>(LocationPart.partId);
			
			int dist = Mathf.RoundToInt( loc.Location.Distance(pos) );
			Direction dir = Direction.GetFromToXYFar(pos, loc.Location);
				
			string s = Capitalize(name.Name) + TextUtil.NewLine;
			s += "Level: ".PadRight (padding) + loc.Location.Layer + TextUtil.NewLine;
			s += "Direction: ".PadRight (padding) + dir + TextUtil.NewLine;
			s += "Distance: ".PadRight (padding) + dist + TextUtil.NewLine;
			
			return s;
		}
	}
}