﻿using System.Collections;
using System.Collections.Generic;

using Map.LayeredGrid;
using Map.LayeredGrid.Impl;
using Game.Parts;
using Game.Data;

namespace Game.Util {
	public class MapUtil {


		public static GridLocation GetFloor(LayeredGrid<TileInfo> map, GridLocation location) {
			TileInfo value = map.Get(location);
			//TileTypeInfo info = Terra.TileInfo [value.Type];
			if (map.IsNoData (value)) {
				GridLocation newPos = new GridLocation(location);
				for (int layer = newPos.Layer; layer >= map.BottomLayer; layer--) {
					GridLocation newPos2 = new GridLocation (layer, newPos.X, newPos.Y);
					TileInfo val = map.Get(newPos2);
					if (!map.IsNoData(val)) {
						TileTypeInfo info2 = Terra.TileInfo [val.Type];
						if (info2.IsWalkable) {
							if (!GameData.SpatialManager.IsAt<MoveBlockingPart> (MoveBlockingPart.partId, newPos2)) {
								newPos.Layer = layer;
								return newPos;
							}
						}
					}
				}
			} else {
				return location;
			}
			return null;
		}
		
	}
}
