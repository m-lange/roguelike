﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Collections;
using Map;
using Map.LayeredGrid;

namespace Game.Util {
	public class InputUtil {

		public static float InitialRepeatDelay = 0.25f;
		public static float RepeatDelay = 0.1f;

		public static string GetInputCharacter() {
			if (Input.anyKeyDown && Input.inputString.Length > 0) { 
				return Input.inputString [0].ToString();
			}
			return null;
		}

		public static T HandleSelectionInput<T>( OrderedDict<string, T> items, Func<T, bool> filter ) {
			if (Input.inputString.Length > 0) {
				string c = Input.inputString[0].ToString();
				if (items.Contains (c.ToString ())) {
					T item = items [c.ToString ()];
					if (filter == null || filter (item)) {
						return item;
					}
				}
			}
			return default(T);
		}

		public static T HandleSelectionInput<T> (T[] items) {
			if (Input.inputString.Length > 0) {
				char c = Input.inputString [0];
				if (GlobalParameters.letterIndices.ContainsKey (c)) {
					int idx = GlobalParameters.letterIndices [c];
					if (idx < items.Length) {
						return items[idx];
					}
				}
			}
			return default(T);
		}
		public static int HandleIndexSelectionInput<T> (T[] items) {
			if (Input.inputString.Length > 0) {
				char c = Input.inputString [0];
				if (GlobalParameters.letterIndices.ContainsKey (c)) {
					int idx = GlobalParameters.letterIndices [c];
					if (idx < items.Length) {
						return idx;
					}
				}
			}
			return -1;
		}


		public static Direction HandleDirectionXYInput() {
			Direction direction = null;
			if (Input.GetKeyDown (KeyCode.Keypad1)) {
				direction = Direction.SW;
			} else if (Input.GetKeyDown (KeyCode.Keypad2)) {
				direction = Direction.S;
			} else if (Input.GetKeyDown (KeyCode.Keypad3)) {
				direction = Direction.SE;
			} else if (Input.GetKeyDown (KeyCode.Keypad4)) {
				direction = Direction.W;
			} else if (Input.GetKeyDown (KeyCode.Keypad6)) {
				direction = Direction.E;
			} else if (Input.GetKeyDown (KeyCode.Keypad7)) {
				direction = Direction.NW;
			} else if (Input.GetKeyDown (KeyCode.Keypad8)) {
				direction = Direction.N;
			} else if (Input.GetKeyDown (KeyCode.Keypad9)) {
				direction = Direction.NE;
			} 
			return direction;
		}

		public static Direction HandleDirectionInput() {
			Direction direction = null;
			if (Input.GetKeyDown (KeyCode.Keypad1)) {
				direction = Direction.SW;
			} else if (Input.GetKeyDown (KeyCode.Keypad2)) {
				direction = Direction.S;
			} else if (Input.GetKeyDown (KeyCode.Keypad3)) {
				direction = Direction.SE;
			} else if (Input.GetKeyDown (KeyCode.Keypad4)) {
				direction = Direction.W;
			} else if (Input.GetKeyDown (KeyCode.Keypad6)) {
				direction = Direction.E;
			} else if (Input.GetKeyDown (KeyCode.Keypad7)) {
				direction = Direction.NW;
			} else if (Input.GetKeyDown (KeyCode.Keypad8)) {
				direction = Direction.N;
			} else if (Input.GetKeyDown (KeyCode.Keypad9)) {
				direction = Direction.NE;
			} else if (Input.GetKeyDown (KeyCode.KeypadPlus)) {
				direction = Direction.UP;
			} else if (Input.GetKeyDown (KeyCode.KeypadMinus)) {
				direction = Direction.DOWN;
			} 
			return direction;
		}

		static float timePassed = 0;
		public static Direction HandleDirectionInputPressed () {
			Direction direction = HandleDirectionInput ();
			if (direction != null) {
				timePassed = 0;
				return direction;
			}

			if (Input.GetKey (KeyCode.Keypad1)) {
				direction = Direction.SW;
			} else if (Input.GetKey (KeyCode.Keypad2)) {
				direction = Direction.S;
			} else if (Input.GetKey (KeyCode.Keypad3)) {
				direction = Direction.SE;
			} else if (Input.GetKey (KeyCode.Keypad4)) {
				direction = Direction.W;
			} else if (Input.GetKey (KeyCode.Keypad6)) {
				direction = Direction.E;
			} else if (Input.GetKey (KeyCode.Keypad7)) {
				direction = Direction.NW;
			} else if (Input.GetKey (KeyCode.Keypad8)) {
				direction = Direction.N;
			} else if (Input.GetKey (KeyCode.Keypad9)) {
				direction = Direction.NE;
			} else if (Input.GetKey (KeyCode.KeypadPlus)) {
				direction = Direction.UP;
			} else if (Input.GetKey (KeyCode.KeypadMinus)) {
				direction = Direction.DOWN;
			} 
			if (direction != null) {
				timePassed += Time.deltaTime;
				if (timePassed >= InitialRepeatDelay) {
					timePassed -= RepeatDelay;
					return direction;
				}
			}
			return null;
		}

		public static GridLocation GetMousePosition(GridLocation cameraLocation, Vector2 mouse) {
			
			Plane plane = new Plane(
				Vector3.forward,
				GameData.GridToWorld(cameraLocation));
			Ray ray = Camera.main.ScreenPointToRay(mouse);

			float distance;
			if (plane.Raycast(ray, out distance)){
				Vector3 hitPoint = ray.GetPoint(distance);
				return GameData.WorldToGrid (hitPoint);
			}
			return null;
		}
	}
}
