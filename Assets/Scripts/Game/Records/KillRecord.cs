﻿using System.Collections;
using System.Collections.Generic;

using ComponentSystem;
using Game.Parts;
using Game.Util;

namespace Game.Records {
	public class KillRecord {

		Dictionary<string, int> kills;

		public KillRecord () {
			kills = new Dictionary<string, int>();
		}

		public Dictionary<string, int> Kills {
			get { return kills; }
			set { kills = value; }
		}

		public void AddKill (ActorPart killed) {
			string name = TextUtil.GetNameSimple (killed);
			if (kills.ContainsKey (name)) {
				kills [name] += 1;
			} else {
				kills [name] = 1;
			}
		}
		
	}
}
