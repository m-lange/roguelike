﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map.LayeredGrid;
using Game.Parts.Sub;


namespace Game.Visual {
	public class EntityVisual : MonoBehaviour, VisualRepresentation {
			
		Transform mTransform;
		GameObject go;
		//MeshRenderer mRenderer;

		void Awake() {
			mTransform = transform;
			go = gameObject;
			//mRenderer = GetComponent<MeshRenderer> ();
		}

		public void SetLocation(GridLocation location) {
			//Debug.Log (location);
			((mTransform == null) ? transform : mTransform).position = new Vector3 (location.X, location.Y, location.Layer * -GlobalParameters.Instance.LayerHeight);
		}
		public void SetVisible(bool visible) {
			if (go.activeSelf != visible) {
				go.SetActive (visible);
			}
			/*
			if (mRenderer != null) {
				mRenderer.enabled = visible;
			}
			*/
		}
		public bool IsVisible() {
			return go.activeSelf;
			/*
			if (mRenderer != null) {
				return mRenderer.enabled;
			} else {
				return false;
			}
			*/
		}
		/*
		public void SetTexture(string texture) {
			if (mRenderer != null) {
				mRenderer.material.mainTexture = (Texture) Resources.Load (texture);
			}
		}
		*/
		public void DestroyVisual() {
			GameObject.Destroy ( go );
		}

	}
}
