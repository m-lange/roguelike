﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Map.LayeredGrid;
using Game.Parts.Sub;


namespace Game.Visual {
	public class CameraVisual : MonoBehaviour, VisualRepresentation {
			
		Transform mTransform;
		Vector2 target;
		Vector2 velocity = new Vector2();

		float dampTime = 0.4f;
		float maxSpeed = 200f;

		//Vector2 smoothedPos = new Vector2();
		//Vector2 smoothedVelocity = new Vector2();

		void Awake() {
			mTransform = transform;
		}

		void LateUpdate () {
			Vector2 pos = new Vector2 (mTransform.position.x, mTransform.position.y);
			Vector2 diff = target - pos;
			float dist = diff.magnitude;
			//if (Time.deltaTime > 0.035f) {
			//	UnityEngine.Debug.Log(Time.deltaTime);
			//}
			if (dist > 0.1f) {
				Vector2 newPos = Vector2.SmoothDamp (pos, target, ref velocity, dampTime, maxSpeed, Time.deltaTime);
				mTransform.position = new Vector3 (newPos.x, newPos.y, mTransform.position.z);
			}
		}

		public void SetLocation(GridLocation location) {
			target = new Vector2 (location.X, location.Y);
			Transform trans = ((mTransform == null) ? transform : mTransform);
			trans.position = new Vector3 (trans.position.x, trans.position.y, location.Layer * -GlobalParameters.Instance.LayerHeight);
			//trans.position = target;
		}

		public void SetLocationForced(GridLocation location) {
			target = new Vector2 (location.X, location.Y);
			Transform trans = ((mTransform == null) ? transform : mTransform);
			trans.position = new Vector3 (location.X, location.Y, location.Layer * -GlobalParameters.Instance.LayerHeight);
			//smoothedPos = trans.position;
		}

		public void SetVisible(bool visible) {
			
		}
		public bool IsVisible() {
			return true;
		}
		public void SetTexture(string texture) {

		}

		public void DestroyVisual() {

		}
	}
}
