﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Map.LayeredGrid;

namespace Game.Visual {
	public class Tile : MonoBehaviour {

		GridLocation position;
		bool isSeen = true;
		bool isVisible = true;
		public int type = 0;

		//public Texture texture;

		public GridLocation Position {
			get{ return position; }
		}

		public bool IsSeen {
			get { return isSeen; }
		}
		public void SetSeen(bool seen) {
			if (seen != isSeen) {
				isSeen = seen; 
				gameObject.SetActive (isSeen);
			}
		}
		public void SetVisible (bool visible) {
			if (visible != isVisible) {
				isVisible = visible; 
				if (isVisible) {
					mFilter.sharedMesh = Game.Data.Terra.MeshVisible (type);
				} else {
					mFilter.sharedMesh = Game.Data.Terra.MeshSeen(type);
				}
			}
		}

		Transform mTransform;
		//MeshRenderer mRenderer;
		MeshFilter mFilter;

		void Awake() {
			mTransform = transform;
			//mRenderer = GetComponent<MeshRenderer> ();
			mFilter = GetComponent<MeshFilter> ();
			//mRenderer.material.mainTexture = texture;
		}
		/*
		public void SetVisibleMesh () {
			mFilter.mesh = Game.Data.Terra.MeshVisible (type);
		}
		public void SetSeenMesh() {
			mFilter.mesh = Game.Data.Terra.MeshSeen(type);
		}
		*/
		//public void SetColor(Color col) {
			//mRenderer.material.color = col;
		//}
		//public void SetTexture(Texture tex) {
			//mRenderer.material.mainTexture = tex;
		//}

		public void SetPosition(GridLocation pos, float layerHeight) {
			this.position = pos;
			((mTransform == null) ? transform : mTransform).position = new Vector3 (pos.X, pos.Y, pos.Layer * -layerHeight); 
		}

	}
}