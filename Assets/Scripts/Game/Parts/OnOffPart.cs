﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.ActorEffects;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Has an effect when stepped on.
	/// </summary>
	[DataContract]
	[KnownType(typeof(OnOffPart))]
	public class OnOffPart : Part<OnOffPart> {
		
		bool isOn;

		public OnOffPart ( bool isOn ) {
			this.isOn = isOn;
		}

		[DataMember]
		public bool IsOn {
			get { return isOn; }
			set { isOn = value; }
		}
		
		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
