﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Events;
using Game.Util;

using UnityEngine;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(LimitedUsePart))]
	public class LimitedUsePart : Part<LimitedUsePart> {


		int maxUses;
		int usesRemaining;
		bool breaksAtRandom;
		string message;

		public LimitedUsePart(int maxUses, bool breaksAtRandom, string message) {
			this.maxUses = maxUses;
			this.usesRemaining = maxUses;
			this.breaksAtRandom = breaksAtRandom;
			this.message = message;
		}

		[DataMember]
		public int MaxUses {
			get { return maxUses; }
			set { maxUses = value; }
		}

		[DataMember]
		public int UsesRemaining {
			get { return usesRemaining; }
			set { usesRemaining = value; }
		}

		[DataMember]
		public bool BreaksAtRandom {
			get { return breaksAtRandom; }
			set { breaksAtRandom = value; }
		}

		[DataMember]
		public string Message {
			get { return message; }
			set { message = value; }
		}


		public bool Damage (int uses) {
			if (breaksAtRandom) {
				float prob = 1f / maxUses;
				prob = 1f - Mathf.Pow(1f - prob, uses);
				return RandUtils.NextBool(prob);
			} else {
				usesRemaining -= uses;
				return usesRemaining <= 0;
			}
		}
		public void Repair () {
			usesRemaining = maxUses;
		}

		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
