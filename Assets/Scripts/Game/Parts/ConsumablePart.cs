﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.ActorEffects;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Health points part.
	/// </summary>
	[DataContract]
	[KnownType(typeof(ConsumablePart))]
	public class ConsumablePart : Part<ConsumablePart> {

		int nutritionValue;
		bool isVegetarian;
		bool isHerb;
		ActorEffect[] effects;

		public ConsumablePart ( int nutritionValue, bool isVegetarian, bool isHerb, params ActorEffect[] effects ) {
			this.nutritionValue = nutritionValue;
			this.isVegetarian = isVegetarian;
			this.isHerb = isHerb;
			if (effects != null && effects.Length > 0) {
				this.effects = effects;
			}
		}

		[DataMember]
		public int NutritionValue {
			get { return nutritionValue; }
			set { nutritionValue = value; }
		}

		[DataMember]
		public bool IsVegetarian {
			get { return isVegetarian; }
			set { isVegetarian = value; }
		}

		[DataMember]
		public bool IsHerb {
			get { return isHerb; }
			set { isHerb = value; }
		}

		[DataMember]
		public ActorEffect[] Effects {
			get { return effects; }
			set { effects = value; }
		}

		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
