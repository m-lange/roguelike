﻿using System.Runtime.Serialization;
using Map.LayeredGrid;
using ComponentSystem;

using UnityEngine;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(MiniMapPart))]
	public class MiniMapPart : Part<MiniMapPart> {

		
		Color mapColor;


		public MiniMapPart(Color mapColor) {
			this.mapColor = mapColor;
		}

		public Color MapColor {
			get {  return mapColor; }
			set {  mapColor = value; }
		}

		[DataMember]
		public int ColorForSerialization {
			get { 
				Color32 c = mapColor;
				return (c.a & 0xff) << 24 | (c.r & 0xff) << 16 | (c.g & 0xff) << 8 | (c.b & 0xff);
			}
			set {
				Color32 c = new Color32();
				c.a = (byte) ((value >> 24) & 0xff); // or color >>> 24
				c.r = (byte) ((value >> 16) & 0xff);
				c.g = (byte) ((value >>  8) & 0xff);
				c.b = (byte) ((value      ) & 0xff);
				mapColor = c;
			}
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
