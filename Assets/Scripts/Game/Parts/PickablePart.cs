﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Rules;

using UnityEngine;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(PickablePart))]
	public class PickablePart : Part<PickablePart> {

		int mass;
		int price;
		HashSet<TradeClass> tradeClasses;
		bool isAtReady;
		long carrierEntity = -1;

		char inventoryKey = default(char);

		[DataMember]
		public int Mass {
			get { return mass;}
			set { mass = value; }
		}

		[DataMember]
		public int Price {
			get { return price;}
			set { price = value; }
		}

		[DataMember]
		public char InventoryKey {
			get { return inventoryKey; }
			set { inventoryKey = value; }
		}

		public HashSet<TradeClass> TradeClasses {
			get { return tradeClasses; }
			set { tradeClasses = value; }
		}
		[DataMember]
		public TradeClass[] TradeClassesForSerialization {
			get { 
				TradeClass[] arr = new TradeClass[ tradeClasses.Count ];
				tradeClasses.CopyTo(arr, 0);
				return arr; 
			}
			set { this.tradeClasses = new HashSet<TradeClass>( value ); }
		}
		[DataMember]
		public bool IsAtReady {
			get { return isAtReady; }
			set { isAtReady = value; }
		}

		[DataMember]
		public long CarrierEntity {
			get { return carrierEntity; }
			set { carrierEntity = value; }
		}

		[IgnoreDataMember]
		public bool InInventory {
			get { return carrierEntity >= 0; }
		}

		public PickablePart(int mass, int price, params TradeClass[] tradeClasses) {
			this.mass = mass;
			this.price = price;
			this.tradeClasses = new HashSet<TradeClass>( tradeClasses );
			this.isAtReady = false;
			this.carrierEntity = -1;
		}

		/*
		public int GetPrice (bool sell) {
			if (sell) {
				return Mathf.RoundToInt (price * GameRules.SellPriceFraction);
			} else {
				return price;
			}
		}
		*/

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
