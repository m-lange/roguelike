﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Events;
using Game.Util;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(LimitedPresencePart))]
	public class LimitedPresencePart : Part<LimitedPresencePart> {

		int turnsRemaining;
		string message;

		public LimitedPresencePart(int turnsRemaining, string message) {
			this.turnsRemaining = turnsRemaining;
			this.message = message;
		}

		[DataMember]
		public int TurnsRemaining {
			get { return turnsRemaining; }
			set { turnsRemaining = value; }
		}

		[DataMember]
		public string Message {
			get { return message; }
			set { message = value; }
		}
		public override void Initialize() {}
		public override void Update () {
			turnsRemaining -= 1;
			if (turnsRemaining <= 0) {
				PickablePart pick = Entity.GetPart<PickablePart> (PickablePart.partId);
				if (pick != null) {
					if (pick.CarrierEntity == GameData.Player.Id) {
						GameData.EventManager.Notify( new MessageEvent(GameData.Player, "The "+TextUtil.GetName(pick)+" "+message+".") );
						Sounds.Play(Sounds.Crack);
					}
				}
				GameData.EventManager.Notify (new RemoveEntityEvent (Entity, true));
				//Entity.SetActive(false);
				GameData.EventManager.Notify (new DestroyEntityEvent (Entity, null));
			}
		}
		public override void Cleanup() {}
	}
}
