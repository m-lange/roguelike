﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(MoneyPart))]
	public class MoneyPart : Part<MoneyPart> {
		

		public MoneyPart() {
			
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
