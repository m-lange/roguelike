﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(PlayerPart))]
	public class PlayerPart : Part<PlayerPart> {
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
