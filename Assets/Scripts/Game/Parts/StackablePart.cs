﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Factories;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(StackablePart))]
	public class StackablePart : Part<StackablePart> {

		string generator;
		int maxCount;
		int count;

		[DataMember]
		public int Count {
			get { return count;}
			set { count = value; }
		}

		[DataMember]
		public int MaxCount {
			get { return maxCount; }
			set { maxCount = value; }
		}

		[DataMember]
		public string Generator {
			get { return generator; }
			set { generator = value; }
		}

		public StackablePart(string generator, int count, int maxCount) {
			this.generator = generator;
			this.count = count;
			this.maxCount = maxCount;
		}

		public bool CanStack (StackablePart other) {
			return (generator == other.Generator) && (count + other.count <= maxCount);
		}

		public bool Stack (StackablePart other) {
			if (count + other.Count >= maxCount) {
				return false;
			}
			if (generator != other.Generator) {
				return false;
			}
			count += other.Count;
			return true;
		}

		public Entity Unstack (bool addToGame) {
			if (count == 0) {
				return null;
			}
			count -= 1;
			if (Entity.HasPart (LocationPart.partId)) {
				return EntityFactory.GetCreator (generator) (Entity.GetPart<LocationPart> (LocationPart.partId).Location, addToGame);
			} else {
				return EntityFactory.GetCreator (generator) ( null, addToGame);
			}
		}

		public void SetContainerDirty () {
			PickablePart pick = Entity.GetPart<PickablePart>(PickablePart.partId);
			if( pick != null && pick.InInventory ) {
				Entity carrier = GameData.TurnManager.EntityIds[pick.CarrierEntity];
				InventoryPart inv = carrier.GetPart<InventoryPart>(InventoryPart.partId);
				if(inv != null) {
					inv.SetDirty();
				}
			}
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
