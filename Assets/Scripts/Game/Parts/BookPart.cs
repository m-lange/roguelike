﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;

using Map.LayeredGrid;
using Game.Factories;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(BookPart))]
	public class BookPart : Part<BookPart> {

		string[] displayedNames;
		string[] identifiedNames;
		string[] contentGenerators;
		int correlatedEntries;
		
		public BookPart ( string[] identifiedNames, string[] contentGenerators, int correlatedEntries ) {
			this.identifiedNames = identifiedNames;
			this.contentGenerators = contentGenerators;
			this.correlatedEntries = correlatedEntries;

			displayedNames = new string[identifiedNames.Length / correlatedEntries];
			for (int i = 0; i < displayedNames.Length; i++) {
				displayedNames[i] = identifiedNames[i * correlatedEntries];
			}
		}

		public bool Contains (string entity) {
			for (int i = 0; i < identifiedNames.Length; i++) {
				if (identifiedNames [i] == entity) {
					return true;
				}
			}
			return false;
		}

		public Entity Get (string entity) {
			for (int i = 0; i < identifiedNames.Length; i++) {
				if (identifiedNames [i] == entity) {
					return EntityFactory.GetCreator( contentGenerators [i] )(new GridLocation(0,0,0), false);
				}
			}
			return null;
		}

		public List<Entity> GetAll (string entity) {
			List<Entity> result = new List<Entity> ();
			for (int i = 0; i < identifiedNames.Length; i++) {
				if (identifiedNames [i] == entity) {
					int start = correlatedEntries * (i / correlatedEntries);
					for (int j = start; j < start + correlatedEntries; j++) {
						result.Add (EntityFactory.GetCreator (contentGenerators [j]) (new GridLocation (0, 0, 0), false));
					}
				}
			}
			return result;
		}

		[DataMember]
		public string[] DisplayedNames {
			get { return displayedNames; }
			set { displayedNames = value; }
		}
		[DataMember]
		public string[] IdentifiedNames {
			get { return identifiedNames; }
			set { identifiedNames = value; }
		}

		[DataMember]
		public string[] ContentGenerators {
			get { return contentGenerators; }
			set { contentGenerators = value; }
		}

		[DataMember]
		public int CorrelatedEntries {
			get { return correlatedEntries; }
			set { correlatedEntries = value; }
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
