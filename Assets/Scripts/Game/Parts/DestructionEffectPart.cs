﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Effects;
using Game.Destructables;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(DestructionEffectPart))]
	public class DestructionEffectPart : Part<DestructionEffectPart> {

		Destructable destructable;
		//int maxHp;
		//int hp;
		//int recoverPerTurn;

		[DataMember]
		public Destructable Destructable {
			get { return destructable; }
			set { destructable = value; }
		}

		public DestructionEffectPart(Destructable destructable) {
			this.destructable = destructable;
		}

		public Effect Destroy(Entity destroyer, out DestructionEffectRule rule) {
			rule = null;
			if (destructable == null) {
				return null;
			}
			DestructionEffectRule[] rules = destructable.Rules;
			foreach (DestructionEffectRule r in rules) {
				Effect effect = r.Apply( destroyer, Entity );
				if (effect != null) {
					rule = r;
					return effect;
				}
			}
			return null;
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
