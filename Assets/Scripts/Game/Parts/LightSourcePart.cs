﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Events;
using Game.Util;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(LightSourcePart))]
	public class LightSourcePart : Part<LightSourcePart> {

		float range;
		int turnsRemaining; 
		int maxTurns;
		OnOffPart onOff;
		bool disappearWhenEmpty;
		int currentLayer;

		public LightSourcePart(float range, int maxTurns, bool disappearWhenEmpty) {
			this.range = range;
			this.turnsRemaining = maxTurns;
			this.maxTurns = maxTurns;
			//this.isOn = isOn;
			this.disappearWhenEmpty = disappearWhenEmpty;
			this.currentLayer = 0;
			onOff = null;
		}

		[DataMember]
		public float Range {
			get { return range; }
			set { range = value; }
		}

		[DataMember]
		public int TurnsRemaining {
			get { return turnsRemaining; }
			set { turnsRemaining = value; }
		}

		[DataMember]
		public int MaxTurns {
			get { return maxTurns; }
			set { maxTurns = value; }
		}

		[IgnoreDataMember]
		public bool IsOn {
			get { 
				if(onOff == null) {
					onOff = Entity.GetPart<OnOffPart>(OnOffPart.partId);
				}
				return onOff.IsOn; 
			}
			set { 
				if(onOff == null) {
					onOff = Entity.GetPart<OnOffPart>(OnOffPart.partId);
				}
				onOff.IsOn = value; 
			}
		}

		[DataMember]
		public bool DisappearWhenEmpty {
			get { return disappearWhenEmpty; }
			set { disappearWhenEmpty = value; }
		}

		[DataMember]
		public int CurrentLayer {
			get { return currentLayer; }
			set { currentLayer = value; }
		}

		public override void Initialize() {}
		public override void Update () {
			if (IsOn) {
				turnsRemaining -= 1;
				if (turnsRemaining <= 0) {
					IsOn = false;
					if (disappearWhenEmpty) {
						GameData.EventManager.Notify (new RemoveEntityEvent (Entity, true));
						GameData.EventManager.Notify (new DestroyEntityEvent (Entity, null));
					}
				} else {
					PickablePart pick = Entity.GetPart<PickablePart> (PickablePart.partId);
					if (pick != null) {
						if (pick.CarrierEntity == GameData.Player.Id) {
							LocationPart loc = GameData.Player.GetPart<LocationPart>(LocationPart.partId);
							int layer = loc.Location.Layer;
							if( layer >= 0 && currentLayer < 0 ) {
								IsOn = false;
								InventoryPart inv = GameData.Player.GetPart<InventoryPart> (InventoryPart.partId);
								if (inv != null) {
									GameData.EventManager.Notify (new ShowInventoryEvent (GameData.Player, "Your inventory", inv.ToDict()));
								}
							}
							currentLayer = layer;
						}
					}
				}
			}
		}
		public override void Cleanup() {}
	}
}
