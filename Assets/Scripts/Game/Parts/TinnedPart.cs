﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Health points part.
	/// </summary>
	[DataContract]
	[KnownType(typeof(TinnedPart))]
	public class TinnedPart : Part<TinnedPart> {


		public TinnedPart ( ) {
		}

		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
