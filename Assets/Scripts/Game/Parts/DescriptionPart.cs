﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(DescriptionPart))]
	public class DescriptionPart : Part<DescriptionPart> {

		string description;

		[DataMember]
		public string Description {
			get { return description;}
			set { description = value; }
		}

		public DescriptionPart(string description) {
			this.description = description;
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
