﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using Map.LayeredGrid;
using Collections;
using ComponentSystem;
using Game.Serialization;
using Game.Data;
using UnityEngine;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(InventoryPart))]
	public class InventoryPart : Part<InventoryPart> {

		List<PickablePart> items;
		int maxItemCount;
		int maxMass;
		int mass;
		float massFactor;
		bool isDirty = true;
		//int mass;

		public InventoryPart() {
			this.items = new List<PickablePart> ();
			this.maxItemCount = 0;
			this.maxMass = 0;
		}
		public InventoryPart(int maxItemCount, int maxMass, float massFactor) {
			this.items = new List<PickablePart> ();
			this.maxItemCount = maxItemCount;
			this.maxMass = maxMass;
			this.massFactor = massFactor;
		}

		[IgnoreDataMember]
		public List<PickablePart> Items {
			get { return items; }
			//set { items = value; } 
		}

		[DataMember]
		public SaveEntity[] ItemsForSerialization {
			get { 
				if (items.Count == 0) {
					return null;
				}
				//SaveInventoryItem[] save = new SaveInventoryItem[items.Count];
				SaveEntity[] save = new SaveEntity[items.Count];
				int cnt = items.Count;
				for (int i = 0; i < cnt; i++) {
					save [i] = new SaveEntity(items [i].Entity); // new SaveInventoryItem (items [i].InventoryKey.ToString(), new SaveEntity (items [i].Entity));
				}
				return save; 
			}
			set { 
				items = new List<PickablePart> ();
				if (value != null) {

					int cnt = value.Length;
					for (int i = 0; i < cnt; i++) {
						//SaveInventoryItem item = value [i];
						SaveEntity item = value [i];
						Entity entity = item.CreateEntity (true);
						PickablePart pick = entity.GetPart<PickablePart> (PickablePart.partId);
						if (pick != null) {
							//AddItem (item.Key[0], pick);
							AddItem (pick.InventoryKey, pick);
						}
					}
				}
			} 
		}

		[DataMember]
		public int MaxItemCount {
			get { return maxItemCount; }
			set { maxItemCount = value; } 
		}
		[DataMember]
		public int MaxMass {
			get { return maxMass; }
			set { maxMass = value; } 
		}
		[DataMember]
		public float MassFactor {
			get { return massFactor; }
			set { massFactor = value; } 
		}

		[DataMember]
		public bool IsDirty {
			get { return isDirty; }
			set { isDirty = value; }
		}

		public void SetDirty () {
			isDirty = true;
		}

		public bool CanAdd(PickablePart item) {
			return items.Count < maxItemCount && (maxMass <= 0 || GetMass() + item.Mass <= maxMass);
		}
		public bool CanAdd(PickablePart item, int count) {
			return items.Count < maxItemCount && (maxMass <= 0 || GetMass() + (item.Mass * count) <= maxMass);
		}
		public bool AddItem (PickablePart item) {
			if (CanAdd (item)) {
				StackablePart toStack = item.Entity.GetPart<StackablePart> (StackablePart.partId);
				if (toStack != null) {
					List<StackablePart> stacks = Filter<StackablePart>(null);
					StackablePart stack;
					int cnt = stacks.Count;
					for(int i=0; i<cnt; i++) {
						stack = stacks[i];
						if (stack.CanStack (toStack)) {
							stack.Stack (toStack);
							//mass += item.Mass * toStack.Count;
							isDirty = true;
							return true;
						}
					}
					//mass += item.Mass * toStack.Count;
				} else {
					//mass += item.Mass;
				}
				
				char key = GetFreeKey ();
				item.InventoryKey = key;
				items.Add( item );
				item.CarrierEntity = Entity.Id;

				isDirty = true;
				return true;
			}
			return false;
		}
		bool AddItem (char key, PickablePart item) {
			item.InventoryKey = key;
			items.Add( item );
			isDirty = true;
			return true;
		}
		public PickablePart RemoveAt (int index, int count) {
			char key = GetKey (index);
			if (key != default(char)) {
				return Remove(key, count);
			}
			return null;
		}
		public PickablePart Remove (char key, int count) {
			int idx = GetIndex(key);
			if (idx >= 0) {
				PickablePart item = items [idx];
				StackablePart st = item.Entity.GetPart<StackablePart> (StackablePart.partId);
				if (st != null) {
					if ( count > 0 && count < st.Count ) {
						if (st.Count > 1) {
							PickablePart unstacked = st.Unstack (false).GetPart<PickablePart> (PickablePart.partId);
							StackablePart unstackedStack = unstacked.Entity.GetPart<StackablePart>(StackablePart.partId);
							unstackedStack.Count += count - 1;
							st.Count -= count - 1;
							//mass -= unstacked.Mass * count;
							LocationPart loc = unstacked.Entity.GetPart<LocationPart> (LocationPart.partId);
							LocationPart eLoc = Entity.GetPart<LocationPart> (LocationPart.partId);
							if (loc != null && eLoc != null) {
								loc.Location = new GridLocation (eLoc.Location);
							}
							isDirty = true;
							return unstacked;
						}
					} else {
						//mass -= item.Mass * st.Count;
					}
				} else {
					//mass -= item.Mass;
				}

				items.RemoveAt (idx);
				item.CarrierEntity = -1;
				item.IsAtReady = false;
				if (item.Entity.HasPart<EquipmentPart> ()) {
					item.Entity.GetPart<EquipmentPart> ().IsEquiped = false;
				}
				if (item.Entity.HasPart<LocationPart> () && Entity.HasPart<LocationPart> ()) {
					LocationPart loc = item.Entity.GetPart<LocationPart> ();
					loc.Location = new GridLocation (Entity.GetPart<LocationPart> ().Location);
				}

				isDirty = true;
				return item;
			} else {
				return null;
			}
		}
		public PickablePart RemoveItem(PickablePart remove, int count) {
			char key = GetKey(remove);
			if (key != default(char)) {
				return Remove (key, count);
			} else {
				return null;
			}
		}
		public void MoveItemUp (PickablePart item) {
			int idx = GetIndex (item);
			if (idx > 0) {
				//char key = GetKey(idx);
				Items.RemoveAt(idx);
				Items.Insert(idx-1, item);
			}
		}
		public void MoveItemDown (PickablePart item) {
			int idx = GetIndex (item);
			if (idx >= 0 && idx < Items.Count-1) {
				//string key = GetKey(idx);
				Items.RemoveAt(idx);
				Items.Insert(idx+1, item);
			}
		}
		public void ReassignItem (char key, PickablePart item) {
			int idx = GetIndex (item);
			if (idx >= 0) {
				char oldKey = GetKey (idx);
				if (key != oldKey) {
					int tempIdx = GetIndex (key);
					if (tempIdx >= 0) {
						PickablePart temp = Items [tempIdx];
						item.InventoryKey = key;
						temp.InventoryKey = oldKey;
						/*
						if (idx < tempIdx) {
							Items.RemoveAt (tempIdx);
							Items.RemoveAt (idx);
							Items.Insert (idx, key, item);
							Items.Insert (tempIdx, oldKey, temp);
						} else {
							Items.RemoveAt (idx);
							Items.RemoveAt (tempIdx);
							Items.Insert (tempIdx, oldKey, temp);
							Items.Insert (idx, key, item);
						}
						*/
					} else {
						//Items.RemoveAt (idx);
						//Items.Insert (idx, key, item);
						item.InventoryKey = key;
					}
				}
			}
		}

		public char GetKey (PickablePart item) {
			return item.InventoryKey;
		}
		public char GetKey (int idx) {
			return items[idx].InventoryKey;
		}
		public int GetIndex (PickablePart item) {
			int cnt = items.Count;
			for (int i = 0; i < cnt; i++) {
				if (items [i] == item) {
					return i;
				}
			}
			return -1;
		}
		public int GetIndex (char key) {
			int cnt = items.Count;
			for (int i = 0; i < cnt; i++) {
				if (items [i].InventoryKey == key) {
					return i;
				}
			}
			return -1;
		}

		public OrderedDict<string, PickablePart> ToDict () {
			OrderedDict<string, PickablePart> dict = new OrderedDict<string, PickablePart> ();
			int cnt = items.Count;
			PickablePart pick;
			for (int i = 0; i < cnt; i++) {
				pick = items[i];
				dict[pick.InventoryKey.ToString()] = pick;
			}
			return dict;
		}

		public OrderedDict<string, PickablePart> ToDict (List<PickablePart> list) {
			OrderedDict<string, PickablePart> dict = new OrderedDict<string, PickablePart> ();
			int cnt = list.Count;
			PickablePart pick;
			for (int i = 0; i < cnt; i++) {
				pick = list[i];
				dict[pick.InventoryKey.ToString()] = pick;
			}
			return dict;
		}

		public int GetMass () {
			if (isDirty) {
				float mass = 0;
				InventoryPart inv;
				StackablePart stack;
				int cnt = items.Count;
				PickablePart p;
				for(int i=0; i<cnt; i++) {
					p = items[i];
				//foreach (PickablePart p in items.Values) {
					inv = p.Entity.GetPart<InventoryPart> (InventoryPart.partId);
					if (inv != null) {
						mass += inv.GetMass ();
					}
					stack = p.Entity.GetPart<StackablePart> (StackablePart.partId);
					if (stack != null) {
						mass += massFactor * p.Mass * stack.Count;
					} else {
						mass += massFactor * p.Mass;
					}
				}
				this.mass = Mathf.RoundToInt(mass);
				isDirty = false;
				return this.mass;
			} else {
				return this.mass;
			}
		}

		public bool Equip(EquipmentPart equip) {
			if (equip.IsEquiped) {
				return false;
			}
			Equip slot = equip.Slot;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				//foreach (PickablePart pick in items.Values) {
				EquipmentPart eq = pick.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
				if (eq != null) {
					if (eq.IsEquiped && eq.Slot == slot) {
						eq.IsEquiped = false;
					}
				}
			}
			equip.IsEquiped = true;
			return true;
		}
		public bool UnEquip(EquipmentPart equip) {
			if (!equip.IsEquiped) {
				return false;
			}
			equip.IsEquiped = false;
			return true;
		}
		public EquipmentPart GetEquipped (Equip slot) {
			EquipmentPart eq;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				eq = pick.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
				if (eq != null) {
					if (eq.IsEquiped && eq.Slot == slot) {
						return eq;
					} 
				}
			}
			return null;
		}
		public bool Quiver(PickablePart quiver) {
			if (quiver.IsAtReady) {
				return false;
			}
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if (pick.IsAtReady) {
					pick.IsAtReady = false;
				}
			}
			quiver.IsAtReady = true;
			return true;
		}
		public bool UnQuiver(PickablePart quiver) {
			if (!quiver.IsAtReady) {
				return false;
			}
			quiver.IsAtReady = false;
			return true;
		}
		public PickablePart UnQuiver () {
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if (pick.IsAtReady) {
					pick.IsAtReady = false;
					return pick;
				}
			}
			return null;
		}


		char GetFreeKey() {
			foreach (char c in GlobalParameters.letters) {
				if (! ContainsKey (c)) {
					return c;
				}
			}
			return default(char);
		}



		public List<PickablePart> Filter (Func<PickablePart, bool> filter) {
			List<PickablePart> result = new List<PickablePart> ();
			int cnt = items.Count;
			PickablePart pick;
			for (int i = 0; i < cnt; i++) {
				pick = items [i];
				if (filter == null || filter (pick)) {
					result.Add(pick);
				}
			}
			return result;
		}


		public List<P> FilterByEntity<P>(Func<Entity, bool> filter) where P : Part<P> {
			List<P> result = new List<P> ();
			int id = PartType.GetFor<P>().id;
			P part;

			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				part = pick.Entity.GetPart<P>(id);
				if ( part != null && (filter == null || filter(pick.Entity)) ) {
					result.Add( part );
				}
			}
			return result;
		}
		public List<P> Filter<P>(Func<P, bool> filter) where P : Part<P> {
			List<P> result = new List<P> ();
			int id = PartType.GetFor<P>().id;
			P part;


			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				part = pick.Entity.GetPart<P>(id);
				if ( part != null && (filter == null || filter(part)) ) {
					result.Add( part );
				}
			}
			return result;
		}
		public P FilterOne<P>(Func<P, bool> filter) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			P part;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				part = pick.Entity.GetPart<P>(id);
				if ( part != null && (filter == null || filter(part)) ) {
					return part;
				}
			}
			return null;
		}

		public OrderedDict<string, Entity> FilterEntities(Func<Entity, bool> filter) {
			OrderedDict<string, Entity> result = new OrderedDict<string, Entity> ();
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if ( (filter == null || filter(pick.Entity)) ) {
					result [pick.InventoryKey.ToString()] = pick.Entity;
				}
			}
			return result;
		}
		public OrderedDict<string, Entity> FilterEntities<P>(Func<Entity, bool> filter) where P : Part<P> {
			OrderedDict<string, Entity> result = new OrderedDict<string, Entity> ();
			int id = PartType.GetFor<P>().id;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if ( pick.Entity.HasPart(id) && (filter == null || filter(pick.Entity)) ) {
					result [pick.InventoryKey.ToString()] = pick.Entity;
				}
			}
			return result;
		}

		public bool ContainsKey (char key) {
			int cnt = items.Count;
			for (int i = 0; i < cnt; i++) {
				if (items [i].InventoryKey == key) {
					return true;
				}
			}
			return false;
		}
		public PickablePart GetItem (char key) {
			int cnt = items.Count;
			for (int i = 0; i < cnt; i++) {
				if (items [i].InventoryKey == key) {
					return items [i];
				}
			}
			return null;
		}
		public bool Contains (Func<Entity, bool> filter) {
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if ( (filter == null || filter(pick.Entity)) ) {
					return true;
				}
			}
			return false;
		}
		public bool Contains<P>(Func<P, bool> filter) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			P part;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				part = pick.Entity.GetPart<P>(id);
				if ( part != null && (filter == null || filter(part)) ) {
					return true;
				}
			}
			return false;
		}

		public PickablePart FindFirst (Func<Entity, bool> filter) {
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				if ( (filter == null || filter(pick.Entity)) ) {
					return pick;
				}
			}
			return null;
		}
		public P FindFirst<P> (Func<P, bool> filter) where P : Part<P> {
			int id = PartType.GetFor<P>().id;
			P part;
			int cnt = items.Count;
			PickablePart pick;
			for(int i=0; i<cnt; i++) {
				pick = items[i];
				part = pick.Entity.GetPart<P>(id);
				if ( part != null ) {
				  	if( filter == null || filter(part) ) {
						return part;
					}
				}
			}
			return null;
		}

		public PickablePart GetQuivered() {
			int cnt = Items.Count;
			PickablePart pick;
			for (int i = 0; i < cnt; i++) {
				pick = Items[i];
				if (pick.IsAtReady) {
					return pick;
				}
			}
			return null;
		}
		
		public EquipmentPart GetLauncher (PickablePart thrown) {
			if (thrown.Entity.HasPart<ProjectilePart> ()) {
				ProjectilePart p = thrown.Entity.GetPart<ProjectilePart> ();
				if (p.LauncherType != Game.Data.LauncherType.None) {
					return FindFirst<EquipmentPart>( (eq) => eq.IsEquiped && eq.LauncherType == p.LauncherType );
				}
			}
			return null;
		}

		public override void Initialize() {
			if (this.items == null) {
				this.items = new List<PickablePart> ();
			}
			foreach (PickablePart p in items) {
				p.Entity.Initialize ();
				GameData.AssignInventoryEntities(p.Entity);
			}
			isDirty = true;
		}

		List<PickablePart> tempItems = new List<PickablePart>();
		public override void Update () {
			if (tempItems == null) {
				tempItems = new List<PickablePart>();
			}
			tempItems.AddRange(items);
			int cnt = tempItems.Count;
			for (int i=0; i<cnt; i++) {
				tempItems[i].Entity.Update ();
			}
			tempItems.Clear();
		}
		public override void Cleanup() {
			
		}


	}
}
