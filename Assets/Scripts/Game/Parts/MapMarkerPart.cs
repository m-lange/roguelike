﻿using System.Runtime.Serialization;
using Map.LayeredGrid;
using ComponentSystem;

using UnityEngine;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(MapMarkerPart))]
	public class MapMarkerPart : Part<MapMarkerPart> {

		
		string label;


		public MapMarkerPart(string label) {
			this.label = label;
		}
		
		[DataMember]
		public string Label {
			get { return label; }
			set { label = value; }
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
