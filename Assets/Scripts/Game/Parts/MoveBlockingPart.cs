﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(MoveBlockingPart))]
	public class MoveBlockingPart : Part<MoveBlockingPart> {
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
