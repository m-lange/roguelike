﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(CameraPart))]
	public class CameraPart : Part<CameraPart> {
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
