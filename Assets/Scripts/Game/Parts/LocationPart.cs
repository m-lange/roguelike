﻿using System;
using Map.LayeredGrid;
using ComponentSystem;
using Game.Events;
using System.Runtime.Serialization;

namespace Game.Parts {

	/// <summary>
	/// Grid Location component.
	/// </summary>
	[DataContract]
	[KnownType(typeof(LocationPart))]
	public class LocationPart : Part<LocationPart> {

		ImmutableGridLocation location;
		bool isStatic;


		public LocationPart(GridLocation location, bool isStatic) {
			this.location = new ImmutableGridLocation (location);
			this.isStatic = isStatic;
		}


		/// <summary>
		/// Gets or sets the location.
		/// Setting the location triggers an EntityMovedEvent, so no need to trigger it explicitly.
		/// Do not try to change the returned GridLocation, as it is immutable.
		/// </summary>
		/// <value>The location.</value>
		[IgnoreDataMember]
		public GridLocation Location {
			get { return location; }
			set { 
				if (isStatic) {
					throw new NotSupportedException ( "Can't set the location of a static LocationPart!" );
				}
				if (location == null || !location.Equals (value)) {
					GridLocation oldLoc = location;
					if (value is ImmutableGridLocation) {
						location = (ImmutableGridLocation) value; 
					} else {
						location = new ImmutableGridLocation (value);
					}
					GameData.EventManager.Notify (new EntityMovedEvent (Entity, this, oldLoc));
				}
			}
		}


		[DataMember]
		public GridLocation LocationForSerialization {
			get { return new GridLocation(location); }
			set { location = new ImmutableGridLocation (value); }
		}


		[DataMember]
		public bool IsStatic {
			get { return isStatic; }
			set { isStatic = value; }
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
