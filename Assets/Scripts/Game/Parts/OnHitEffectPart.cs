﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.EntityEffects;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(OnHitEffectPart))]
	public class OnHitEffectPart : Part<OnHitEffectPart> {
				
		EntityEffect[] effects;

		public OnHitEffectPart ( params EntityEffect[] effects ) {
			if (effects != null && effects.Length > 0) {
				this.effects = effects;
			}
		}

		[DataMember]
		public EntityEffect[] Effects {
			get { return effects; }
			set { effects = value; }
		}
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
