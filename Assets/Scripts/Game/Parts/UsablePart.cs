﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Usables;
using TurnSystem;
using Map.LayeredGrid;
using Game.Effects;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(UsablePart))]
	public class UsablePart : Part<UsablePart> {


		Usable usable;

		public UsablePart(Usable usable) {
			this.usable = usable;
		}

		[DataMember]
		public Usable Usable {
			get { return usable; }
			set { usable = value; }
		}
		public Action GetAction (ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo) {
			return usable.GetAction(actor, tool, useWith, location, locationInfo);
		}
		/*
		public Effect GetEffect(ActorPart actor, Entity tool, Entity useWith, GridLocation location, TileInfo locationInfo, out UseEffectRule rule) {
			UseEffectRule[] rules = usable.Rules;
			rule = null;
			foreach (UseEffectRule r in rules) {
				Effect effect = r.Apply( actor, tool, useWith, location, locationInfo );
				if (effect != null) {
					rule = r;
					return effect;
				}
			}
			return null;
		}
		*/

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
