﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Effects;
using Game.Destructables;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(DestructablePart))]
	public class DestructablePart : Part<DestructablePart> {
		
		public DestructablePart() {
			
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
