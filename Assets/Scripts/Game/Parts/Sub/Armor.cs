﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Runtime.Serialization;
using Game.Rules;
using Game.Data;
using UnityEngine.Diagnostics;

namespace Game.Parts.Sub {

	[DataContract(Namespace="Parts.Sub")]
	public class Armor {
		
		int[] byType;
		
		public Armor(int impact, int blade, int pierce) {
			this.byType = new int[]{ impact, blade, pierce };
		}
		public Armor() : this( 0, 0, 0 ) {}
		public Armor(Armor armor) : this( armor.ByType ) {}
		public Armor(int[] armor) : this( armor[0], armor[0], armor[0] ) {}
		
		
		public bool Any() {
			return byType[0] > 0 || byType[1] > 0 || byType[2] > 0;
		}
		
		public void Add(Armor other) {
			for(int i=0; i<byType.Length; i++) {
				byType[i] += other.ByType[i];
			}
		}
		
		[DataMember]
		public int[] ByType {
			get { return byType; }
			set { byType = value; }
		}
	}
}
