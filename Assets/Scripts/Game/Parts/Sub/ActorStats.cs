﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

using Game.Rules;
using Game.Data;

namespace Game.Parts.Sub {

	[DataContract(Namespace="Parts.Sub")]
	public class ActorStats {


		int speed;

		int strength;
		int strengthPoints;

		int constitution;
		int constitutionPoints;

		int dexterity;
		int dexterityPoints;

		int attack;
		int attackPoints;
		int attackType;

		int attackDuration;

		int defense;
		int defensePoints;

		Armor armor;


		int maxStomachContent;
		int stomachContent;
		GameRules.SatiationStatus satiationStatus;
		GameRules.BurdeningStatus burdeningStatus;
		bool canStarve;

		int[] skill;
		int[] skillPoints;

		bool hasPassiveAttack;

		public ActorStats(int speed, int strength, int constitution, int dexterity, int attackNoWeapon, int attackDuration, AttackTypes attackType, int defense, Armor armor, int maxStomachContent, bool canStarve, bool hasPassiveAttack) {
			this.speed = speed;
			this.strength = strength;
			this.constitution = constitution;
			this.dexterity = dexterity;
			this.attack = attackNoWeapon;
			this.attackDuration = attackDuration;
			this.attackType = (int) attackType;
			this.defense = defense;
			this.armor = armor;

			this.hasPassiveAttack = hasPassiveAttack;

			this.canStarve = canStarve;
			this.maxStomachContent = maxStomachContent;
			this.stomachContent = (int) (maxStomachContent * GameRules.SatiatedStomachContent);

			this.satiationStatus = GameRules.SatiationStatus.Normal;
			this.burdeningStatus = GameRules.BurdeningStatus.Unburdened;

			int max = (int) Enum.GetValues(typeof(Skills)).Cast<Skills>().Max();
			skill = new int[max+1];
			skillPoints = new int[max+1];
		}

		public void Update () {
			UpdateStat( ref strength, ref strengthPoints );
			UpdateStat( ref constitution, ref constitutionPoints );
			UpdateStat( ref dexterity, ref dexterityPoints );
			UpdateStat( ref attack, ref attackPoints );
			UpdateStat( ref defense, ref defensePoints );
		}

		void UpdateStat (ref int stat, ref int points) {
			if (points > GameRules.PointsForLevelGain) {
				stat += 1;
				points -= GameRules.PointsForLevelGain;
				if (stat > GameRules.MaxStatsLevel) {
					stat = GameRules.MaxStatsLevel;
				}
			} else if (points < -GameRules.PointsForLevelGain) {
				stat -= 1;
				points += GameRules.PointsForLevelGain;
				if (stat < GameRules.MinStatsLevel) {
					stat = GameRules.MinStatsLevel;
				}
			}
		}

		public void GainSkill (Skills skill, int value) {
			int idx = (int)skill;
			this.skillPoints [idx] += value;
			if (this.skillPoints [idx] >= GameRules.PointsForSkillGain) {
				this.skillPoints [idx] -= GameRules.PointsForSkillGain;
				this.skill [idx] += 1;
				if (this.skill [idx] > GameRules.MaxSkillLevel) {
					this.skill [idx] = GameRules.MaxSkillLevel;
				}
			}
		}

		[DataMember]
		public int[] Skill {
			get { return skill; }
			set { skill = value; }
		}
		[DataMember]
		public int[] SkillPoints {
			get { return skillPoints; }
			set { skillPoints = value; }
		}

		[DataMember]
		public int BaseConstitution {
			get { return constitution; }
			set { constitution = value; }
		}
		[DataMember]
		public int ConstitutionPoints {
			get { return constitutionPoints; }
			set { constitutionPoints = value; }
		}

		[DataMember]
		public int BaseSpeed {
			get { return speed; }
			set { speed = value; }
		}


		[DataMember]
		public int BaseStrength {
			get { return strength; }
			set { strength = value; }
		}
		[DataMember]
		public int StrengthPoints {
			get { return strengthPoints; }
			set { strengthPoints = value; }
		}


		[DataMember]
		public int BaseDexterity {
			get { return dexterity; }
			set { dexterity = value; }
		}
		[DataMember]
		public int DexterityPoints {
			get { return dexterityPoints; }
			set { dexterityPoints = value; }
		}

		/// <summary>
		/// Bare-handed attack. Not additive to weapon attack!
		/// </summary>
		/// <value>The base attack.</value>
		[DataMember]
		public int BaseAttack {
			get { return attack; }
			set { attack = value; }
		}
		[DataMember]
		public int AttackPoints {
			get { return attackPoints; }
			set { attackPoints = value; }
		}
		
		[DataMember]
		public int AttackDuration {
			get { return attackDuration; }
			set { attackDuration = value; }
		}
		
		[DataMember]
		public int AttackType {
			get { return attackType; }
			set { attackType = value; }
		}


		[DataMember]
		public int BaseDefense {
			get { return defense; }
			set { defense = value; }
		}
		[DataMember]
		public int DefensePoints {
			get { return defensePoints; }
			set { defensePoints = value; }
		}


		[DataMember]
		public Armor BaseArmor {
			get { return armor; }
			set { armor = value; }
		}

		[DataMember]
		public bool HasPassiveAttack {
			get { return hasPassiveAttack; }
			set { hasPassiveAttack = value; }
		}


		[DataMember]
		public int StomachContent {
			get { return stomachContent; }
			set { stomachContent = value; }
		}
		[DataMember]
		public int MaxStomachContent {
			get { return maxStomachContent; }
			set { maxStomachContent = value; }
		}
		[DataMember]
		public bool CanStarve {
			get { return canStarve; }
			set { canStarve = value; }
		}
		[DataMember]
		public GameRules.SatiationStatus SatiationStatus {
			get { return satiationStatus; }
			set { satiationStatus = value; }
		}

		[DataMember]
		public GameRules.BurdeningStatus BurdeningStatus {
			get { return burdeningStatus; }
			set { burdeningStatus = value; }
		}
	}
}
