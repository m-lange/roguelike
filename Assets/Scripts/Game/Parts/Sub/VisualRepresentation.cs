﻿
using Map.LayeredGrid;

namespace Game.Parts.Sub {
	public interface VisualRepresentation {

		void SetLocation(GridLocation location);
		void SetVisible(bool visible);
		bool IsVisible();
		//void SetTexture(string texture);
		void DestroyVisual();

	}
}
