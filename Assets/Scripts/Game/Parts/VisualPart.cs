﻿using System.Runtime.Serialization;

using UnityEngine;

using Map.LayeredGrid;
using Map.LayeredGrid.Algorithm;
using ComponentSystem;
using Game.Parts.Sub;
using Game.Visual;

namespace Game.Parts {


	[DataContract]
	[KnownType(typeof(VisualPart))]
	public class VisualPart : Part<VisualPart> {
		
		public string prefab;
		public string texture;
		bool isCamera;
		bool alwaysVisible;

		VisualRepresentation visual;

		public VisualPart() {

		}
		public VisualPart(string prefab, string texture, bool isCamera, bool alwaysVisible) {
			this.prefab = prefab;
			this.texture = texture;
			this.isCamera = isCamera;
			this.alwaysVisible = alwaysVisible;
		}

		[IgnoreDataMember]
		public VisualRepresentation Visual {
			get { return visual;}
			set { visual = value; }
		}

		[DataMember]
		public string Prefab {
			get { return prefab; }
			set { prefab = value; }
		}
		[DataMember]
		public string Texture {
			get { return texture; }
			set { texture = value; }
		}
		[DataMember]
		public bool IsCamera {
			get { return isCamera; }
			set { isCamera = value; }
		}
		[DataMember]
		public bool AlwaysVisible {
			get { return alwaysVisible; }
			set { alwaysVisible = value; }
		}

		public void SetLocation(GridLocation location) {
			if (visual != null) {
				visual.SetLocation (location);
			}
		}
		public void SetVisible(bool visible) {
			if ( visual != null && ((!alwaysVisible) || visible)) {
				visual.SetVisible (visible);
			}
		}
		public bool IsVisible() {
			return (visual != null) && visual.IsVisible ();
		}
		
		public void InitTexture() {
			LocationPart loc = Entity.GetPart<LocationPart> (LocationPart.partId);
			
			GameObject mgo = (GameObject)GameObject.Instantiate (Resources.Load ("Entities/AtlasTile"));
			mgo.transform.SetParent (GlobalParameters.Instance.transform);
			visual = mgo.AddComponent<EntityVisual> ();
			mgo.GetComponent<MeshFilter> ().sharedMesh = Game.Data.Sprites.GetMesh (prefab, texture);
			if (loc != null) {
				SetLocation (loc.Location);
			}
		}
		
		public override void Initialize () {
			if ((!Entity.HasPart (PickablePart.partId)) || (!Entity.GetPart<PickablePart> (PickablePart.partId).InInventory)) {
				if (visual == null) {
					if (isCamera) {
						visual = GameObject.Find (prefab).GetComponent<CameraVisual> ();
					} else {
						InitTexture();
					}
				}
				LocationPart loc = Entity.GetPart<LocationPart> (LocationPart.partId);
				if (loc != null) {
					VisibilityGrid vis = GameData.Visibility.Visible;
					visual.SetVisible (vis != null && vis.Contains(loc.Location) && vis.IsCenterVisible (loc.Location));
				} else {
					visual.SetVisible (false);
				}
			}
		//	Debug.Log ("Initializing "+Entity.Id+": "+TextUtil.GetName(Entity));
		}
		public override void Update() {}
		public override void Cleanup () {
			if (visual != null) {
				visual.SetVisible (false);
				visual.DestroyVisual ();
				visual = null;
			}
		}
	}
}
