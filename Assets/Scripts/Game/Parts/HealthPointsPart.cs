﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Health points part.
	/// </summary>
	[DataContract]
	[KnownType(typeof(HealthPointsPart))]
	public class HealthPointsPart : Part<HealthPointsPart> {

		int maxHp;
		int maxHpPoints;

		int hp;
		int hpPoints;

		int recoverPointsPerTurn;
		bool immortal;

		[DataMember]
		public int MaxHp {
			get { return maxHp; }
			set { maxHp = value; }
		}

		[DataMember]
		public int MaxHpPoints {
			get { return maxHpPoints; }
			set { maxHpPoints = value; }
		}

		[DataMember]
		public int Hp {
			get { return hp; }
			set { hp = value; }
		}

		[DataMember]
		public int HpPoints {
			get { return hpPoints; }
			set { hpPoints = value; }
		}

		[DataMember]
		public int RecoverPointsPerTurn {
			get { return recoverPointsPerTurn; }
			set { recoverPointsPerTurn = value; }
		}

		[DataMember]
		public bool Immortal {
			get { return immortal; }
			set { immortal = value; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Game.Parts.HealthPointsPart"/> class.
		/// Negative values of recoverPerTurn result in random recovery by ont point with
		/// probability 1/recoverPerTurn
		/// </summary>
		/// <param name="maxHp">Max hp.</param>
		/// <param name="hp">Hp.</param>
		/// <param name="recoverPerTurn">Recover per turn.</param>
		public HealthPointsPart(int maxHp, int hp, int recoverPointsPerTurn) {
			this.maxHp = maxHp;
			this.hp = hp;
			this.recoverPointsPerTurn = recoverPointsPerTurn;
		}

		public bool DealDamage(int damage) {
			hp -= damage;
			if(immortal) return false;
			return hp <= 0;
		}

		public void Heal (int value) {
			hp += value;
			if (hp > maxHp) {
				hp = maxHp;
			}
		}
		public void ExtraHeal (int value) {
			hp += value;
			if (hp > maxHp) {
				maxHp += 1;
				hp = maxHp;
			}
		}
		public void FullHeal () {
			hp = maxHp;
		}

		public void GainHpPoints (int points) {
			hpPoints += points;
			if (hpPoints >= GameRules.PointsForLevelGain) {
				hp += 1;
				hpPoints -= GameRules.PointsForLevelGain;
				if (hp > maxHp) {
					hp = maxHp;
				}
			} else if (hpPoints <= -GameRules.PointsForLevelGain) {
				hp -= 1;
				hpPoints += GameRules.PointsForLevelGain;
			}
		}
		public void GainMaxHpPoints (int points) {
			maxHpPoints += points;
			if (maxHpPoints > GameRules.PointsForLevelGain) {
				maxHp += 1;
				maxHpPoints -= GameRules.PointsForLevelGain;
				if (maxHp > GameRules.MaxStatsLevel) {
					maxHp = GameRules.MaxStatsLevel;
				}
			} else if (maxHpPoints < -GameRules.PointsForLevelGain) {
				maxHp -= 1;
				maxHpPoints += GameRules.PointsForLevelGain;
				if (maxHp < GameRules.MinStatsLevel) {
					maxHp = GameRules.MinStatsLevel;
				}
			}
		}

		public override void Initialize() {}
		public override void Update () {
			ActorPart aPart = Entity.GetPart<ActorPart> (ActorPart.partId);
			if (aPart != null) {
				GainHpPoints( GameRules.CalcHpPointsRegeneration( aPart, this ) );
			} else {
				if (hp >= maxHp) {
					return;
				}
				GainHpPoints( recoverPointsPerTurn );
			}
			if (hp > maxHp) {
				hp = maxHp;
			}
		}
		public override void Cleanup() {}
	}
}
