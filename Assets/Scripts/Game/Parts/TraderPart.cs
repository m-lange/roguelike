﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Collections;
using Game.Data;
using Game.Events;
using Game.Factories;
using Game.Rules;
using UnityEngine;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(TraderPart))]
	public class TraderPart : Part<TraderPart> {

		bool isSpecialized;
		HashSet<TradeClass> buyClasses;	
		HashSet<TradeClass> sellClasses;
		string[] tradeItems;		
		int updateOffset;		

		public TraderPart(bool isSpecialized, string[] tradeItems, TradeClass[] buyClasses, TradeClass[] sellClasses = null) {
			this.isSpecialized = isSpecialized;
			this.tradeItems = tradeItems;
			this.buyClasses = new HashSet<TradeClass>( buyClasses );
			this.sellClasses = sellClasses == null 
						? new HashSet<TradeClass>( buyClasses ) 
						: new HashSet<TradeClass>( sellClasses );
			this.updateOffset = RandUtils.NextInt( GameRules.TraderUpdateInterval );
		}

		public List<PickablePart> Sells () {
			if (!Entity.HasPart<InventoryPart> ()) {
				return new List<PickablePart> ();
			}
			InventoryPart inv = Entity.GetPart<InventoryPart> ();
			List<PickablePart> wares = inv.Filter ((p) => CanSell (p));
			return wares;
		}

		public bool CanSell (PickablePart pick) {
			if (pick.TradeClasses.Contains (TradeClass.None)) {
				return false;
			}
			if (sellClasses.Contains (TradeClass.All) || pick.TradeClasses.Contains (TradeClass.All)) {
				return true;	
			}
			foreach (TradeClass cl in pick.TradeClasses) {
				if (sellClasses.Contains (cl)) {
					return true;
				}
			}
			return false;
		}
		public bool CanBuy (PickablePart pick) {
			if (pick.TradeClasses.Contains (TradeClass.None)) {
				return false;
			}
			if (buyClasses.Contains (TradeClass.All) || pick.TradeClasses.Contains (TradeClass.All)) {
				return true;	
			}
			foreach (TradeClass cl in pick.TradeClasses) {
				if (buyClasses.Contains (cl)) {
					return true;
				}
			}
			return false;
		}
		
		[DataMember]
		public bool IsSpecialized {
			get { return isSpecialized; }
			set { isSpecialized = value; }
		}
		
		[DataMember]
		public int UpdateOffset {
			get { return updateOffset; }
			set { updateOffset = value; }
		}
		
		[DataMember]
		public string[] TradeItems {
			get { return tradeItems; }
			set { tradeItems = value; }
		}

		[IgnoreDataMember]
		public HashSet<TradeClass> BuyClasses {
			get { return buyClasses; }
			set { buyClasses = value; }
		}
		[IgnoreDataMember]
		public HashSet<TradeClass> SellClasses {
			get { return sellClasses; }
			set { sellClasses = value; }
		}
		[DataMember]
		public TradeClass[] BuyClassesForSerialization {
			get { 
				TradeClass[] arr = new TradeClass[ buyClasses.Count ];
				buyClasses.CopyTo(arr, 0);
				return arr; 
			}
			set { this.buyClasses = new HashSet<TradeClass>( value ); }
		}
		[DataMember]
		public TradeClass[] SellClassesForSerialization {
			get { 
				TradeClass[] arr = new TradeClass[ sellClasses.Count ];
				sellClasses.CopyTo(arr, 0);
				return arr; 
			}
			set { this.sellClasses = new HashSet<TradeClass>( value ); }
		}
		public override void Initialize() {}
		public override void Update() {
			//Debug.Log( (GameData.TurnManager.TurnCounter + updateOffset) % GameRules.TraderUpdateInterval );
			if( (GameData.TurnManager.TurnCounter + updateOffset) % GameRules.TraderUpdateInterval == 0 ) {
				UpdateInventory();
			}
		}
		protected void UpdateInventory() {
			InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
			List<PickablePart> wares = inv.Filter ((p) => CanSell (p));
			int cnt = wares.Count;
			
			MoneyPart mp = inv.FilterOne<MoneyPart> (null);
			StackablePart money = mp.Entity.GetPart<StackablePart> (StackablePart.partId);
			
			if( money.Count < GameRules.TraderMoneyMin ) {
				money.Count += GameRules.TraderMoneyInc;
			} else if( money.Count > GameRules.TraderMoneyMax ) {
				money.Count -= GameRules.TraderMoneyInc;
			}
			
			if( tradeItems.Length > 0 ) {
				int target = ( buyClasses.Count == 1 && buyClasses.Contains(TradeClass.Books) ) ? GameRules.BookTraderItems : GameRules.DefaultTraderItems;
				if( cnt > target || (cnt > target * 0.5 && RandUtils.NextBool(0.5f)) ) {
					PickablePart pick = RandUtils.Select( wares );
					Debug.Log("Remove trade item: " + pick.Entity.GetPart<NamePart>().Name);
					GameData.EventManager.Notify (new RemoveEntityEvent (pick.Entity, true));
					GameData.EventManager.Notify (new DestroyEntityEvent (pick.Entity, null));
				}
				if( cnt < target || (cnt < target * 1.5 && RandUtils.NextBool(0.5f)) ) {
					LocationPart loc = Entity.GetPart<LocationPart>(LocationPart.partId);
					string creator = RandUtils.Select( tradeItems );
					Entity item = EntityFactory.GetCreator( creator )(loc.Location, false);
					Debug.Log("Add trade item: " + item.GetPart<NamePart>().Name);
					EntityFactory.AddToInventory(inv, item, false );
				}
			}
		}
		public override void Cleanup() {}
	}
}
