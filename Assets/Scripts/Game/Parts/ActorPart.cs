﻿using System.Collections;
using System.Collections.Generic;
using TurnSystem;
using ComponentSystem;
using Game.AI;
using Game.Rules;
using Game.Parts.Sub;
using Game.Events;
using Game.Messages;
using Game.ActorEffects;
using Game.ActorEffects.Continuous;
using System.Runtime.Serialization;
using UnityEngine;

namespace Game.Parts {

	/// <summary>
	/// Turn-taking actor component.
	/// </summary>
	[DataContract]
	[KnownType(typeof(ActorPart))]
	public class ActorPart : Part<ActorPart> {


		//string aiClass;
		ActorAI ai = null;
		int faction;

		Action nextAction;


		ActorStats stats;
		int actionPoints;
		int agressiveAgainstPlayer;

		ActorEffect[] attackEffects;
		List<ContinuousEffect> effects;

		public ActorPart(string faction, ActorAI ai, ActorStats stats, params ActorEffect[] attackEffects) {
			this.ai = ai;
			this.stats = stats;
			this.faction = GameData.Factions.Get (faction).id;

			this.actionPoints = (stats.BaseSpeed > 1) ? RandUtils.NextInt (stats.BaseSpeed + 1) : 0;
			this.agressiveAgainstPlayer = 0;

			this.effects = new List<ContinuousEffect>();
			this.attackEffects = attackEffects;
		}

		public void GainStrength (int points) {
			stats.StrengthPoints += points;
		}
		public void GainConstitution (int points) {
			stats.ConstitutionPoints += points;
		}
		public void GainDexterity (int points) {
			stats.DexterityPoints += points;
		}
		public void GainAttack (int points) {
			stats.AttackPoints += points;
		}
		public void GainDefense (int points) {
			stats.DefensePoints += points;
		}

		[DataMember]
		public ActorAI ActorAi {
			get { return ai; }
			set { ai = value; }
		}

		[DataMember]
		public ActorStats Stats {
			get { return stats; }
			set { stats = value; }
		}

		[DataMember]
		public int Faction {
			get { return faction; }
			set { faction = value; }
		}

		public void SetFaction (string faction) {
			this.faction = GameData.Factions.Get (faction).id;
		}

		[IgnoreDataMember]
		public int Speed {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseSpeed; 
				}
				int sp = stats.BaseSpeed;
				EquipmentPart eq;

				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped) {
							sp += eq.SpeedBonus;
						}
					}
				}
				int len = effects.Count;
				ContinuousEffect effect;
				for (int i = 0; i < len; i++) {
					effect = effects [i];
					if (effect is ModifyStatsEffect) {
						sp += (effect as ModifyStatsEffect).SpeedBonus;
					}
				}
				if (GameData.IsPlayer (this)) {
					sp = (int) (sp * (1f - ((int)stats.BurdeningStatus) / 6f));
					if (sp < 1) {
						sp = 1;
					}
				}

				return sp;
			}
		}
		[IgnoreDataMember]
		public int Strength {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseStrength; 
				}
				int st = stats.BaseStrength;
				EquipmentPart eq;
				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped) {
							st += eq.StrengthBonus;
						}
					}
				}
				int len = effects.Count;
				ContinuousEffect effect;
				for(int i=0; i<len;i++) {
					effect = effects[i];
					if (effect is ModifyStatsEffect) {
						st += (effect as ModifyStatsEffect).StrengthBonus;
					}
				}
				return st;
			}
		}
		[IgnoreDataMember]
		public int Dexterity {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseDexterity; 
				}
				int dx = stats.BaseDexterity;
				EquipmentPart eq;
				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped) {
							dx += eq.DexterityBonus;
						}
					}
				}
				int len = effects.Count;
				ContinuousEffect effect;
				for(int i=0; i<len;i++) {
					effect = effects[i];
					if (effect is ModifyStatsEffect) {
						dx += (effect as ModifyStatsEffect).DexterityBonus;
					}
				}
				return dx;
			}
		}

		/// <summary>
		/// Attack value. Either BaseAttack if no weapon(s), sum of weapons otherwise.
		/// </summary>
		/// <value>The attack.</value>
		[IgnoreDataMember]
		public int Attack {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseAttack; 
				}
				int at = 0;
				EquipmentPart eq;
				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped && eq.AttackBonus > 0) {
							int att = eq.AttackBonus;
							if (eq.Skill != Game.Data.Skills.None) {
								att = (int) (att * (1f + Stats.Skill[ (int) eq.Skill ] / 10f));
							}
							at += att;
						}
					}
				}
				if (at == 0) {
					at = stats.BaseAttack;
				}
				int len = effects.Count;
				ContinuousEffect effect;
				for(int i=0; i<len;i++) {
					effect = effects[i];
					if (effect is ModifyStatsEffect) {
						at += (effect as ModifyStatsEffect).AttackBonus;
					}
				}
				return at;
			}
		}
		
		[IgnoreDataMember]
		public int Defense {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseDefense; 
				}
				float def = 1f - (0.01f * stats.BaseDefense);
				EquipmentPart eq;
				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped) {
							int de = eq.DefenseBonus;
							if( de >= 0 ) {
								def *= 1f - (0.01f * de);
							} else {
								def = 1f - (1f - def) * (1f + (0.01f * de));
							}
						}
					}
				}
				int len = effects.Count;
				ContinuousEffect effect;
				for(int i=0; i<len;i++) {
					effect = effects[i];
					if (effect is ModifyStatsEffect) {
						int de = (effect as ModifyStatsEffect).DefenseBonus;
						if( de >= 0 ) {
							def *= 1f - (0.01f * de);
						} else {
							def = 1f - (1f - def) * (1f + (0.01f * de));
						}
					}
				}
				return (int) Mathf.Round(100 * (1f - def));
			}
		}
		//[IgnoreDataMember]
		public Armor Armor {
			get { 
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv == null) {
					return stats.BaseArmor; 
				}
				int len = stats.BaseArmor.ByType.Length;
				float[] ar = new float[len];
				for(int i = 0; i<len; i++) ar[i] = 1f - (0.01f * stats.BaseArmor.ByType[i]);
				
				EquipmentPart eq;
				List<PickablePart> items = inv.Items;
				PickablePart p;
				int cnt = items.Count;
				for(int i=0; i<cnt; i++) {
					p = items[i];
					eq = p.Entity.GetPart<EquipmentPart> (EquipmentPart.partId);
					if (eq != null) {
						if (eq.IsEquiped) {
							int[] a = eq.ArmorBonus.ByType;
							for(int j = 0; j<len; j++) ar[j] *= 1f - (0.01f * a[j]);
						}
					}
				}
				int effLen = effects.Count;
				ContinuousEffect effect;
				for(int i=0; i<effLen;i++) {
					effect = effects[i];
					if (effect is ModifyStatsEffect) {
						int[] a = (effect as ModifyStatsEffect).ArmorBonus.ByType;
						for(int j = 0; j<len; j++) ar[j] *= 1f - (0.01f * a[j]);
					}
				}
				Armor arm = new Armor();
				int[] arr = arm.ByType;
				for(int i = 0; i<len; i++) arr[i] = (int) Mathf.Round(100 * (1f - ar[i]));
				return arm;
			}
		}
		public int Constitution {
			get { return stats.BaseConstitution; }
		}

		[DataMember]
		public int ActionPoints {
			get { return actionPoints; }
			set { actionPoints = value; }
		}

		[DataMember]
		public int AgressiveAgainstPlayer {
			get { return agressiveAgainstPlayer; }
			set { agressiveAgainstPlayer = value; }
		}

		[DataMember]
		public Action[] NextActionForSerialization {
			get { return nextAction == null ? null : new Action[] {nextAction}; }
			set { nextAction = value == null ? null : value[0]; }
		}
		[IgnoreDataMember]
		public List<ContinuousEffect> Effects {
			get { return effects; }
		}

		[DataMember]
		public ContinuousEffect[] EffectsForSerialization {
			get {
				if (effects.Count == 0)
					return null;
				return effects.ToArray ();
			}
			set {
				if (value == null) {
					effects = new List<ContinuousEffect> ();
				} else {
					effects = new List<ContinuousEffect> (value);
				}
			}
		}

		[IgnoreDataMember]
		public ActorEffect[] AttackEffects {
			get { return attackEffects; }
		}
		[DataMember]
		public ActorEffect[] AttackEffectsForSerialization {
			get { return attackEffects.Length == 0 ? null : attackEffects; }
			set { attackEffects = value == null ? new ActorEffect[0] : value; }
		}

		public Action GetNextAction() {
			if (ai == null || nextAction != null) {
				Action a = nextAction;
				nextAction = null;
				return a;
			} else {
				Action a = ai.GetNextAction(this);
				nextAction = null;
				return a;
			}
		}
		public void SetNextAction(Action a) {
			nextAction = a;
		}
		public void CancelAction() {
			nextAction = null;
		}
		public bool CanAct() {
			return actionPoints >= Game.Rules.GameRules.DefaultCost;
		}
		public void UseActionPoints(Action action) {
			int cost = action.GetCost();
			if (cost > 0) {
				actionPoints -= cost;
			}
		}
		public void UseActionPoints(int value) {
			if (value > 0) {
				actionPoints -= value;
			}
		}
		public void UseEnergy(int energy) {
			if (energy > 0) {
				stats.StomachContent -= energy;
			}
		}
		public void AddEnergy(int energy) {
			if (energy > 0) {
				stats.StomachContent += energy;
			}
		}

		public void Attacked(ActorPart attacker) {
			CancelAction();
			if (GameData.IsPlayer(attacker)) {
				agressiveAgainstPlayer = 1;
			}
		}
		public bool Notify (Message message) {
			if (ai != null) {
				return ai.Notify(this, message);
			}
			return false;
		}
		public void UpdateActionPoints () {
			actionPoints += Speed;
		}

		public bool HasEffect<E> () where E : ContinuousEffect {
			int len = effects.Count;
			ContinuousEffect effect;
			for(int i=0; i<len;i++) {
				effect = effects[i];
				if (effect is E) {
					return true;
				}
			}
			return false;
		}
		public E GetEffect<E> () where E : ContinuousEffect {
			int len = effects.Count;
			ContinuousEffect effect;
			for(int i=0; i<len;i++) {
				effect = effects[i];
				if (effect is E) {
					return effect as E;
				}
			}
			return null;
		}
		public IEnumerable<E> GetEffects<E> () where E : ContinuousEffect {
			int len = effects.Count;
			ContinuousEffect effect;
			for(int i=0; i<len;i++) {
				effect = effects[i];
				if (effect is E) {
					yield return effect as E;
				}
			}
		}
		public void AttachEffect (ContinuousEffect effect) {
			effects.Add( effect );
			effect.OnAttach(this);
		}
		public void DetachEffect (ContinuousEffect effect) {
			effects.Remove( effect );
			effect.OnDetach(this);
		}

		public override void Initialize() { }

		public override void Update () {
			//actionPoints += Speed;
			//if (actionPoints > 24) {
			//	UnityEngine.Debug.Log("AP: "+actionPoints);
			//}
			if (ai != null) {
				ai.Update (this);
			}
			stats.Update ();
			if (stats.CanStarve) {
				stats.StomachContent -= GameRules.StomachContentPerTurn;
				float cont = stats.StomachContent / (float)stats.MaxStomachContent;
				if (cont <= 0) {
					if (stats.SatiationStatus != GameRules.SatiationStatus.Starving) {
						GameData.EventManager.Notify (new ShowWarningEvent (Entity, "You starve!"));
					}
					stats.SatiationStatus = GameRules.SatiationStatus.Starving;
				} else if (cont <= GameRules.WeakStomachContent) {
					if (stats.SatiationStatus != GameRules.SatiationStatus.Weak) {
						GameData.EventManager.Notify (new ShowWarningEvent (Entity, "You are weak!"));
					}
					stats.SatiationStatus = GameRules.SatiationStatus.Weak;
					stats.ConstitutionPoints -= GameRules.LoseConstitutionPointsWeak;
				} else if (cont <= GameRules.HungryStomachContent) {
					if (stats.SatiationStatus != GameRules.SatiationStatus.Hungry) {
						GameData.EventManager.Notify (new ShowWarningEvent (Entity, "You are hungry!"));
					}
					stats.SatiationStatus = GameRules.SatiationStatus.Hungry;
					stats.ConstitutionPoints -= GameRules.LoseConstitutionPointsHungry;
				} else if (cont > 1) {
					stats.SatiationStatus = GameRules.SatiationStatus.Bursting;
				} else if (cont > GameRules.OversatiatedStomachContent) {
					stats.SatiationStatus = GameRules.SatiationStatus.Oversatiated;
					stats.ConstitutionPoints -= GameRules.LoseConstitutionPointsOversatiated;
				} else if (cont > GameRules.SatiatedStomachContent) {
					stats.SatiationStatus = GameRules.SatiationStatus.Satiated;
					stats.ConstitutionPoints -= GameRules.LoseConstitutionPointsSatiated;
				} else {
					stats.SatiationStatus = GameRules.SatiationStatus.Normal;
					stats.ConstitutionPoints += GameRules.GainConstitutionPointsNormal;
				}
				if (stats.SatiationStatus == GameRules.SatiationStatus.Bursting) {
					HealthPointsPart hp = Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId);
					if (hp != null) {
						hp.Hp = 0;
						if (GameData.IsPlayer (this))
							GameData.EventManager.Notify (new MessageEvent (Entity, "You burst from oversatiation!"));
					}
				} else if (stats.SatiationStatus == GameRules.SatiationStatus.Starving) {
					HealthPointsPart hp = Entity.GetPart<HealthPointsPart> (HealthPointsPart.partId);
					if (hp != null) {
						hp.Hp = 0;
						if (GameData.IsPlayer (this))
							GameData.EventManager.Notify (new MessageEvent (Entity, "You starve to death!"));
					}
				}
			}

			if (GameData.IsPlayer (this)) {
				InventoryPart inv = Entity.GetPart<InventoryPart> (InventoryPart.partId);
				if (inv != null) {
					int maxMass = GameRules.CalcMaxMass (this);
					int mass = inv.GetMass ();
					float relMass = mass / (float)maxMass;

					GameRules.BurdeningStatus prevStatus = stats.BurdeningStatus;
					if (relMass > GameRules.OverloadedMass) {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Overloaded;

					} else if (relMass > GameRules.OvertaxedMass) {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Overtaxed;

					} else if (relMass > GameRules.StrainedMass) {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Strained;

					} else if (relMass > GameRules.StressedMass) {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Stressed;

					} else if (relMass > GameRules.BurdenedMass) {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Burdened;

					} else {
						stats.BurdeningStatus = GameRules.BurdeningStatus.Unburdened;
					} 
					if (prevStatus != stats.BurdeningStatus) {
						GameData.EventManager.Notify (new ShowWarningEvent (Entity, "You are " + stats.BurdeningStatus.ToString ().ToLower () + "!"));
					}
				}
			}

			int cnt = effects.Count;
			ContinuousEffect e;
			for (int i = 0; i < cnt; i++) {
				e = effects [i];
				if (e.Duration > 0) {
					e.OnUpdate (this);
					e.Duration -= 1;
				}
				if (e.Duration <= 0) {
					DetachEffect(e);
					i--;
					cnt--;
				}
			}
		}

		public override void Cleanup() {}
	}
}
