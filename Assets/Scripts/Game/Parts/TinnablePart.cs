﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Health points part.
	/// </summary>
	[DataContract]
	[KnownType(typeof(TinnablePart))]
	public class TinnablePart : Part<TinnablePart> {


		public TinnablePart() {
		}


		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
