﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(ProjectilePart))]
	public class ProjectilePart : Part<ProjectilePart> {
		
		int range;
		int damage;
		int reloadTurns;

		int rangeFired;
		int damageFired;
		int reloadTurnsFired;

		LauncherType launcherType;
		int attackType;
		Skills skill;

		[DataMember]
		public int Range {
			get { return range;}
			set { range = value; }
		}

		[DataMember]
		public int Damage {
			get { return damage; }
			set { damage = value; }
		}
		[DataMember]
		public int ReloadTurns {
			get { return reloadTurns; }
			set { reloadTurns = value; }
		}

		[DataMember]
		public int RangeFired {
			get { return rangeFired;}
			set { rangeFired = value; }
		}

		[DataMember]
		public int DamageFired {
			get { return damageFired; }
			set { damageFired = value; }
		}

		[DataMember]
		public int ReloadTurnsFired {
			get { return reloadTurnsFired; }
			set { reloadTurnsFired = value; }
		}

		[DataMember]
		public LauncherType LauncherType {
			get {return launcherType;}
			set {launcherType = value;}
		}
		[DataMember]
		public int AttackType {
			get {return attackType;}
			set {attackType = value;}
		}
		[DataMember]
		public Skills Skill {
			get { return skill; }
			set { skill = value; }
		}

		public ProjectilePart(int range, int damage, int reloadTurns, 
								int rangeFired, int damageFired, int reloadTurnsFired, LauncherType launcherType, AttackTypes attackType, Skills skill) {
			this.range = range;
			this.damage = damage;
			this.reloadTurns = reloadTurns;
			this.rangeFired = rangeFired;
			this.damageFired = damageFired;
			this.reloadTurnsFired = reloadTurnsFired;
			this.launcherType = launcherType;
			this.attackType = (int) attackType;
			this.skill = skill;
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
