﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ComponentSystem;
using Map.LayeredGrid;
using Game.Events;
using Game.Util;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(PlantPart))]
	public class PlantPart : Part<PlantPart> {

		string tuftGenerator;
		int maxHarvest;
		int harvestRemaining;
		int growthProbPerUpdate;

		int maxTurnsRemaining;
		int turnsRemaining;
		int updateStep;
		int stepsRemaining;
		string message;

		HashSet<int> growTerrain;

		public PlantPart ( string tuftGenerator, int maxHarvest, int maxTurnsRemaining, int updateStep, int growthProbPerUpdate, string message, int[] growTerrain ) {
			this.tuftGenerator = tuftGenerator;
			this.maxHarvest = maxHarvest;
			this.harvestRemaining = maxHarvest+1;
			this.growthProbPerUpdate = growthProbPerUpdate;
			this.maxTurnsRemaining = maxTurnsRemaining;
			this.turnsRemaining = maxTurnsRemaining;
			this.updateStep = updateStep;
			this.stepsRemaining = RandUtils.NextInt (updateStep) + 1;
			this.message = message;

			if (growTerrain != null) {
				this.growTerrain = new HashSet<int> (growTerrain);
			} else {
				this.growTerrain = new HashSet<int> ();
			}
			//UnityEngine.Debug.Log(tuftGenerator);
		}

		public Entity Harvest () {
			
			GridLocation loc = Entity.GetPart<LocationPart> (LocationPart.partId).Location;
			Entity e = Game.Factories.EntityFactory.GetCreator(tuftGenerator)(loc, false);
			harvestRemaining -= 1;
			return e;
		}

		[DataMember]
		public string TuftGenerator {
			get { return tuftGenerator; }
			set { tuftGenerator = value; }
		}

		[DataMember]
		public int MaxTurnsRemaining {
			get { return maxTurnsRemaining; }
			set { maxTurnsRemaining = value; }
		}
		[DataMember]
		public int TurnsRemaining {
			get { return turnsRemaining; }
			set { turnsRemaining = value; }
		}
		[DataMember]
		public int MaxHarvest {
			get { return maxHarvest; }
			set { maxHarvest = value; }
		}
		[DataMember]
		public int HarvestRemaining {
			get { return harvestRemaining; }
			set { harvestRemaining = value; }
		}
		[DataMember]
		public int GrowthProbPerUpdate {
			get { return growthProbPerUpdate; }
			set { growthProbPerUpdate = value; }
		}

		[DataMember]
		public int UpdateStep {
			get { return updateStep; }
			set { updateStep = value; }
		}
		[DataMember]
		public int StepsRemaining {
			get { return stepsRemaining; }
			set { stepsRemaining = value; }
		}

		[DataMember]
		public string Message {
			get { return message; }
			set { message = value; }
		}

		[IgnoreDataMember]
		public HashSet<int> GrowTerrain {
			get { return growTerrain; }
			set { growTerrain = value; }
		}

		[DataMember]
		public int[] GrowTerrainForSerialization {
			get { 
				if (growTerrain.Count == 0) {
					return null;
				}
				int[] arr = new int[ growTerrain.Count ];
				growTerrain.CopyTo(arr, 0);
				return arr; 
			}
			set { 
				if (value != null) {
					this.growTerrain = new HashSet<int> (value);
				} else {
					this.growTerrain = new HashSet<int> ();
				}
			}
		}

		public override void Initialize() {}

		public override void Update () {
			stepsRemaining -= 1;
			if (stepsRemaining <= 0) {
				stepsRemaining = updateStep;

				bool growing = true;
				PickablePart pick = Entity.GetPart<PickablePart> (PickablePart.partId);
				if (pick != null) {
					if (pick.CarrierEntity >= 0) {
						growing = false;
					}
				}
				if (growing) {
					int type = GameData.Map.Get(Entity.GetPart<LocationPart> (LocationPart.partId).Location).Type;
					growing = growTerrain.Contains (type);
				}
				if (growing) {
					turnsRemaining += updateStep;
					if (turnsRemaining > maxTurnsRemaining) {
						turnsRemaining = maxTurnsRemaining;
					}
					if (harvestRemaining < maxHarvest+1) {
						harvestRemaining += RandUtils.NextBool (0.01f * growthProbPerUpdate) ? 1 : 0;
						if (harvestRemaining > maxHarvest+1) {
							harvestRemaining = maxHarvest+1;
						}
					}
				} else {
					turnsRemaining -= updateStep;
					if (turnsRemaining <= 0) {
						if (pick != null) {
							if (pick.CarrierEntity == GameData.Player.Id) {
								GameData.EventManager.Notify (new MessageEvent (GameData.Player, "The " + TextUtil.GetName (pick) + " " + message + "."));
							}
						}
						GameData.EventManager.Notify (new RemoveEntityEvent (Entity, true));
					}
				}
			}
		}
		public override void Cleanup() {}
	}
}
