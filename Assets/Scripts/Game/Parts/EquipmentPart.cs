﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Data;
using Game.Parts.Sub;

namespace Game.Parts {


	[DataContract]
	[KnownType(typeof(EquipmentPart))]
	public class EquipmentPart : Part<EquipmentPart> {

		Equip slot;
		bool isEquiped;

		int speedBonus;
		int strengthBonus;
		int dexterityBonus;
		int defenseBonus;
		Armor armorBonus;
		int attackBonus;
		int attackDuration;
		int attackType;

		LauncherType launcherType;
		Skills skill;

		public EquipmentPart(Equip slot, LauncherType launcherType, Skills skill, int speedBonus, int strengthBonus, int dexterityBonus, int attackBonus, int attackDuration, AttackTypes attackType, int defenseBonus, Armor armorBonus) {
			this.slot = slot;
			this.skill = skill;
			this.speedBonus = speedBonus;
			this.strengthBonus = strengthBonus;
			this.dexterityBonus = dexterityBonus;
			this.attackBonus = attackBonus;
			this.attackDuration = attackDuration;
			this.attackType = (int) attackType;
			this.defenseBonus = defenseBonus;
			this.armorBonus = armorBonus;
			this.launcherType = launcherType;
			this.isEquiped = false;
		}

		[DataMember]
		public Equip Slot {
			get { return slot; }
			set { slot = value; }
		}

		[DataMember]
		public int SpeedBonus {
			get { return speedBonus; }
			set { speedBonus = value; }
		}

		[DataMember]
		public int StrengthBonus {
			get { return strengthBonus; }
			set { strengthBonus = value; }
		}

		[DataMember]
		public int DexterityBonus {
			get { return dexterityBonus; }
			set { dexterityBonus = value; }
		}

		[DataMember]
		public int DefenseBonus {
			get { return defenseBonus; }
			set { defenseBonus = value; }
		}

		[DataMember]
		public Armor ArmorBonus {
			get { return armorBonus; }
			set { armorBonus = value; }
		}

		[DataMember]
		public int AttackBonus {
			get { return attackBonus; }
			set { attackBonus = value; }
		}

		[DataMember]
		public int AttackDuration {
			get { return attackDuration; }
			set { attackDuration = value; }
		}

		[DataMember]
		public int AttackType {
			get { return attackType; }
			set { attackType = value; }
		}

		[DataMember]
		public bool IsEquiped {
			get { return isEquiped; }
			set { isEquiped = value; }
		}

		[DataMember]
		public LauncherType LauncherType {
			get {return launcherType;}
			set {launcherType = value;}
		}

		[DataMember]
		public Skills Skill {
			get { return skill; }
			set { skill = value; }
		}
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
