﻿using System.Runtime.Serialization;
using Map.LayeredGrid;
using ComponentSystem;

using UnityEngine;

namespace Game.Parts {
	/// <summary>
	/// Prevents item from being put into a container, prevents jumping and climbing.
	/// </summary>
	[DataContract]
	[KnownType(typeof(BulkyPart))]
	public class BulkyPart : Part<BulkyPart> {

		
		public BulkyPart() {
		}
	
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
