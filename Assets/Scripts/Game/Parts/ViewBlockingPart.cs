﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {
	[DataContract]
	[KnownType(typeof(ViewBlockingPart))]
	public class ViewBlockingPart : Part<ViewBlockingPart> {
		
		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
