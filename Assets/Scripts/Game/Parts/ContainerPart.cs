﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Health points part.
	/// </summary>
	[DataContract]
	[KnownType(typeof(ContainerPart))]
	public class ContainerPart : Part<ContainerPart> {

		bool lockable;
		bool isLocked;

		public ContainerPart ( bool lockable, bool isLocked ) {
			this.lockable = lockable;
			this.isLocked = lockable && isLocked;
		}

		[DataMember]
		public bool Lockable {
			get { return lockable; }
			set { lockable = value; }
		}

		[DataMember]
		public bool IsLocked {
			get { return isLocked; }
			set { isLocked = value; }
		}

		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
