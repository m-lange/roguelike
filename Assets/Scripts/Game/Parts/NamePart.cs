﻿using System.Runtime.Serialization;
using ComponentSystem;

namespace Game.Parts {

	[DataContract]
	[KnownType(typeof(NamePart))]
	public class NamePart : Part<NamePart> {
		
		string name;
		string nameIdentified;

		[DataMember]
		public string Name {
			get { return name;}
			set { name = value; }
		}

		[DataMember]
		public string NameIdentified {
			get { return nameIdentified;}
			set { nameIdentified = value; }
		}

		public NamePart(string name) : this(name, null) {
			
		}
		public NamePart(string name, string nameIdentified) {
			this.name = name;
			this.nameIdentified = nameIdentified;
		}

		public override void Initialize() {}
		public override void Update() {}
		public override void Cleanup() {}
	}
}
