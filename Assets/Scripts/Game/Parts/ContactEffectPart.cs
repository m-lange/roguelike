﻿using System.Runtime.Serialization;
using ComponentSystem;
using Game.ActorEffects;
using Game.Events;
using Game.Rules;

namespace Game.Parts {

	/// <summary>
	/// Has an effect when stepped on.
	/// </summary>
	[DataContract]
	[KnownType(typeof(ContactEffectPart))]
	public class ContactEffectPart : Part<ContactEffectPart> {
		
		bool disappear;
		ActorEffect[] effects;
		OnOffPart onOff;

		public ContactEffectPart ( bool disappear, params ActorEffect[] effects ) {
			this.disappear = disappear;
			if (effects != null && effects.Length > 0) {
				this.effects = effects;
			}
		}
		
		public void OnContact(ActorPart actor) {
			if( IsOn ) {
				foreach (ActorEffect effect in Effects) {
					if (effect.Execute (actor)) {
						if (GameData.IsPlayer (actor.Entity) && effect.Message != null) {
							GameData.EventManager.Notify (new MessageEvent (actor.Entity, effect.Message));	
						}
					}
				}
				if(disappear) {
					IsOn = false;
					GameData.EventManager.Notify (new RemoveEntityEvent (Entity, true));
				}
			}
		}

		[DataMember]
		public ActorEffect[] Effects {
			get { return effects; }
			set { effects = value; }
		}
		
		[IgnoreDataMember]
		public bool IsOn {
			get { 
				if(onOff == null) {
					onOff = Entity.GetPart<OnOffPart>(OnOffPart.partId);
				}
				return onOff == null || onOff.IsOn; 
			}
			set { 
				if(onOff == null) {
					onOff = Entity.GetPart<OnOffPart>(OnOffPart.partId);
				}
				if(onOff != null) onOff.IsOn = value; 
			}
		}

		public override void Initialize() {}
		public override void Update () {}
		public override void Cleanup() {}
	}
}
