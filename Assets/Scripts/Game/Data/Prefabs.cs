﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using Map.LayeredGrid.IO;

namespace Game.Data {


	public static class Prefabs {

		public static readonly string CellHighlight = "CellHighlight";
		public static readonly string RangeHighlight = "RangeHighlight";
		public static readonly string RangedIndicator = "RangedIndicator";

		public static readonly string Icon32px = "UI/Icon32px";
					
	}
}
