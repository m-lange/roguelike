﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace Game.Data {
	//[DataContract(Namespace="GameData.Equip")]
	public enum Equip {
		None = 0, 
		HandPrim, HandSec, Ranged,
		BodyArmor, BodyCoat,
		Head, Face,
		Feet, Cloves
	}
}
