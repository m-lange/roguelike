﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Game.Data {

	[DataContract(Namespace="http://Game.Data.GameData")]
	public class GameOptions {

		
		int volume = 50;

		[DataMember]
		public int Volume {
			get { return volume; }
			set { volume = value; }
		}
	}
}
