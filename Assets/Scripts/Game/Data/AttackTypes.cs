﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace Game.Data {


	//[DataContract(Namespace="GameData.TradeClass")]
	public enum AttackTypes {
		Impact = 0, 
		Blade,
		Pierce
	}
}
