﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Map.LayeredGrid.IO;

namespace Game.Data {


	public static class Terra {


		public const int ACCESS_OPEN = 0;
		public const int ACCESS_CLOSED = 1;
		public const int ACCESS_LOCKED = 2;


		public static readonly string Void = "Void";
		public static readonly string FloorMeadow = "FloorMeadow";
		public static readonly string FloorField = "FloorField";
		public static readonly string FloorForest = "FloorForest";
		public static readonly string FloorPath  = "FloorPath";
		public static readonly string FloorStone = "FloorStone";
		public static readonly string FloorWood = "FloorWood";
		public static readonly string FloorShop = "FloorShop";
		public static readonly string WaterShallow = "WaterShallow";
		public static readonly string WaterDeep = "WaterDeep";
		public static readonly string StairsDown = "StairsDown";
		public static readonly string StairsUp = "StairsUp";
		public static readonly string Wall = "Wall";
		public static readonly string Column = "Column";
		public static readonly string Roof = "Roof";
		public static readonly string DoorClosed = "DoorClosed";
		public static readonly string DoorOpen = "DoorOpen";
		public static readonly string DoorLocked = "DoorLocked";
		public static readonly string DoorHidden = "DoorHidden";
		public static readonly string WindowClosed = "WindowClosed";
		public static readonly string WindowOpen = "WindowOpen";
		public static readonly string WindowLocked = "WindowLocked";
		public static readonly string WindowDestroyed = "WindowDestroyed";


		static readonly TileTypeInfo[] tileInfo;
		static readonly TileApprearanceInfo[] tileAppearance;

		static readonly Dictionary<string, byte> tileInfoIndexMap;
		static readonly Dictionary<string, TileTypeInfo> tileInfoMap;
		static readonly Dictionary<string, TileApprearanceInfo> tileAppearanceMap;

		static readonly CharacterMapping defaultMapping;

		static readonly Texture2D textureAtlas;
		static readonly Mesh[] meshes;

		static Terra () {

			tileInfo = new TileTypeInfo[] {
				new TileTypeInfo (Void, 		"void", 			' ',  isWalkable: false, isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorMeadow, 	"floor", "meadow", 	'.',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorField, 	"floor", "field", 	'"',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorForest, 	"floor", "forest", 	'/',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorPath, 	"floor", "path", 	'+',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorStone, 	"floor", "stone", 	',',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorWood, 	"floor", "wood", 	'-',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (FloorShop, 	"floor", "shop", 	'\'', isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (WaterShallow, "water", "water", 	'=',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (WaterDeep, 	"water", 			'~',  isWalkable: false, isWall: false, isViewBlocking: false),
				new TileTypeInfo (StairsDown, 	"stairs", 			'<',  isWalkable: true,  isWall: false, isViewBlocking: false, isStairsDown: true),
				new TileTypeInfo (StairsUp, 	"stairs", 			'>',  isWalkable: true,  isWall: false, isViewBlocking: false, isStairsUp: true),
				new TileTypeInfo (Wall, 		"wall", 			'#',  isWalkable: false, isWall: true,  isViewBlocking: true),
				new TileTypeInfo (Column, 		"wall", 			'0',  isWalkable: false, isWall: true,  isViewBlocking: true),
				new TileTypeInfo (Roof, 		"floor", "roof", 	'^',  isWalkable: true,  isWall: false, isViewBlocking: false),
				new TileTypeInfo (DoorClosed, 	"door", 			'C',  isWalkable: false, isWall: true,  isViewBlocking: true, isDoorClosed: true),
				new TileTypeInfo (DoorOpen, 	"door", 			'O',  isWalkable: true,  isWall: false, isViewBlocking: false, isDoorOpen: true),
				new TileTypeInfo (DoorLocked, 	"door", 			'L',  isWalkable: false, isWall: true,  isViewBlocking: true, isDoorLocked: true),
				new TileTypeInfo (DoorHidden, 	"wall", 			'H',  isWalkable: false, isWall: true,  isViewBlocking: true),
				new TileTypeInfo (WindowClosed, "window", 			'c',  isWalkable: false, isWall: true,  isViewBlocking: false, isDoorClosed: true),
				new TileTypeInfo (WindowOpen, 	"window", 			'o',  isWalkable: true,  isWall: false, isViewBlocking: false, isDoorOpen: true),
				new TileTypeInfo (WindowLocked, "window", 			'l',  isWalkable: false, isWall: true,  isViewBlocking: false, isDoorLocked: true),
				new TileTypeInfo (WindowDestroyed, "window",		'd',  isWalkable: true,  isWall: false, isViewBlocking: false)
			};
			tileAppearance = new TileApprearanceInfo[] {
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorMeadowTex", mapColor: new Color32 (0, 0, 0, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorMeadowTex", mapColor: new Color32 (210, 240, 205, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorFieldTex", mapColor: new Color32 (200, 200, 150, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorForestTex", mapColor: new Color32 (100, 150, 100, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorPathTex", mapColor: new Color32 (200, 200, 200, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorStoneTex", mapColor: new Color32 (200, 200, 200, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorWoodTex", mapColor: new Color32 (190, 160, 115, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/FloorShopTex", mapColor: new Color32 (230, 200, 140, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/WaterShallowTex", mapColor: new Color32 (160, 160, 255, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/WaterDeepTex", mapColor: new Color32 (100, 100, 240, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/StairsDownTex", mapColor: new Color32 (180, 0, 0, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/StairsUpTex", mapColor: new Color32 (0, 150, 0, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/WallTex", mapColor: new Color32 (80, 80, 60, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Column", texture: "Terrain/ColumnTex", mapColor: new Color32 (80, 80, 60, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/RoofTex", mapColor: new Color32 (150, 0, 0, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/DoorClosedTex", mapColor: new Color32 (150, 150, 30, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/DoorOpenTex", mapColor: new Color32 (220, 220, 100, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/DoorClosedTex", mapColor: new Color32 (120, 120, 30, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/WallTex", mapColor: new Color32 (80, 80, 60, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/WindowClosedTex", mapColor: new Color32 (100, 100, 200, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/WindowOpenTex", mapColor: new Color32 (100, 100, 200, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Wall", texture: "Terrain/WindowClosedTex", mapColor: new Color32 (100, 100, 200, 255)),
				new TileApprearanceInfo (prefab: "Terrain/Tile", texture: "Terrain/WindowDestroyedTex", mapColor: new Color32 (100, 100, 200, 255))
			};

			tileInfoMap = new Dictionary<string, TileTypeInfo> ();
			tileAppearanceMap = new Dictionary<string, TileApprearanceInfo> ();
			tileInfoIndexMap = new Dictionary<string, byte> ();
			Dictionary<char, int> alias = new Dictionary<char, int> ();

			for (int i = 0; i < tileInfo.Length; i++) {
				TileTypeInfo info = tileInfo [i];
				tileInfoIndexMap [info.Id] = (byte)i;
				tileInfoMap [info.Id] = info;
				tileAppearanceMap [info.Id] = tileAppearance [i];

				if (alias.ContainsKey (info.DefaultMapping)) {
					throw new System.ArgumentException("Duplicate mapping of character "+info.DefaultMapping);
				}
				alias [info.DefaultMapping] = i;
			}
			defaultMapping = new CharacterMapping (alias);

			string[] textures = new string[tileAppearance.Length];
			for (int i = 0; i < textures.Length; i++) {
				textures [i] = tileAppearance [i].TextureName;
			}
			
			Rect[] uv;
			textureAtlas = new TexAtlas.TexturePacker ().CreateAtlas (
				"terrain", textures, 
				new Color32[]{ new Color32 (127, 127, 127, 255) }, 
				512, 32, 4, out uv);
			

			meshes = CreateMeshes (uv);

		}

		static Mesh[] CreateMeshes (Rect[] uv) {
			Mesh[] meshes = new Mesh[uv.Length];
			for (int i = 0; i < uv.Length; i++) {
				Rect uvRect = uv[i];

				GameObject go = (GameObject) GameObject.Instantiate( tileAppearance[i/2].Prefab );
				Mesh orig = go.GetComponent<MeshFilter>().mesh;

				Mesh mesh = new Mesh();
				mesh.vertices = orig.vertices;
				mesh.normals = orig.normals;

				Vector2[] uvs = orig.uv;
				for (int j = 0; j < uvs.Length; j++) {
					Vector2 pos = uvs[j];
					pos.x = pos.x * uvRect.width + uvRect.x;
					pos.y = pos.y * uvRect.height + uvRect.y;
					uvs[j] = pos;
				}
				mesh.uv = uvs;
				mesh.triangles = orig.triangles;
				mesh.name = tileInfo[i/2].Id+i;
				meshes[i] = mesh;

				GameObject.Destroy(go);
			}

			GameObject go2 = (GameObject) GameObject.Instantiate( Resources.Load("Terrain/AtlasTile") );
			go2.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = textureAtlas;
			GameObject.Destroy(go2);

			return meshes;
		}

		public static Mesh MeshVisible (int idx) {
			return meshes[idx * 2];
		}
		public static Mesh MeshSeen (int idx) {
			return meshes[idx * 2 + 1];
		}

		public static TileTypeInfo[] TileInfo {
			get{ return tileInfo; }
		}
		public static TileApprearanceInfo[] TileAppearance {
			get{ return tileAppearance; }
		}

		public static CharacterMapping DefaultMapping {
			get { return defaultMapping; }
		}

		public static byte TileInfoId(string name) {
			return tileInfoIndexMap [name];
		}
		public static TileTypeInfo TileInfoByName(string name) {
			return tileInfoMap [name];
		}
		public static TileApprearanceInfo TileAppearanceByName(string name) {
			return tileAppearanceMap [name];
		}

		public static Dictionary<string, TileTypeInfo> TileInfoMap {
			get { return tileInfoMap; }
		}

		public static Dictionary<string, byte> TileInfoIndexMap {
			get { return tileInfoIndexMap; }
		}

	}
}
