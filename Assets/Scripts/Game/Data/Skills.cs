﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace Game.Data {


	//[DataContract(Namespace="GameData.TradeClass")]
	public enum Skills {
		None = 0,
		Axe,
		AxeRanged,
		ClubAndMace,
		Sword,
		Dagger,
		DaggerRanged,
		SpearAndHalberd,
		SpearRanged,
		Sling,
		Bow,
		Crossbow
	}
}
