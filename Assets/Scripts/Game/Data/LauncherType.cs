﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace Game.Data {
	//[DataContract(Namespace="GameData.Launcher")]
	public enum LauncherType {
		None = 0, 
		Bow, Longbow, Crossbow, Sling
	}
}
