﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data {
	public class Factions {

		public static readonly int NEUTRAL = 0;
		public static readonly int FRIENDLY = 1;
		public static readonly int HOSTILE = -1;

		Faction[] factions;
		Dictionary<string, Faction> factionByName;
		int[,] relations;

		public Factions(params string[] names) {
			this.factionByName = new Dictionary<string, Faction> ();
			this.factions = new Faction[names.Length];
			this.relations = new int[names.Length, names.Length];
			for (int i = 0; i < names.Length; i++) {
				Faction f = new Faction (i, names [i]);
				this.factions[i] = f;
				this.factionByName[names[i]] = f;
				this.relations[i,i] = FRIENDLY;
			}
		}

		public void SetFriendlyTo(int from, int to) {
			relations [from, to] = FRIENDLY;
		}
		public void SetHostileTo(int from, int to) {
			relations [from, to] = HOSTILE;
		}
		public void SetFriendlyTo(string from, string to) {
			relations [factionByName[from].id, factionByName[to].id] = FRIENDLY;
		}
		public void SetHostileTo(string from, string to) {
			relations [factionByName[from].id, factionByName[to].id] = HOSTILE;
		}


		public void SetFriendlyWith(int from, int to) {
			relations [from, to] = FRIENDLY;
			relations [to, from] = FRIENDLY;
		}
		public void SetHostileWith(int from, int to) {
			relations [from, to] = HOSTILE;
			relations [to, from] = HOSTILE;
		}
		public void SetNeutralWith(int from, int to) {
			relations [from, to] = NEUTRAL;
			relations [to, from] = NEUTRAL;
		}
		public void SetFriendlyWith(string from, string to) {
			int id1 = factionByName [from].id;
			int id2 = factionByName [to].id;
			SetFriendlyWith(id1, id2);
		}
		public void SetHostileWith(string from, string to) {
			int id1 = factionByName [from].id;
			int id2 = factionByName [to].id;
			SetHostileWith(id1, id2);
		}
		public void SetNeutralWith(string from, string to) {
			int id1 = factionByName [from].id;
			int id2 = factionByName [to].id;
			SetNeutralWith(id1, id2);
		}

		public void SetNeutralWithAll (string from) {
			int id1 = factionByName [from].id;
			for (int i = 0; i < factions.Length; i++) {
				SetNeutralWith(id1, i);
			}
		}

		public int GetRelation(int from, int to) {
			return relations [from, to];
		}
		public bool IsHostile(int from, int to) {
			return relations [from, to] < 0;
		}
		public bool IsFriendly(int from, int to) {
			return relations [from, to] > 0;
		}
		public Faction Get(int id) {
			return factions [id];
		}
		public Faction Get(string name) {
			return factionByName [name];
		}
	}


	public class Faction {

		public readonly int id;
		public readonly string name;

		public Faction(int id, string name) {
			this.id = id;
			this.name = name;
		}
	}
}
