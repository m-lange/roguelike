﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Collections;
using Game.Util;
using UnityEngine;

namespace Game.Data {
	public static class HelpText {

		
		public static readonly string CommandHelp; /* = new string[] {
			"Keyboard controls",
			"   upper case = Shift + key",
			"     ",
			"?            Show help",
			"Ctrl+h       Show help texts",
			"Ctrl+q       Quit and save game",
			"S            Save game",
			"     ",
			"Numpad 1-9   Walk around",
			"Numpad +/-   Walk stairs up/down",
			"PgUp/PgDown  View higher/lower level",
			"     ",
			"Esc          Cancel",
			".            Wait one turn",
			"o            Open",
			"c            Close",
			"C            Consume/eat",
			"i            Show inventory",
			"I            Arrange inventory",
			"Ctrl+i       Inspect item",
			"Ctrl+c       Show character stats",
			"j            Jump",
			"p            Pick all item(s)",
			"P            Pick some item(s)",
			"d            Drop item(s)",
			"h            Harvest herbs",
			"l            Loot a container",
			"a            Apply (tools)",
			"e            Equip/wear",
			"E            Unequip/take off",
			"t            Throw something",
			"f            Fire from quiver",
			"q            Quiver something",
			"T            Trade something",
			"r            Read a book",
			"s            Search (hidden doors)",
			"w            Walk by shortest path",
			",            List what is here",
			";            List what is there",
			"Left mouse         - \" -",
			"     ",
			"z/Z          Zoom minimap",
			"Ctrl+Numpad  Scroll minimap",
			"Arrows       Scroll inventory window",
			"     ",
			"Special commands (#)",
			"#reveal      reveal entire map",
			"#immortal    become immortal",
			"#gold        get 1000 gold",
			"#debug       show debug information\n"+
			"             e.g. damage dealt"
		};*/
		
		public static readonly OrderedDict<string, string> HelpTopics = new OrderedDict<string, string>();
		
		static HelpText() {
			CommandHelp = Resources.Load<TextAsset>("HelpControls").text;
			
			HelpTopics["Interface"] = Resources.Load<TextAsset>("HelpInterface").text;
			HelpTopics["Character Control"] = Resources.Load<TextAsset>("HelpCharacterControl").text;
			HelpTopics["Inventory & Equipment"] = Resources.Load<TextAsset>("HelpEquipment").text;
			HelpTopics["Trade"] = Resources.Load<TextAsset>("HelpTrade").text;
			HelpTopics["Books"] = Resources.Load<TextAsset>("HelpBooks").text;
			HelpTopics["Plants"] = Resources.Load<TextAsset>("HelpPlants").text;
			HelpTopics["Commands"] = CommandHelp;
		}
	}
}
