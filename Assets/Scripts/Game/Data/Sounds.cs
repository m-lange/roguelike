﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data {

	[RequireComponent (typeof (AudioSource))]
	public class Sounds : MonoBehaviour {


		public static string Hit1 = "hit1";
		public static string Hit2 = "hit2";
		public static string Miss = "swing";
		public static string RangedHit = "dart";
		public static string RangedMiss = "dart_miss";
		public static string Crack = "crack";
		public static string Trap = "trap";
		public static string Boom = "kaboom";

		public static string DoorLocked = "door1";
		public static string DoorClose = "door3";
		public static string DoorOpen = "door2";
		public static string DoorDestroy = "fills";
		public static string WindowDestroy = "shatter";
		public static string Lock = "lock";

		public static string Harvest = "harvest";

		public static string Eat = "munch";
		public static string Vomit = "vomit";
		public static string Register = "register";
		public static string Ouch = "ouch";
		public static string Evil = "evil";

		public static string Flute = "flute_trill";
		public static string Tinning = "tinning";
		public static string Untinning = "untinning";

		static Sounds instance;
		AudioSource source;

		Dictionary<string, AudioClip> clips;
		Queue<AudioClip> clipQueue;
		
		public static Sounds Instance {
			get { return instance; }
		}

		void Awake () {
			instance = this;
			source = GetComponent<AudioSource>();
			source.loop = false;

			clips = new Dictionary<string, AudioClip>();
			clipQueue = new Queue<AudioClip>();
		}
		void Start () {
			source.volume = 0.01f * GameData.Options.Volume;
			StartCoroutine( PlayQueue () );
		}

		public static void Play (string file) {
			instance.PlayAudio(file);
		}
		public static void PlayNow (string file) {
			instance.PlayAudioNow(file);
		}


		public void PlayAudio (string file) {
			if (!clips.ContainsKey (file)) {
				clips [file] = Resources.Load<AudioClip> (file);
				if (clips [file] == null) {
					throw new System.ArgumentException("Audio clip missing: "+file);
				}
			}
			clipQueue.Enqueue(clips[file]);
		}
		public void PlayAudioNow (string file) {
			if (!clips.ContainsKey (file)) {
				clips [file] = Resources.Load<AudioClip> (file);
				if (clips [file] == null) {
					throw new System.ArgumentException("Audio clip missing: "+file);
				}
			}
			source.clip = clips [file];
			source.Play ();
		}


		IEnumerator PlayQueue () {
			while ( true ) {
				if (clipQueue.Count == 0) {
					yield return null;
				} else {
					source.clip = clipQueue.Dequeue ();
					source.Play ();
					yield return new WaitForSeconds (source.clip.length);
				}
			}
        }
	}
}
