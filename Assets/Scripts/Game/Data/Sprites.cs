﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using Map.LayeredGrid.IO;

namespace Game.Data {


	public static class Sprites {

		static readonly Texture2D textureAtlas;
		static readonly Dictionary<string, Rect> uv;
		static readonly Dictionary<string, Dictionary<string, Mesh>> meshes;


		static Sprites () {
			string texPath = GlobalParameters.Instance.TexturePath + "/Entities";

			string[] textures = GetFiles (texPath).ToArray ();

			Rect[] uv;
			textureAtlas = new TexAtlas.TexturePacker ().CreateAtlas (
				"entities", textures, 
				new Color32[]{ }, 
				512, 32, 4, out uv);

			Sprites.uv = new Dictionary<string, Rect> ();
			meshes = new Dictionary<string, Dictionary<string, Mesh>> ();
			for (int i = 0; i < textures.Length; i++) {
				Sprites.uv[textures[i]] = uv[i];
			}

			GameObject go2 = (GameObject) GameObject.Instantiate( Resources.Load("Entities/AtlasTile") );
			go2.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = textureAtlas;
			GameObject.Destroy(go2);
		}

		static List<string> GetFiles (string texPath) {
			string[] dirs = Directory.GetDirectories (texPath);
			List<string> result = new List<string> ();
			string baseDir = new DirectoryInfo(texPath).Name;
			foreach (string dir in dirs) {
				string[] files = Directory.GetFiles (dir, "*.png");
				foreach (string file in files) {
					result.Add ( baseDir+"/"+new DirectoryInfo(dir).Name+"/"+(new FileInfo(file).Name.Replace(".png","")) );
					//Debug.Log(baseDir+"/"+new DirectoryInfo(dir).Name+"/"+new FileInfo(file).Name);
				}
			}
			return result;
		}

		static void CreateMeshes (string prefab, string tex) {
			if (!Sprites.meshes.ContainsKey (prefab)) {
				Sprites.meshes [prefab] = new Dictionary<string, Mesh> ();
			}
			if (!Sprites.meshes [prefab].ContainsKey (tex)) {
				//Debug.Log(prefab+" "+tex);
				GameObject go = (GameObject)GameObject.Instantiate (Resources.Load (prefab));
				Mesh orig = go.GetComponent<MeshFilter> ().mesh;
				if (!Sprites.uv.ContainsKey (tex)) {
					throw new ArgumentException("Texture "+tex+" not found.");
				}
				Rect uvRect = Sprites.uv[tex];
				Mesh mesh = new Mesh();
				mesh.vertices = orig.vertices;
				mesh.normals = orig.normals;

				Vector2[] uvs = orig.uv;
				for (int j = 0; j < uvs.Length; j++) {
					Vector2 pos = uvs[j];
					pos.x = pos.x * uvRect.width + uvRect.x;
					pos.y = pos.y * uvRect.height + uvRect.y;
					uvs[j] = pos;
				}
				mesh.uv = uvs;
				mesh.triangles = orig.triangles;
				mesh.name = prefab+"-"+tex;

				GameObject.Destroy(go);

				Sprites.meshes[prefab][tex] = mesh;
			}
		}


		public static Mesh GetMesh (string prefab, string tex) {
			string key = "Entities/"+tex;
			if ( (!meshes.ContainsKey (prefab)) || (!meshes[prefab].ContainsKey(key))) {
				CreateMeshes(prefab, key);
			}
			return meshes[prefab][key];
		}
	}
}
