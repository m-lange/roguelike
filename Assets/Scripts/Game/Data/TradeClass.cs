﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace Game.Data {


	//[DataContract(Namespace="GameData.TradeClass")]
	public enum TradeClass {
		None = 0, 
		All,
		Money,
		HumanCarcass, AnimalCarcass, 
		Consumables, Herbs,
		Weapon, Armor, Tools, Lights,
		Containers,
		Books,
		Stuff
	}
}
