﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Map.LayeredGrid;
using MapGen;

namespace Game.AI.Managers {
	public class PatrolPointManager {

		Dictionary<string, List<GridLocation>> patrolPoints;
		LayeredGrid<TileInfo> map;

		public PatrolPointManager ( LayeredGrid<TileInfo> map ) {
			this.map = map;
			patrolPoints = new Dictionary<string, List<GridLocation>>();
		}

		public void GeneratePoints (string name, Func<TileInfo, bool> filter, int count) {
			patrolPoints[name] = RandUtils.Draw( map.GetAll (0, (info) => info.Type == TownGenerator.FLOOR_PATH), count );
		}

		public List<GridLocation> Get (string name) {
			return patrolPoints[name];
		}

		public GridLocation GetNear (string name, GridLocation source, float maxDist, int maxTrials) {
			float maxDistSq = maxDist * maxDist;
			GridLocation target;
			int cnt = 0;
			while ( cnt < maxTrials ) {
				target = RandUtils.Select (patrolPoints [name]);
				if (source.DistanceSq (target) <= maxDistSq) {
					return target;
				}
				cnt++;
			}
			return null;
		}
	}
}
