﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TurnSystem;
using Game.Parts;
using Game.Messages;
using Game.ActorEffects;

namespace Game.AI {

	[DataContract]
	public abstract class ActorAI {


		Dictionary<System.Type, ActorEffect> messages;


		private ActorAI () {

		}

		protected ActorAI(Dictionary<System.Type, ActorEffect> messages) {
			this.messages = (messages == null) ? new Dictionary<System.Type, ActorEffect>() : new Dictionary<System.Type, ActorEffect>( messages );
		}

		public abstract Action GetNextAction(ActorPart actor);
		public abstract bool Notify (ActorPart actor, Message message);
		public abstract void Update(ActorPart actor);


		[IgnoreDataMember]
		public Dictionary<System.Type, ActorEffect> Messages {
			get { return messages; }
		}

		[DataMember]
		public object[][] MessagesForSerialization {
			get { 
				if (messages.Count == 0) {
					return null;
				}
				object[][] arr = new object[ messages.Count ][];
				int cnt = 0;
				foreach (KeyValuePair<System.Type, ActorEffect> tp in messages) {
					arr [cnt] = new object[]{ tp.Key.FullName, tp.Value };
					cnt++;
				}
				return arr; 
			}
			set { 
				this.messages = new Dictionary<System.Type, ActorEffect> (); 
				if (!(value == null)) {
					for (int i = 0; i < value.Length; i++) {
						this.messages[ System.Type.GetType ( (string) value[i][0] ) ] = (ActorEffect) value[i][1];
					}
				}
			}
		}
		
	}
}
