﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Map;
using ComponentSystem;
using TurnSystem;
using Map.LayeredGrid;
using Game.Actions.Actor;
using Game.AI;
using Game.Parts;
using Game.Messages;
using Game.ActorEffects;
using Game.Data;
using Game.Util;

namespace Game.AI {


	[DataContract]
	[KnownType(typeof(PatrolAI))]
	public class PatrolAI : ActorAI {

		bool agressiveWhenAttacked;
		int visionRange;
		int accessLevel;
		bool allowStairs;
		int alertVisionRange;
		int alertTime;

		LocationPart location;

		public PatrolAI(bool agressiveWhenAttacked, int visionRange, int alertVisionRange, int alertTime, int accessLevel, bool allowStairs, Dictionary<Type, ActorEffect> messages)
				: base(messages) {
			this.agressiveWhenAttacked = agressiveWhenAttacked;
			this.accessLevel = accessLevel;
			this.allowStairs = allowStairs;
			this.visionRange = visionRange;
			this.alertVisionRange = alertVisionRange;
			this.alertTime = alertTime;
		}

		[DataMember]
		public bool AllowStairs {
			get { return allowStairs; }
			set { allowStairs = value; }
		}
		[DataMember]
		public int AccessLevel {
			get { return accessLevel; }
			set { accessLevel = value; }
		}
		[DataMember]
		public bool AgressiveWhenAttacked {
			get { return agressiveWhenAttacked; }
			set { agressiveWhenAttacked = value; }
		}
		[DataMember]
		public int VisionRange {
			get { return visionRange; }
			set { visionRange = value; }
		}
		[DataMember]
		public int AlertVisionRange {
			get { return alertVisionRange; }
			set { alertVisionRange = value; }
		}
		[DataMember]
		public int AlertTime {
			get { return alertTime; }
			set { alertTime = value; }
		}

		public override TurnSystem.Action GetNextAction (ActorPart actor) {
			if( actor.AgressiveAgainstPlayer > 0 ) {
				if (agressiveWhenAttacked) {
					UnityEngine.Debug.Log ("Switching to chase");
					actor.ActorAi = new ChasePlayerAI (true, true, visionRange, alertVisionRange, alertTime, accessLevel, allowStairs, null);
					return actor.ActorAi.GetNextAction (actor);
				}
			} else if( actor.AgressiveAgainstPlayer < 0 ) {
				UnityEngine.Debug.Log ("Switching to flee");
				actor.ActorAi = new FleeFromPlayerAI (true, true, visionRange, alertVisionRange, alertTime, accessLevel, allowStairs, null);
				return actor.ActorAi.GetNextAction (actor);
			}
			
			if (location == null) {
				location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
			}
			GridLocation target = GameData.PartolPointManager.GetNear ("path", location.Location, 30, 10);
			if ( target != null && ! location.Location.Equals(target)) {
				return new FollowPathAction (actor, target, Terra.ACCESS_OPEN, false, false, true, 30);
			} else {
				return WaitAction.Instance;
			}
		}

		public override bool Notify (ActorPart actor, Message message) {
			if (Messages.ContainsKey (message.GetType ())) {
				if (location == null) {
					location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				}
				if (message.Location == null || GameData.Visibility.CanSee (location.Location, message.Location, visionRange, ! message.RequireLoS)
					|| GameData.Visibility.CanSee (location.Location, message.Entity.GetPart<LocationPart> (LocationPart.partId).Location, visionRange, ! message.RequireLoS)) {

					if (message.IsRelevant (actor)) {
						Messages [message.GetType ()].Execute (actor);
						return true;
					} 
				}
			}
			return false;
		}


		public override void Update(ActorPart actor) {

		}
	}
}
