﻿using System.Runtime.Serialization;
using Map.LayeredGrid;

namespace Game.AI.Constraints {

	[DataContract]
	public class BoxConstraint : SpatialConstraint {

		int xmin;
		int xmax;
		int ymin;
		int ymax;
		int lmin;
		int lmax;

		public BoxConstraint() : this(0, 0, 0, 0, 0, 0) {

		}

		public BoxConstraint(int xmin, int ymin, int xmax, int ymax) : this (0, 0, xmin, ymin, xmax, ymax) {
			
		}
		public BoxConstraint(int lmin, int lmax, int xmin, int ymin, int xmax, int ymax) {
			this.lmin = lmin;
			this.lmax = lmax;
			this.xmin = xmin;
			this.ymin = ymin;
			this.xmax = xmax;
			this.ymax = ymax;
		}


		public int GetWidth() {
			return xmax - xmin;
		}
		public int GetHeight() {
			return ymax - ymin;
		}

		public bool Contains(GridLocation pos) {
			return Contains(pos.Layer, pos.X, pos.Y);
		}
		public bool Contains(int l, int x, int y) {
			return l >= lmin && l <= lmax && x >= xmin && x <= xmax && y >= ymin && y <= ymax;
		}

		public GridLocation GetRandomLocation() {
			return new GridLocation( 
				RandUtils.NextInt(lmin, lmax+1),
				RandUtils.NextInt(xmin, xmax+1),
				RandUtils.NextInt(ymin, ymax+1) );
		}

		[DataMember]
		public int Xmin {
			get { return xmin; }
			set { xmin = value; }
		}
		[DataMember]
		public int Xmax {
			get { return xmax; }
			set { xmax = value; }
		}
		[DataMember]
		public int Ymin {
			get { return ymin; }
			set { ymin = value; }
		}
		[DataMember]
		public int Ymax {
			get { return ymax; }
			set { ymax = value; }
		}
		[DataMember]
		public int Lmin {
			get { return lmin; }
			set { lmin = value; }
		}
		[DataMember]
		public int Lmax {
			get { return lmax; }
			set { lmax = value; }
		}

	}
}
