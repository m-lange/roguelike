﻿using System.Runtime.Serialization;
using Map.LayeredGrid;

namespace Game.AI.Constraints {

	public interface SpatialConstraint {
		
		bool Contains(GridLocation loc);
		bool Contains(int l, int x, int y);

		GridLocation GetRandomLocation();
	}
}
