﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Map;
using ComponentSystem;
using TurnSystem;
using Game.Actions.Actor;
using Game.AI;
using Game.Parts;
using Game.Messages;
using Game.ActorEffects;
using Game.Data;
using Game.Util;

namespace Game.AI {


	[DataContract]
	[KnownType(typeof(RandomWalkAI))]
	public class RandomWalkAI : ActorAI {

		bool doRandomWalk;
		bool agressiveWhenAttacked;
		int visionRange;
		int accessLevel;
		bool allowStairs;
		int alertVisionRange;
		int alertTime;

		int lastDirection;

		//HashSet<Type> aggravateMessages;
		LocationPart location;

		public RandomWalkAI(bool doRandomWalk, bool agressiveWhenAttacked, int visionRange, int alertVisionRange, int alertTime, int accessLevel, bool allowStairs, Dictionary<Type, ActorEffect> messages)
				: base(messages) {
			this.doRandomWalk = doRandomWalk;
			this.agressiveWhenAttacked = agressiveWhenAttacked;
			this.accessLevel = accessLevel;
			this.allowStairs = allowStairs;
			this.visionRange = visionRange;
			this.alertVisionRange = alertVisionRange;
			this.alertTime = alertTime;

			lastDirection = RandUtils.SelectIndex( Direction.HORIZONTAL );
		}


		[DataMember]
		public bool DoRandomWalk {
			get { return doRandomWalk; }
			set { doRandomWalk = value; }
		}

		[DataMember]
		public bool AllowStairs {
			get { return allowStairs; }
			set { allowStairs = value; }
		}
		[DataMember]
		public int AccessLevel {
			get { return accessLevel; }
			set { accessLevel = value; }
		}
		[DataMember]
		public bool AgressiveWhenAttacked {
			get { return agressiveWhenAttacked; }
			set { agressiveWhenAttacked = value; }
		}
		[DataMember]
		public int VisionRange {
			get { return visionRange; }
			set { visionRange = value; }
		}
		[DataMember]
		public int AlertVisionRange {
			get { return alertVisionRange; }
			set { alertVisionRange = value; }
		}
		[DataMember]
		public int AlertTime {
			get { return alertTime; }
			set { alertTime = value; }
		}
		[DataMember]
		public int LastDirection {
			get { return lastDirection; }
			set { lastDirection = value; }
		}

		public override TurnSystem.Action GetNextAction (ActorPart actor) {
			if( actor.AgressiveAgainstPlayer > 0 ) {
				if (agressiveWhenAttacked) {
					UnityEngine.Debug.Log ("Switching to chase");
					actor.ActorAi = new ChasePlayerAI (doRandomWalk, true, visionRange, alertVisionRange, alertTime, accessLevel, allowStairs, null);
					return actor.ActorAi.GetNextAction (actor);
				}
			} else if( actor.AgressiveAgainstPlayer < 0 ) {
				UnityEngine.Debug.Log ("Switching to flee");
				actor.ActorAi = new FleeFromPlayerAI (doRandomWalk, true, visionRange, alertVisionRange, alertTime, accessLevel, allowStairs, null);
				return actor.ActorAi.GetNextAction (actor);
			}
			if (doRandomWalk) {
				int len = Direction.HORIZONTAL.Length;
				int nextDirection = (lastDirection + len + RandUtils.NextInt(-1, 2)) % len;
				lastDirection = nextDirection;
				return new WalkAction (actor, Direction.HORIZONTAL[nextDirection], false, false, true);
			} else {
				return WaitAction.Instance;
			}
		}

		public override bool Notify (ActorPart actor, Message message) {
			if (Messages.ContainsKey (message.GetType ())) {
				if (location == null) {
					location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				}
				if (message.Location == null || GameData.Visibility.CanSee (location.Location, message.Location, visionRange, ! message.RequireLoS)
					|| GameData.Visibility.CanSee (location.Location, message.Entity.GetPart<LocationPart> (LocationPart.partId).Location, visionRange, ! message.RequireLoS)) {

					if (message.IsRelevant (actor)) {
						Messages [message.GetType ()].Execute (actor);
						return true;
					} 
				}
			}
			return false;
		}


		public override void Update(ActorPart actor) {

		}
	}
}
