﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Map;
using Map.LayeredGrid;
using ComponentSystem;
using TurnSystem;
using Game.Actions.Actor;
using Game.AI;
using Game.Parts;
using Game.Data;
using Game.Util;
using Game.Messages;
using Game.Rules;
//using UnityEngine;

namespace Game.AI {
	
	[DataContract]
	[KnownType(typeof(FleeFromPlayerAI))]
	public class FleeFromPlayerAI : ActorAI {

		bool doRandomWalk;
		bool isMobile;
		int visionRange;
		int alertVisionRange;
		int alertTime;
		int alertTimeRemaining;
		int accessLevel;
		bool allowStairs;

		int lastDirection;

		LocationPart location;

		public FleeFromPlayerAI(bool doRandomWalk, bool isMobile, int visionRange, int alertVisionRange, int alertTime, int accessLevel, bool allowStairs, Dictionary<System.Type, Game.ActorEffects.ActorEffect> messages)
			: base(messages) {
			this.doRandomWalk = doRandomWalk;
			this.isMobile = isMobile;
			this.visionRange = visionRange;
			this.accessLevel = accessLevel;
			this.allowStairs = allowStairs;
			this.alertVisionRange = alertVisionRange;
			this.alertTime = alertTime;
			this.alertTimeRemaining = 0;

			lastDirection = RandUtils.SelectIndex( Direction.HORIZONTAL );
		}

		[DataMember]
		public bool DoRandomWalk {
			get { return doRandomWalk; }
			set { doRandomWalk = value; }
		}

		[DataMember]
		public bool IsMobile {
			get { return isMobile; }
			set { isMobile = value; }
		}

		[DataMember]
		public bool AllowStairs {
			get { return allowStairs; }
			set { allowStairs = value; }
		}
		[DataMember]
		public int AccessLevel {
			get { return accessLevel; }
			set { accessLevel = value; }
		}
		[DataMember]
		public int VisionRange {
			get { return visionRange; }
			set { visionRange = value; }
		}
		[DataMember]
		public int AlertVisionRange {
			get { return alertVisionRange; }
			set { alertVisionRange = value; }
		}
		[DataMember]
		public int AlertTime {
			get { return alertTime; }
			set { alertTime = value; }
		}
		[DataMember]
		public int AlertTimeRemaining {
			get { return alertTimeRemaining; }
			set { alertTimeRemaining = value; }
		}
		[DataMember]
		public int LastDirection {
			get { return lastDirection; }
			set { lastDirection = value; }
		}

		public override Action GetNextAction (ActorPart actor) {
			if (location == null) {
				location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
			}
			if( actor.AgressiveAgainstPlayer > 0 ) {
				UnityEngine.Debug.Log ("Switching to chase");
				actor.ActorAi = new ChasePlayerAI (doRandomWalk, true, visionRange, alertVisionRange, alertTime, accessLevel, allowStairs, null);
				return actor.ActorAi.GetNextAction (actor);
			}

			if (allowStairs || location.Location.Layer == GameData.PlayerLocation.Location.Layer) {
				if (alertTimeRemaining <= 0) {
					if (GameData.Visibility.CanSee (location.Location, GameData.PlayerLocation.Location, visionRange, false)) {
						alertTimeRemaining = alertTime;
					}
				} 
				if (alertTimeRemaining > 0) {
					if (GameData.Visibility.CanSee (location.Location, GameData.PlayerLocation.Location, alertVisionRange, true)) {
						alertTimeRemaining = alertTime;
						if (isMobile) {
							List<GridLocation> path = GameData.PathManager.FindPath (location.Location, GameData.PlayerLocation.Location, accessLevel, (int)(alertVisionRange * 1.5), allowStairs, true);
							if (path != null && path.Count > 1) {
								GridLocation closest = path [1];
								List<GridLocation> target = new List<GridLocation>();
								GridLocation tempNeighbor = new GridLocation (0, 0, 0);
								float maxDist = float.MinValue;
								foreach (object dummy in GameData.PathManager.GetNeighbours(location.Location, allowStairs, tempNeighbor)) {
									float cost = GameData.PathManager.GetCost(location.Location, tempNeighbor, accessLevel, false, true);
									if( cost > 0 ) {
										float dist = tempNeighbor.Distance3d( closest ) / cost;
										if( System.Math.Abs(dist - maxDist) < 0.02 ) {
											target.Add( new ImmutableGridLocation(tempNeighbor) );
										} else if(dist > maxDist) {
											target.Clear();
											target.Add( new ImmutableGridLocation(tempNeighbor) );
											maxDist = dist;
										}
									}
								}
								if(target.Count > 0) {
									//Debug.Log(target.Count);
									Direction dir = Direction.GetFromTo (path [0], RandUtils.Select(target) );
									if (dir != null) {
										return new WalkAction (actor, dir, accessLevel > Terra.ACCESS_OPEN, false, true);
									}
								}
							} else {
								alertTimeRemaining = 0;
							}
						}
					}
				}
			}
			if (doRandomWalk) {
				int len = Direction.HORIZONTAL.Length;
				int nextDirection = (lastDirection + len + RandUtils.NextInt(-1, 2)) % len;
				lastDirection = nextDirection;
				return new WalkAction (actor, Direction.HORIZONTAL[nextDirection], false, false, true);
			} else {
				return WaitAction.Instance;
			}
		}

		public override bool Notify (ActorPart actor, Message message) {
			if (Messages.ContainsKey (message.GetType ())) {
				if (location == null) {
					location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				}
				if (message.Location == null || GameData.Visibility.CanSee (location.Location, message.Location, visionRange, ! message.RequireLoS)
					|| GameData.Visibility.CanSee (location.Location, message.Entity.GetPart<LocationPart> (LocationPart.partId).Location, visionRange, ! message.RequireLoS)) {

					if (message.IsRelevant (actor)) {
						Messages [message.GetType ()].Execute (actor);
						return true;
					} 
				}
			}
			return false;
		}



		public override void Update (ActorPart actor) {
			if (alertTimeRemaining <= 0) {
				if (location == null) {
					location = actor.Entity.GetPart<LocationPart> (LocationPart.partId);
				}
				if (location.Location.Layer == GameData.PlayerLocation.Location.Layer) {
					if (GameData.Visibility.CanSee (location.Location, GameData.PlayerLocation.Location, visionRange, false)) {
						alertTimeRemaining = alertTime;
					}
				}
			}
			if (alertTimeRemaining > 0) {
				alertTimeRemaining -= 1;
			}
		}

	}
}
