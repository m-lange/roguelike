﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

using ComponentSystem;
using TurnSystem;
using Game.Actions;
using Game.Parts;
using Game.Effects;

namespace Game.Destructables {

	[DataContract]
	[KnownType(typeof(SpawnEntitiesDestructable))]
	public class SpawnEntitiesDestructable : Destructable {
		

		string entityType;
		int entityCount;


		public SpawnEntitiesDestructable(string entityType, int entityCount) {
			this.entityType = entityType;
			this.entityCount = entityCount;
		}


		public DestructionEffectRule[] Rules { 
			get{ 
				return new DestructionEffectRule[] {
					new DestructionEffectRule("Spawn", (destroyer, destroyed) => {
						if( destroyed != null && destroyed.HasPart(LocationPart.partId) ) {
							LocationPart loc = destroyed.GetPart<LocationPart>(LocationPart.partId);
							long owner = destroyed.HasPart(PickablePart.partId) 
								? destroyed.GetPart<PickablePart>(PickablePart.partId).CarrierEntity
								: -1;
							return new SpawnEntitiesEffect( entityType, entityCount, loc.Location, owner );
						}
						return null;
					})
				};
			} 
		}

		[DataMember]
		public string EntityType {
			get { return entityType; }
			set { entityType = value; }
		}

		[DataMember]
		public int EntityCount {
			get { return entityCount; }
			set { entityCount = value; }
		}

	}
}
