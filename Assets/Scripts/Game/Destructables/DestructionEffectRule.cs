﻿using System;
using ComponentSystem;
using Game.Effects;
using Game.Parts;
using Map.LayeredGrid;

namespace Game.Destructables {
	public class DestructionEffectRule {

		string name;
		public string Name { get{ return name; } }


		public delegate Effect Rule(Entity destroyer, Entity destroyed);

		Rule rule;
	
		public DestructionEffectRule(string name, Rule rule) {
			this.name = name;
			this.rule = rule;
		}

		public Effect Apply(Entity destroyer, Entity destroyed) {
			return rule( destroyer, destroyed );
		}
		
	}
}
