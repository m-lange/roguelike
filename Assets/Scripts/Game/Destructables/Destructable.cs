﻿using System.Collections;
using System.Collections.Generic;

using Collections;
using ComponentSystem;
using TurnSystem;
using Game.Actions;
using Game.Parts;

namespace Game.Destructables {
	public interface Destructable {

		DestructionEffectRule[] Rules { get; }

	}
}
