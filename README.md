[TOC]

# Unity Roguelike #

## What is this? ##

This is a roguelike(ish) game, set in a medieval/trash world.

![Screenshot_v0.1.03_001.png](https://bitbucket.org/repo/akRXaAe/images/3221249189-Screenshot_v0.1.03_001.png)

## Getting started ##

1. Download the latest version from the [downloads section](https://bitbucket.org/m-lange/roguelike/downloads)
2. Extract the downloaded archive to some location with write access, e.g. to Desktop
3. Start the executable
4. Follow the instructions...

See the **[wiki](https://bitbucket.org/m-lange/roguelike/wiki/Home)** for further information.

## Features (not-so-roguelike) ##

* No magic! But there are plants with effects.
* Tile graphics. No ASCII!
* Large map, starting in a medieval town.
* No levels. The whole world is connected across layers.
